/**
 * get method
 */
const http = require('http');
const url = require('url');
const querystring = require('querystring');

const server = http.createServer();

server.on('request', function (req, res) {
    let pathname = url.parse(req.url).pathname;
    let paramStr = url.parse(req.url).query;
    let param = querystring.parse(paramStr);
    //过滤浏览器的/favicon.ico请求
    if (pathname === '/favicon.ico') {
        return;
    }
    //打印请求路径
    console.log(pathname);
    //判断是否有请求参数
    console.log(paramStr ? paramStr : 'no params');
    //打印请求参数
    console.log(param);
    //解决中文乱码问题
    res.writeHead(200, {'Content-Type': 'text/plain;charset=utf-8'});
    res.end('成功');
});

server.listen(3000, function () {
    console.log('server is listening at port 3000');
});
