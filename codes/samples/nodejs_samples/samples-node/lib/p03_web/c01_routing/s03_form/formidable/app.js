const http = require("http");
const formidable = require("formidable");

let server = http.createServer();

server.on("request", function (req, res) {
    if (req.url === "/upload" && req.method.toLowerCase() === "post") {
        // 解析上传的文件
        let form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            //打印上传的非文件类型的普通表单键值对
            console.log("fields==>");
            console.log(fields);
            //打印上传的文件
            console.log("files==>");
            console.log(files);
            res.writeHead(200, { "Content-Type": "text/plain;charset=utf-8" });
            res.end("文件已成功上传！");
        });
    } else {
        // 显示文件上传表单
        res.writeHead(200, { "content-type": "text/html;charset=utf-8" });
        res.end(`
    <h2>formidable包测试案例</h2>
    <form action="/upload" enctype="multipart/form-data" method="post">
      <div>标题: <input type="text" name="title" /></div>
      <div>上传文件: <input type="file" name="multipleFiles" multiple="multiple" /></div>
      <input type="submit" value="上传" />
    </form>
  `);
    }
});

server.listen(3000, "127.0.0.1", function () {
    console.log("server is running at 127.0.0.1:3000");
});
