const fs = require('fs')
const path = require('path')
const http = require('http')
const url = require('url')
const querystring = require('querystring')
const util = require('util')

let server = http.createServer()

server.on('request', function (req, res) {
    let pathname = url.parse(req.url).pathname
    if (pathname === '/login') {
        resAdd(res, req)
    } else {
        resIndex(res)
    }
})
server.listen(3000, function () {
    console.log('server is listening at port 3000');
})

//读取post.html
function resIndex(res) {
    /* 获取当前post.html的路径 */
    let readPath = path.join(__dirname, url.parse('post.html').pathname)
    fs.readFile(readPath, function (err, data) {
        if (err) {
            return res.end(err.message)
        }
        res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
        res.end(data)
    })
}

//将form提交的数据转换为字符串，并显示到浏览器页面上
function resAdd(res, req) {
    let postData = '';
    // 设置接收数据编码格式为 UTF-8
    req.setEncoding('utf8');
    // 接收数据块并将其赋值给 postData
    req.on('data', function (postDataChunk) {
        postData += postDataChunk;
    });
    req.on('end', function () {
        // 数据接收完毕，执行回调函数
        let param = querystring.parse(postData);
        res.writeHead(200, {'Content-Type': 'text/plain;charset=utf-8'});
        //将param转换为字符串
        res.end(util.inspect(param));
    });
}
