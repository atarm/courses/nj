/**
 * 使用HTTP提供静态资源服务
 */
let http = require('http');
//用于读取静态资源
let fs = require('fs');
//用于做路径拼接
let path = require('path');

let server = http.createServer();

server.on('request', function (request, response) {
    //获取静态资源路径
    let url = request.url;
    if (url === '/') {
        //读取相应静态资源内容
        fs.readFile(path.join(__dirname, 'views/index.html'), 'utf8', function (err, data) {
            //如果出现异常抛出异常
            if (err) {
                throw err
            }
            //将读取的静态资源数据响应给浏览器
            response.end(data)
        })
    } else if (url === '/login') {
        fs.readFile(path.join(__dirname, 'views/login.html'), 'utf8', function (err, data) {
            if (err) {
                throw err
            }
            response.end(data)
        })
    } else if (url === '/register') {
        fs.readFile(path.join(__dirname, 'views/register.html'), 'utf8', function (err, data) {
            if (err) {
                throw err
            }
            response.end(data)
        })
    } else if (url === '/css/main.css') {
        let cssPath = path.join(__dirname, '/css/main.css')
        fs.readFile(cssPath, 'utf8', function (err, data) {
            if (err) {
                throw err
            }
            response.end(data)
        })
    } else if (url === '/img/logo.jpg') {
        let imgPath = path.join(__dirname, '/img/logo.jpg')
        fs.readFile(imgPath, function (err, data) {
            if (err) {
                throw err
            }
            response.end(data)
        })
    } else {
        fs.readFile(path.join(__dirname, 'views/err.html'), 'utf8', function (err, data) {
            if (err) {
                throw err
            }
            response.end(data)
        })
    }
})

server.listen(3000, function () {
    console.log('server is listening at port 3000');
});
