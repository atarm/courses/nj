let http = require("http");
let fs = require("fs");
let path = require("path");
let _ = require("underscore");
let server = http.createServer();

server.on("request", function (req, res) {
    let url = req.url;
    if (url === "/") {
        fs.readFile(
            path.join(__dirname, "index.html"),
            "utf8",
            function (err, data) {
                if (err) {
                    return res.end(err.message);
                }
                let compiled = _.template(data);
                let htmlStr = compiled({
                    title: "hello world",
                    arr: [{ name: "Jack" }, { name: "rose" }, { name: "levi" }],
                });
                res.end(htmlStr);
            }
        );
    }
});

server.listen(3000, function () {
    console.log("server is running at port 3000");
});
