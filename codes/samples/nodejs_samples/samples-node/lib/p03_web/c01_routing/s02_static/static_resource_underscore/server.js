const http = require("http");
const fs = require("fs");
const path = require("path");
const _ = require("underscore");

// 设置静态资源目录
const staticDir = path.join(__dirname, "public");

// 创建静态资源映射函数
const mapStaticResources = (dir) => {
    return fs.readdirSync(dir).map((file) => {
        return {
            name: file,
            path: path.join(dir, file),
        };
    });
};

// 创建 HTTP 服务器
const server = http.createServer((req, res) => {
    const filePath = path.join(
        staticDir,
        req.url === "/" ? "index.html" : req.url
    );
    const extname = String(path.extname(filePath)).toLowerCase();
    const mimeTypes = {
        ".html": "text/html",
        ".js": "text/javascript",
        ".css": "text/css",
        ".json": "application/json",
        ".png": "image/png",
        ".jpg": "image/jpg",
        ".gif": "image/gif",
        ".wav": "audio/wav",
        ".mp4": "video/mp4",
        ".woff": "application/font-woff",
        ".ttf": "application/font-ttf",
        ".eot": "application/vnd.ms-fontobject",
        ".otf": "application/font-otf",
        ".svg": "application/image/svg+xml",
    };

    const contentType = mimeTypes[extname] || "application/octet-stream";

    fs.readFile(filePath, (error, content) => {
        if (error) {
            if (error.code == "ENOENT") {
                res.writeHead(404, { "Content-Type": "text/html" });
                res.end("404 Not Found", "utf-8");
            } else {
                res.writeHead(500);
                res.end(
                    "Sorry, check with the site admin for error: " +
                        error.code +
                        " ..\n"
                );
            }
        } else {
            res.writeHead(200, { "Content-Type": contentType });
            res.end(content, "utf-8");
        }
    });
});

// 启动服务器
server.listen(3000, () => {
    console.log("Server running at http://127.0.0.1:3000/");
});
