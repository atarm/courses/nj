const http = require("http");
const server = http.createServer();

server.on("request", function (req, res) {
    console.log("收到请求了，请求路径是" + req.url);
    //根据不同的请求路劲发送不同的响应结果
    //1.获取请求路径
    //2.判断路径处理响应
    let url = req.url;
    if (url === "/") {
        // res.write("username")
        res.end("index page");
    } else if (url === "/login") {
        res.end("login page");
    } else if (url === "/reg") {
        res.end("register page");
    } else {
        res.end("404 not found");
    }
})

server.listen(3000, '127.0.0.1', function () {
    console.log("server is running at http://127.0.0.1:3000");
})
