/**
 * 动态处理静态资源请求
 */
let http = require('http');
let fs = require('fs');
let path = require('path')
let server = http.createServer();
server.on('request', function (req, res) {
    let url = req.url;
    let fullPath = path.join(__dirname, url);

    // 当用户访问 / 的时候，默认让用户访问 index.html
    if (url === '/') {
        fullPath = path.join(__dirname, 'views/index.html');
    }
    console.log(fullPath);
    fs.readFile(fullPath, function (err, data) {
        if (err) {
            return res.end(err.message);
        }
        res.end(data);
    });
});

server.listen(3000, function () {
    console.log('server is running at port 3000...');
})

