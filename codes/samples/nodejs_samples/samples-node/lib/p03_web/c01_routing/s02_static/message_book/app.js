const http = require('http');
const fs = require('fs');
const path = require('path');
const url = require('url');
const _ = require('underscore');

//留言数据
let comments = [{
    name: '小明', message: 'Node.js 是一个基于Chrome JavaScript 运行时建立的一个平台', dataTime: '2020-2-28'
}, {
    name: '哈哈', message: '我要认真学习node.js', dataTime: '2020-2-28'
}, {
    name: '嘻嘻', message: 'Node.js是一个事件驱动I/O服务端JavaScript环境，基于Google的V8引擎', dataTime: '2020-2-28'
}, {
    name: 'levi', message: 'Node.js中采用了非阻塞型异步I/O机制', dataTime: '2020-2-28'
}, {
    name: 'jack', message: '好好学习，天天向上', dataTime: '2020-2-28'
}]

let server = http.createServer();

server.on('request', function (req, res) {
    //第二个参数为true的话，直接将查询的字符串转换为对象
    let reqObj = url.parse(req.url, true)
    let pathname = reqObj.pathname
    //读取首页
    if (pathname === '/') {
        fs.readFile(path.join(__dirname, 'views', 'index.html'), function (err, data) {
            if (err) {
                return res.end(err.message)
            }
            let compiled = _.template(data.toString())
            let htmlStr = compiled({
                comments: comments
            })
            res.setHeader('Content-Type', 'text/html;charset=utf-8')
            res.end(htmlStr)
        })
    } else if (pathname.startsWith('/public/')) {
        fs.readFile(path.join(__dirname, pathname), function (err, data) {
            if (err) {
                return res.end(err.message)
            }
            res.end(data)
        })
    } else if (pathname === '/post') {
        fs.readFile(path.join(__dirname, 'views/post.html'), function (err, data) {
            if (err) {
                return res.end(err.message)
            }
            res.setHeader('Content-Type', 'text/html;charset=utf-8')
            res.end(data)
        })
    } else if (pathname === '/pinglun') {
        //将从页面获取到的数据放到comments中去
        let postcomment = reqObj.query
        console.log(postcomment)
        //获取当前发表留言的时间
        let date = new Date()
        console.log(date)
        let now = date.toLocaleString()
        console.log(now)
        //给postcomment对象追加一个dateTime属性
        postcomment.dataTime = now
        comments.unshift(postcomment)
        //重新发送请求 /
        //状态码设置为302，临时重定向
        res.statusCode = 302
        //响应头中的Location表示重定向的路径
        res.setHeader('Location', '/')
        res.end()
    }
});

server.listen(3000, '127.0.0.1', function () {
    console.log('server is running at 127.0.0.1:3000');
});