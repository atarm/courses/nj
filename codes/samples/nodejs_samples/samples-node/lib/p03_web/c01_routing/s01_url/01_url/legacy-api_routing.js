const http = require('http');
const url = require('url');

const server = http.createServer((req, res) => {
    console.log(`req.url is ${req.url}`);

    const req_url = url.parse(req.url);

    switch (req_url.pathname) {
        case '/':
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end('home page');
            break;
        case '/login':
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end('login page');
            break;
        case '/register':
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end('register page');
            break;
        default:
            res.writeHead(404, { 'Content-Type': 'text/plain' });
            res.end('404 not found');
            break;
    }
});

server.listen(3000, () => {
    console.log('server is listening at port 3000');
});

/*
http://127.0.0.1:3000
http://127.0.0.1:3000/login
http://127.0.0.1:3000/register
http://127.0.0.1:3000/other
*/