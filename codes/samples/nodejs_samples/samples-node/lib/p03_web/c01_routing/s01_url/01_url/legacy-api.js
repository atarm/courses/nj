const url = require("url");

let parsed_url = url.parse(
    "https://example.com:8080/path/name?query=string#hash"
);

console.log(parsed_url);
console.log(parsed_url.query);

parsed_url = url.parse(
    "https://example.com:8080/path/name?query=string#hash",
    true
);

console.log(parsed_url);
console.log(parsed_url.query);
