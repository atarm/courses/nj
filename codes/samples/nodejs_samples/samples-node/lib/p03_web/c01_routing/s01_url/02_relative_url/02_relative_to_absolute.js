const http = require('http');

const server = http.createServer((req, res) => {
    // 判断请求使用的协议
    const protocol = req.connection.encrypted ? 'https://' : 'http://';

    // 基准 URL，可以是服务器的主机名和端口
    const baseUrl = `${protocol}${req.headers.host}`;

    // 将相对链接转换为绝对链接
    const absoluteUrl = new URL(req.url, baseUrl);

    // 输出绝对链接和协议
    console.log(`Protocol: ${protocol}`);
    console.log(`Absolute URL: ${absoluteUrl.href}`);

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end(`Your Request URL is: ${absoluteUrl.href}`);
});

server.listen(8080, () => {
    console.log('Server running at http://127.0.0.1:8080/');
});