const url = require('node:url');

const currentURL = new url.URL('https://user:pass@sub.example.com:8080/p/a/t/h?query=string#hash');

const legacyURL =
    url.parse('https://user:pass@sub.example.com:8080/p/a/t/h?query=string#hash',true);

console.log("currentURL is instance of URL: ", currentURL instanceof url.URL);
console.log(currentURL);

console.log("legacyURL is instance of url: ", legacyURL instanceof url.Url);
console.log(legacyURL);
