const http = require('http');
const url = require('url');
const querystring = require('querystring');

let server = http.createServer();

server.on('request', function (req, res) {
    console.log(`req.url is ${req.url}`);
    let realpath1 = url.parse(req.url);
    let qs1 = querystring.parse(realpath1.query);
    console.log(`req.querystring is parsed to: ${JSON.stringify(qs1)}`);

    let realpath2 = url.parse(req.url, true);
    let qs2 = realpath2.query;
    console.log(`req.querystring is parsed to: ${JSON.stringify(qs2)}`);

    let areEqual = JSON.stringify(qs1) === JSON.stringify(qs2);
    console.log(`Query strings are equal: ${areEqual}`);

    res.end(JSON.stringify({ areEqual }));
});

server.listen(3000, function () {
    console.log('server is listening at port 3000');
});

/**
 * http://127.0.0.1:3000/login?username=jack&password=123456
 */