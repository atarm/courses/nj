const http = require("http");

let server = http.createServer();

//request请求事件处理函数，需要接收两个参数
//request 请求对象：请求对象可以用来获取客户端的一些请求信息，例如请求路径
//response 响应对象：响应对象可以用来给客户端发送响应消息
server.on("request", function (req, res) {
    console.log("收到客户端的请求了,请求路径是" + req.url + "\t数据类型是：" + typeof req.url);
    res.end();
});

server.listen(3000, "127.0.0.1", function () {
    console.log("服务器启动成功");
});

/**
 * http://127.0.0.1:3000
 * http://127.0.0.1:3000/name
 * http://127.0.0.1:3000/login?name=jack&password=123456
 */
