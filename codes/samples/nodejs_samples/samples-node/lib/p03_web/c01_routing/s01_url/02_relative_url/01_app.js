const url = require('url');

console.log(url.parse('hash.js'));                // will work

console.log(new URL('hash.js', 'http://complete-url/'));  // will work
// console.log(new URL('hash.js'));                       // TypeError: Invalid URL: #hash

