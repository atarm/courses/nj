// 导入
const express = require("express");
const app = express();
const fs = require("fs");

// 路由
// app.get("/", (req, res) => {
//     throw new Error("自己制造的异常");
// });


app.get("/", (req, res) => {
    let url = "kljhkljgkgh.txt";
    // try：尝试，结果具备未知性，可能会成功可能会失败
    try {
        // 如果成功，则执行这里的后续代码
        let data = fs.readFileSync(url);
        res.send(data);
    } catch (error) {
        // 如果失败，则会执行这里的代码，此处允许我们对异常进行处理
        throw new Error("你要找的文件失踪了。。。。");
    }
});

// 兜底，接住了异常
app.use(function (err, req, res, next) {
    // 自己搞html页面，最终输出错误html页面
    res.send("你的错误是：" + err.message);
    // 此时的next已经没有什么使用价值了，所以可以省略不写。

});

// 兜底操作，如果所有的路由都匹配不上 则走这个中间件
// 404中间件不能放在路由上面，如果放到了上面，则后续所有的请求都是404
app.use(function (req, res, next) {
    // 自定义404需要输出的效果（提示、排版）
    // res.status(404);
    // res.send("404");
    res.status(404).send("404");
});

// 启动
app.listen(8088, () => {
    console.log("server is running at http://127.0.0.1:8088");
});
