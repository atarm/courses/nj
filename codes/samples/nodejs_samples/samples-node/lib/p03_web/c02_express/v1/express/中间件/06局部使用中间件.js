// 导入
const express = require("express");
const app = express();
// 导入bodyparser
const bodyParser = require("body-parser");

// 使用bodyParser解析post数据
// app.use(bodyParser.urlencoded({ extended: false }));
// 使用bodyParser解析json数据
// app.use(bodyParser.json());
dian
kao// 路由
// 使用单个中间件（局部）
app.post("/post1", bodyParser.urlencoded({ extended: false }), (req, res) => {
    console.log(req.body);
});
// 使用多个中间件（局部）
// 使用多个中间件（局部）
// 多个参数方式
app.post(
    "/post2",
    bodyParser.urlencoded({ extended: false }),
    bodyParser.json(),
    (req, res) => {
        console.log(req.body);
    }
);
// 数组形式（传递单个数组）
app.post(
    "/post3",
    [bodyParser.urlencoded({ extended: false }), bodyParser.json()],
    (req, res) => {
        console.log(req.body);
    }
);
app.post(
    "/post4",
    (req, res) => {
        console.log(req.body);
    }
);

app.get("/get1", (req, res) => {
    console.log(req.body);
});

app.get("/get2", (req, res) => {
    console.log(req.body);
});

// 启动
app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
});
