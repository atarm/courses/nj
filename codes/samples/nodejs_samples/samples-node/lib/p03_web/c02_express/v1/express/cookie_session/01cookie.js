const express = require("express");
const app = express();
const cookieParser = require("cookie-parser");

app.use(cookieParser());

// 路由
app.get("/", (req, res) => {
    // 获取方式，需要使用req.cookies属性
    if (req.cookies.is_old_user){
        res.send('欢迎回来，心悦vip3！');
    }else{
        // 设置方式
        // 如果没设置过期时间，则默认为会话期间有效，如果要设置通过maxAge选项，但是单位是毫秒
        res.cookie("is_old_user", 1, { maxAge: 86400 * 1000});//cookie的设置是在服务端，获取是在客户端
        console.log(req.cookies.is_old_user)
        res.send("欢迎成为我们的新会员，你需要冲8w到顶级");
    }

});

app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
});
