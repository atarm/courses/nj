var config=require('../../config/mysql');
var mysql=require('mysql');
var params;//参数
var pool=mysql.createPool(config.constr);//数据池连接对象


//BaseModel

// 导出
module.exports = {
    findUser: function(req,callback){
        pool.getConnection(function (err,conn) {
            if (err){
                return callback("连接池连接失败！"+err,null)
            }
            //参数绑定的方式防止sql  4342313132 or 1=1
            var sql = 'select * from users where username=? and password=?';
            // console.log(req)
            params = [req.body.username, req.body.password];
            conn.query(sql,params,function (err,results) {
                conn.release()
                if (err) {
                    return callback("查询失败！"+err,null)
                }
                callback(null,results)
            })
        })
    }
}
