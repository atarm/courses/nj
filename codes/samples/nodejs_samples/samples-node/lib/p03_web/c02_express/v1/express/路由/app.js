// 导入
const express = require("express");

// 创建实例
const app = express();


/*// 前台的用户列表操作
app.post("/frontend/user/add", (req, res) => {
    res.send("前台的用户添加操作！");
});
// 前台的用户删除操作
app.delete("/frontend/user/:id", (req, res) => {
    res.send("前台的用户删除操作！");
});
// 前台的用户修改操作
app.put("/frontend/user/:id", (req, res) => {
    res.send("前台的用户修改操作！");
});
// 前台的用户列表操作
app.get("frontend/user", (req, res) => {
    res.send("前台的用户列表操作！");
});



// 后台的用户列表操作
app.post("/backend/user/add", (req, res) => {
    res.send("后台的用户添加操作！");
});
// 后台的用户删除操作
app.delete("/backend/user/:id", (req, res) => {
    res.send("后台的用户删除操作！");
});
// 后台的用户修改操作
app.put("/backend/user/:id", (req, res) => {
    res.send("后台的用户修改操作！");
});
app.get("/backend/user", (req, res) => {
    res.send("后台的用户修改操作！");
});*/

// app.use()       应用级别

// 导入已经模块化的路由
const backend = require("./router/backend");
const frontend = require("./router/frontend");


// 定义路由
// 假定是一个真实的项目，有以下几个路由
// 后台的用户添加操作
// 在这里去使用导入的路由（支持使用前缀，提炼公共部分）
app.use("/backend", backend);
app.use("/frontend",frontend);

//.......

// 启动服务
app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
});
