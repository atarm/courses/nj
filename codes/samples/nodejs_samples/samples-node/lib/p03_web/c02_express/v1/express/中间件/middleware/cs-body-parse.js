const querystring = require("querystring");
var csBodyParse =  (req, res, next) => {
    // 在中间件中，需要监听req对象的data事件来获取客户端发送到服务器的数据。如果数据量比较大，无法一次性发送完毕，则客户端会把数据切割后分批次发送给服务器。所以data事件可能会被触发多次，每次触发data事件时，收到的数据只是全部数据的一部分，因此需要做数据的拼接才能得到完整的数据。
    let arr = [];
    req.on("data", (buffer) => {
        arr.push(buffer);
    });

    console.log(arr)
/*    var postConten = '';
    req.on("data", function (funk) {
        postConten += funk;
    });*/
    // 当请求体数据传输完毕后会触发end事件，拿到全部数据后可以继续处理post数
    req.on("end", () => {
        let buffer = Buffer.concat(arr);
        console.log(buffer)
        let post = querystring.parse(buffer.toString());
        // 将数据写入到req.body
        // console.log(post);
        req.body = post;
        // req.on是一个异步的处理程序，next必须要在里面执行，如果是放在外面的话，则会导致，数据并没有挂到body上而直接下一步了
        // 在最后加上继续往后执行的netx方法
        next();
    });

}
// 导出
module.exports = csBodyParse;