// 1. 安装mongoose，npm i -S mongoose
// 2. 导入mongoose
const express = require("express");
const app = express();
const mongoose = require("mongoose");
// 配置模版引擎
app.engine("html", require("express-art-template"));
app.set("views", "./views");
app.set("view engine", "html");
// 3. 连接mongodb数据库
// 连接时的数据库必须要存在
mongoose.connect("mongodb://127.0.0.1:27017/jiaoyou", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
// 4. 建立schema（表字段的约束）
const user_schema = new mongoose.Schema({
    // 表字段的规则
    // 字段名: {是否必填:true,数据类型:String,.....}
    username: {
        type: String,
        required: true,
        minlength: 4,
    },
    password: {
        type: String,
        required: true,
    },
    gender: {
        type: Number,
        required: false,
    },
    mobile: {
        type: String,
        required: true,
    },
    // ...
});
// 5. 建立mode
// 参数1：是模型名（一般就是表名，建议首字母大写）
// 参数2：是schema
// 参数3：是真实意义上的表名（可以省略不写，如果不写表名默认为模型名的复数形式）。主要取决于中西方编程文化的差异
const model = mongoose.model("User", user_schema, "user");

app.get("/", (req, res) => {
    // 6. 增删改查（C，create/U，update/R，read/D，delete）交互操作
    // 获取get参数
    // console.log(req.query);
    model.insertMany(req.query);
});

app.put("/update/:id", (req, res) => {
    // 调用修改操作
});

app.delete("/update/:id", (req, res) => {
    // 调用删除操作
});

app.get("/select", (req, res) => {
    // 调用查询操作（耦合式开发，实现列表）
    // model所提供的操作方法是异步方法
    model.find().then((ret) => res.render("list.html", { ret }));
});

app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
});
