// 导入模块
const express = require("express");
// 创建路由模块对象
const router = express.Router();

// 前台的新闻列表操作
router.get("/news", (req, res) => {
    res.send("前台的新闻列表操作！");
});
// 前台的新闻详情操作
router.get("/news/:id", (req, res) => {
    res.send("前台的新闻详情操作！");
});
// 前台的新闻分享操作
router.get("/news/share/:id", (req, res) => {
    res.send("前台的新闻分享操作！");
});
// 前台的新闻点赞操作
router.post("/news/zan/:id", (req, res) => {
    res.send("前台的新闻点赞操作！");
});

// router.use() // 路由级别的

// 导出
module.exports = router;
