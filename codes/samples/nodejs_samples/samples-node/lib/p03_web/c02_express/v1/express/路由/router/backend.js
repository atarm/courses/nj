// 导入模块
const express = require("express");
// 创建路由模块对象
const router = express.Router();

// 定义路由（模块化）
// 后台的用户列表操作
router.post("/user/add", (req, res) => {
    res.send("后台的用户添加操作！");
});
// 后台的用户删除操作
router.delete("/user/:id", (req, res) => {
    res.send("后台的用户删除操作！");
});
// 后台的用户修改操作
router.put("/user/:id", (req, res) => {
    res.send("后台的用户修改操作！");
});
router.get("/user", (req, res) => {
    res.send("后台的用户修改操作！");
});

// 导出
module.exports = router;
