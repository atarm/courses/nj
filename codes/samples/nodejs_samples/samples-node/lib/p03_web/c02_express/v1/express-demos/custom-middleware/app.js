const express = require('express');
const app = express();
// 引入自己封装的中间件模块cs-body-parse
const myBodyParser = require('./my_body_parser.js');

// csBodyParse的使用
app.use(myBodyParser);

// 路由
app.post('/post',(req,res) => {
    console.log(req.body);
})

app.listen(8081,() => {
    console.log('server is running at http://127.0.0.1:8081...');
})