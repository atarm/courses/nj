// 目标：新建一个users表，并且只要需要有1条数据
const express = require("express");
const app = express();
const port = 3000;
const bodyParser = require("body-parser");
// 导入md5包，用于对密码进行加密
// 用法：md5(待加密的字符串)
const md5 = require("md5");

// 使用bodyparser中间件帮助接收post数据
app.use(bodyParser.urlencoded({ extended: false }));

// 自定义中间件：加密密码
app.use(function (req, res, next) {
    // 此处需要用到“加盐/料加密”的思想，直接md5会有被爆破的风险
    req.body.password = md5(
        req.body.password + md5(req.body.password).substr(10, 10)
    );
    next();
});

// 登录用户信息的初始化，为了得到加密的密码，用完之后为了安全建议注释/删除
// app.post("/init", (req, res) => {
//     console.log(req.body);
// });

app.listen(port, () =>
    console.log(`server is running at htp://127.0.0.1:${port}!`)
);
