// 导入
const express = require("express");

const app = express();

// 自定义中间件
app.use(require("./middleware/cs-body-parse"));

// 路由
app.post("/post", (req, res) => {
    console.log(req.body);
    // res.send(req.body);
});

// 启动
app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
});
