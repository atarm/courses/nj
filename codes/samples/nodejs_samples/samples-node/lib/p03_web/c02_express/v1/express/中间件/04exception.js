// 导入
const express = require("express");
const app = express();
const fs = require("fs");

// 路由
//程序员知道这里代码出错了  那就手动抛异常
/*
app.get("/", (req, res) => {

    throw new Error("自己制造的异常");
});
*/



app.get("/", (req, res) => {
    let url = "kljhkljgkgh.txt";
    // try：尝试，结果具备未知性，可能会成功可能会失败
    try {
        // 如果成功，则执行这里的后续代码
        let data = fs.readFileSync(url);
        res.send(data);
    } catch (error) {
        // 如果失败，则会执行这里的代码，此处允许我们对异常进行处理
        throw new Error("你要找的文件失踪了。。。。"+error.message);
    }
});

// 兜底，接住了异常
app.use(function (err, req, res, next) {
    // 自己搞html页面，最终输出错误html页面
    res.send("你的错误是：" + err.message);
    // 此时的next已经没有什么使用价值了，所以可以省略不写。
    // next();
});

// 启动
app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
});
