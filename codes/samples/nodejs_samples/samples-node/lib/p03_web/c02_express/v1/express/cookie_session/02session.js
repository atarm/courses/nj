const express = require("express");
const app = express();
const cookieSession = require("cookie-session");

// 实现记录，您是访问本网站的第14886002位访客
app.use(
    cookieSession({
        // 在cookie中用户记录sessionid的名字
        name: "sessionId",
        // 用户对明文的sessionID进行加密处理，内容任意，但是必须要写
        secret: "LKGJLGlhkl3546453546mjf",
        // 设置不活动session清除的时间
        maxAge: 20 * 60 * 1000,
        // 让时间滚动刷新（刷新cd时间）
        rolling: true,
    })
);

// 设置session的需要指定一个配置项，是一个任意内容的字符串
app.get("/", (req, res) => {
    // 获取session
    if (req.session["count"]) {
        req.session["count"]++;
        res.send(`您是访问本网站的第${req.session["count"]}位访客`);
    } else {
        req.session["count"] = 1;//cookie的设置和获取都是在客户端
        res.send(`您是访问本网站的第1位访客`);
    }
});

app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
});
