const express = require("express");

const app = express();

//必须传递的动态参数
app.put("/user/:id", (req, res) => {
    res.send('传递的条件是id=' + req.params.id);
});


app.put("/user/:id?", (req, res) => {
    console.log(req.params.id)
    if (typeof req.params.id == 'undefined') {
        res.send('没传递内容');
    }
    res.send('传递的条件是id=' +  req.params.id);
});

app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
})