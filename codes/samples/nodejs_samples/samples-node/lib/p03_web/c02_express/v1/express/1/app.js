const express = require("express");

const app = express();

app.get("/", (req, res) => {
    res.send('这是get请求');
});
app.post("/", (req, res) => {
    res.send('这是post请求');
});

app.put("/", (req, res) => {
    res.send('这是put请求');
});
app.delete("/", (req, res) => {
    res.send('这是delete请求');
});

app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
})