// 引入querystring模块
const querystring = require("querystring");

// 核心代码
let my_body_parser = (req, res, next) => {
    let data = [];
    req.on("data", (buffer) => {
        data.push(buffer);
    });
    req.on("end", () => {
        // 挂载到req.body上
        req.body = querystring.parse(Buffer.concat(data).toString());
        // 继续后续的请求处理
        next();
    });
}

// 导出
module.exports = my_body_parser;