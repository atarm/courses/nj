const express = require("express");
const app = express();
const port = 3000;

// 配置模版引擎
app.engine("html", require("express-art-template"));
// 指定模板试图路径
app.set("views", "./views");
// 省略指定模板文件后缀名称（可选，在渲染时可以省略的后缀）
app.set("view engine", "html");

// 基本渲染
app.get("/", (req, res) => res.render("base.html"));

// 视图需要使用数据，但是数据来源响应的方法（后端）
app.get("/art1", (req, res) => {
    let name = "zhangsan";
    let age = 29;
    let gender = "男";
    // render方法接收第2个参数（对象，里面可以存放很多的数据），允许传递后端的数据给视图
    res.render("art1.html", {
        name,
        age,
        gender,
    });
});

// 复杂的数据
app.get("/art2", (req, res) => {
    let arr = ["吃饭", "睡觉", "打豆豆"];
    // render方法接收第2个参数（对象，里面可以存放很多的数据），允许传递后端的数据给视图
    res.render("art2.html", {
        arr,
    });
});

// 判断
app.get("/art3", (req, res) => {
    let age = 31;
    // render方法接收第2个参数（对象，里面可以存放很多的数据），允许传递后端的数据给视图
    res.render("art3.html", {
        age,
    });
});

// 特殊变量
app.get("/art4", (req, res) => {
    let str = "<a href='http://www.baidu.com' target='_blank'>百度</a>";
    // render方法接收第2个参数（对象，里面可以存放很多的数据），允许传递后端的数据给视图
    res.render("art4.html", {
        str,
    });
});

// 模版的包含
app.get("/art5", (req, res) => {
    res.render("art5.html");
});

// 模版的继承
app.get("/shouji", (req, res) => {
    res.render("shouji.html");
});
app.get("/shipin", (req, res) => {
    res.render("shipin.html");
});

app.listen(port, () =>
    console.log(`server is running at http://127.0.0.1:${port}!`)
);
