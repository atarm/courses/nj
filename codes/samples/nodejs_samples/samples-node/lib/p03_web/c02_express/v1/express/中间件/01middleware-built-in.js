// 案例：使用json、urlencoded中间件来接收json数据与表单post数据，发送可以通过postman来进行
const express = require("express");
const app = express();

// 使用中间件
// extended:false，表示要求在解析数据的时候使用querystring库，会剔除传递过来的方法和对象
app.use(express.urlencoded({ extended: false }));
// 默认为extended:true，表示要求在解析数据的时候使用qs库，不会剔除方法和对象
// app.use(express.urlencoded());

// 使用json中间件来实现json数据的接收
app.use(express.json());

// 具体路由
app.post("/post", (req, res) => {
    console.log(req.body);
    res.send(req.body)
});

app.post("/post123", (req, res) => {
    console.log(req.body);
});

// 启动
app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
});
