// 导入
const express = require("express");
const app = express();
// 导入bodyparser
const bodyParser = require("body-parser");

// 使用bodyParser解析post数据
app.use(bodyParser.urlencoded({ extended: false }));
// 使用bodyParser解析json数据
app.use(bodyParser.json());

// 路由
app.post("/post", (req, res) => {
    console.log(req.body);
});

// 启动
app.listen(8080, () => {
    console.log("server is running at http://127.0.0.1:8080");
});
