// import { Buffer } from 'node:buffer';

// let Buffer = require('Buffer');

// Creates a new Buffer containing the UTF-8 bytes of the string 'buffer'.
const buf_01 = Buffer.from([0x62, 0x75, 0x66, 0x66, 0x65, 0x72]);

console.log(buf_01); // prints: <Buffer 62 75 66 66 65 72>

const buf_02 = Buffer.alloc(5);

console.log(buf_02); // prints: <Buffer 00 00 00 00 00>

const buf1 = Buffer.from('buffer');
const buf2 = Buffer.from(buf1);

buf1[0] = 0x61;

console.log(buf1.toString());
// Prints: auffer
console.log(buf2.toString());
// Prints: buffer

const buf = Buffer.from([0x1, 0x2, 0x3, 0x4, 0x5]);
const json = JSON.stringify(buf);

console.log(json);
// Prints: {"type":"Buffer","data":[1,2,3,4,5]}

const copy = JSON.parse(json, (key, value) => {
    return value && value.type === 'Buffer' ?
        Buffer.from(value) : value;
});

console.log(copy);
// Prints: <Buffer 01 02 03 04 05>