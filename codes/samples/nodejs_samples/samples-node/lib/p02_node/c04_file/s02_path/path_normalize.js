const path = require('path');

const windowsPath = 'C:\\temp\\foo\\bar\\..\\';
const normalizedWindowsPath = path.normalize(windowsPath);
console.log(normalizedWindowsPath); // 输出: 'C:\temp\foo\bar\..\'

const unixPath = '/foo/bar//baz/asdf/quux/..';
const normalizedUnixPath = path.normalize(unixPath);
console.log(normalizedUnixPath); // 输出: '/foo/bar/baz/asdf/'