let fs = require('fs');

// 创建读入流和写入流
let rs = fs.createReadStream('./data/src/a.jpg');
let ws = fs.createWriteStream('./data/out/b.jpg');

// 监听流的打开和关闭
rs.once('open', function () {
    console.log('读出通道已经打开');
}).once('close', function () {
    console.log('读出通道已经关闭');
});

ws.once('open', function () {
    console.log('写入通道已经打开');
}).once('close', function () {
    console.log('写入通道已经关闭');
});

// 绑定data
rs.on('data', function (chunk) {
    ws.write(chunk);
});

ws.end();
