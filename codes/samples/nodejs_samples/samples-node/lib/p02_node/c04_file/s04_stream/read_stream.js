//1.引入模块
let fs = require('fs');

//2.创建读入流
let total = '';
let rs = fs.createReadStream('./data/src/read_stream.txt');
rs.setEncoding('utf8');

//chunk是整个数据流的数据块
rs.on('data', function (chunk) {
    //每次拿到chunk，就放到total里面
    total += chunk;
});

rs.on('end', function (chunk) {
    console.log(total);
});

rs.on('error', function (err) {
    console.log(err);
});
