let fs = require('fs');
let zlib = require('zlib');

let r = fs.createReadStream('./data/src/a.jpg');
let z = zlib.createGzip();
let w =fs.createWriteStream('./data/out/a.jpg.gz');

r.pipe(z).pipe(w);
