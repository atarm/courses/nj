//1. 引入模块
let fs = require('fs');

//2. 创建流
let rs = fs.createReadStream('./data/src/a.jpg');
let ws = fs.createWriteStream('./data/out/c.jpg');

//3. 建立管道
// if (rs.pipe(ws)){
//     console.log('复制成功');
// }else {
//     console.log('复制失败');
// }

// 3. 管道读写
rs.pipe(ws).on('finish', function () {
    setTimeout(function () {
        console.log('复制成功');
    }, 1000);
}).on('error', function () {
    setTimeout(function () {
        console.log('复制失败');
    }, 1000);
});

// would be executed before the finish event is emitted
console.log('程序执行完毕');