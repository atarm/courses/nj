const fs = require("fs");
const file_path = "resources/example.txt";

// 使用 Buffer 读取文件
fs.readFile(file_path, (err, data) => {
    if (err) throw err;
    console.log("Buffer data:", data);
    console.log("Buffer to string:", data.toString());
});

// 使用 Stream 读取文件
const readableStream = fs.createReadStream(file_path);
readableStream.on("data", (chunk) => {
    console.log("Stream chunk:", chunk);
    console.log("Chunk to string:", chunk.toString());
});
readableStream.on("end", () => {
    console.log("Stream ended");
});
