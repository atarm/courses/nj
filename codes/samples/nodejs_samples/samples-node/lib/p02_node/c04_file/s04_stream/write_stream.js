//1.引入模块
let fs = require('fs');

//2.创建写入流
let ws = fs.createWriteStream('./data/out/write_stream.txt');
console.log(ws);

//3.打开通道
//once(event, fn)：为指定事件注册一个单次监听器，单次监听器最多只触发一次，触发后立即解除监听器；
ws.once('open', function () {
    console.log('写入流打开');
    //4.写入内容
    ws.write('好好学习，天天向上');
    ws.end();
});
ws.once('close', function () {
    console.log('写入流关闭');
});
ws.once('finish', function () {
    console.log('写入流完成');
});
