const fs = require("fs");
const path = require("path");
const src = path.join(
    __dirname,
    "./resources/Node.js is a serious thing now… (2023).mp4"
);
const dst_dir = "~dst";
const dst_ext_sync = ".sync-io";
const dst_ext_async_buffer = ".async-buffer-io";
const dst_ext_async_stream = ".async-stream-io";
const dst_ext_async_pipe = ".async-pipe-io";

src_dirname = path.dirname(src);
console.log("src_dirname:", src_dirname);
src_ext = path.extname(src);
console.log("src_ext:", src_ext);
src_basename = path.basename(src, src_ext);
console.log("src_basename:", src_basename);

const dst_dir_path = path.join(src_dirname, dst_dir);
if (!fs.existsSync(dst_dir_path)) {
    fs.mkdirSync(dst_dir_path);
}

const dst_sync = path.join(
    src_dirname,
    dst_dir,
    src_basename + dst_ext_sync + src_ext
);
console.log("dst_sync:", dst_sync);
const dst_async_buffer = path.join(
    src_dirname,
    dst_dir,
    src_basename + dst_ext_async_buffer + src_ext
);
console.log("dst_async_buffer:", dst_async_buffer);
const dst_async_stream = path.join(
    src_dirname,
    dst_dir,
    src_basename + dst_ext_async_stream + src_ext
);
console.log("dst_async_stream:", dst_async_stream);
const dst_async_pipe = path.join(
    src_dirname,
    dst_dir,
    src_basename + dst_ext_async_pipe + src_ext
);
console.log("dst_async_pipe:", dst_async_pipe);

const src_stat = fs.statSync(src);
// console.log('src_stat:', src_stat);
console.log("src_stat.size:", src_stat.size);

console.log("");

// Synchronous file copy
function copyFileSync(src, dst) {
    const start_time = process.hrtime();
    data = fs.readFileSync(src);
    console.log("Sync-io data size:", data.length);
    fs.writeFileSync(dst, fs.readFileSync(src));
    console.log("Sync-io time:", process.hrtime(start_time));
}

// Asynchronous file copy using buffer
function copyFileAsyncBuffer(src, dst) {
    const start_time = process.hrtime();
    fs.readFile(src, (err, data) => {
        if (err) throw err;
        console.log("Async-io data size:", data.length);
        fs.writeFile(dst, data, (err) => {
            if (err) throw err;
            console.log("Async-io time:", process.hrtime(start_time));
        });
    });
}

// Asynchronous file copy using stream
function copyFileAsyncStream(src, dst) {
    const readStream = fs.createReadStream(src);
    const writeStream = fs.createWriteStream(dst);
    const start_time = process.hrtime();
    let i = 0;

    readStream.on("data", (chunk) => {
        if (!i++) console.log("Async-stream-io data size(each):", chunk.length);
        writeStream.write(chunk);
    });

    readStream.on("end", () => {
        writeStream.end();
        console.log("Async-stream-io time:", process.hrtime(start_time));
    });
}

// Asynchronous file copy using stream pipe
function copyFileAsyncPipe(src, dst) {
    const readStream = fs.createReadStream(src);
    const writeStream = fs.createWriteStream(dst);
    readStream.pipe(writeStream);
}

copyFileSync(src, dst_sync);
copyFileAsyncBuffer(src, dst_async_buffer);
copyFileAsyncStream(src, dst_async_stream);
copyFileAsyncPipe(src, dst_async_pipe);
