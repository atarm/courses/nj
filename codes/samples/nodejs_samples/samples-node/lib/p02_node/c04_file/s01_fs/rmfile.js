const fs = require("fs");
const path = require("path");

const file_to_rm = "./resources/file_to_rm.txt";

fs.unlink(path.join(__dirname, file_to_rm), function (err) {
    if (err) {
        return console.error(err);
    }
    console.log("文件删除成功！");
});