1
00:00:00,539 --> 00:00:04,799
over the years I've found that node.js

2
00:00:03,299 --> 00:00:04,200
is kind of strange in the world of

3
00:00:05,339 --> 00:00:10,199
back-end development let's say you wrote

4
00:00:07,500 --> 00:00:12,240
some code for a back-end API server and

5
00:00:10,199 --> 00:00:14,939
you deploy that code to a machine with

6
00:00:12,240 --> 00:00:16,618
four gigs of memory and two CPUs then

7
00:00:14,939 --> 00:00:18,958
you start receiving traffic to your

8
00:00:16,618 --> 00:00:21,598
server over the internet now this is a

9
00:00:18,960 --> 00:00:22,799
node.js server which means well what

10
00:00:21,600 --> 00:00:24,899
does that mean

11
00:00:22,800 --> 00:00:27,179
first of all node.js is not a

12
00:00:24,899 --> 00:00:29,517
programming language JavaScript is the

13
00:00:27,179 --> 00:00:33,419
language and typescript gets transpiled

14
00:00:29,518 --> 00:00:35,218
down to JavaScript also node.js is not a

15
00:00:33,420 --> 00:00:37,860
server there are plenty of general

16
00:00:35,219 --> 00:00:40,738
purpose apps built with node.js that

17
00:00:37,859 --> 00:00:43,979
have nothing to do with servers node.js

18
00:00:40,738 --> 00:00:46,377
is a JavaScript runtime environment it's

19
00:00:43,979 --> 00:00:48,717
a thing that executes JavaScript outside

20
00:00:46,378 --> 00:00:50,818
of the browser and most of the time we

21
00:00:48,719 --> 00:00:53,280
use it to build back-end applications

22
00:00:50,820 --> 00:00:55,260
and so since our app is written in

23
00:00:53,280 --> 00:00:57,480
JavaScript that means it's a single

24
00:00:55,259 --> 00:01:00,419
threaded application it can only utilize

25
00:00:57,479 --> 00:01:02,339
one CPU at a time which is a big waste

26
00:01:00,420 --> 00:01:05,338
of resources this is why we typically

27
00:01:02,340 --> 00:01:07,618
run multiple node.js processes each

28
00:01:05,338 --> 00:01:09,419
server running on a different port and

29
00:01:07,618 --> 00:01:12,239
for that to work we have to use a load

30
00:01:09,420 --> 00:01:14,460
balancer like nginx pm2 is a process

31
00:01:12,239 --> 00:01:16,739
manager kubernetes is a container

32
00:01:14,459 --> 00:01:18,899
manager and then Heroku and Amazon are

33
00:01:16,739 --> 00:01:20,937
Cloud platforms but all of these are

34
00:01:18,900 --> 00:01:23,220
solutions to the load balancer problem

35
00:01:20,938 --> 00:01:25,258
now what's interesting is that if we we

36
00:01:23,219 --> 00:01:27,118
chose just about any other mainstream

37
00:01:25,259 --> 00:01:29,340
language then we can use true

38
00:01:27,118 --> 00:01:31,558
multi-threading meaning your application

39
00:01:29,340 --> 00:01:34,200
running on a single process can utilize

40
00:01:31,560 --> 00:01:36,478
all of the CPUs on the machine and also

41
00:01:34,200 --> 00:01:38,700
threads are a lot lighter than processes

42
00:01:36,478 --> 00:01:40,978
they're faster to spin up and they take

43
00:01:38,700 --> 00:01:43,320
up less memory ultimately making your

44
00:01:40,978 --> 00:01:45,298
application a lot more efficient so it's

45
00:01:43,319 --> 00:01:47,698
kind of a bummer that in order to

46
00:01:45,299 --> 00:01:50,220
achieve maximum performance with node.js

47
00:01:47,700 --> 00:01:52,499
you have to spin up multiple processes

48
00:01:50,219 --> 00:01:55,138
or at least that's what I thought until

49
00:01:52,500 --> 00:01:56,939
recently alright ladies and gentlemen so

50
00:01:55,140 --> 00:01:59,460
it turns out that there is a way to

51
00:01:56,938 --> 00:02:01,319
achieve multi-threading with node.js but

52
00:01:59,459 --> 00:02:02,398
I think I should demonstrate first why

53
00:02:01,319 --> 00:02:04,319
you would even want to use

54
00:02:02,399 --> 00:02:06,479
multi-threading when we have an

55
00:02:04,319 --> 00:02:08,338
asynchronous runtime right node.js is

56
00:02:06,478 --> 00:02:09,659
async you'd already handle concurrent

57
00:02:08,340 --> 00:02:11,940
requests so what do you need

58
00:02:09,659 --> 00:02:14,578
multi-threading for so here I have a

59
00:02:11,939 --> 00:02:16,259
little demo I have a Fibonacci function

60
00:02:14,580 --> 00:02:18,359
then I have this thing called do FIB

61
00:02:16,259 --> 00:02:20,457
which is just wrapping the Fibonacci

62
00:02:18,360 --> 00:02:22,199
function inside of a promise and the

63
00:02:20,459 --> 00:02:24,300
goal is to make this Fibonacci function

64
00:02:22,199 --> 00:02:26,578
asynchron furnace and make it so that we

65
00:02:24,300 --> 00:02:28,679
can evoke it multiple times concurrently

66
00:02:26,580 --> 00:02:30,599
of course if you have enough experience

67
00:02:28,680 --> 00:02:32,640
with node you know that we can't do this

68
00:02:30,598 --> 00:02:35,098
but we're going to try it anyway here I

69
00:02:32,639 --> 00:02:38,278
have promise.all and I'm running do FIB

70
00:02:35,098 --> 00:02:40,019
10 times hopefully concurrently then I'm

71
00:02:38,280 --> 00:02:41,879
going to print all the values and let's

72
00:02:40,020 --> 00:02:44,579
go ahead and see what happens here so

73
00:02:41,878 --> 00:02:47,398
let's go ahead and run this

74
00:02:44,580 --> 00:02:50,160
okay so we can see it is evoking the

75
00:02:47,400 --> 00:02:51,720
function a few times so we see that each

76
00:02:50,159 --> 00:02:54,359
individual time it evoked the function

77
00:02:51,719 --> 00:02:57,358
it took about 470 milliseconds and in

78
00:02:54,360 --> 00:02:59,940
the end it took about 4.7 seconds to get

79
00:02:57,360 --> 00:03:01,738
it all done so obviously it didn't

80
00:02:59,939 --> 00:03:04,439
actually execute all of these

81
00:03:01,739 --> 00:03:06,899
concurrently it actually did it one at a

82
00:03:04,439 --> 00:03:09,118
time it did it synchronously why because

83
00:03:06,900 --> 00:03:11,098
in JavaScript you can't just take a

84
00:03:09,120 --> 00:03:13,560
synchronous function and then wrap it in

85
00:03:11,098 --> 00:03:15,657
an async promise and expect it to

86
00:03:13,560 --> 00:03:17,878
magically become async so this is

87
00:03:15,658 --> 00:03:20,518
ultimately the big problem with single

88
00:03:17,878 --> 00:03:22,858
threading is if you do any CPU intensive

89
00:03:20,519 --> 00:03:25,019
calculations it's going to block the

90
00:03:22,860 --> 00:03:26,940
entire event Loop which in turn is going

91
00:03:25,019 --> 00:03:29,280
to block your entire app and that's

92
00:03:26,939 --> 00:03:31,498
especially bad in a server environment

93
00:03:29,280 --> 00:03:33,658
okay so this time around we're going to

94
00:03:31,500 --> 00:03:35,1000
do the same demo except we're going to

95
00:03:33,658 --> 00:03:38,219
use worker threads instead so we have

96
00:03:36,000 --> 00:03:40,200
our do FIB function and so inside of

97
00:03:38,219 --> 00:03:42,199
this do FIB function we're saying worker

98
00:03:40,199 --> 00:03:44,939
use new worker we're going to execute

99
00:03:42,199 --> 00:03:46,679
fib.js and pass in some arbitrary very

100
00:03:44,939 --> 00:03:48,358
parameters there we're going to say once

101
00:03:46,680 --> 00:03:50,099
we receive a message from the worker

102
00:03:48,360 --> 00:03:52,379
we're going to log that and then resolve

103
00:03:50,098 --> 00:03:54,118
the promise and once we receive an error

104
00:03:52,378 --> 00:03:56,759
we're going to reject the promise and

105
00:03:54,120 --> 00:03:58,379
this is how fib.js looks so we have our

106
00:03:56,759 --> 00:04:00,419
Fibonacci function here we're getting

107
00:03:58,378 --> 00:04:03,238
the result and we're saying parent Port

108
00:04:00,419 --> 00:04:05,877
dot post message and so this is going to

109
00:04:03,239 --> 00:04:07,860
send the data from the worker back up to

110
00:04:05,878 --> 00:04:10,437
the parent and so in terms of the main

111
00:04:07,860 --> 00:04:13,260
logic we're doing promise.all with do

112
00:04:10,438 --> 00:04:15,418
FIB 10 times same as before and remember

113
00:04:13,259 --> 00:04:17,339
before it did all of these synchronously

114
00:04:15,419 --> 00:04:18,838
did them one at a time what we're hoping

115
00:04:17,339 --> 00:04:21,358
is that this is going to evoke these all

116
00:04:18,839 --> 00:04:24,138
concurrently instead so let's run the

117
00:04:21,358 --> 00:04:24,137
code and see what happens

118
00:04:24,418 --> 00:04:29,518
okay boom that was fast so it took about

119
00:04:27,120 --> 00:04:31,620
one second for each worker to complete

120
00:04:29,519 --> 00:04:34,078
everything was done in about 900

121
00:04:31,620 --> 00:04:36,660
milliseconds as well so this suggests

122
00:04:34,079 --> 00:04:38,819
that these did not evoke one at a time

123
00:04:36,660 --> 00:04:40,919
sequentially no they evoked all

124
00:04:38,819 --> 00:04:42,359
concurrently and we know that these all

125
00:04:40,918 --> 00:04:45,238
evoked on a different thread of

126
00:04:42,360 --> 00:04:47,699
execution because it didn't block the

127
00:04:45,240 --> 00:04:50,220
entire app so there you go that's proof

128
00:04:47,699 --> 00:04:53,040
that worker threads do actually create

129
00:04:50,220 --> 00:04:55,439
new threads and this is not the same as

130
00:04:53,040 --> 00:04:57,120
using Fork because when you Fork a

131
00:04:55,439 --> 00:04:59,578
process that's creating a whole new

132
00:04:57,120 --> 00:05:01,860
child process which has a lot of memory

133
00:04:59,579 --> 00:05:03,659
overhead it's also very slow to spin

134
00:05:01,860 --> 00:05:06,239
those up if you try to spin up a bunch

135
00:05:03,660 --> 00:05:08,520
of processes on the Fly it's not gonna

136
00:05:06,240 --> 00:05:10,320
be very performant so this is pretty

137
00:05:08,519 --> 00:05:12,478
awesome the fact that we do have true

138
00:05:10,319 --> 00:05:14,578
multi-threading now the API is a little

139
00:05:12,478 --> 00:05:16,438
bit funny right passing messages around

140
00:05:14,579 --> 00:05:19,019
and all that but it's not really that

141
00:05:16,439 --> 00:05:20,458
unusual nowadays message passing is kind

142
00:05:19,019 --> 00:05:22,378
of a popular way of doing

143
00:05:20,459 --> 00:05:24,838
multi-threading so one of the issues

144
00:05:22,379 --> 00:05:26,339
with using worker threads is that it's a

145
00:05:24,839 --> 00:05:28,799
little bit different than traditional

146
00:05:26,339 --> 00:05:31,798
multi-threading with worker threads each

147
00:05:28,800 --> 00:05:33,899
thread runs in its own context it has

148
00:05:31,800 --> 00:05:35,579
its own block of memory let's say you

149
00:05:33,899 --> 00:05:37,499
have a one or two gigabyte buffer of

150
00:05:35,579 --> 00:05:39,119
memory and you want to pass that to a

151
00:05:37,500 --> 00:05:41,039
worker thread and then perform some

152
00:05:39,120 --> 00:05:42,959
Transformations and then pass it back to

153
00:05:41,038 --> 00:05:44,518
the main thread that's a lot of data

154
00:05:42,959 --> 00:05:46,257
transfer especially in a server

155
00:05:44,519 --> 00:05:48,239
environment so what you generally want

156
00:05:46,259 --> 00:05:50,219
to do with performance is not

157
00:05:48,240 --> 00:05:52,740
necessarily copy the data around but

158
00:05:50,220 --> 00:05:55,439
rather pass ownership of data around you

159
00:05:52,740 --> 00:05:57,360
want to share memory between threads and

160
00:05:55,439 --> 00:05:59,338
fortunately there is a way to do that

161
00:05:57,360 --> 00:06:01,800
with worker threads and so here what

162
00:05:59,339 --> 00:06:03,417
we've done is create a shared buffer and

163
00:06:01,800 --> 00:06:06,899
what we're going to do is fill this

164
00:06:03,418 --> 00:06:09,418
buffer with five and so five five five

165
00:06:06,899 --> 00:06:11,278
five and then we're going to create a

166
00:06:09,418 --> 00:06:13,679
worker and we're going to pass in that

167
00:06:11,279 --> 00:06:16,977
shared buffer to the worker the worker

168
00:06:13,680 --> 00:06:18,479
is w dot JS right and then once we get a

169
00:06:16,978 --> 00:06:20,279
message from the buffer we're going to

170
00:06:18,478 --> 00:06:22,798
go ahead and print that buffer

171
00:06:20,279 --> 00:06:25,798
ultimately the goal here is that this w

172
00:06:22,800 --> 00:06:28,139
dot JS file should to mutate the buffer

173
00:06:25,800 --> 00:06:31,319
directly without needing to copy it

174
00:06:28,139 --> 00:06:33,659
around so here with w dot JS we have the

175
00:06:31,319 --> 00:06:35,698
buffer and we grab the shared buffer

176
00:06:33,660 --> 00:06:37,439
from the worker data that was passed in

177
00:06:35,699 --> 00:06:39,659
from the parent and then now we're going

178
00:06:37,439 --> 00:06:41,218
to mutate that buffer and we're not

179
00:06:39,660 --> 00:06:42,418
going to pass the data back to the

180
00:06:41,220 --> 00:06:44,460
parent we're just going to tell the

181
00:06:42,418 --> 00:06:46,799
parent that we're done if this works

182
00:06:44,459 --> 00:06:48,598
this is the ultimate evidence that we're

183
00:06:46,800 --> 00:06:50,639
not actually copying data around but

184
00:06:48,600 --> 00:06:52,859
we're able to mutate it between two

185
00:06:50,639 --> 00:06:55,139
different threads so the goal is that by

186
00:06:52,860 --> 00:06:58,800
the time we get down here the buffer

187
00:06:55,139 --> 00:07:01,919
should be 7777 so it should have started

188
00:06:58,800 --> 00:07:05,639
off as 55555 we passed the shared buffer

189
00:07:01,918 --> 00:07:08,339
to the worker then afterwards is 7777 so

190
00:07:05,639 --> 00:07:12,060
let's see if this works so do it and

191
00:07:08,339 --> 00:07:14,818
boom buffer before modify 5555 buffer

192
00:07:12,060 --> 00:07:17,699
after modify 7777 so this is

193
00:07:14,819 --> 00:07:20,280
undisputable proof that we can indeed

194
00:07:17,699 --> 00:07:22,979
share a buffer of memory between threads

195
00:07:20,279 --> 00:07:25,738
without needing to copy that buffer

196
00:07:22,978 --> 00:07:27,659
around and this is a really great thing

197
00:07:25,740 --> 00:07:29,519
when it comes to Performance let's say

198
00:07:27,660 --> 00:07:32,698
you want to spin off some worker threads

199
00:07:29,519 --> 00:07:35,038
to resize images create thumbnails or

200
00:07:32,699 --> 00:07:36,659
convert PDF files or there's a lot of

201
00:07:35,038 --> 00:07:38,998
things you might want to do and

202
00:07:36,660 --> 00:07:41,160
fortunately we can do those in a very

203
00:07:39,000 --> 00:07:43,080
efficient way so there you go ladies and

204
00:07:41,160 --> 00:07:45,179
gentlemen kind of crazy right JavaScript

205
00:07:43,079 --> 00:07:47,399
is single threaded but with worker

206
00:07:45,180 --> 00:07:49,019
threads we can achieve multi-threading

207
00:07:47,399 --> 00:07:51,418
and we can do it in a relatively

208
00:07:49,019 --> 00:07:53,879
efficient way yes we've always had web

209
00:07:51,418 --> 00:07:55,799
workers on the front end but to be able

210
00:07:53,879 --> 00:07:58,438
to do this stuff on the back end is kind

211
00:07:55,800 --> 00:08:00,059
of a new thing and I think it is kind of

212
00:07:58,439 --> 00:08:01,559
a game changer I don't think it's

213
00:08:00,060 --> 00:08:04,079
necessarily going to replace other

214
00:08:01,560 --> 00:08:05,758
languages like go and rust and whatnot

215
00:08:04,079 --> 00:08:07,679
but nonetheless for folks who are

216
00:08:05,759 --> 00:08:09,900
already using node.js in their stack

217
00:08:07,680 --> 00:08:12,418
this is just another tool to help

218
00:08:09,899 --> 00:08:14,278
achieve higher performance so hey if you

219
00:08:12,418 --> 00:08:15,958
like the video please like And subscribe

220
00:08:14,279 --> 00:08:19,158
support a little YouTuber like myself

221
00:08:15,959 --> 00:08:19,159
and thank you for watching

