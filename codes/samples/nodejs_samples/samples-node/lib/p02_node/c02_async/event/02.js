let events = require('events');
let emitter = new events.EventEmitter();
emitter.on('someEvent', function(param1, param2) {
    console.log('listener1', param1, param2);
});
emitter.on('someEvent', function(param1, param2) {
    console.log('listener2', param1, param2);
});
emitter.emit('someEvent', 'arg1参数', 'arg2参数');