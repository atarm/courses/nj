const EventEmitter = require('node:events');
const myEmitter = new EventEmitter();
myEmitter.on('error', (err) => {
    console.error('whoops! there was an error');
    console.error('process will abort...');
    process.abort();
});
myEmitter.emit('error', new Error('whoops!'));
// Prints: whoops! there was an error
// Prints: process will abort...
// prints: core file will be generated, signal 6: SIGABRT