let spawn = require("child_process").spawn;
let pwd = spawn("pwd");

console.log(spawn);

pwd.stdout.on("data", function (data) {
    console.log("stdout: " + data);
});

pwd.stderr.on("data", function (data) {
    console.error("stderr: " + data);
});

pwd.on("close", function (code) {
    console.log("child process exited with code " + code);
});

// `-g` is illegal option, so it will throw an error
// pwd: illegal option -- g
// usage: pwd [-L | -P]
let pwd2 = spawn("pwd", ["-g"]);

pwd2.stderr.on("data", function (data) {
    console.error("pwd2 stderr: " + data);
});
