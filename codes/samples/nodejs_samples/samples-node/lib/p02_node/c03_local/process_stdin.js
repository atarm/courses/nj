// setEncoding will convert the buffer to string
process.stdin.setEncoding("utf8");

process.stdin.on("readable", () => {
    const chunk = process.stdin.read();
    console.log("chunk type: " + typeof(chunk));
    if (chunk !== null) {
        process.stdout.write(`your input: ${chunk}`);

        if (chunk.trim() === "exit") {
            process.exit(0);
        }
    }
});
