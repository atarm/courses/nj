//导入http模块
const http = require("http");
//调用createServer()创建http.Server服务器对象
const server = http.createServer();
//绑定request事件，一旦客户端请求则触发该事件，并接收request和response对象
server.on("request", function (req, res) {
    console.log(req);
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.write("hello");
    res.end();
});
//调用listen()函数启动服务器监听
server.listen(3000, function () {
    console.log("server listening on port 3000......");
});