const http = require('http');
const fs = require('fs');
const path = require('path');

// Ensure the directory exists
const dir = './client_requests';
if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}

const server = http.createServer((req, res) => {
    const timestamp = Date.now();
    const requestTime = new Date().toISOString();
    const filePath = path.join(dir, `${timestamp}.txt`);

    const requestData = `Request Time: ${requestTime}\nMethod: ${req.method}\nURL: ${req.url}\nHeaders: ${JSON.stringify(req.headers, null, 2)}`;

    // Write request data to file
    fs.writeFile(filePath, requestData, (err) => {
        if (err) {
            res.writeHead(500, {'Content-Type': 'text/plain'});
            res.end('Internal Server Error');
            return;
        }

        // Read the file content and send it back to the client
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                res.writeHead(500, {'Content-Type': 'text/plain'});
                res.end('Internal Server Error');
                return;
            }

            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.end(data);
        });
    });
});

server.listen(3000, () => {
    console.log('Server listening on port 3000...');
});