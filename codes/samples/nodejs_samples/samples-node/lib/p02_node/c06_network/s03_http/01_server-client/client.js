// 客户端代码
const http = require("http");

const options = {
    hostname: "localhost",
    port: 3000,
    path: "/",
    method: "GET",
};

const req = http.request(options, (res) => {
    let data = "";

    // `res` 是 `http.IncomingMessage` 对象
    console.log("res is instance of http.IncomingMessage: " + (res instanceof http.IncomingMessage));

    res.on("data", (chunk) => {
        data += chunk;
    });

    res.on("end", () => {
        console.log("Response:", data);
    });
});

req.on("error", (e) => {
    console.error(`Problem with request: ${e.message}`);
});

// `req` 是 `http.ClientRequest` 对象
console.log("req is instance of http.ClientRequest: " + (req instanceof http.ClientRequest));

req.end();