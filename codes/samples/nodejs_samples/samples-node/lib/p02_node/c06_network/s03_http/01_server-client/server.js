// 服务器端代码
const http = require("http");

const server = http.createServer((req, res) => {

    console.log("req is instance of http.IncomingMessage: " + (req instanceof http.IncomingMessage));
    console.log("res is instance of http.ServerResponse: " + (res instanceof http.ServerResponse))

    console.log("Received request:", req.method, req.url);

    // 设置响应头部
    res.setHeader("Content-Type", "text/plain");

    // 设置状态码
    res.statusCode = 200;

    // 发送响应体
    res.end("Hello, World!\n");
    // `res` 是 `http.ServerResponse` 对象
});

server.listen(3000, () => {
    console.log("Server running at http://localhost:3000/");
});
