const http = require("http");

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    res.end("Hello, World!\n");
});

// 设置属性
server.maxHeadersCount = 2000;
server.timeout = 5000;
server.keepAliveTimeout = 5000;
server.headersTimeout = 5000;

// 监听事件
server.on("request", (req, res) => {
    console.log("Request received");
});

server.on("connection", (socket) => {
    console.log("New connection established");
});

server.on("close", () => {
    console.log("Server closed");
});

// 启动服务器
server.listen(3000, "localhost", () => {
    console.log("Server running at http://localhost:3000/");
});

// 获取活动连接数
server.getConnections((err, count) => {
    if (err) {
        console.error("Error getting connections:", err);
    } else {
        console.log("Active connections:", count);
    }
});
