/* *
* 统计在线人数服务器
 */
//1.加载net模块
let net = require("net");

//2.创建Net.Server对象
let server = net.createServer();
let count = 0;   //保存在线人数

//3.绑定connection事件，监听客户的连接
server.on("connection", function (socket) {
    //获取客户端端口识别不同客户端
    console.log("客户端" + socket.remotePort + "连接成功了。")
    //当前在线人数+1
    count++;
    console.log("当前在线人数：" + count);
    //利用当前客户端的socket向客户端发送消息
    socket.write("当前在线人数：" + count);

    socket.on("error", function () {
        console.log("客户端" + socket.remotePort + "退出了");
    });
});

//4.调用listen()启动监听
server.listen(3000, "127.0.0.1", function () {
    console.log("服务器在端口3000启动了。")
});

