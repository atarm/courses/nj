/* *
* 统计在线人数客户端
 */
//1.加载net模块
let net = require("net");
//2.调用createConnection()创建socket对象，并与服务器建立连接
let client=net.createConnection({
    host:"127.0.0.1",
    port:3000
});
//3.绑定connect事件，连接建立成功触发
client.on("connect",function () {
    console.log("与服务器连接成功。")
});
//4.绑定data事件，接收服务器发送过来的数据
client.on("data",function (data) {
   console.log(data.toString());
});

