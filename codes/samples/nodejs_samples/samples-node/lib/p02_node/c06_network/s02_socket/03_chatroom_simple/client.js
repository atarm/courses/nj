//1.导入net
let net = require("net");
//2.建立连接,产生socket对象
let socket = net.createConnection({
    host: "127.0.0.1", port: 3000
});
//3.输入消息
socket.on("connect", function () {
    console.log("进入聊天室......");
    //准备输入聊天消息
    //Node.js中提供了process模块，支持对终端设备进行IO操作，全局模块，不需要导入
    //stdin对象获取键盘输入
    //接收到键盘输入，就将消息发送到服务器
    process.stdin.on("data", function (data) {
        socket.write(data.toString().trim());
    });
});
//4.接收消息
socket.on("data", function (data) {
    console.log(data.toString().trim());
});
socket.on("error", function (err) {
    console.log("服务器异常退出。");
});
