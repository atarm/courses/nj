//1.导入net模块
let net = require("net");

//2.创建Server对象
let server = net.createServer();
let clients = [];  //保存所有客户的socket对象

//3.绑定connection事件
server.on("connection", function (socket) {

    //a.告诉所有其他客户，有新成员进来了
    clients.forEach(function (client) {
        client.write(socket.remotePort + "进入聊天室.");
    });
    //b.保存当前socket
    clients.push(socket);

    //c.设置当前socket的消息传输的处理方式
    socket.on("data", function (data) {
        //一旦收到消息，就转发给其他所有的客户
        data = data.toString().trim();
        clients.forEach(function (user) {
            if (user !== socket) {//不是自己
                user.write(socket.remotePort + ":" + data);
            }
        });
    });
    socket.on("error", function (err) {
        console.log(socket.remotePort + "退出聊天室");
    });
});
//4.启动监听
server.listen(3000, "127.0.0.1", function () {
    console.log("服务器已在端口3000启动监听。。。。。。");
});
