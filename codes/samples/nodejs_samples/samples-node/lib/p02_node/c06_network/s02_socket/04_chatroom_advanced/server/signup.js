/* *
* 用户注册
* @param socket
* @param data 用户名
* {mstype:"signup",
*   username: "小高"
* @param users  用户组
 */
exports.signup = function (socket, data, users) {
    //获得注册用户的用户名
    let username = data.username;
    if (!users[username]) {//不存在，则保存用户名和socket
        users[username] = socket;
        let send = {
            mstype: "signup", code: 1000, username: username, message: "注册成功"
        };
        socket.write(JSON.stringify(send));
    } else {//存在
        let send = {
            mstype: "signup", code: 1001, message: "用户名已被占用，请重新输入用户名："
        }
        socket.write(JSON.stringify(send));
    }
};
