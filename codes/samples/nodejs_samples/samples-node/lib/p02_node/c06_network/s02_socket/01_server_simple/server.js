//1. 加载net模块
let net=require("net");
//2. 创建Net.Server对象
let server=net.createServer();
//3. 绑定connection事件，监听客户的连接
server.on("connection",function (socket) {
    console.log("有客户端连接成功了。。。")
});
//4. 调用listen()启动监听
server.listen(3000,"127.0.0.1",function () {
    console.log("服务器在端口3000启动了。。。")
});
server.on("error",function (err) {
    console.log(err);
})
server.on("close",function () {
    console.log("服务器关闭了。。。")
});

/*
node server.js

telnet 127.0.0.1 3000
 */