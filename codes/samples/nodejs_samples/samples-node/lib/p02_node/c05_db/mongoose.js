const mongoose = require("mongoose");

// 连接到 MongoDB
mongoose.connect("mongodb://localhost:27017/myDatabase", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log("Connected to MongoDB");
}).catch(err => {
    console.error("Failed to connect to MongoDB", err);
});

// 定义一个模式
const userSchema = new mongoose.Schema({
    name: String,
    age: Number,
});

// 创建一个模型
const User = mongoose.model("User", userSchema);

async function run() {
    try {
        // 插入文档
        const user = new User({ name: "Alice", age: 25 });
        await user.save();
        console.log("User saved:", user);

        // 查询文档
        const foundUser = await User.findOne({ name: "Alice" });
        console.log("User found:", foundUser);
    } finally {
        // 关闭连接
        await mongoose.connection.close();
    }
}

run().catch(console.dir);
