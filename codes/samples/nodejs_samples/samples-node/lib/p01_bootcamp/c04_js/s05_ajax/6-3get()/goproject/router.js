//路由
const fs=require('fs');
const path=require('path');
var config=require('./config');
var urlLib=require('url');
var mysql=require('mysql');

var sql;//查询字符串
var params;//参数
var pool=mysql.createPool(config.constr);//数据池连接对象

module.exports=function (req,res) {
    //将请求变成对象
    var obj=urlLib.parse(req.url,true);
    var url=obj.pathname;//取路由
    var GET=obj.query;//请求的数据
    var method=req.method.toLowerCase();//请求方式
    var filename;
    // console.log(url,GET);
    //加载主页
    if(url==='/'){
        url='newAddress.html';
    }
    //加载html
    if(url.endsWith('.html')){
        filename=path.join(__dirname,'views',url);
        //支持中文 ，用utf-8
        fs.readFile(filename,'utf-8',function (err,data) {
            if(err){
                return res.end(err.message);
            }else{
                res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'});
                res.end(data);
            }
        })
    }
    //加载静态资源
    if((url.startsWith('/public/')&&method=='get')||(url.startsWith('/node_modules/')&&method=='get')){
        filename=path.join(__dirname,url);
        fs.readFile(filename,function (err,data) {
            if(err){
                return res.end(err.message);
            }else{
                res.end(data);
            }
        })
    }
    //处理数据请求
    //添加新收货地址
    if(url==='/insertAddr'&&method=='get'){
       sql='insert into go_address (user_id,receiver_tel,receiver,address)value(?,?,?,?)'
        params=[GET.userId,GET.receiverTel,GET.receiver,GET.address];//从ajax里取的数据
        pool.getConnection(function (err,connect) {
            connect.query(sql,params,function (err,results) {
                if(results.affectedRows>0){
                    var json={ok:true,msg:'添加成功',userId:GET.userId};
                    res.end(JSON.stringify(json));
                }
            })
            connect.release();
        })
    }
    //刷新收货地址的请求
    if(url==='/refreshAddress'&&method=='get'){
        sql='select * from go_address where user_id=?';
        params=[GET.userId];
        console.log(params)
        pool.getConnection(function (err,connect) {
            connect.query(sql,params,function (err,results) {
                if(results.length!=0){
                    console.log(results);
                    res.end(JSON.stringify(results));
                }
            })
            connect.release();
        })
    }
}