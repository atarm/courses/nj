/*
*连接数据库
 */
var mysql=require('mysql')

//1.创建连接
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '123456',
    database : 'goproject'
})
//2.连接数据库
connection.connect()
//将收货地址信息存储到数据表中
module.exports.doInsertAddress=function (userId, receiverTel, receiver, address, callback) {
    var sql='insert into go_address (user_id,receiver_tel,receiver,address)value(?,?,?,?)'
    connection.query(sql,[userId,receiverTel,receiver,address],function(err,results){
        if(err)
        {
            callback(err,null)
        }
        else
        {
            callback(err,results)
        }
    })
}

module.exports.getAddressList = function (userId,callback) {
    var sql='select * from go_address where user_id=?'
    connection.query(sql,[userId],function(err,results){
        // console.log(results[0].address)
        console.log(results)
        if(err)
        {
            callback(err,null)
        }
        else
        {
            callback(err,results)
        }
    })
}