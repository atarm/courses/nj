$(function () {
    'use strict';
    //创建distpicker对象
    let $distpicker = $('#distpicker');
    //distpicker初始化
    $distpicker.distpicker({
        province: '湖南省', city: '长沙市', district: '天心区'
    });
    //还原默认值
    $('#reset').click(function () {
        $distpicker.distpicker('reset');
    });
    //无默认值状态
    $('#reset-deep').click(function () {
        $distpicker.distpicker('reset', true);
    });
    //保存地址
    $('#save_addr').click(function () {
        //拼接地址
        let address = [$('#province').val(), $('#city').val(), $('#district').val(), $('#newAddress').val()];
        //构造ajax数据,假设用户号为1，在后面的实例中用户号由服务器转递
        $.ajax({
            url: '/insertAddr', type: 'GET', data: {
                userId: 1, receiver: $('#receiver').val(), receiverTel: $('#receiverTel').val(), address: address.toString()
            }, success: function (str) {//Ajax数据请求成功，收到响应
                //识别响应
                let json = eval("(" + str + ")");
                if (json.ok) {
                    //添加成功
                    alert('添加成功');
                    $('#popup_win').css('display', 'none');
                    $('#add_Address').css('display', 'none');
                    //此处添加刷新地址列表的请求与响应代码
                }
            }, error: function (err) {//Ajax请求不成功
                console.log(err);
            }
        });
    });
});