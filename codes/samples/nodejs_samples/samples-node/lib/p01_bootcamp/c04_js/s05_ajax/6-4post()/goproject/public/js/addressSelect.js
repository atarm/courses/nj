$(function () {
    'use strict';
    //创建distpicker对象
    var $distpicker = $('#distpicker');
    //distpicker初始化
    $distpicker.distpicker({
        province: '湖南省',
        city: '长沙市',
        district: '天心区'
    });
    //还原默认值
    $('#reset').click(function () {
        $distpicker.distpicker('reset');
    });
    //无默认值状态
    $('#reset-deep').click(function () {
        $distpicker.distpicker('reset', true);
    });

    //保存地址
    $('#save_addr').click(function () {
        //拼接地址
        var address=[$('#province').val(),$('#city').val(),$('#district').val(),$('#newAddress').val()];
        //构造ajax数据
        $.ajax({
            url:'/insertAddr',
            type:'GET',
            data:{
                userId:1,
                receiver:$('#receiver').val(),
                receiverTel:$('#receiverTel').val(),
                address:address.toString()
            },
            success:function (str) {//Ajax数据请求成功，收到响应
                //识别响应
                var json=eval("("+str+")");
                if(json.ok){
                    //添加成功
                    alert('添加成功');
                    $('#popup_win').css('display','none');
                    $('#add_Address').css('display','none');
                    // window.location.href='/userCenter';
                    $.get({
                        url:'/refreshAddress',
                        data:{userId:json.userId},
                        success:function(result) {
                            //接收到来自服务器的数据msg并转化
                            var data = eval("(" + result + ")");
                            console.log(data)//控制台进行数据查看
                            var htmlStr = '';
                            for (var i = 0; i < data.length; i++) {
                                htmlStr += '<label><input type="checkbox" value="' + data[i].address_id + '" id="' + data[i].address_id + '" class="checkbox_anniu">';
                                htmlStr += data[i].receiver + data[i].receiver_tel + data[i].address +'</label></p>';
                            }
                            $("#addresslist").append( htmlStr)
                        }
                    })
                }
            },
            error:function (err) {//Ajax请求不成功
                console.log(err);
            }
        });
    });
});


