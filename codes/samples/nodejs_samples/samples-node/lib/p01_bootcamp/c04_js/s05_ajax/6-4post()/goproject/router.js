//路由
const fs=require('fs');
const path=require('path');
var config=require('./config');
var _=require('underscore');
var urlLib=require('url');
var mysql=require('mysql');
var qs= require('querystring');//用于监听post数据读取

var sql;//查询字符串
var params;//参数
var pool=mysql.createPool(config.constr);//数据池连接对象

module.exports=function (req,res) {
    //将请求变成对象
    var obj=urlLib.parse(req.url,true);
    var url=obj.pathname;//取路由
    var method=req.method.toLowerCase();//请求方式
    var filename;//服务器上完整文件路径
    //按请求方式不同区分路由处理
    if(method==='get')
    {
        var GET=obj.query;//请求的数据
        //加载主页
        if(url==='/'){
            url='login.html';
        }
        //加载html
        if(url.endsWith('.html')){
            filename=path.join(__dirname,'views',url);
            //支持中文 ，用utf-8
            fs.readFile(filename,'utf-8',function (err,data) {
                if(err){
                    return res.end(err.message);
                }else{
                    res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'});
                    res.end(data);
                }
            })
        }


        if(url==='/userCenter'){
            sql='select * from user_info where user_id=?;select * from go_address where user_id=?';
            params=[query.userId,query.userId];
            pool.getConnection(function (err,connect) {
                connect.query(sql,params,function (err,results) {
                    console.log(results);
                    // res.end(JSON.stringify(results));
                    var list={userInfo:results};
                    filename=path.join(__dirname,'views',url,'.html');
                    //支持中文 ，用utf-8
                    fs.readFile(filename,'utf-8',function (err,datas) {
                        if(err){
                            return res.end(err.message);
                        }else{
                            res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'});
                            res.end(datas);
                        }
                        // //执行解析
                        // var compiled=_.template(datas);
                        // var htmlStr=compiled(list||{});
                        // res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'});
                        // res.end(htmlStr);
                    })
                })
                connect.release();
            })
        }

        //加载静态资源
        if(url.startsWith('/public/')||url.startsWith('/node_modules/')){
            filename=path.join(__dirname,url);
            fs.readFile(filename,function (err,data) {
                if(err){
                    return res.end(err.message);
                }else{
                    res.end(data);
                }
            })
        }
        //处理数据请求
        //添加新收货地址
        if(url==='/insertAddr'){
            sql='insert into go_address (user_id,receiver_tel,receiver,address)value(?,?,?,?)'
            params=[GET.userId,GET.receiverTel,GET.receiver,GET.address];//从ajax里取的数据
            pool.getConnection(function (err,connect) {
                connect.query(sql,params,function (err,results) {
                    if(results.affectedRows>0){
                        var json={ok:true,msg:'添加成功',userId:GET.userId};
                        res.end(JSON.stringify(json));
                    }
                })
                connect.release();
            })
        }
        //get方式加载个人中心的请求
        //添加收货地址后刷新收货地址列表
        if(url==='/refreshAddress'){
            sql='select * from go_address where user_id=?';
            params=[GET.userId];
            pool.getConnection(function (err,connect) {
                connect.query(sql,params,function (err,results) {
                    if(results.length!=0){
                        console.log(results);
                        //响应数据到客户端，在客户端Ajax局部刷新
                        res.end(JSON.stringify(results));
                    }
                })
                connect.release();
            })
        }
    }else if(method==='post'){
        var postData = "";
        /**
         * 因为post方式的数据不太一样可能很庞大复杂，
         * 所以要添加监听来获取传递的数据
         * 也可写作 req.on("data",function(data){});
         */
        req.addListener("data", function (data) {
            postData += data;
        });
        /**
         * 这个是如果数据读取完毕就会执行的监听方法
         */
        req.addListener("end", function () {
            var query = qs.parse(postData);
            console.log(query);
            //post方式加载个人中心的请求
            if(url==='/loginAjax'){
                sql='select * from user_info where phone=? and password=?';
                params=[query.username,query.password];
                pool.getConnection(function (err,connect) {
                    connect.query(sql,params,function (err,result) {
                        if (err){
                            return res.end(JSON.stringify(err.message))
                        }
                        console.log(result);
                        if (result.length==0){
                            json={ok:false,msg:"用户名或密码不正确,请重新输入！"};
                            res.end(JSON.stringify(json))
                        }else {
                            json={ok:true,msg:"登录成功",useId:result[0].user_id};
                            res.end(JSON.stringify(json))
                        }
                    })
                    connect.release();
                })
            }
        });

    }else
    {
        console.log("出错了！");
    }
}

