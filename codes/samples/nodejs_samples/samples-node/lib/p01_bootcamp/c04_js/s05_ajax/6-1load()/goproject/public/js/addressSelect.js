﻿$(function () {
    'use strict';
    //创建distpicker对象
    var $distpicker = $('#distpicker');
    //distpicker初始化
    $distpicker.distpicker({
        province: '湖南省',
        city: '长沙市',
        district: '天心区'
    });
    //还原默认值
    $('#reset').click(function () {
        $distpicker.distpicker('reset');
    });
    //无默认值状态
    $('#reset-deep').click(function () {
        $distpicker.distpicker('reset', true);
    });
});