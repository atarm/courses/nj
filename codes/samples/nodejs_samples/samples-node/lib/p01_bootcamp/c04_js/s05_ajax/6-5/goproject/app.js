//后端服务器
const  http=require('http');
var config=require('./config');
var router=require('./router');


//创建服务
var server=http.createServer();
//收到请请求并响应
server.on('request',function (req,res) {
    //调用路由
    router(req,res);
})
//启动监听
server.listen(config.port,function (err) {
    if(err){
        console.log('启动失败');
    }else{
        console.log('服务器已经启动'+config.port);
    }
})


