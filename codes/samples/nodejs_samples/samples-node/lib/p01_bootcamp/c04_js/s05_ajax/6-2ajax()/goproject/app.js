/*
* 服务器
*/
var http = require('http');//http模块
var urlLib = require('url');//路由模块
var path = require('path');//路径模块
var fs = require('fs');//文件模块
var model = require('./model');
var server = http.createServer();//创建http服务器
var _ = require('underscore')

//接收请求req,并响应res
server.on('request', function (req, res) {
    //将请求变成对象
    var obj = urlLib.parse(req.url, true);
    var pathname = obj.pathname;//取路由
    var GET = obj.query;//请求
    var method = req.method.toLowerCase();
    //展示首页
    if (pathname == '/' && method == 'get') {
        fs.readFile(path.join(__dirname, 'views/newAddress.html'), function (err, data) {
            if (err) {
                return res.end(err.message);
            }

            model.getAddressList(1, function (err, results2) {
                if (err) {
                    return res.end(err.message);
                }
                console.log(results2)
                var compiled = _.template(data.toString())
                var htmlStr = compiled({
                    address: results2
                })

                res.end(htmlStr);
            })
        })
        //公开public和node_modules中的静态资源
    } else if ((pathname.startsWith('/public/') && method == 'get') || (pathname.startsWith('/node_modules/') && method == 'get')) {
        fs.readFile(path.join(__dirname, pathname), function (err, data) {
            if (err) {
                return res.end(err.message);
            }
            res.end(data);
        })
    } else if (pathname === '/distpicker.html' && method === 'get') {
        fs.readFile(path.join(__dirname, 'views/distpicker.html'), function (err, data) {
            if (err) {
                return res.end(err.message);
            }
            res.end(data);
        })
    } else if (pathname === '/insertAddr' && method === 'get') {
        model.doInsertAddress(GET.userId, GET.receiverTel, GET.receiver, GET.address, function (err, results2) {
            if (err) {
                return res.end(err.message);
            }
            let json = {ok: true, msg: '收货地址添加成功', userID: GET.userId};
            res.end(JSON.stringify(json));
        });
    }
})
//启动监听
server.listen(3000, '127.0.0.1', function () {
    console.log('server is running at port 3000')
});

