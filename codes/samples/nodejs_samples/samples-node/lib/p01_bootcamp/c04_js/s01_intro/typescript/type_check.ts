function add(a: number, b: number): number {
    return a + b;
}

// 编译时报错
// 如果忽略编译错误，运行时不报错
const result = add(10, "20");
console.log(result);

/*
npm install -g ts-node
ts-node --transpile-only type_check.ts
*/

/*
tsc
*/