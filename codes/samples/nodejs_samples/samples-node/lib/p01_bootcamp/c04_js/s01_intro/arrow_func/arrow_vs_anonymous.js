let obj = {
  name: 'Alice',
  sayHello: function() {
    setTimeout(() => {
      console.log(`Hello, ${this.name}!`);
    }, 1000);
  }
};

obj.sayHello(); // 输出：Hello, Alice!

obj = {
  name: 'Alice',
  sayHello: function() {
    setTimeout(function() {
      console.log(`Hello, ${this.name}!`);
    }, 1000);
  }
};

obj.sayHello(); // 输出：Hello, undefined!

obj = {
  name: 'Alice',
  sayHello: function() {
    const self = this;
    setTimeout(function() {
      console.log(`Hello, ${self.name}!`);
    }, 1000);
  }
};

obj.sayHello(); // 输出：Hello, Alice!

obj = {
  name: 'Alice',
  sayHello: function() {
    setTimeout(() => {
      console.log(`Hello, ${name}!`);   // ReferenceError: name is not defined
    }, 1000);
  }
};

obj.sayHello();