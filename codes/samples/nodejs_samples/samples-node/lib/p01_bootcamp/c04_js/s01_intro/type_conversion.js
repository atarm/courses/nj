let intString = "42";
let floatString = "3.14";

let intValue = parseInt(intString);   // 42
let floatValue = parseFloat(floatString); // 3.14

console.log(intValue);   // 输出: 42
console.log(floatValue); // 输出: 3.14

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});
rl.question(`请输入您的年龄：`, age => {
    console.log(`您的年龄是 ${parseInt(age)}!`);
    rl.question(`请输入您的体重：`, weight => {
        console.log(`您的体重是 ${parseFloat(weight)}!`);
        rl.close();
    });
});