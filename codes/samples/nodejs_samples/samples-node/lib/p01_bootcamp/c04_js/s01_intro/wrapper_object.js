var v1 = new Number(123);
var v2 = new String('abc');
var v3 = new Boolean(true);

console.log(typeof v1)    // "object"
console.log(typeof v2)    // "object"
console.log(typeof v3)    // "object"

console.log(v1 == 123)   // true
console.log(v2 == 'abc') // true
console.log(v3 == true)  // true

console.log(v1 === 123)   // false
console.log(v2 === 'abc') // false
console.log(v3 === true)  // false