let http = require('http');
let port_number = 3000;


http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World\n');
    }).listen(port_number);

// stop the application by pressing Ctrl+C
console.log(`Server running at http://127.0.0.1:${port_number}`);