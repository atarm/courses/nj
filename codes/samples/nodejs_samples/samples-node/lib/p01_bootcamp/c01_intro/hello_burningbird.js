let http = require('http');
let fs = require('fs');
let port_number = 3000;
let image_folder = 'resources/images/';

http.createServer(function (req, res) {
    // require the url module in chain with parse method to get the query string
    let name  = require('url').parse(req.url, true).query.name;
    if (name === undefined) name = 'world';
    if (name === 'burningbird') {
        let file = image_folder + 'burningbird.jpg';
        fs.stat(file, function(err, stat) {
            if (err) {
                console.error(err);
                res.writeHead(200, {'Content-Type': 'text/plain'});
                res.end('Sorry, Burningbird is missing...');
            } else {
                // read image file synchronously
                let img = fs.readFileSync(file);
                res.contentType = 'image/jpg';
                res.contentLength = stat.size;
                res.end(img, 'binary');
            }
        });
    }else {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Hello ' + name + '\n');
    }
}).listen(port_number);

// stop the application by pressing Ctrl+C
console.log(`Server running at http://127.0.0.1:${port_number}`);