//获取显示小时的区域框对象
let hour = document.getElementById("h");
//获取显示分钟的区域框对象
let minute = document.getElementById("m");
//获取显示秒的区域框对象
let second = document.getElementById("s");

//获取当前时间
function getCurrentTime() {
    let date = new Date();
    let h = date.getHours();
    let m = date.getMinutes();
    let s = date.getSeconds();

    let hour = document.getElementById("h");
    let minute = document.getElementById("m");
    let second = document.getElementById("s");

    if (h < 10) h = "0" + h; //以确保0-9时也显示成两位数
    if (m < 10) m = "0" + m; //以确保0-9分钟也显示成两位数
    if (s < 10) s = "0" + s; //以确保0-9秒也显示成两位数

    hour.innerHTML = h;
    minute.innerHTML = m;
    second.innerHTML = s;
}

//每秒更新一次时间
setInterval("getCurrentTime()", 1000);