﻿/**
 * @fileoverview This file contains the database model for querying product and user information.
 * @module model
 */

const mysql = require("mysql2");
const config = require("./config/db_config");

let pool = mysql.createPool({
    connectionLimit: 100, // max connection
    multipleStatements: true, // multiple statements
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database,
});

/**
 * Finds product data by querying the database.
 *
 * Gets a connection from the pool, builds a SQL query to select product data
 * filtered by product type and with limits, executes the query, handles any errors,
 * and returns the results to the callback.
  */
module.exports.findProduct = function (callback) {
    pool.getConnection(function (err, conn) {
        if (err) {
            return callback("连接池连接失败！" + err, null);
        }
        let sql = `
            SELECT *
                FROM go_product
                WHERE p_type = 'ad-product-computer'
                LIMIT 4;
            SELECT *
                FROM go_product
                WHERE p_type = 'ad-product-phone'
                LIMIT 4;
            SELECT *
                FROM go_product
                WHERE p_type = 'ad-product-pad'
                LIMIT 4;
            SELECT *
                FROM go_product
                WHERE p_type = 'ad-product-ear'
                LIMIT 4;
            SELECT *
                FROM go_product;
        `;
        conn.query(sql, function (err, results) {
            conn.release();
            if (err) {
                return callback("查询失败！" + err, null);
            }
            callback(null, results);
        });
    });
};

/**
 * Finds user info by querying the database.
 *
 * Gets a connection from the pool, builds a SQL query to select all columns
 * from the user_info table where the phone number matches the given phone parameter.
 * Executes the query, handles any errors, and returns the results to the callback.
 *
 * @param {string} phone - The phone number to search for in the user_info table.
 * @param {function} callback - The callback function to handle the query results.
 * @returns {void}
 */
module.exports.findPhone = function (phone, callback) {
    pool.getConnection(function (err, conn) {
        if (err) {
            return callback("连接池连接失败！" + err, null);
        }
        let sql = "select * from user_info where phone=?";
        conn.query(sql, [phone], function (err, results) {
            conn.release();
            console.log("results ==> " + results);
            if (err) {
                return callback("查询失败！" + err, null);
            }
            callback(null, results);
        });
    });
};

/**
 * Registers a new user by inserting their information into the database.
 *
 * Gets a connection from the pool, builds a SQL query to insert the user's phone, nickname, and password
 * into the user_info table. Executes the query, handles any errors, and returns the results to the callback.
 *
 * @param {string} phone - The phone number of the new user.
 * @param {string} nickname - The nickname of the new user.
 * @param {string} password - The password of the new user.
 * @param {function} callback - The callback function to handle the query results.
 * @returns {void}
 */
module.exports.doReg = function (phone, nickname, password, callback) {
    pool.getConnection(function (err, conn) {
        if (err) {
            return callback("连接池连接失败！" + err, null);
        }
        let sql =
            "insert into user_info(user_id,phone,nickname,password) values (null,?,?,?)";
        conn.query(sql, [phone, nickname, password], function (err, results) {
            conn.release();
            if (err) {
                return callback("查询失败！" + err, null);
            }
            callback(null, results);
        });
    });
};
