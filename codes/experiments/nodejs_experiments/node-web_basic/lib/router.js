﻿const url = require("url");
const path = require("path");
const fs = require("fs");
const formidable = require("formidable");
const _ = require("underscore");
const model = require("./model");

module.exports = function (req, res) {
    let urlObj = url.parse(req.url, true);
    let pathname = urlObj.pathname;
    let method = req.method.toLowerCase();
    // 为req追加一个query属性，属性值urlObj.query
    req.query = urlObj.query;

    if (pathname === "/" && method === "get") {
        // index page
        fs.readFile(
            path.join(__dirname, "views/index.html"),
            function (err, data) {
                if (err) {
                    return res.end(err.message);
                }
                model.findProduct(function (err, results) {
                    let compiled = _.template(data.toString());
                    let htmlStr = compiled({
                        computerList: results[0],
                        phoneList: results[1],
                        padList: results[2],
                        earphoneList: results[3],
                        productList: results[4],
                    });
                    res.setHeader("Content-Type", "text/html;charset=utf-8");
                    res.end(htmlStr);
                });
            }
        );
    } else if (pathname.startsWith("/public/") && method === "get") {
        // static resources
        fs.readFile(path.join(__dirname, pathname), function (err, data) {
            if (err) {
                return res.end(err.message);
            }
            res.end(data);
        });
    } else if (pathname === "/login" && method === "get") {
        // login page
        fs.readFile(
            path.join(__dirname, "views/login.html"),
            function (err, data) {
                if (err) {
                    return res.end(err.message);
                }
                res.end(data);
            }
        );
    } else if (pathname === "/login" && method === "post") {
        // login action
        let form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            if (err) {
                return res.end(err.message);
            }
            let phone = fields.phone[0];
            let password = fields.password[0];
            model.findPhone(phone, function (err, results) {
                if (err) {
                    return res.end(err.message);
                }
                if (results.length === 0) {
                    res.writeHead(200, {
                        "Content-Type": "text/html;charset=utf8",
                    });
                    res.end(
                        '<script>alert("电话号码未注册,请重新输入！");window.location.href="/login"</script>'
                    );
                } else {
                    if (results[0].password !== password) {
                        res.writeHead(200, {
                            "Content-Type": "text/html;charset=utf8",
                        });
                        res.end(
                            '<script>alert("密码不正确,请重新输入！");window.location.href="/login"</script>'
                        );
                    } else {
                        res.writeHead(200, {
                            "Content-Type": "text/html;charset=utf8",
                        });
                        res.end(
                            '<script>alert("登录成功！");window.location.href="/"</script>'
                        );
                    }
                }
            });
        });
    } else if (pathname === "/register" && method === "get") {
        // register page
        fs.readFile(
            path.join(__dirname, "views/register.html"),
            function (err, data) {
                if (err) {
                    return res.end(err.message);
                }
                res.end(data);
            }
        );
    } else if (pathname === "/register" && method === "post") {
        // register action
        let form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            let phone = fields.phone[0];
            let nickname = fields.nickname[0];
            let password = fields.password[0];
            model.findPhone(phone, function (req, results) {
                if (err) {
                    res.writeHead(500, {
                        "Content-Type": "text/html;charset=utf8",
                    });
                    return res.end(err.message);
                }
                if (results.length === 0) {
                    model.doReg(
                        phone,
                        nickname,
                        password,
                        function (err, results2) {
                            if (err) {
                                res.writeHead(500, {
                                    "Content-Type": "text/html;charset=utf8",
                                });
                                return res.end(err.message);
                            }
                            res.writeHead(200, {
                                "Content-Type": "text/html;charset=utf8",
                            });
                            res.end(
                                '<script>alert("注册成功，前去登录！");window.location.href="/login"</script>'
                            );
                        }
                    );
                } else {
                    res.writeHead(200, {
                        "Content-Type": "text/html;charset=utf8",
                    });
                    res.end(
                        '<script>alert("此电话已经注册，请重新输入！");window.location.href="/register"</script>'
                    );
                }
            });
        });
    }
};
