const http = require('http');
const config = require('./lib/config/server-config.js');
const router = require('./lib/router.js');

let server = http.createServer();

server.on('request', function (req, res) {
    router(req, res);
});

server.listen(config.port, config.host, function () {
    console.log('server is running at  ' + config.host + ':' + config.port + '...');
});