﻿//1.加载mysql模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'goproject',
    timezone: '+08:00',
    multipleStatements: true
});

//3.连接mysql
connection.connect();

let query = 'SELECT * FROM `user_info`;';
query += 'SELECT nickname FROM `user_info` WHERE u_id=2;';

//4.多语句查询
connection.query(query, function (err, result) {
    if (err) {
        console.log(err.message);
    }
    console.log(result);
    console.log(result[0]);
    console.log(result[1]);
    //5.关闭连接
    connection.end();
});