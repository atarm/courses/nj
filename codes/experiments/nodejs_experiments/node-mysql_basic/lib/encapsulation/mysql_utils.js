let mysql = require('mysql2');
let mysql_config = require('./mysql.config');

//向外暴露方法query
//sql:sql语句
//values:查询占位符的值
//callback:回调函数
module.exports.query = function (sql, values, callback) {
    //创建连接对象
    let connection = mysql.createConnection(mysql_config);
    //建立连接
    connection.connect(function (err) {
        if (err) {
            console.log('数据库连接失败');
            throw err;
        }
        //执行数据库操作
        let query = connection.query(sql, values)
            .on('error', function (err) {
                console.log(err);
            }).on('fields', function (fields) {
                // console.log(fields);
            }).on('result', function (rows) {
                console.log(rows);
            }).on('end', function () {
                connection.end();
            });
    });
}