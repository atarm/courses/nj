//1.加载模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost', user: 'root', password: 'root', database: 'goproject',timezone: '+08:00'
});

//3.连接
connection.connect()

//4.执行操作--插入
let sql = 'insert into `user_info`(u_id,phone,nickname,password) values (null,?,?,?)'
let sqlValues = ['15655665566', '小龙', '123456']
connection.query(sql, sqlValues, function (err, results) {
    if (err) {
        return console.log(err.message);
    }
    console.log(results.insertId);
    console.log(results);
    //5.关闭连接
    connection.end();
})

