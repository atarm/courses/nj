//1.加载模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost', user: 'root', password: 'root', database: 'goproject', timezone: '+08:00'
});

//3.连接
connection.connect();

//4.执行操作--修改
let sql = 'update user_info set password=? where u_id=?';
let updateValue = ['a123456', 1];
connection.query(sql, updateValue, function (err, results) {
    if (err) {
        return console.log(err.message);
    }
    console.log(results);
    //5.关闭连接
    connection.end();
});
