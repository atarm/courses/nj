﻿//1.加载mysql模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'goproject',
    timezone: '+08:00'
});

//3.连接mysql
connection.connect(function (err) {
    if (err) {
        // 捕获异常堆栈信息
        return console.log('error connecting:' + err.stack);
    }
    // 提示连接成功
    console.log('连接成功');
    // id
    console.log(('connected as id ' + connection.threadId));

    //4.关闭连接
    connection.end();
});
