//1.加载模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost', user: 'root', password: 'root', database: 'goproject',timezone: '+08:00'
});

//3.连接
connection.connect();

//4.执行操作--查询
let sql = 'select * from `user_info`';
connection.query(sql, function (err, result, fields) {
    if (err) {
        return console.log('查询失败' + err.message);
    }
    console.log(result);
    console.log(fields);
    //5.关闭连接
    connection.end();
});


