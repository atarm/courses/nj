/* *
* 广播消息
* @param data JSON格式的广播消息
* {
*  mstype:"broadcast",
*  from: "小高",
*  message:"hello"
*  }
*  @param users  用户组
 */
exports.broadcast = function (data, users) {
    let from = data.from; //获得发消息方
    let message = data.message;   //获得消息
    message = from + "说：" + message;  //改变消息，在前面加上谁说的
    //构建消息
    let send = {
        mstype: "broadcast", message: message
    };
    // send=new Buffer(JSON.stringify(send));
    // Buffer.alloc(JSON.stringify(send));
    //遍历用户组所有用户，发送除发送方的所有用户
    for (let username in users) {
        if (username !== from) {
            users[username].write(JSON.stringify(send));
        }
    }
};