let net = require("net");
let config = require("./config");

let client = net.createConnection({
    port: config.port, host: config.host
});
let username;
client.on("connect", function () {
    console.log("请输入用户名：");
});

process.stdin.on("data", function (data) {
    data = data.toString().trim();
    //判断用户名是否已存在
    if (!username) {
        let send = {
            mstype: "signup", username: data
        };
        client.write(JSON.stringify(send));
        return;
    }
    let regex = /(.{1,18}):(.+)/;
    let matches = regex.exec(data);
    if (matches) { //能匹配则是P2P
        let from = username;  //发送方是自己
        let to = matches[1];  //发送给谁
        let message = matches[2];  //消息内容
        //构建JSON形式的信息
        let send = {
            mstype: "p2p", from: username, to: to, message: message
        };
        client.write(JSON.stringify(send));
    } else {//广播消息
        let send = {
            mstype: "broadcast", from: username, message: data
        };
        client.write(JSON.stringify(send));
    }
});

client.on("data", function (data) {
    data = JSON.parse(data);
    switch (data.mstype) {
        case "signup":
            // let code = data.code;
            switch (data.code) {
                case 1000:
                    username = data.username;
                    console.log(data.message);
                    break;
                case 1001:
                    console.log(data.message);
                    break;
                default:
                    break;
            }
            break;
        case "broadcast":
            console.log(data.message);
            break;
        case "p2p":
            // let code = data.code;
            switch (data.code) {
                case 2000:
                    console.log(data.message);
                    break;
                case 2001:
                    console.log(data.message);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
});

client.on("error", function () {
    console.log("聊天室已关闭！");
})