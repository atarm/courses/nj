let net = require("net");
let config = require("./config");
let broadcast = require("./broadcast");
let p2p = require("./p2p");
let signup = require("./signup");
let users = {};
let server = net.createServer();

server.on("connection", function (socket) {
    socket.on("data", function (data) {
        data = JSON.parse(data);
        switch (data.mstype) {
            case "signup":
                signup.signup(socket, data, users);
                break;
            case "broadcast":
                broadcast.broadcast(data, users);
                break;
            case "p2p":
                p2p.p2p(socket, data, users);
                break;
            default:
                break;
        }
    });
    socket.on("error", function () {
        console.log("有客户端异常退出了...");
    });
});

server.listen(config.port, config.host, function () {
    console.log("服务器在端口" + config.port + "启动了监听...");
});
