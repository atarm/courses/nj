/* *
* P2P消息
* @param socket  发送方socket
* @param data
* {
*  mstype: "p2p",
*  from: "小高",
*  to: "小张",
*  message:"hello"
*  }
*  @param users  用户组
 */
exports.p2p = function (socket, data, users) {
    let from = data.from;
    let to = data.to;
    let message = data.message;
    let receiver = users[to];
    if (!receiver) {//接收方不存在
        let send = {
            mstype: "p2p", code: 2001, message: "用户" + to + "不存在"
        }
        socket.write(JSON.stringify(send));
    } else {//存在，则向接收方发送信息
        let send = {
            mstype: "p2p", code: 2000, from: from, message: from + "对你说：" + message
        }
        receiver.write(JSON.stringify(send));
    }
};
