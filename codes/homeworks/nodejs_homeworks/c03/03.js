function multi(x, y, callback) {
    console.log('a');
    setTimeout(function () {
        console.log('b');
        callback(x * y);
    }, 0);
}

multi(3, 6, function (result) {
    console.log(result);
});