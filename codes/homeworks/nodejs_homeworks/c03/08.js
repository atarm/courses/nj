let mysql = require('mysql2');

let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'goproject'
});

connection.connect();
connection.query('select 1+1 as result', function (err, results, fields) {
    if(err){
        return console.log(err.message);
    }
    console.log(results[0].result);
    connection.end();
});