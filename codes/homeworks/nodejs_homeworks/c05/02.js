const http = require('http');
const server = http.createServer();

server.on('request', (req, res) => {
    console.log(req.url);
    res.end();
});

server.listen(3000, '127.0.0.1', () => {
});