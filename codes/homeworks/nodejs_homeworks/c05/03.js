const http = require('http');
const url = require('url');
const server = http.createServer();

server.on('request', (req, res) => {
    console.log(url.parse(req.url).pathname);
    res.end();
});

server.listen(3000, () => {
});