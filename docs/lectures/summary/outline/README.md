# Node.js Review

- [1. ECMAScript基础](#1-ecmascript基础)
    - [1.1. 变量与数据类型](#11-变量与数据类型)
        - [1.1.1. 变量声明](#111-变量声明)
        - [1.1.2. 数据类型](#112-数据类型)
        - [1.1.3. 类型检测](#113-类型检测)
    - [1.2. 运算符](#12-运算符)
    - [1.3. 控制流程](#13-控制流程)
    - [1.4. 函数](#14-函数)
        - [1.4.1. 函数定义与调用](#141-函数定义与调用)
        - [1.4.2. 参数传递](#142-参数传递)
    - [1.5. 定时器](#15-定时器)
    - [1.6. 标准库](#16-标准库)
        - [1.6.1. Array](#161-array)
        - [1.6.2. String](#162-string)
        - [1.6.3. Object](#163-object)
        - [1.6.4. Math](#164-math)
    - [1.7. 异步编程模型](#17-异步编程模型)
        - [1.7.1. Callback](#171-callback)
        - [1.7.2. Promise](#172-promise)
        - [1.7.3. async/await](#173-asyncawait)
        - [1.7.4. 事件循环(Event Loop)](#174-事件循环event-loop)
    - [1.8. jQuery](#18-jquery)
- [2. Web API](#2-web-api)
    - [2.1. DOM](#21-dom)
    - [2.2. BOM操作](#22-bom操作)
        - [2.2.1. window对象](#221-window对象)
        - [2.2.2. location对象](#222-location对象)
- [3. Node.js模块化](#3-nodejs模块化)
    - [3.1. 模块系统](#31-模块系统)
    - [3.2. npm包管理](#32-npm包管理)
- [4. Node.js异步编程](#4-nodejs异步编程)
    - [4.1. EventEmitter](#41-eventemitter)
    - [4.2. Asynchronous Queues](#42-asynchronous-queues)
- [5. Node.js重要模块](#5-nodejs重要模块)
    - [5.1. fs](#51-fs)
    - [5.2. net](#52-net)
    - [5.3. http](#53-http)
    - [5.4. url and querystring](#54-url-and-querystring)
- [6. MySQL数据库操作](#6-mysql数据库操作)
- [7. Express.js](#7-expressjs)
    - [7.1. 基础应用](#71-基础应用)
    - [7.2. RESTful API](#72-restful-api)
- [8. JSON Web Token](#8-json-web-token)
- [9. Comprehensive Application](#9-comprehensive-application)

## 1. ECMAScript基础

### 1.1. 变量与数据类型

#### 1.1.1. 变量声明

1. `var`、`let`、`const`
   - `var`：函数作用域，存在变量提升，可重复声明
   - `let`：块级作用域，不存在变量提升，存在暂时性死区(TDZ)，不可重复声明
   - `const`：与`let`类似，常量（声明后不能重新赋值，对象的属性仍可修改）
2. 变量命名规则
    - 以`字母`、`下划线_`或`美元符号$`开头
    - 不能使用`保留字reserved words`
    - 区分大小写
3. 变量提升(Hoisting)
   - 声明会被提升到作用域顶部
   - `var`声明的变量会被初始化为`undefined`
   - `let/const`声明的变量不会被初始化（暂时性死区）
4. 暂时性死区(TDZ, Temporal Dead Zone)：解决`var`变量提升引起的问题，达到与大多数编程语言一致的行为
   - 从块作用域开始到变量声明处的区域
   - 在此区域内访问变量会报`ReferenceError`
   - 影响`let/const`声明的变量

#### 1.1.2. 数据类型

1. 基本类型/值类型
   - `String`：字符串类型，用于文本数据
   - `Number`：数字类型，包括整数和浮点数
   - `Boolean`：布尔类型，true 或 false
   - `Symbol`：`ES2015`，唯一标识符
   - `BigInt`：`ES2020`，大整数，用于超出 Number 范围的整数计算
2. 复合数据类型/引用类型
   - `Array`：数组，有序的数据集合
   - `Object`：对象，键值对的集合
   - `Function`：函数，可执行的代码块
   - `Date`：日期对象
   - `RegExp`：正则表达式对象
   - 其他
3. 特殊类型/特殊值
   - `undefined`：未定义
   - `null`：空值
   - `NaN`：非数值

#### 1.1.3. 类型检测

1. `typeof`：判断基本的类型归属，返回`undefined`、`string`、`number`、`boolean`、`symbol`、`bigint`、`function`、`object`
2. `instanceof`：检查原型链
<!-- 3. Object.prototype.toString.call()：最准确的类型检测方法 -->

### 1.2. 运算符

1. 自增/自减运算符：`++`、`--`
2. 算术运算符：`+`、`-`、`*`、`/`、`%`
3. 比较运算符：`==`、`===`、`!=`、`!==`、`>`、`<`、`>=`、`<=`
4. 逻辑运算符：`&&`、`||`、`!`
5. 位运算符：`&`、`|`、`^`、`~`、`<<`、`>>`、`>>>`
6. 赋值运算符：`=`、`+=`、`-=`、`*=`、`/=`、`%=`
7. 三元运算符：`? :`
8. 字符串拼接：`+`
9. 逗号运算符：`,`
10. 特殊运算符：`delete`, `in`, `instanceof`, `typeof`, `void`

### 1.3. 控制流程

1. `if`/`else`：条件判断
2. `switch`：多分支判断，语句穿透特性
3. `for`/`while`/`do...while`：循环
4. `break`/`continue`：跳出/继续循环
5. `try`/`catch`/`finally`：异常处理
6. `throw`：抛出异常
7. `with`：作用域扩展

### 1.4. 函数

#### 1.4.1. 函数定义与调用

1. 命名函数
   - `function`关键字
   - 函数名
   - 形参列表
   - 函数体
2. 匿名函数
   - `function`关键字
   - 形参列表
   - 函数体
3. 箭头函数
   - 形参列表
   - `=>`关键字
   - 函数体

```javascript {.line-numbers}
// 命名函数
function add(a, b) {
    return a + b;
}

// 匿名函数
let add = function(a, b) {
    return a + b;
}

// 箭头函数
let add = (a, b) => a + b;
```

#### 1.4.2. 参数传递

### 1.5. 定时器

1. `setTimeout()/clearTimeout()`：定时执行一次任务
2. `setInterval()/clearInterval()`：定时循环执行任务
3. `setImmediate()/clearImmediate()` (`Node.js`特有)：在当前事件循环的末尾执行
4. `process.nextTick()` (`Node.js`特有)：在当前事件循环的末尾执行（higher priority than `setImmediate()`）

### 1.6. 标准库

#### 1.6.1. Array

1. 主要属性
    1. `length`：数组长度
2. 修改数组
   - `push()`：末尾添加元素
   - `pop()`：删除末尾元素
   - `shift()`：删除首个元素
   - `unshift()`：开头添加元素
   - `splice()`：任意位置增删改
3. 数组遍历
   - `forEach()`：遍历数组
   - `map()`：映射新数组
   - `filter()`：过滤数组
   - `reduce()`：归并操作
   - `some()`/`every()`：条件判断
4. 查找元素
   - `find()`：返回首个满足条件的元素
   - `findIndex()`：返回首个满足条件的索引
   - `includes()`：检查是否包含某元素
   - `indexOf()`/`lastIndexOf()`：查找元素索引

#### 1.6.2. String

1. 检索方法
   - `indexOf()`/`lastIndexOf()`：查找子串位置
   - `includes()`：检查是否包含子串
   - `startsWith()`/`endsWith()`：检查开头/结尾
2. 转换方法
   - `split()`：分割字符串为数组
   - `toLowerCase()`/`toUpperCase()`：大小写转换
   - `trim()`：去除两端空格
   - `padStart()`/`padEnd()`：补全字符串

#### 1.6.3. Object

1. 属性操作
   - `Object.keys()`：获取所有键名
   - `Object.values()`：获取所有值
   - `Object.entries()`：获取所有键值对
   - `Object.defineProperty()`：定义属性特性
2. 对象操作
   - `Object.assign()`：对象合并
   - `Object.create()`：创建新对象
   - `Object.freeze()`：冻结对象
   - `Object.seal()`：密封对象

#### 1.6.4. Math

1. 常用方法
   - `Math.abs()`：绝对值
   - `Math.ceil()`/`Math.floor()`：向上/向下取整
   - `Math.round()`：四舍五入
   - `Math.max()`/`Math.min()`：最大/最小值
   - `Math.pow()`：幂运算
   - `Math.sqrt()`：平方根
   - `Math.random()`：随机数
   - `Math.sin()`/`Math.cos()`/`Math.tan()`：三角函数
   - `Math.PI`/`Math.E`：常数
   - `Math.log()`/`Math.exp()`：对数/指数

### 1.7. 异步编程模型

#### 1.7.1. Callback

1. 回调地狱问题
2. `Error-first callbacks`(Node.js风格)

#### 1.7.2. Promise

1. 状态
   - `pending`：初始状态
   - `fulfilled`：完成状态
   - `rejected`：拒绝状态
2. 方法
   - `then()`：处理成功状态
   - `catch()`：处理错误状态
   - finally()：最终处理
<!-- 3. 静态方法
   - `Promise.all()`：This returned promise fulfills when all of the input's promises fulfill
   - `Promise.race()`：This returned promise settles with the eventual state of the first promise that settles
   - `Promise.allSettled()`：This returned promise fulfills when all of the input's promises settle -->

#### 1.7.3. async/await

1. 基本使用
   - `async`：声明异步函数
   - `await`：等待`Promise`完成
   - `try/catch`：错误处理
2. 注意事项
   - `await`只能在`async`函数内使用
   - `await`会暂停函数执行
   - 错误处理使用`try/catch`

#### 1.7.4. 事件循环(Event Loop)

1. 执行顺序
   - 同步代码
   - 微任务：`Promise`、`process.nextTick`
   - 宏任务：`setTimeout`、`setInterval`、`setImmediate`、`IO Callback`
2. 任务队列
   - 微任务队列优先级高于宏任务队列
   - 每个宏任务执行完会清空微任务队列

### 1.8. jQuery

1. `jQuery`：简化`JavaScript`开发，提供`DOM`操作、`AJAX`、事件处理、动画效果等功能
2. 基本用法
   - `$(selector)`：选择器
   - `$(selector).method()`：方法调用
3. 选择器
   - `#id`：`ID`选择器
   - `.class`：类选择器
   - `tag`：标签选择器
   - `*`：通配符选择器

## 2. Web API

### 2.1. DOM

1. `DOM(Document Object Module)`：文档对象模型，提供对文档的动态访问和更新
2. 节点对象重要属性
   - `nodeType`：节点类型
   - `nodeName`：节点名称
   - `nodeValue`：节点值
   - `parentNode`：父节点
   - `childNodes`：子节点
   - `firstChild`/`lastChild`：第一个/最后一个子节点
   - `nextSibling`/`previousSibling`：下一个/上一个兄弟节点
   - `attributes`：属性列表
   - `innerHTML`/`innerText`：节点内容
   - `style`：样式
   - `classList`：类名列表
   - `dataset`：自定义属性
3. 获取节点
   - `document.getElementById()`：获取`ID`匹配元素
   - `document.getElementByName()`：获取`name`属性匹配元素
   - `document.getElementByTagName()`：获取标签名匹配元素
   - `document.getElementsByClassName()`：获取类名匹配元素
   - `document.querySelector()`：获取第一个匹配元素
   - `document.querySelectorAll()`：获取所有匹配元素
4. 修改节点
   - `<node>.appendChild()`：添加子节点
   - `<node>.removeChild()`：删除子节点
   - `<node>.replaceChild()`：替换节点
   - `<node>.insertBefore()`：插入节点
5. 事件
   - `document.onload`：文档加载完成
   - `document.DOMContentLoaded`：文档加载且解析完成
   - `click`：点击事件
   - `input`：输入事件
   - `change`：改变事件
6. 事件绑定
   - `addEventListener()`：添加事件监听器
   - `removeEventListener()`：移除事件监听器

<!-- #### 2.1.2. 事件处理

1. 事件流
   - 捕获阶段：从外到内
   - 目标阶段：到达目标元素
   - 冒泡阶段：从内到外
2. 事件委托
   - 利用事件冒泡
   - 减少事件监听器数量
   - 动态元素事件处理 -->

### 2.2. BOM操作

1. `BOM(Browser Object Module)`：浏览器对象模型，提供对浏览器的访问和控制

#### 2.2.1. window对象

1. `browser`中的顶层对象
1. 重要属性
   - `innerHeight`/`innerWidth`：视口大小
   - `pageXOffset`/`pageYOffset`：滚动位置
2. 重要方法
   - `open()`：打开新窗口
   - `close()`：关闭窗口
   - `scrollTo()`：滚动到指定位置

#### 2.2.2. location对象

1. 重要属性
   - `href`：完整`URL`
   - `pathname`：路径部分
   - `search`：查询字符串
   - `hash`：锚点
2. 重要方法
   - `reload()`：重新加载
   - `replace()`：替换历史记录
   - `assign()`：添加历史记录

## 3. Node.js模块化

### 3.1. 模块系统

1. `CommonJS`
   - `require`导入
   - `module.exports`或`exports`导出
   - 同步加载
   - 值拷贝
2. `ESM(ES Modules)`
   - `import`导入
   - `export`导出
   - 异步加载
   - 值引用

### 3.2. npm包管理

1. `npm`: `Node Package Man`
2. 常用命令
   - `npm install`：安装依赖
   - `npm update`：更新依赖
   - `npm run`：运行脚本
   - `npm publish`：发布包
3. `package.json`
   - `name`：包名
   - `version`：版本号
   - `dependencies`：生产依赖
   - `devDependencies`：开发依赖
   - `scripts`：脚本
4. 版本号规则
   - `^`：不更改主版本号
   - `~`：不更改主版本号和次版本号
   - `*`：任意版本
5. 模块加载规则
6. 模块内变量为模块作用域，`global`/`globalThis`为全局作用域
6. 模块内置变量
   - `__dirname`：当前模块目录
   - `__filename`：当前模块文件名
   - `exports`：导出对象
   - `module`：模块对象
   - `require`：导入模块

## 4. Node.js异步编程

### 4.1. EventEmitter

1. 基本用法
   - `on()`：注册事件
   - `once()`：一次性事件
   - `removeListener()`：移除监听器
   - `emit()`：触发事件
2. 错误处理
   - `error event`
   - 监听器异常捕获
   - 异步错误处理

### 4.2. Asynchronous Queues

1. 微任务队列`Microtask Queue`
   - `process.nextTick`
   - `Promise`：`then`、`catch`、`finally`
2. 宏任务队列`Macrotask Queue`
   - `setTimeout`/`setInterval`
   - `setImmediate`
   - `I/O Callback`

## 5. Node.js重要模块

### 5.1. fs

1. 文件操作
   - `readFile/writeFile`：异步读写，一次性完整读写
   - `createReadStream/createWriteStream`：异步读写，多次流式读写
   - `readFileSync/writeFileSync`：同步读写，一次性完整读写
2. 目录操作
   - `mkdir`：创建目录
   - `rmdir`：删除目录
   - `readdir`：读取目录

### 5.2. net

1. `net`模块提供了基于`tcp`的网络通信功能
    1. `net.Server`：`tcp server`的抽象
    2. `net.Socket`：`tcp socket`/`tcp connection`的抽象
2. `net.Server`
    1. 重要方法
        - `server.listen()`：启动服务器
        - `server.close()`：关闭服务器
    2. 重要事件
        - `connection`：新连接
        - `close`：关闭
        - `error`：错误
        - `listening`：监听
3. `net.Socket`
    1. 重要方法
        - `socket.connect()`：连接服务器
        - `socket.write()`：发送数据
        - `socket.end()`：关闭连接
    2. 重要事件
        - `connect`：连接成功
        - `data`：接收数据
        - `end`：关闭连接
        - `error`：错误

### 5.3. http

1. 服务器
   - `createServer`：创建服务器
   - `request` 事件处理
   - `response` 响应处理
2. 客户端
   - `request`：发送请求
   - `get/post`：发送`GET/POST`请求
   - 错误处理

### 5.4. url and querystring

1. `current api`
    1. `url.URL` class
    2. `url.URLSearchParams` class
2. `legacy api`
    1. `url.Url` class
    2. `url` module functions: `url.parse()`, `url.format()`, `url.resolve()`
    3. `querystring` module functions: `querystring.parse()`, `querystring.stringify()`

## 6. MySQL数据库操作

1. 第三方模块：`mysql`, `mysql2`
2. 连接数据库：`mysql.createConnection()`
3. 操作数据：`connection.query('SQL statement', callback)`
4. 连接池：`mysql.createPool()`
5. 连接池操作：`pool.getConnection()`、`connection.query()`

## 7. Express.js

### 7.1. 基础应用

1. 路由
    1. 路由方法：app.get()、app.post()、app.put()、app.delete()
    2. 路由参数：
        1. 使用`:参数名`定义路由参数，如`/user/:id`
        2. 通过`req.params`获取
2. 中间件
    1. 全局中间件：`app.use(someMiddleware)`
    2. 路由中间件：`app.use(PATH, someMiddleware)`, `router.use(PATH, someMiddleware)`, `app.<METHOD>(PATH, someMiddleware)`
3. 内置中间件：`express.json()`、`express.urlencoded()`
4. 视图渲染
    1. 设置视图引擎
        1. 配置视图引擎：`app.set('view engine', 'ejs');`
        2. 设置视图文件夹路径：`app.set('views', path.join(__dirname, 'views'));`
    2. 创建视图文件：在`views/`目录下创建模板文件，例如`index.ejs`
    3. 渲染动态内容：`res.render()`

### 7.2. RESTful API

1. RESTful API
    1. `REST`：`Representational State Transfer`
    2. `RESTful`：符合`REST`风格的`API`
    3. 特点：资源、统一接口、状态转移、无状态
1. CRUD
    1. 创建数据：`POST`
    2. 读取数据：`GET`
    3. 更新数据：`PUT`或`PATCH`
    4. 删除数据：`DELETE`

## 8. JSON Web Token

1. `JWT`：`JSON Web Token`
2. 三个组成部分：`header`头部、`payload`负载/载荷(`body`主体)、`signature`签名
3. 安装`jsonwebtoken`模块：`npm install jsonwebtoken`
4. 生成`JWT`：`jwt.sign(payload, secret, options)`
5. 验证`JWT`：`jwt.verify(token, secret, options)`

## 9. Comprehensive Application

1. Setup `Express` and `MySQL2` connection
2. Create `database schema`
3. Implement `CRUD` endpoints:
    1. `GET /students` (list all)
    2. `GET /students/:id` (get one)
    3. `POST /students` (create)
    4. `PUT /students/:id` (update)
    5. `DELETE /students/:id` (delete)
4. Add `error` handling and `middleware`

```sql {.line-numbers}
CREATE DATABASE IF NOT EXISTS school;
USE school;

CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(100) UNIQUE NOT NULL,
    grade INT
);
```

```javascript {.line-numbers}
const express = require('express');
const mysql = require('mysql2');
const app = express();

// Middleware
app.use(express.json());

// Database connection
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'your_password',
  database: 'school'
});

// GET all students
app.get('/students', (req, res) => {
  connection.query('SELECT * FROM students', (err, results) => {
    if (err) {
      res.status(500).json({ error: err.message });
      return;
    }
    res.json(results);
  });
});

// GET single student
app.get('/students/:id', (req, res) => {
  connection.query(
    'SELECT * FROM students WHERE id = ?',
    [req.params.id],
    (err, results) => {
      if (err) {
        res.status(500).json({ error: err.message });
        return;
      }
      if (results.length === 0) {
        res.status(404).json({ message: 'Student not found' });
        return;
      }
      res.json(results[0]);
    }
  );
});

// CREATE student
app.post('/students', (req, res) => {
  connection.query(
    'INSERT INTO students SET ?',
    req.body,
    (err, result) => {
      if (err) {
        res.status(500).json({ error: err.message });
        return;
      }
      res.status(201).json({
        id: result.insertId,
        ...req.body
      });
    }
  );
});

// UPDATE student
app.put('/students/:id', (req, res) => {
  connection.query(
    'UPDATE students SET ? WHERE id = ?',
    [req.body, req.params.id],
    (err, result) => {
      if (err) {
        res.status(500).json({ error: err.message });
        return;
      }
      if (result.affectedRows === 0) {
        res.status(404).json({ message: 'Student not found' });
        return;
      }
      res.json({
        id: parseInt(req.params.id),
        ...req.body
      });
    }
  );
});

// DELETE student
app.delete('/students/:id', (req, res) => {
  connection.query(
    'DELETE FROM students WHERE id = ?',
    [req.params.id],
    (err, result) => {
      if (err) {
        res.status(500).json({ error: err.message });
        return;
      }
      if (result.affectedRows === 0) {
        res.status(404).json({ message: 'Student not found' });
        return;
      }
      res.json({ message: 'Student deleted successfully' });
    }
  );
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
```

```bash {.line-numbers}
# Start the server
nodemon app.js

# Test API endpoints
curl -X GET http://localhost:3000/students
curl -X POST http://localhost:3000/students -H "Content-Type: application/json" -d '{"name":"John Doe","email":"john@example.com","grade":90}'
curl -X PUT http://localhost:3000/students/1 -H "Content-Type: application/json" -d '{"grade":95}'
curl -X DELETE http://localhost:3000/students/1
```
