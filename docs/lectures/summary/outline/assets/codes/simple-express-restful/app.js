const express = require("express");
const mysql = require("mysql2");
const app = express();

// Middleware
app.use(express.json());

// Database connection
const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "school",
});

// GET all students
app.get("/students", (req, res) => {
    connection.query("SELECT * FROM students", (err, results) => {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        res.json(results);
    });
});

// GET single student
app.get("/students/:id", (req, res) => {
    connection.query("SELECT * FROM students WHERE id = ?", [req.params.id], (err, results) => {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        if (results.length === 0) {
            res.status(404).json({ message: "Student not found" });
            return;
        }
        res.json(results[0]);
    });
});

// CREATE student
app.post("/students", (req, res) => {
    connection.query("INSERT INTO students SET ?", req.body, (err, result) => {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        res.status(201).json({
            id: result.insertId,
            ...req.body,
        });
    });
});

// UPDATE student
app.put("/students/:id", (req, res) => {
    connection.query("UPDATE students SET ? WHERE id = ?", [req.body, req.params.id], (err, result) => {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        if (result.affectedRows === 0) {
            res.status(404).json({ message: "Student not found" });
            return;
        }
        res.json({
            id: parseInt(req.params.id),
            ...req.body,
        });
    });
});

// DELETE student
app.delete("/students/:id", (req, res) => {
    connection.query("DELETE FROM students WHERE id = ?", [req.params.id], (err, result) => {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        if (result.affectedRows === 0) {
            res.status(404).json({ message: "Student not found" });
            return;
        }
        res.json({ message: "Student deleted successfully" });
    });
});

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
