# Simple Express RESTful Demonstration

## Start the Server

1. `cd environment && docker compose up -d`: start the mysql database
2. `npm install`: optional if there is `node_modules` folder
3. `npm install -g nodemon`: optional if you had `nodemon` installed
4. `npm start` or `nodemon ./app.js` or `node ./app.js`: start the server

## Test the API

### Use `postman` to Test the API

1. `GET http://localhost:3000/students`: get all students
2. `POST http://localhost:3000/students`: create a student, use `JSON` format in the body, e.g. `{"name":"John Doe","email":"john@example.com","grade":90}`
3. `PUT http://localhost:3000/students/1`: update a student, use `JSON` format in the body, e.g. `{"grade":95}`
4. `DELETE http://localhost:3000/students/1`: delete a student

### Use `curl` to Test the API

⚠️ in `Windows`, it maybe fail while using `curl` ⚠️

```bash {.line-numbers}
# Test API endpoints
curl -X GET http://localhost:3000/students
curl -X POST http://localhost:3000/students -H "Content-Type: application/json" -d '{"name":"John Doe","email":"john@example.com","grade":90}'
curl -X PUT http://localhost:3000/students/1 -H "Content-Type: application/json" -d '{"grade":95}'
curl -X DELETE http://localhost:3000/students/1
```
