# Simple Express RESTful with JWT Demonstration

## start the server

1. `cd environment && docker compose up -d`: start the mysql database
2. `npm install`: optional if there is `node_modules` folder
3. `npm install -g nodemon`: optional if you had `nodemon` installed
4. `npm start` or `nodemon ./app.js` or `node ./app.js`: start the server

## test the API

### Use `postman` to Test the API

1. `POST http://localhost:3000/login`: get the jwt token, use username `jack` and password `a12345678`
2. `GET(with Authorization, Auth Type "Bearer Token") http://localhost:3000/get_user_info`: get the user info with `jwt token`(the `token` is got from the previous step)

### Use `curl` to Test the API

⚠️ in `Windows`, it maybe fail while using `curl` ⚠️

```bash {.line-numbers}
# Test API endpoints
curl -X POST http://localhost:3000/login \
    -H "Content-Type: application/json" \
    -d '{"username":"jack","password":"a12345678"}'

curl -X GET http://localhost:3000/get_user_info \
    -H "Authorization: Bearer <YOUR_TOKEN_GOT_FROM_PREVIOUS_STEP>"
```
