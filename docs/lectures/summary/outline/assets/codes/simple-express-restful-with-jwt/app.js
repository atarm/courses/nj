// [阮一峰. JSON Web Token 入门教程. 2018-07-23.](https://www.ruanyifeng.com/blog/2018/07/json_web_token-tutorial.html)

/**
 * 1. 引入依赖
 * 2. 创建数据库连接池
 * 3. 读取JWT密钥
 * 4. 创建Express实例
 * 5. 使用bodyParser中间件解析post数据
 * 6. 自定义中间件passwdCrypt，用于加密密码
 * 7. 定义endpoints
 * 8. 监听端口
 */

// #region dependencies
const fs = require("fs");
const qs = require("querystring"); //用于监听post数据读取
const express = require("express");
const bodyParser = require("body-parser");
const md5 = require("md5");     // 用于对密码进行加密
const jwt = require("jsonwebtoken"); // 用于签发和验证jwt字符串
const mysql = require("mysql2");
// #endregion dependencies

// #region mysql pool
const config = require("./config/mysql");
let pool = mysql.createPool(config.constr);
// #endregion mysql pool

// #region jwt secret
const jwt_secret = fs.readFileSync("./.env", "utf-8");
// #endregion jwt secret

// #region express app
const app = express();
const port = 3000;
// #endregion express app

// #region bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// #endregion bodyParser

// #region passwdCrypt
let passwdCrypt = function (req, res, next) {
    console.log(req.body);
    // 此处需要用到“加盐/料加密”的思想，直接md5会有被爆破的风险
    req.body.password = md5(
        req.body.password + md5(req.body.password).substr(10, 10)
    );
    next();
};
// #endregion passwdCrypt

// #region endpoint `/login`
// encryption only used in login, cannot set as global middleware `app.use(passwdCrypt);`
app.post("/login", passwdCrypt, (req, res) => {
    let query = req.body;
    console.log(query);
    let sql = "select * from users where user_name=? and user_psw=?";
    let params = [query.username, query.password];
    pool.getConnection(function (err, connect) {
        connect.query(sql, params, function (err, results) {
            if (err) {
                throw new Error(err.message);
            }
            if (results.length == 0) { // no such user
                res.send({ error_code: 1000, message: "帐号或密码错误！" });
            } else {// user exists, return token
                res.send({
                    error_code: 0,
                    message: "ok",
                    jwt_token: jwt.sign(
                        { userId: results[0].user_id }, // payload
                        jwt_secret
                    ),
                });
            }
        });
        connect.release();
    });
});
// #endregion endpoint `/login`

// #region endpoint `/get_user_info`
// required: Authorization header ("Bearer <token>")
app.get("/get_user_info", (req, res) => {
    // Bearer <token> ==> ["Bearer", "<token>"]
    let tmp = req.headers.authorization.split(" ");
    console.log("Authorization header:", tmp);
    let token = tmp[tmp.length - 1];
    console.log("Token being verified:", token);
    console.log("Secret being used:", jwt_secret); // Be careful with logging secrets in production
    try {
        const payload = jwt.verify(token, jwt_secret);
        console.log("Decoded payload:", payload); 

        // constraints: token expires after 24 minutes
        if (new Date().getTime() / 1000 - payload.iat > 24 * 60) {
            res.send({
                error_code: 1001,
                message: "令牌已经过期！",
            });
        } else {
            let sql = "select * from users where user_id=?";
            let params = [payload.userId];
            console.log("params:", params);
            pool.getConnection(function (err, connect) {
                connect.query(sql, params, function (err, results) {
                    console.log(results);
                    if (results.length == 0) { // no such user
                        res.send({
                            error_code: 1000,
                            message: "帐号或密码错误！",
                        });
                    } else {
                        res.send({
                            error_code: 0,
                            message: "ok",
                            data: {
                                userId: results[0].user_id,
                                userName: results[0].user_name,
                                userEmall: results[0].user_email,
                            },
                        });
                    }
                });
                connect.release();
            });
        }
    } catch (error) {
        throw new Error("token令牌鉴定失败！");
    }
});
// #endregion endpoint `/get_user_info`

app.listen(port, () =>
    console.log(`server is running at http://127.0.0.1:${port}!`)
);
