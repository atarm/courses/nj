# Node Web编程基础Basics of Node Web Programming

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=3 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [前置条件](#前置条件)
4. [设计与实现指引](#设计与实现指引)
    1. [内容01：建立数据库环境](#内容01建立数据库环境)
    2. [内容02：新建`node`项目并安装依赖](#内容02新建node项目并安装依赖)
    3. [内容03：复制静态资料](#内容03复制静态资料)
    4. [内容04：创建数据库连接配置文件`config.js`](#内容04创建数据库连接配置文件configjs)
    5. [内容05：实现路由功能`router.js`](#内容05实现路由功能routerjs)
    6. [内容06：实现业务逻辑`model.js`](#内容06实现业务逻辑modeljs)
    7. [内容07：实现服务器`app.js`](#内容07实现服务器appjs)
5. [运行验证指引](#运行验证指引)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计型
1. 目的：
    1. 掌握`node`异步编程
    1. 掌握`node`操作`mysql`
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 系统环境：`Windows`或`Linux`或`MacOS`
    1. 开发环境：`WebStorm`或`VSCode`或`Sublime Text`或`Atom`或`Notepad++`等主流编辑器

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，例如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"**

## 前置条件

1. 已安装并启动`mysql 5.x`或`mysql 8.x`，`mysql 5.x`或`mysql 8.x`均可
1. 请使用`mysql2`依赖包（而不是`mysql`）

## 设计与实现指引

❗ 数据库连接的`user`和`password`请修改成你自已数据库环境下的用户名和密码 ❗

### 内容01：建立数据库环境

请通过数据库客户端连接数据库并运行["./resources/database/goproject.sql"](./resources/database/goproject.sql)建立数据库环境（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）

### 内容02：新建`node`项目并安装依赖

1. 新建`node-mysql_basic_{fn_and_en}`文件夹
1. 在命令行中进入`node-mysql_basic_{fn_and_en}`文件夹并运行`npm init -y`
1. 修改`package.json`，配置`name`、`version`、`author`属性，参考实现如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）
1. 运行`npm install --save mysql2 bootstrap underscore formidable`安装`mysql2`依赖（❗ **注意：是`mysql2`** ❗）

```json {.line-numbers}
{
    "name": "web_basic",
    "version": "0.0.1",
    "description": "",
    "main": "index.js",
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
    },
    "keywords": [],
    "author": "{fn_and_en}",
    "dependencies": {
        "bootstrap": "^5.3.2",
        "formidable": "^3.5.1",
        "mysql2": "^3.6.3",
        "underscore": "^1.13.6"
    }
}
```

### 内容03：复制静态资料

1. 在`node-mysql_basic_{fn_and_en}`下新建文件夹`lib/`
1. 将[./resources/web/public.zip](./resources/web/public.zip)和[./resources/web/views.zip](./resources/web/views.zip)解压到`lib/`下，目录结构如下所示：

```bash {.line-numbers}
.
└── lib
    ├── public
    │   ├── css
    │   ├── img
    │   │   ├── icon
    │   │   └── product
    │   └── js
    └── views
```

### 内容04：创建数据库连接配置文件`config.js`

新建`lib/config.js`文件，参考实现如下（❗ `user`和`password`请修改成你自已数据库环境下的用户名和密码 ❗）：

```javascript {.line-numbers}
module.exports = {
    host: '127.0.0.1', port: 3000, user: 'root', password: 'root', database: 'goproject'
};
```

### 内容05：实现路由功能`router.js`

新建`lib/router.js`，参考实现如下：

```javascript {.line-numbers}
const url = require('url');
const path = require('path');
const fs = require('fs');
const formidable = require('formidable');
const _ = require('underscore');
const model = require('./model');

module.exports = function (req, res) {
    let urlObj = url.parse(req.url, true);
    let pathname = urlObj.pathname;
    let method = req.method.toLowerCase();
    // 为req追加一个query属性，属性值urlObj.query
    req.query = urlObj.query;

    if (pathname === '/' && method === 'get') {// index page
        fs.readFile(path.join(__dirname, 'views/index.html'), function (err, data) {
            if (err) {
                return res.end(err.message);
            }
            model.findProduct(function (err, results) {
                let compiled = _.template(data.toString());
                let htmlStr = compiled({
                    computerList: results[0],
                    phoneList: results[1],
                    padList: results[2],
                    earphoneList: results[3],
                    productList: results[4]
                });
                res.setHeader('Content-Type', 'text/html;charset=utf-8');
                res.end(htmlStr);
            })
        });
    } else if (pathname.startsWith('/public/') && method === 'get') {// static resources
        fs.readFile(path.join(__dirname, pathname), function (err, data) {
            if (err) {
                return res.end(err.message);
            }
            res.end(data);
        })
    } else if (pathname === '/login' && method === 'get') {// login page
        fs.readFile(path.join(__dirname, 'views/login.html'), function (err, data) {
            if (err) {
                return res.end(err.message)
            }
            res.end(data)
        })
    } else if (pathname === '/login' && method === 'post') {// login action
        let form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            if (err) {
                return res.end(err.message);
            }
            let phone = fields.phone[0];
            let password = fields.password[0];
            model.findPhone(phone, function (err, results) {
                if (err) {
                    return res.end(err.message);
                }
                if (results.length === 0) {
                    res.writeHead(200, {'Content-Type': 'text/html;charset=utf8'});
                    res.end('<script>alert("电话号码未注册,请重新输入！");window.location.href="/login"</script>');
                } else {
                    if (results[0].password !== password) {
                        res.writeHead(200, {'Content-Type': 'text/html;charset=utf8'});
                        res.end('<script>alert("密码不正确,请重新输入！");window.location.href="/login"</script>');
                    } else {
                        res.writeHead(200, {'Content-Type': 'text/html;charset=utf8'});
                        res.end('<script>alert("登录成功！");window.location.href="/"</script>');
                    }
                }
            })
        })
    } else if (pathname === '/register' && method === 'get') {// register page
        fs.readFile(path.join(__dirname, 'views/register.html'), function (err, data) {
            if (err) {
                return res.end(err.message);
            }
            res.end(data);
        })
    } else if (pathname === '/register' && method === 'post') {// register action
        let form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            let phone = fields.phone[0];
            let nickname = fields.nickname[0];
            let password = fields.password[0];
            model.findPhone(phone, function (req, results) {
                if (err) {
                    return res.end(err.message);
                }
                if (results.length === 0) {
                    model.doReg(phone, nickname, password, function (err, results2) {
                        if (err) {
                            return res.end(err.message);
                        }
                        res.writeHead(200, {'Content-Type': 'text/html;charset=utf8'});
                        res.end('<script>alert("注册成功，前去登录！");window.location.href="/login"</script>');
                    });
                } else {
                    res.writeHead(200, {'Content-Type': 'text/html;charset=utf8'});
                    res.end('<script>alert("此电话已经注册，请重新输入！");window.location.href="/register"</script>');
                }
            });
        });
    }
}
```

### 内容06：实现业务逻辑`model.js`

新建`lib/model.js`，参考实现如下：

```javascript {.line-numbers}
const mysql = require('mysql2');
const config = require('./config');

let pool = mysql.createPool({
    connectionLimit: 100,       // max connection
    multipleStatements: true,   // multiple statements
    host: config.host, user: config.user, password: config.password, database: config.database
});

module.exports.findProduct = function (callback) {
    pool.getConnection(function (err, conn) {
        if (err) {
            return callback("连接池连接失败！" + err, null);
        }
        let sql = `SELECT *
                   FROM go_product
                   WHERE p_type = 'ad-product-computer'
                   LIMIT 4;
        SELECT *
        FROM go_product
        WHERE p_type = 'ad-product-phone'
        LIMIT 4;
        SELECT *
        FROM go_product
        WHERE p_type = 'ad-product-pad'
        LIMIT 4;
        SELECT *
        FROM go_product
        WHERE p_type = 'ad-product-ear'
        LIMIT 4;
        SELECT *
        FROM go_product;
        `;
        conn.query(sql, function (err, results) {
            conn.release();
            if (err) {
                return callback("查询失败！" + err, null);
            }
            callback(null, results);
        });
    });
}

// find user_info by phone
module.exports.findPhone = function (phone, callback) {
    pool.getConnection(function (err, conn) {
        if (err) {
            return callback("连接池连接失败！" + err, null);
        }
        let sql = "select * from user_info where phone=?";
        conn.query(sql, [phone], function (err, results) {
            conn.release();
            console.log("results ==> " + results);
            if (err) {
                return callback("查询失败！" + err, null);
            }
            callback(null, results);
        })
    })
}

// register dao
module.exports.doReg = function (phone, nickname, password, callback) {
    pool.getConnection(function (err, conn) {
        if (err) {
            return callback("连接池连接失败！" + err, null);
        }

        let sql = 'insert into user_info(user_id,phone,nickname,password) values (null,?,?,?)';
        conn.query(sql, [phone, nickname, password], function (err, results) {
            conn.release();
            if (err) {
                return callback("查询失败！" + err, null);
            }
            callback(null, results);
        });
    });
}

```

### 内容07：实现服务器`app.js`

新建`app.js`文件，参考实现如下：

```javascript {.line-numbers}
const http = require('http');
const config = require('./lib/config.js');
const router = require('./lib/router.js');

let server = http.createServer();

server.on('request', function (req, res) {
    //调用路由
    router(req, res);
});

server.listen(config.port, config.host, function () {
    console.log('server is running at  ' + config.host + ':' + config.port + '...');
});
```

## 运行验证指引

1. 在命令行中运行`node app.js`，启动服务器
1. 在浏览器中访问`http://127.0.0.1:3000/`，查看首页
1. 点击首页右上角的`"注册"`打开注册页面，进行注册
    1. ❗ 点击"`获取验证码`"后的`验证码`区分大小写 ❗
    1. 注册用户名请使用`{fn_and_en}`（如：`zhangsan68`）
1. 注册完成后，点击首页右上角的`"登录"`打开登录页面，进行登录
