drop database if exists `goproject`;

create database `goproject`;

use `goproject`;

set FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for go_address
-- ----------------------------
DROP TABLE IF EXISTS `go_address`;

CREATE TABLE `go_address`
(
    `address_id` int(20)      NOT NULL AUTO_INCREMENT,
    `address`    varchar(255) DEFAULT '请添加地址',
    `user_id`    varchar(255) NOT NULL,
    PRIMARY KEY (`address_id`)
) ENGINE = MyISAM
  AUTO_INCREMENT = 37
  DEFAULT CHARSET = utf8mb4;

-- ----------------------------
-- Records of go_address
-- ----------------------------
INSERT INTO `go_address`
VALUES ('36', '11湖南省常德市1 邮编：415000', '11111111');
INSERT INTO `go_address`
VALUES ('33', '请添加地址', '11111111');

-- ----------------------------
-- Table structure for go_product
-- ----------------------------
DROP TABLE IF EXISTS `go_product`;
CREATE TABLE `go_product`
(
    `p_id`       int(11)      NOT NULL AUTO_INCREMENT,
    `p_name`     varchar(255) NOT NULL,
    `p_img_1`    varchar(255) NOT NULL,
    `p_img_2`    varchar(255) NOT NULL,
    `p_img_3`    varchar(255) NOT NULL,
    `p_img_4`    varchar(255) NOT NULL,
    `p_img_5`    varchar(255) DEFAULT NULL,
    `p_img_6`    varchar(255) DEFAULT NULL,
    `p_img_7`    varchar(255) DEFAULT NULL,
    `p_img_8`    varchar(255) DEFAULT NULL,
    `p_type`     varchar(255) NOT NULL,
    `p_price1`   varchar(255) NOT NULL,
    `p_price2`   varchar(255) NOT NULL,
    `p_present`  varchar(255) NOT NULL,
    `p_stock`    varchar(255) NOT NULL,
    `p_version`  varchar(255) DEFAULT NULL,
    `p_version2` varchar(255) DEFAULT NULL,
    `p_color`    varchar(255) DEFAULT NULL,
    `p_color2`   varchar(255) DEFAULT NULL,
    PRIMARY KEY (`p_id`)
) ENGINE = MyISAM
  AUTO_INCREMENT = 23
  DEFAULT CHARSET = utf8mb4;

-- ----------------------------
-- Records of go_product
-- ----------------------------
INSERT INTO `go_product`
VALUES ('1', 'MacBookPro', 'MacBookPro_gray_0.png', 'MacBookPro_gray_1.png', 'MacBookPro_gray_2.png',
        'MacBookPro_gray_3.png', 'MacBookPro_silver_0.png', 'MacBookPro_silver_1.png', 'MacBookPro_silver_2.png',
        'MacBookPro_silver_3.png', 'ad-product-computer', '18999', '21935',
        '2.6GHz 6 核第九代 Intel Core i7 处理器，Turbo Boost 最高可达 4.5GHz\r\n16GB 2666MHz DDR4 内存', '100',
        '16GB 2666MHz DDR4', '32GB 2666MHz DDR4', '深空灰色', '银色');
INSERT INTO `go_product`
VALUES ('2', 'MagicBookPro', 'MagicBookPro_gray_0.png', 'MagicBookPro_gray_1.png', 'MagicBookPro_gray_2.png',
        'MagicBookPro_gray_3.png', 'MagicBookPro_silver_0.png', 'MagicBookPro_silver_1.png',
        'MagicBookPro_silver_2.png', 'MagicBookPro_silver_3.png', 'ad-product-computer', '5499', '6199',
        '16.1英寸高清全面屏，100%色域值，窄边框轻薄机身，全新第八代酷睿处理器，高性能体验，14小时长续航，魔法一碰传高速传输，指纹一键开机登录，游戏级散热，新一代MX250独显，高能视觉体验！',
        '100', 'i5 8GB 512GB 独显', 'i5 8GB 512GB', '星空灰', '冰河银');
INSERT INTO `go_product`
VALUES ('3', 'RedmiBook 14 增强版', 'RedmiBook14_grey_0.png', 'RedmiBook14_grey_1.png', 'RedmiBook14_grey_2.png',
        'RedmiBook14_grey_3.png', 'RedmiBook14_silver_0.png', 'RedmiBook14_silver_1.png', 'RedmiBook14_silver_2.png',
        'RedmiBook14_silver_3.png', 'ad-product-computer', '4199', '3999',
        '全新十代酷睿处理器 / MX250独显 / 14英寸超窄边框高清屏 / 小米手环极速解锁 / 炫酷多彩任你挑选', '100',
        'i5 8G 512G MX250', 'i5 8G 256G MX250', '深空灰', '月光银');
INSERT INTO `go_product`
VALUES ('4', '小米游戏本2019款', 'xiaomigamebook_0.png', 'xiaomigamebook_1.png', 'xiaomigamebook_2.png',
        'xiaomigamebook_3.png', null, null, null, null, 'ad-product-computer', '8999', '8599',
        '九代酷睿标压处理器 / 144Hz 刷新率 / 3+2包围式热管 / 12V 台机级别散热风扇 / 72%色域', '100', 'i7 16G 1T PCle',
        'i7 16G 512GB PCle', '暗夜黑', '梦之白');
INSERT INTO `go_product`
VALUES ('5', 'HUAWEI Mate 30 Pro 5G ', 'huaweimate30pro5g_black_0.png', 'huaweimate30pro5g_black_1.png',
        'huaweimate30pro5g_black_2.png', 'huaweimate30pro5g_black_3.png', 'huaweimate30pro5g_green_0.png',
        'huaweimate30pro5g_green_1.png', 'huaweimate30pro5g_green_2.png', 'huaweimate30pro5g_green_3.png',
        'ad-product-phone', '6999', '7899',
        '全网通 8GB+256GB 麒麟990 5G旗舰SoC芯片，支持双模SA/NSA；超感光徕卡电影四摄；超曲面OLED环幕屏；40W', '100',
        '6GB+256GB', '8GB+512GB', '亮黑色', '翡冷翠');
INSERT INTO `go_product`
VALUES ('6', 'HUAWEI P30 Pro', 'huaweip30pro_black_0.png', 'huaweip30pro_black_1.png', 'huaweip30pro_black_2.png',
        'huaweip30pro_black_3.png', 'huaweip30pro_skyblue_0.png', 'huaweip30pro_skyblue_1.png',
        'huaweip30pro_skyblue_2.png', 'huaweip30pro_skyblue_3.png', 'ad-product-phone', '4988', '5488',
        '麒麟980 超感光徕卡四摄 屏内指纹 双景录像 华为P30 Pro尊享金卡专属服务权益包含：①保修期内（不含延保）可享2次免费保养②1小时快修服务③金卡专属热线服务',
        '100', '8GB+128GB', '8GB+256GB', '亮黑色', '天空之境');
INSERT INTO `go_product`
VALUES ('7', '小米9 Pro 5G', 'xiaomi9pro5g_black_0.png', 'xiaomi9pro5g_black_1.png', 'xiaomi9pro5g_black_2.png',
        'xiaomi9pro5g_black_3.png', 'xiaomi9pro5g_white_0.png', 'xiaomi9pro5g_white_1.png', 'xiaomi9pro5g_white_2.png',
        'xiaomi9pro5g_white_3.png', 'ad-product-phone', '3699', '4299',
        '5G双卡全网通超高速网络 / 骁龙855Plus旗舰处理器 / 40W有线闪充+30W无线闪充+10W无线反充，4000mAh长续航 / 4800万全焦段三摄 / 超振感横向线性马达 / VC液冷散热 / 高色准三星AMOLED屏幕 / 多功能NFC / 赠送小米云服务1TB云存储',
        '100', '8GB+128GB', '12GB+512GB', '钛银黑', '梦之白');
INSERT INTO `go_product`
VALUES ('8', '小米CC9 Pro', 'xiaomicc9pro_black_0.png', 'xiaomicc9pro_black_1.png', 'xiaomicc9pro_black_2.png',
        'xiaomicc9pro_black_3.png', 'xiaomicc9pro_green_0.png', 'xiaomicc9pro_green_1.png', 'xiaomicc9pro_green_2.png',
        'xiaomicc9pro_green_3.png', 'ad-product-phone', '2799', '3099',
        '新品火爆开售中，分期享6期免息，付款成功可参与抽奖，最高赢70英寸小米电视，评论前3000名赠CC果冻包，6GB+128GB魔法绿境稀缺货源少量到货，先到先得」1亿像素主摄 / 全场景五摄像头 / 四闪光灯 / 3200万自拍 / 10 倍混合光学变焦，50倍数字变焦 / 5260mAh ⼤电量 / 标配 30W疾速快充 / ⼩米⾸款超薄屏下指纹 / 德国莱茵低蓝光认证 / 多功能NFC / 红外万能遥控 / 1216超线性扬声器',
        '100', '6GB+128GB', '8GB+128GB', '暗夜魅影', '魔法绿境');
INSERT INTO `go_product`
VALUES ('9', '华为平板 M6 高能版 8.4英寸', 'HUAWEIM6_blue_0.png', 'HUAWEIM6_blue_1.png', 'HUAWEIM6_blue_2.png',
        'HUAWEIM6_blue_3.png', 'HUAWEIM6_red_0.png', 'HUAWEIM6_red_1.png', 'HUAWEIM6_red_2.png', 'HUAWEIM6_red_3.png',
        'ad-product-pad', '2699', '2799', '麒麟980芯片 2K超清画质 哈曼卡顿调音 液冷散热 GPU Turbo加速游戏体验', '100',
        '6GB+128GB WiFi', '6GB+128GB 全网通', '幻影蓝', '幻影红');
INSERT INTO `go_product`
VALUES ('10', '华为平板 M5 青春版 8.0英寸', 'HUAWEIM5_golden_0.png', 'HUAWEIM5_golden_1.png', 'HUAWEIM5_golden_2.png',
        'HUAWEIM5_golden_3.png', 'HUAWEIM5_gray_0.png', 'HUAWEIM5_gray_1.png', 'HUAWEIM5_gray_2.png',
        'HUAWEIM5_gray_3.png', 'ad-product-pad', '18999', '1599',
        '智能声控平板 哈曼卡顿调音 麒麟710芯片 AI智慧语音控制一切好说、哈曼卡顿调音细腻鲜活、麒麟710芯片强大内芯、5100毫安大电池持久续航。',
        '100', '4GB+128GB ', '4GB+128GB ', '白色', '黑色');
INSERT INTO `go_product`
VALUES ('11', '华为平板 M6 10.8英寸 麒麟980芯片 ', 'HUAWEIM510_gray_0.png', 'HUAWEIM510_gray_1.png',
        'HUAWEIM510_gray_2.png', 'HUAWEIM510_gray_3.png', 'HUAWEIM510_golden_0.png', 'HUAWEIM510_golden_1.png',
        'HUAWEIM510_golden_2.png', 'HUAWEIM510_golden_3.png', 'ad-product-pad', '2699', '3999',
        '2K高清屏 应用分屏 一屏两用 哈曼卡顿调音 四声道四扬声器', '100', '4GB+64GB ', '6GB+256GB ', '黑色', '白色');
INSERT INTO `go_product`
VALUES ('12', '小米平板4 8 / 10.1 英寸', 'mi_black_0.png', 'mi_black_1.png', 'mi_black_2.png', 'mi_black_3.png',
        'mi_golden_0.png', 'mi_golden_1.png', 'mi_golden_2.png', 'mi_golden_3.png', 'ad-product-pad', '1499', '1899',
        '大电量，超长续航 / 支持AI人脸识别 / 后置1300万，前置500万摄像头 / 金属机身，超窄边框 / 骁龙660八核处理器', '100',
        '8英寸 LTE版  64GB', '10英寸 LTE版', '黑色', '粉色');
INSERT INTO `go_product`
VALUES ('13', 'AirPodsPro', 'AirPodsPro_white_0.png', 'AirPodsPro_white_1.png', 'AirPodsPro_white_2.png',
        'AirPodsPro_white_3.png', 'AirPodsPro_black_0.png', 'AirPodsPro_black_1.png', 'AirPodsPro_black_2.png', null,
        'ad-product-ear', '1999', '2100', '主动降噪|通透模式|全新设计|卓越音质', '100', 'IPX5', 'IPX6', '白色', '黑色');
INSERT INTO `go_product`
VALUES ('14', 'vivoTWSEarphone 真无线蓝牙耳机', 'vivoTWSEarphone_blue_0.png', 'vivoTWSEarphone_blue_1.png',
        'vivoTWSEarphone_blue_2.png', 'vivoTWSEarphone_blue_3.png', 'vivoTWSEarphone_white_0.png',
        'vivoTWSEarphone_white_1.png', 'vivoTWSEarphone_white_2.png', 'vivoTWSEarphone_white_3.png', 'ad-product-ear',
        '999', '1200', '疾速体验 | 智慧连接 | 高清音质 | 语音声控 | 无感佩戴（温馨提示：拆封包装后不支持无理由退货）',
        '100', '3.5mm', 'Type-c', '星际蓝', '皓月白');
INSERT INTO `go_product`
VALUES ('15', '小米蓝牙耳机青春版', 'mi6_black_0.png', 'mi6_black_1.png', 'mi6_black_2.png', 'mi6_black_3.png',
        'mi6_white_0.png', 'mi6_white_1.png', 'mi6_white_2.png', 'mi6_white_3.png', 'ad-product-ear', '59', '108',
        '6.5克轻巧 / 蓝牙4.1高清通话音质 / 支持切歌、音量调节', '100', '标准', '豪华', '黑色', '白色');
INSERT INTO `go_product`
VALUES ('16', 'HUAWEIFreeBuds 3 无线耳机', 'HUAWEIFreeBuds3_white_0.png', 'HUAWEIFreeBuds3_white_1.png',
        'HUAWEIFreeBuds3_white_2.png', 'HUAWEIFreeBuds3_white_3.png', 'HUAWEIFreeBuds3_black_0.png',
        'HUAWEIFreeBuds3_black_1.png', 'HUAWEIFreeBuds3_black_2.png', 'HUAWEIFreeBuds3_black_3.png', 'ad-product-ear',
        '2000', '2100',
        '半开放式主动降噪，麒麟A1芯片，抗干扰，低时延，无线快充，佩戴稳固舒适，震撼低音，搭配Mate30系列体验更佳', '100',
        '动圈式', '圈铁混合', '陶瓷白', '碳晶黑');
INSERT INTO `go_product`
VALUES ('22', '哈哈8888', 'AirPodsPro_black_0.png', 'HUAWEIFreeBuds3_black_0.png', 'AirPodsPro_black_2.png',
        'HUAWEIFreeBuds3_black_1.png', '', '', '', '', 'ad-product-phone', '6600', '8800',
        '很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好很好',
        '100', '6+128', '8+256', '亮白色', '月光银');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    `user_id`    int(10)     NOT NULL AUTO_INCREMENT,
    `user_name`  varchar(20) NOT NULL,
    `user_psw`   varchar(20) NOT NULL,
    `user_email` varchar(50) NOT NULL,
    PRIMARY KEY (`user_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 9
  DEFAULT CHARSET = utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user`
VALUES ('1', 'jack', 'a12345678', 'jack@163.com');
INSERT INTO `user`
VALUES ('2', 'ErenJaeger', 'abc123456', 'ErenJaeger@163.com');
INSERT INTO `user`
VALUES ('3', '夏目', 'abc123456', 'xiamu@163.com');
INSERT INTO `user`
VALUES ('8', '哈哈', 'abc123456', 'haha@163.com');

-- ----------------------------
-- Table structure for user2
-- ----------------------------
DROP TABLE IF EXISTS `user2`;
CREATE TABLE `user2`
(
    `user_id`    int(10)     NOT NULL AUTO_INCREMENT,
    `user_name`  varchar(20) NOT NULL,
    `user_psw`   varchar(20) NOT NULL,
    `user_email` varchar(50) NOT NULL,
    PRIMARY KEY (`user_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8mb4;

-- ----------------------------
-- Records of user2
-- ----------------------------
INSERT INTO `user2`
VALUES ('1', 'jack', 'a12345678', 'jack@163.com');
INSERT INTO `user2`
VALUES ('2', 'ErenJaeger', 'abc123456', 'ErenJaeger@163.com');
INSERT INTO `user2`
VALUES ('3', '夏目', 'abc123456', 'xiamu@163.com');
INSERT INTO `user2`
VALUES ('4', '哈哈', 'abc123456', 'haha@163.com');
INSERT INTO `user2`
VALUES ('5', 'haha', 'a12345678', 'haha@qq.com');

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`
(
    `user_id`       int(4)               NOT NULL AUTO_INCREMENT COMMENT '买家id',
    `phone`         varchar(11)          NOT NULL COMMENT '买家电话',
    `nickname`      varchar(12)                   DEFAULT NULL COMMENT '买家用户名',
    `email`         varchar(255)                  DEFAULT NULL COMMENT '注册邮箱',
    `password`      varchar(24)          NOT NULL COMMENT '买家密码',
    `birthday`      date                          DEFAULT NULL COMMENT '买家生日日期',
    `sex`           enum ('女','男')              DEFAULT '男' COMMENT '性别',
    `bankcard`      varchar(20)                   DEFAULT NULL COMMENT '银行卡号',
    `true_name`     varchar(50)                   DEFAULT NULL COMMENT '真实姓名',
    `balance`       decimal(8, 2)                 DEFAULT '0.00' COMMENT '账户余额',
    `register_time` timestamp            NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
    `head_img`      varchar(255)                  DEFAULT 'img/user/member.png' COMMENT '用户头像',
    `status`        enum ('冻结','正常') NOT NULL DEFAULT '正常',
    `interduce`     varchar(255)                  DEFAULT NULL COMMENT '我的简介',
    PRIMARY KEY (`user_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = utf8mb4;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info`
VALUES ('1', '13344433322', '小明', 'jje@163.com', '123456', '1990-01-01', null, null, '李小明', '0.00',
        '2020-01-06 21:48:26', 'img/user/member.jpg', '正常', '简介');
INSERT INTO `user_info`
VALUES ('3', '15623652589', '杰少', '2315625@qq.com', '123456', '1992-11-10', null, null, '赵杰', '0.00',
        '2019-12-27 14:41:16', 'img/user/member.jpg', '正常', '请完善用户信息');
INSERT INTO `user_info`
VALUES ('5', '15873407134', '大大大胖子', '1437092734@qq.com', '123456', '1990-07-24', null, null, '颜鹏飞', '0.00',
        '2019-12-28 13:35:23', 'img/user/member.jpg', '正常', null);
INSERT INTO `user_info`
VALUES ('6', '18773449141', '高凌', '2068438422@qq.com', '123456', '1996-06-11', null, null, '高凌', '0.00',
        '2019-12-28 13:50:51', 'img/user/member.jpg', '正常', null);
