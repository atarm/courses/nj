# Node-MySQL基础Basics of Using Mysql in Node

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=3 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [前置条件](#前置条件)
4. [设计与实现指引](#设计与实现指引)
    1. [内容01：`node-mysql`的基本操作](#内容01node-mysql的基本操作)
    2. [内容02：封装`node-mysql`的操作](#内容02封装node-mysql的操作)
5. [运行指引](#运行指引)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计型
1. 目的：
    1. 掌握`node`异步编程
    1. 掌握`node`操作`mysql`
1. 内容与要求：
    1. 内容01：`node-mysql`的基本操作
    1. 内容02：封装`node-mysql`的操作
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 系统环境：`Windows`或`Linux`或`MacOS`
    1. 开发环境：`WebStorm`或`VSCode`或`Sublime Text`或`Atom`或`Notepad++`等主流编辑器

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，例如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"**

## 前置条件

1. 已安装并启动`mysql 5.x`或`mysql 8.x`，`mysql 5.x`或`mysql 8.x`均可
1. 请使用`mysql2`依赖包（而不是`mysql`）

## 设计与实现指引

❗ 数据库连接的`user`和`password`请修改成你自已的用户名和密码 ❗

### 内容01：`node-mysql`的基本操作

#### 内容01步骤01：新建`node`项目并安装依赖

1. 新建`node-mysql_basic_{fn_and_en}`文件夹
1. 在命令行中进入`node-mysql_basic_{fn_and_en}`文件夹并运行`npm init -y`
1. 修改`package.json`，配置`name`、`version`、`author`属性，参考实现如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）
1. 运行`npm install mysql2 --save`安装`mysql2`依赖（❗ **注意：是`mysql2`** ❗）

```json {.line-numbers}
{
    "name": "node-mysql_basic",
    "version": "0.0.1",
    "author": "{fn_and_en}",
    "dependencies": {
        "mysql2": "^3.6.2"
    }
}
```

#### 内容01步骤02：建立数据库环境

运行如下`sql script`建立数据库环境（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）

```sql {.line-numbers}
drop database if exists goproject;

create database goproject;

use goproject;

drop table if exists `user_info`;

create table `user_info`(
    `u_id` int(11) unsigned not null auto_increment comment '用户id' primary key,
    `phone` varchar(20) not null comment '手机号',
    `nickname` varchar(20) not null comment '昵称',
    `email` varchar(40) default null comment '邮箱',
    `password` varchar(40) not null comment '密码',
    `birthday` date default null comment '生日',
    `sex` char(1) default null comment '性别',
    `bankcard` varchar(20) default null comment '银行卡号',
    `true_name` varchar(20) default null comment '真实姓名',
    `balance` decimal(10,2) default 0.00 comment '账户余额',
    `register_time` timestamp default current_timestamp comment '注册时间',
    `head_img` varchar(255) default 'img/user/member.jpg' comment '用户头像',
    `status` enum ('0','1') default '1' comment '用户状态 0:冻结 1:正常'
)engine = innodb default charset = utf8mb4 comment = '用户信息表';

insert into `user_info`(`phone`, `nickname`, `password`)
values ('13344433322', '{fn_and_en}', '123456'),
        ('15623652589', '小杰', '1123456')
;
```

#### 内容01步骤03：新建`node`文件并实现`node-mysql`的数据库连接操作

新建`connection.js`文件实现数据库连接操作，参考实现如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```javascript {.line-numbers}
//1.加载mysql模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root', // 按实际情况修改用户名
    password: 'root', // 按实际情况修改用户密码
    database: 'goproject',
    timezone: '+08:00'
});

//3.连接mysql
connection.connect(function (err) {
    if (err) {
        // 捕获异常堆栈信息
        return console.log('error connecting:' + err.stack);
    }
    // 提示连接成功
    console.log('连接成功');
    // id
    console.log(('connected as id ' + connection.threadId));

    //4.关闭连接
    connection.end();
});
```

#### 内容01步骤04：新建`node`文件并实现`node-mysql`的数据库查询操作

新建`select.js`文件实现数据库查询操作，参考实现如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```javascript {.line-numbers}
//1.加载模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root', // 按实际情况修改用户名
    password: 'root', // 按实际情况修改用户密码
    database: 'goproject',
    timezone: '+08:00'
});

//3.连接
connection.connect();

//4.执行操作--查询
let sql = 'select * from `user_info`';
connection.query(sql, function (err, result, fields) {
    if (err) {
        return console.log('查询失败' + err.message);
    }
    console.log(result);
    console.log(fields);
    //5.关闭连接
    connection.end();
});
```

#### 内容01步骤05：新建`node`文件并实现`node-mysql`的数据库插入操作

新建`insert.js`文件实现数据库插入操作，参考实现如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```javascript {.line-numbers}
//1.加载模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root', // 按实际情况修改用户名
    password: 'root', // 按实际情况修改用户密码
    database: 'goproject',
    timezone: '+08:00'
});

//3.连接
connection.connect()

//4.执行操作--插入
let sql = 'insert into `user_info`(u_id,phone,nickname,password) values (null,?,?,?)'
let sqlValues = ['15655665566', '{fn_and_en}', '123456']
connection.query(sql, sqlValues, function (err, results) {
    if (err) {
        return console.log(err.message);
    }
    console.log(results.insertId);
    console.log(results);
    //5.关闭连接
    connection.end();
})
```

#### 内容01步骤06：新建`node`文件并实现`node-mysql`的数据库更新操作

新建`update.js`文件实现数据库更新操作，参考实现如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```javascript {.line-numbers}
//1.加载模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root', // 按实际情况修改用户名
    password: 'root', // 按实际情况修改用户密码
    database: 'goproject',
    timezone: '+08:00'
});

//3.连接
connection.connect();

//4.执行操作--修改
let sql = 'update user_info set password=? where u_id=?';
let updateValue = ['{fn_and_en}', 1];
connection.query(sql, updateValue, function (err, results) {
    if (err) {
        return console.log(err.message);
    }
    console.log(results);
    //5.关闭连接
    connection.end();
});
```

#### 内容01步骤07：新建`node`文件并实现`node-mysql`的数据库删除操作

新建`delete.js`文件实现数据库删除操作，参考实现如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```javascript {.line-numbers}
//1.加载模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root', // 按实际情况修改用户名
    password: 'root', // 按实际情况修改用户密码
    database: 'goproject',
    timezone: '+08:00'
});

//3.连接
connection.connect();

//4.执行操作--删除
let sql = 'delete from user_info where u_id=3';
connection.query(sql, function (err, results) {
    if (err) {
        return console.log(err.message);
    }
    console.log(results);
    //5.关闭连接
    connection.end();
});
```

#### 内容01步骤03：新建`node`文件并实现`node-mysql`的数据库多语句操作

新建`multi_statements.js`文件实现数据库多语句操作，参考实现如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```javascript {.line-numbers}
//1.加载mysql模块
let mysql = require('mysql2');

//2.创建连接
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root', // 按实际情况修改用户名
    password: 'root', // 按实际情况修改用户密码
    database: 'goproject',
    timezone: '+08:00',
    multipleStatements: true
});

//3.连接mysql
connection.connect();

let query = 'SELECT * FROM `user_info`;';
query += 'SELECT nickname FROM `user_info` WHERE u_id=2;';

//4.多语句查询
connection.query(query, function (err, result) {
    if (err) {
        console.log(err.message);
    }
    console.log(result);
    console.log(result[0]);
    console.log(result[1]);
    //5.关闭连接
    connection.end();
});
```

### 内容02：封装`node-mysql`的操作

#### 内容02步骤01：新建数据库连接配置文件

新建`encapsulation/mysql.config`，参考实现如下：

```javascript {.line-numbers}
module.exports={
    host:'localhost',
    user:'root', // 按实际情况修改用户名
    password:'root', // 按实际情况修改用户密码
    database:'goproject',
    timezone: '+08:00'
}
```

#### 内容02步骤02：新建数据库操作封装文件

新建`encapsulation/mysql_utils.js`，参考实现如下：

```javascript {.line-numbers}
let mysql = require('mysql2');
let mysql_config = require('./mysql.config');

//向外暴露方法query
//sql:sql语句
//values:查询占位符的值
//callback:回调函数
module.exports.query = function (sql, values, callback) {
    //创建连接对象
    let connection = mysql.createConnection(mysql_config);
    //建立连接
    connection.connect(function (err) {
        if (err) {
            console.log('数据库连接失败');
            throw err;
        }
        //执行数据库操作
        let query = connection.query(sql, values)
            .on('error', function (err) {
                console.log(err);
            }).on('fields', function (fields) {
                // console.log(fields);
            }).on('result', function (rows) {
                console.log(rows);
            }).on('end', function () {
                connection.end();
            });
    });
}
```

#### 内容02步骤03：新建数据库操作业务文件

新建`encapsulation/index.js`，参考实现如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```javascript {.line-numbers}
let db = require('./mysql_utils.js');

let select_sql = 'select * from `user_info`';

db.query(select_sql, [], function (err, result) {
    err ? console.log(err) : console.log(result);
});

select_sql = 'select * from `user_info` where `u_id`=?';

db.query(select_sql, [1], function (err, result) {
    err ? console.log(err) : console.log(result);
});

update_sql = 'update `user_info` set `password`=? where `u_id`=?';

db.query(update_sql, ['{fn_and_en}', 1], function (err, result) {
    err ? console.log(err) : console.log(result);
});

```

## 运行指引

1. 在命令行中运行`node connection.js`，查看数据库连接结果
1. 在命令行中运行`node select.js`，查看数据库查询结果
1. 在命令行中运行`node insert.js`，查看数据库插入结果
1. 在命令行中运行`node update.js`，查看数据库更新结果
1. 在命令行中运行`node delete.js`，查看数据库删除结果
1. 在命令行中运行`node multi_statements.js`，查看数据库多语句查询结果
1. 在命令行中运行`node encapsulation/index.js`，查看数据库封装操作结果
