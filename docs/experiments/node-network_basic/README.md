# Node网络编程基础Basics of Node Network Programming

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=3 orderedList=true} -->

<!-- code_chunk_output -->

- [基本信息Basic Information](#基本信息basic-information)
- [说明及注意事项Cautions](#说明及注意事项cautions)
- [前置条件](#前置条件)
- [设计与实现指引](#设计与实现指引)
    - [内容01：新建`node`项目](#内容01新建node项目)
    - [内容02：新建`lib/config.js`配置`server`参数](#内容02新建libconfigjs配置server参数)
    - [内容03：新建`lib/signup.js`实现注册功能](#内容03新建libsignupjs实现注册功能)
    - [内容04：新建`lib/broadcast.js`实现广播群聊功能](#内容04新建libbroadcastjs实现广播群聊功能)
    - [内容05：新建`lib/p2p.js`实现点对点私聊功能](#内容05新建libp2pjs实现点对点私聊功能)
    - [内容06：新建`lib/server.js`实现`server`功能](#内容06新建libserverjs实现server功能)
    - [内容07：新建`lib/client.js`实现`client`功能](#内容07新建libclientjs实现client功能)
    - [内容08：修改`package.json`实现`npm`运行`server`和`client`](#内容08修改packagejson实现npm运行server和client)
- [运行验证指引](#运行验证指引)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计型
1. 目的：
    1. 掌握`node`异步编程
    1. 掌握`node`操作`mysql`
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 系统环境：`Windows`或`Linux`或`MacOS`
    1. 开发环境：`WebStorm`或`VSCode`或`Sublime Text`或`Atom`或`Notepad++`等主流编辑器

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，例如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"**

## 前置条件

## 设计与实现指引

### 内容01：新建`node`项目

1. 新建`node-network_basic_{fn_and_en}`文件夹
1. 在命令行中进入`node-network_basic_{fn_and_en}`文件夹并运行`npm init -y`
1. 修改`package.json`，配置`name`、`version`、`author`属性，参考实现如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）

```json {.line-numbers}
{
    "name": "node-network_basic",
    "version": "0.0.1",
    "author": "{fn_and_en}"
}
```

### 内容02：新建`lib/config.js`配置`server`参数

1. 在`node-network_basic_{fn_and_en}`文件夹中新建`lib`文件夹
1. 在`lib`文件夹中新建`config.js`文件，`config.js`文件内容参考如下：

```javascript {.line-numbers}
module.exports={
    "port":3000,
    "host":"127.0.0.1"
}
```

### 内容03：新建`lib/signup.js`实现注册功能

在`lib`文件夹中新建`signup.js`文件，`signup.js`文件内容参考如下：

```javascript {.line-numbers}
exports.signup = function (socket, data, users) {
    let username = data.username;
    if (!users[username]) {
        users[username] = socket;
        let send = {
            mstype: "signup", code: 1000, username: username, message: "注册成功"
        };
        socket.write(JSON.stringify(send));
    } else {
        let send = {
            mstype: "signup", code: 1001, message: "用户名已被占用，请重新输入用户名："
        }
        socket.write(JSON.stringify(send));
    }
};
```

### 内容04：新建`lib/broadcast.js`实现广播群聊功能

在`lib`文件夹中新建`broadcast.js`文件，`broadcast.js`文件内容参考如下：

```javascript {.line-numbers}
exports.broadcast = function (data, users) {
    let from = data.from;
    let message = data.message;
    message = from + "说：" + message;

    let send = {
        mstype: "broadcast", message: message
    };

    for (let username in users) {
        if (username !== from) {
            users[username].write(JSON.stringify(send));
        }
    }
};
```

### 内容05：新建`lib/p2p.js`实现点对点私聊功能

在`lib`文件夹中新建`p2p.js`文件，`p2p.js`文件内容参考如下：

```javascript {.line-numbers}
exports.p2p = function (socket, data, users) {
    let from = data.from;
    let to = data.to;
    let message = data.message;
    let receiver = users[to];
    if (!receiver) {//接收方不存在
        let send = {
            mstype: "p2p", code: 2001, message: "用户" + to + "不存在"
        }
        socket.write(JSON.stringify(send));
    } else {//存在，则向接收方发送信息
        let send = {
            mstype: "p2p", code: 2000, from: from, message: from + "对你说：" + message
        }
        receiver.write(JSON.stringify(send));
    }
};
```

### 内容06：新建`lib/server.js`实现`server`功能

在`lib`文件夹中新建`server.js`文件，`server.js`文件内容参考如下：

```javascript {.line-numbers}
let net = require("net");
let config = require("./config");
let broadcast = require("./broadcast");
let p2p = require("./p2p");
let signup = require("./signup");
let users = {};
let server = net.createServer();

server.on("connection", function (socket) {
    socket.on("data", function (data) {
        data = JSON.parse(data);
        switch (data.mstype) {
            case "signup":
                signup.signup(socket, data, users);
                break;
            case "broadcast":
                broadcast.broadcast(data, users);
                break;
            case "p2p":
                p2p.p2p(socket, data, users);
                break;
            default:
                break;
        }
    });
    socket.on("error", function () {
        console.log("有客户端异常退出了...");
    });
});

server.listen(config.port, config.host, function () {
    console.log("服务器在端口" + config.port + "启动了监听...");
});
```

### 内容07：新建`lib/client.js`实现`client`功能

在`lib`文件夹中新建`client.js`文件，`client.js`文件内容参考如下：

```javascript {.line-numbers}
let net = require("net");
let config = require("./config");

let client = net.createConnection({
    port: config.port, host: config.host
});
let username;
client.on("connect", function () {
    console.log("请输入用户名：");
});

process.stdin.on("data", function (data) {
    data = data.toString().trim();

    if (!username) {
        let send = {
            mstype: "signup", username: data
        };
        client.write(JSON.stringify(send));
        return;
    }
    let regex = /(.{1,18}):(.+)/;
    let matches = regex.exec(data);
    if (matches) {
        let from = username;
        let to = matches[1];
        let message = matches[2];

        let send = {
            mstype: "p2p", from: username, to: to, message: message
        };
        client.write(JSON.stringify(send));
    } else {//广播消息
        let send = {
            mstype: "broadcast", from: username, message: data
        };
        client.write(JSON.stringify(send));
    }
});

client.on("data", function (data) {
    data = JSON.parse(data);
    switch (data.mstype) {
        case "signup":
            switch (data.code) {
                case 1000:
                    username = data.username;
                    console.log(data.message);
                    break;
                case 1001:
                    console.log(data.message);
                    break;
                default:
                    break;
            }
            break;
        case "broadcast":
            console.log(data.message);
            break;
        case "p2p":
            switch (data.code) {
                case 2000:
                    console.log(data.message);
                    break;
                case 2001:
                    console.log(data.message);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
});

client.on("error", function () {
    console.log("聊天室已关闭！");
})
```

### 内容08：修改`package.json`实现`npm`运行`server`和`client`

修改`package.json`，在`scripts`中添加内容，参考如下：

```json {.line-numbers}
"scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start_server": "node ./lib/server.js",
    "new_client": "node ./lib/client.js"
},
```

## 运行验证指引

1. 运行`server`：在命令行中进入`node-network_basic_{fn_and_en}`文件夹并运行`npm run start_server`
2. 运行`client`：在命令行中进入`node-network_basic_{fn_and_en}`文件夹并运行`npm run new_client`
3. 运行多个`client`进行注册登录，并进行广播群聊和点对点私聊
    1. 广播群聊：直接输入消息内容并回车发送
    2. 点对点私聊：`<username>:<message>`格式输入消息内容并回车发送，例如，`张三:您好`（请注意`:`是英文状态下的冒号）
