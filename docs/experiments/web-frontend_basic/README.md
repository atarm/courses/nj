# Web前端基础Basics of Web Front-End

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=3 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [前置条件](#前置条件)
4. [设计与实现指引](#设计与实现指引)
    1. [内容01：简单的电子时钟的设计与实现](#内容01简单的电子时钟的设计与实现)
    2. [内容02：简单的电子日历的设计与实现](#内容02简单的电子日历的设计与实现)
5. [运行指引](#运行指引)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计型
1. 目的：
    1. 掌握`web`前端原生技术 `html`、`css`、`javascript` 的基本使用
1. 内容与要求：
    1. 内容01：简单的电子时钟的设计与实现
    1. 内容02：简单的电子日历的设计与实现
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 系统环境：`Windows`或`Linux`或`MacOS`
    1. 开发环境：`WebStorm`或`VSCode`或`Sublime Text`或`Atom`或`Notepad++`等主流编辑器

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，例如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"**

## 前置条件

无

## 设计与实现指引

### 内容01：简单的电子时钟的设计与实现

#### 新建并实现`default.css`

在`web-frontend_basic_{fn_and_en}/css`文件夹下新建`default.css`文件，参考内容如下：

```css {.line-numbers}
h1 {
    text-align: center;
}
```

#### 新建并实现`clock.css`

在`web-frontend_basic_{fn_and_en}/css`文件夹下新建`clock.css`文件，参考内容如下：

```css {.line-numbers}
@import url("./default.css");

.container {
    text-align:center;
    margin: auto;
}

/*电子时钟总体样式设置*/
#clock {
    width: 800px;
    font-size: 80px;
    font-weight: bold;
    color: red;
    /*text-align: right;*/
    display:flex;
    justify-content:center;
    margin: auto;
}
/*时分秒数字区域的样式设置*/
.box1 {
    margin-right: 10px;
    width: 100px;
    height: 100px;
    line-height: 100px;
    float: right;
    border: grey 1px solid;
}
/*冒号区域的样式设置*/
.box2 {
    width: 30px;
    float: right;
    margin-right: 10px;
}
```

#### 新建并实现`clock.js`

在`web-frontend_basic_{fn_and_en}/js`文件夹下新建`clock.js`文件，参考内容如下：

```javascript {.line-numbers}
//获取显示小时的区域框对象
let hour = document.getElementById("h");
//获取显示分钟的区域框对象
let minute = document.getElementById("m");
//获取显示秒的区域框对象
let second = document.getElementById("s");

//获取当前时间
function getCurrentTime() {
    let date = new Date();
    let h = date.getHours();
    let m = date.getMinutes();
    let s = date.getSeconds();

    let hour = document.getElementById("h");
    let minute = document.getElementById("m");
    let second = document.getElementById("s");

    if (h < 10) h = "0" + h; //以确保0-9时也显示成两位数
    if (m < 10) m = "0" + m; //以确保0-9分钟也显示成两位数
    if (s < 10) s = "0" + s; //以确保0-9秒也显示成两位数

    hour.innerHTML = h;
    minute.innerHTML = m;
    second.innerHTML = s;
}

//每秒更新一次时间
setInterval("getCurrentTime()", 1000);
```

#### 新建并实现`clock.html`

在`web-frontend_basic_{fn_and_en}`文件夹下新建`clock.html`文件，参考内容如下：

```html {.line-numbers}
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <title>{fn_and_en}设计并实现的电子时钟</title>
    <link href="css/clock.css" rel="stylesheet">
    <script src="js/clock.js"></script>
</head>
<body onload="getCurrentTime()">
<!--标题-->
<h1>{fn_and_en}设计并实现的电子时钟</h1>
<!--水平线-->
<hr/>
<div class="container">
    <!--电子时钟区域-->
    <div id="clock">
        <div class="box1" id="h"></div>
        <div class="box2">:</div>
        <div class="box1" id="m"></div>
        <div class="box2">:</div>
        <div class="box1" id="s"></div>
    </div>
</div>
</body>
</html>
```

### 内容02：简单的电子日历的设计与实现

#### 新建并实现`calender.css`

在`web-frontend_basic_{fn_and_en}/css`文件夹下新建`calender.css`文件，参考内容如下：

```css {.line-numbers}
@import url("./default.css");

div{
    text-align:center;
    margin-top:10px;
    margin-bottom:10px;
}

#calendar{
    width: 400px;
    margin:auto;
}

button{
    width: 25%;
    float:left;
    height:40px;
}

#month{
    width: 50%;
    float:left;
}

.everyday{
    width: 14%;
    float:left;
}
```

#### 新建并实现`calender.js`

在`web-frontend_basic_{fn_and_en}/js`文件夹下新建`calender.js`文件，参考内容如下：

```javascript {.line-numbers}
let today = new Date();
let year = today.getFullYear();
//获取当前年份
let month = today.getMonth() + 1;
//获取当前月份
let day = today.getDate();
//获取当前日期
let total_day = 0;
//当前月份的总天数

//用于推算当前的月份一共多少天
function count() {
    if (month !== 2) {
        if ((month === 4) || (month === 6) || (month === 9) || (month === 11)) {
             total_day = 30;
            //4、6、9、11月份为30天
        } else {
             total_day = 31;
            //其他月份为31天（不包括2月份）
        }
    } else {
        //如果是2月份需要判断当前是否为闰年
        if (((year % 4) === 0 && (year % 100) !== 0) || (year % 400) === 0) {
             total_day = 29;
            //闰年的2月份是29天
        } else {
             total_day = 28;
            //非闰年的2月份是28天
        }
    }
}

//显示日历标题中的当前年份和月份
function showMonth() {
    document.getElementById("month").innerHTML = year + "年" + month + "月";
}

//显示当前月份的日历
function showDate() {
    showMonth();//在年份月份显示牌上显示当前的年月
    count();//计算当前月份的总天数

    //获取本月第一天的日期对象
    let first_date = new Date(year, month - 1, 1);
    //推算本月第一天是星期几
    let day_of_week = first_date.getDay();

    //动态添加HTML元素
    let date_row = document.getElementById("day");
    date_row.innerHTML = "";

    //如果本月第一天不是周日，则前面需要用空白元素补全日期
    if (day_of_week !== 0) {
        for (let i = 0; i < day_of_week; i++) {
            let dayElement = document.createElement("div");
            dayElement.className = "everyday";
            date_row.appendChild(dayElement);
        }
    }

    //使用循环语句将当前月份的所有日期显示出来
    for (let j = 1; j <=  total_day; j++) {
        let dayElement = document.createElement("div");
        dayElement.className = "everyday";
        dayElement.innerHTML = j + "";

        //如果日期为今天，将内容显示为红色
        if (j === day) {
            dayElement.style.color = "red";
        }

        date_row.appendChild(dayElement);
    }
}

//显示上个月的日历
function lastMonth() {
    if (month > 1) {
        month -= 1;
    } else {
        month = 12;
        year -= 1;
    }
    showDate();
}

//显示下个月的日历
function nextMonth() {
    if (month < 12) {
        month += 1;
    } else {
        month = 1;
        year += 1;
    }
    showDate();
}
```

#### 新建并实现`calender.html`

在`web-frontend_basic_{fn_and_en}`文件夹下新建`calender.html`文件，参考内容如下：

```html {.line-numbers}
<!DOCTYPE html>
<html lang="zh">
<head>
    <title>{fn_and_en}设计并实现的电子日历</title>
    <meta charset="utf-8">
    <link href="css/calendar.css" rel="stylesheet">
    <script src="js/calendar.js"></script>
</head>
<body onload="showDate()">
<h1>{fn_and_en}设计并实现的电子日历</h1>
<hr/>
<div id="calendar">
    <!--状态栏-->
    <div>
        <!--显示上个月按钮-->
        <button onclick="lastMonth()">上个月</button>
        <!--显示当前年份和月份-->
        <div id="month"></div>
        <!--显示下个月按钮-->
        <button onclick="nextMonth()">下个月</button>
    </div>

    <!--显示星期几-->
    <div>
        <div class="everyday">日</div>
        <div class="everyday">一</div>
        <div class="everyday">二</div>
        <div class="everyday">三</div>
        <div class="everyday">四</div>
        <div class="everyday">五</div>
        <div class="everyday">六</div>
    </div>

    <!--显示当前月份每天的日期-->
    <div id="day"></div>
</div>
</body>
</html>
```

## 运行指引

使用`Chrome`或`Firefox`或`Edge`等当前主流浏览器打开``和`simple_calender.html`文件运行
