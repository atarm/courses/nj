# Startup

## Futhermore

### Standards and Specifications

<!-- {standards, specifications, conventions goes here} -->

### Documents and Supports

<!-- {trusted or official documents goes here} -->

### Manuals and CheatSheets

<!-- {manuals goes here} -->

### Books and Monographs

<!-- {publishes goes here} -->

### Courses and Tutorials

<!-- {systematic courses and tutorials goes here} -->

1. [深入理解javascript原型和闭包.](https://www.cnblogs.com/wangfupeng1988/p/3977924.html)

### Papers and Articles

<!-- {special topics goes here} -->

### Playgrounds and Exercises

<!-- {exercises here} -->

### Examples and Templates

<!-- {examples here} -->

### Auxiliaries and Tools

<!-- {auxiliaries here} -->

### Miscellaneous

<!-- {misc here} -->
