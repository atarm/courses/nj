---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_HyperText Markup Language(HTML)与Uniform Resource Locator(URL)_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# HyperText Markup Language(HTML), (HTTP)与Uniform Resource Locator(URL)

>1. [阮一峰. HTML教程.](https://wangdoc.com/html/url)
>1. [QuickRef.ME. HTML cheatsheet.](https://quickref.me/html)
>1. [DevDocs. HTML reference](https://devdocs.io/html/)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [目标Goals](#目标goals)
2. [HTML简介](#html简介)
3. [HTML基础](#html基础)
4. [URL简介](#url简介)
5. [URL的结构](#url的结构)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## 目标Goals

---

- **知识目标**：
  1. 了解`HTML`的历史
  2. 掌握`HTML`的基本结构
  3. 掌握`URL`的结构
- **能力目标**：
  1. 能自学拓展`HTML`和`URL`
  2. 能使用`HTML`编写简单的静态网页
  3. 能解释`URL`的各个组成部分及其作用
- **素养目标**：
  1. 良好的自学能力
  2. 良好的逻辑思维和结构化分析能力

---

<!--2nd leading page-->

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## HTML简介

---

### HTML是什么

1. `HTML`是一种标记语言（`Markup Language`），用于描述网页的结构和内容
2. `HTML`是描述类语言，不是编程语言（`Program Language`）
3. `HTML`通过`http/https`协议在`Browser`和`Server`之间传输
4. 通过`Browser`访问网站，从`Server`下载`HTML`，然后`Browser`解析`HTML`，渲染出网页

---

1. `HTML`最初既包括内容逻辑，也包括内容样式，后来逐渐演化为只包括内容逻辑，样式由`CSS`负责
2. `JavaScript`负责行为逻辑，最初只有客户端行为逻辑，后来增加客户端-服务端交互逻辑（`AJAX`），然后再扩展到服务端行为逻辑

---

### HTML的历史

1. `HTML 1.0`：1993年，`Tim Berners-Lee`发明`HTML`，`HTML`是`SGML`的子集，`HTML`的诞生标志着`WWW(World Wide Web, 万维网)`的诞生
2. `HTML 2.0`：1995-11-24
3. `HTML 3.2`：1997-01-14
4. `HTML 4.0`：1997-12-18
5. `HTML 4.01`：1999-12-24
6. `HTML 5.0`：2014-10-28

---

![height:550](./.assets/image/v2-78813e85a726647503fa655b9264990c_720w.png)

---

1. `HTML 5.0`: 2014-10-28
1. `HTML 5.1`: 2016-11-01
1. `HTML 5.2`: 2017-12-14

>[wiki. HTML.](https://en.wikipedia.org/wiki/HTML)

---

```html
<!DOCTYPE html> <!--文档类型声明Document Type Declaration-->
<html lang="en"><!--语言代码-->
<head>
    <meta charset="utf-8"/><!--字符集编码-->
    <title>hello</title>
</head>
<body>
    <p>Hello World</p>
</body>
</html>
```

---

```html {.line-numbers}
<!DOCTYPE html>
<html lang="en"><!--main language-->
<head>
    <meta charset="utf-8"/>
    <title>Multilingual Page</title>
</head>
<body>
    <p>Hello World</p> <!-- Inherits lang="en" from <html> -->
    <p lang="zh-CN">你好，世界</p> <!-- Explicitly sets lang="zh-CN" -->
    <p lang="es">Hola Mundo</p> <!-- Explicitly sets lang="es" -->
</body>
</html>
```

---

>XHTML is a separate language that began as a reformulation of HTML 4.01 using XML 1.0. It is now referred to as "the XML syntax for HTML" and no longer being developed as a separate standard.

---

1. `HTML 4.01`: 1999-12-24
1. `XHTML 1.0`: 2000-01-26
1. `XHTML 1.1`: 2001-05-31
1. `XHTML 2.0`: 2002-08-01, a working draft, abandoned in 2009 in favor of work on HTML5 and XHTML5

>1. [wiki. HTML.](https://en.wikipedia.org/wiki/HTML)
>1. [wiki. XHTML.](https://en.wikipedia.org/wiki/XHTML)

---

```html {.line-numbers}
<!--XHTML5 syntax-->
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <title>XHTML5 Example</title>
</head>
<body>
    <p>Hello World</p>
</body>
</html>
```

---

<!--2nd leading page-->

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## HTML基础

---

### 标签tag

1. `HTML`代码由不同的`tag`构成，形成树形结构（`tree`）
2. `tag`用于告诉`browser`如何处理该段代码，`browser`将`HTML`代码解析成一颗树结构，每个标签是树的一个节点（`node`），节点也称为元素（`element`）
3. `tag`和`element`基本同义，但`tag`更强调从代码文本的角度看，`element`更强调从代码结构的角度看，而`node`更强调从运行时内存结构的角度看
4. 学习`HTML`语言，也就是学习各种`tag`的用法

---

1. `tag`由`<`和`>`包围
2. `tag`可以不用空格、换行分隔，也可以不缩进，但建议使用空格、换行和缩进，以提高可读性
3. `tag`通常成对出现，`<xxx>`和`</xxx>`
4. `tag`也可以单独出现，`<xxx>`或`<xxx/>`
5. `tag`可以嵌套，但不能交叉，即`<a><b></a></b>`是错误的
6. `tag`可以有属性
7. `tag`大小写不敏感，但建议使用小写

---

### 块级元素与行内元素

1. 块级元素（`block`）：默认占据一个独立的区域，在网页上会自动另起一行，占据100%的宽度
2. 行内元素（`inline`）：默认与其他元素在同一行，不产生换行

---

### 属性

1. 属性（`attribute`）是标签的额外信息，使用空格与标签名和其他属性分隔
2. 属性由`name="value"`构成，属性值可以用双引号`"`或单引号`'`包围，也可以不用引号包围，但建议使用双引号包围

```html
<img src="demo.jpg" width="500">
```

---

### 基本结构

```html
<!DOCTYPE html><!--这是注释-->
<html lang="zh-CN">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
    </body>
</html>
```

---

1. `<!DOCTYPE html>`：文档类型声明，告诉`browser`使用`HTML5`解析`HTML`代码，按惯例，`<!DOCTYPE>`通常使用全大写
2. `<html>`：`HTML`文档的根元素，包含`<head>`和`<body>`两个子元素

---

1. `<head>`：容器标签（`container`）
   1. 用于放置网页的元信息，内容不会出现在网页上，而是为网页渲染提供额外信息
   2. `<head>`是`<html>`的第一个子元素。如果网页不包含`<head>`，`browser`会自动创建一个
2. `<body>`：容器标签
   1. 用于放置网页的主体内容，浏览器显示的页面内容，都放置在它的内部
   2. `<body>`是`<html>`的第二个子元素，紧跟在`<head>`之后

---

1. `<!-- -->`：注释，`browser`会忽略注释内容，以`<!--`开头，以`-->`结尾
2. 空格与换行处理：
   1. 标签内容的头部和尾部的空格，忽略不计
   2. 标签内容中的多个连续空格，合并成一个空格
   3. 标签内容中的换行符，合并成一个空格

```html
<p>  hello world   </p>
<p>hello      world</p>
<p>hello

world
</p>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## HTTP简介

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## HTTP基础

---

<!--2nd leading page-->

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## URL简介

---

1. 统一资源定位符（`Uniform Resource Locator`, `URL`）：也称为"网址"，表示互联网上的资源地址，如`https://www.example.com/path/index.html`
2. 资源（`resource`）：即可以通过互联网访问的文件，如网页、图像、音频、视频、脚本等
3. 链接（`link`）：网页中包含的`URL`

---

<!--2nd leading page-->

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## URL的结构

---

![height:500](./.assets/image/url-structure-composition.png)

> [Implementing Node.js URL parser in WebAssembly with Rust.](https://www.yagiz.co/implementing-node-js-url-parser-in-webassembly-with-rust)

---

![URI syntax diagram](./.assets/image/1068px-URI_syntax_diagram.svg.jpg)

`https://www.example.com:443/path/to/myfile.html?key1=value1&key2=value2#anchor`

>[wiki. URL.](https://en.wikipedia.org/wiki/URL)

---

``` {.line-numbers}
URI = scheme ":" ["//" authority] path ["?" query] ["#" fragment]
authority = [userinfo "@"] host [":" port]
```

1. 协议（`protocol`）/`scheme`
1. 用户（`user`）
1. 主机（`host`）
1. 端口（`port`）
1. 路径（`path`）
1. 查询字符串（`query string`）/查询参数（`query argument`）
1. 锚点（`anchor`）/片段（`fragment`）

---

### URL字符

1. `URL`只能使用`ASCII`内的`非URL保留字符`
   1. 英文字母（大写和小写）
   1. 阿拉伯数字
   1. 连词号（`-`）
   1. 下划线（`_`）
   1. 句点（`.`）

---

1. `ASCII`内的`URL保留字符`和非`ASCII`字符需要转义
   1. `URL`保留字符（`reserved character`）：只能出现在特定位置，如需表示保留字符本身需要进行转义，如：`foo?bar.html` --> `foo%3Fbar.html`

>[encodeURI().](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURI)

---

### 绝对URL与相对URL

1. 绝对URL（`absolute URL`）：只依靠`URL`本身就能定位资源的位置
2. 相对URL（`relative URL`）：根据另一个`URL`计算出来的`URL`，如：`/path/to/myfile.html` --> `https://www.example.com/path/to/myfile.html`
   1. 以斜杠（`/`）开头时，表示网站的根目录
   2. `.`表示当前目录
   3. `..`表示上级目录

---

1. `<base>`：指定网页内部的所有相对`URL`的计算基准
   1. 一个网页只能有一个`<base>`
   2. 只能放置在`<head>`里面

```html
<head>
    <base href="https://www.example.com/files/" target="_blank">
</head>
```

---

<!--end of slide page-->

<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
