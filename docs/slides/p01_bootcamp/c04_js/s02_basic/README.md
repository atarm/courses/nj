---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_ECMAScript 6_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# ECMAScript 6

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`let` and `const`](#let-and-const)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `let` and `const`

---

1. `browser`环境中，`let`和`const`定义的全局变量不会挂载到`window`对象上
2. `let`和`const`是块级作用域
3. `let`和`const`不会变量提升

---

```javascript {.line-numbers}
{
  let a = 10;
  var b = 1;
}

console.log(a); // ReferenceError: a is not defined.
console.log(b); // 1
```

---

1. `for`循环中`()`内是一个父作用域，循环体内部是一个单独的子作用域

```javascript {.line-numbers}
for (let i = 0; i < 3; ++i) {
    let i = 'abc';
    console.log(i); // abc
}
```

---

1. 暂时性死区（temporal dead zone，TDZ）：ES6明确规定，如果区块中存在let和const命令，这个区块对这些命令声明的变量，从一开始就形成了封闭作用域。凡是在声明之前就使用这些变量，就会报错。即，在代码块内，使用let命令声明变量之前，该变量都是不可用的。
1. "暂时性死区"也意味着typeof不再是一个百分之百安全的操作，这样的设计是为了让大家养成良好的编程习惯，变量一定要在声明之后使用，否则就报错。

---

```javascript {.line-numbers}
if (true) {
  // TDZ开始
  tmp = 'abc'; // ReferenceError
  console.log(tmp); // ReferenceError

  let tmp; // TDZ结束
  console.log(tmp); // undefined

  tmp = 123;
  console.log(tmp); // 123
}
```

```javascript {.line-numbers}
function bar(x = y, y = 2) {
  return [x, y];
}

bar(); // 报错
```

---

```javascript {.line-numbers}
typeof x; // ReferenceError
let x;

typeof undeclared_variable // "undefined"
```

---

1. let不允许在相同作用域内，重复声明同一个变量
1. 块级作用域的出现，实际上使得获得广泛应用的匿名立即执行函数表达式（匿名 IIFE）不再必要
1. ES6 引入了块级作用域，明确允许在块级作用域之中声明函数。ES6 规定，块级作用域之中，函数声明语句的行为类似于let，在块级作用域之外不可引用。

```javascript {.line-numbers}
// IIFE 写法
(function () {
  var tmp = ...;
  ...
}());

// 块级作用域写法
{
  let tmp = ...;
  ...
}
```

---

1. ES6 的块级作用域必须有大括号，如果没有大括号，JavaScript 引擎就认为不存在块级作用域。

```javascript {.line-numbers}
// 第一种写法，报错
if (true) let x = 1;

// 第二种写法，不报错
if (true) {
  let x = 1;
}
```

---

1. `const`定义的变量值不可变，但变量指向的对象的值可变
1. 如果希望将对象冻结，应使用`Object.freeze()`

```javascript {.line-numbers}
const a = [];
a.push('Hello'); // 可执行
a.length = 0;    // 可执行
a = ['Dave'];    // 报错
```

---

```javascript {.line-numbers}
const foo = Object.freeze({});

// 常规模式时，下面一行不起作用；
// 严格模式时，该行会报错
foo.prop = 123;
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
