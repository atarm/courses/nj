---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "__"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Objects: `function` and `object`

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction](#introduction)
2. [`function`](#function)
3. [`object`](#object)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction

---

### 基础模块结构Basic Module Structure：函数与对象

1. 可重用性
2. 可维护性
3. 名字空间

⚠️ 本节的"模块"指的是广义的"模块" ⚠️

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `function`

1. 函数`function`：一组可反复调用的语句块，可以接受参数，可以返回值
2. 函数如果没有`return`语句，返回值为`undefined`
3. `return`语句只返回一个值，如果有多个值，可以返回一个数组

>[阮一峰. 函数.](https://wangdoc.com/javascript/types/function)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 定义函数

1. `function define`函数定义
2. `anonymous function`匿名函数，或称为`function expression`函数表达式
3. `function constructor`函数构造器
4. `arrow function`箭头函数（`ES6`）

---

#### `function define`

```javascript {.line-numbers}
// function关键字
function print(s) {
    console.log(s);
    return s;
}
```

---

#### `anonymous function`

```javascript {.line-numbers}
// anonymous function
let print = function(s) {
    console.log(s);
    return s;
};
```

```javascript {.line-numbers}
// the function name 'x' is only visible inside the function body
let print = function x(){
  console.log(typeof x);    // function
};

x();     // ReferenceError: x is not defined

print(); // function
```

---

#### `function constructor`

```javascript {.line-numbers}
// function constructor
var add = new Function(
  'x',
  'y',
  'return x + y'
);

// equal to
function add(x, y) {
  return x + y;
}
```

```javascript {.line-numbers}
new Function(arg1, arg2, /* …, */ argN, functionBody)

/*
functionBody: A string containing the JavaScript statements comprising the function definition.
*/
```

---

#### `arrow function`箭头函数

```javascript {.line-numbers}
x => x * x

// 等价于
function (x) {
    return x * x;
}
```

>1. [廖雪峰. JavaScript教程-箭头函数.](https://www.liaoxuefeng.com/wiki/1022910821149312/1031549578462080)

---

```javascript {.line-numbers}
(param1, param2, …, paramN) => { statements }
(param1, param2, …, paramN) => expression
//相当于：(param1, param2, …, paramN) =>{ return expression; }

// 只有一个参数时，圆括号可选
(singleParam) => { statements }
singleParam => { statements }

// 无参时应该写成一对圆括号
() => { statements }
```

>1. [MDN. 箭头函数.](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/Arrow_functions)

<!--

---

```javascript {.line-numbers}
function getSum(){
    let sumNum = 0;
    for (var i = 1; i <= 100; i++) {
        sumNum += i;
    }
    console.log(sumNum);
}
// 调用函数
getSum();
```
-->

---

#### `arrow function`的作用

1. 简化语法
2. 重新定义`this`的指向：总是指向定义时包围`arrow function`的对象

---

1. 箭头函数内部的`this`是词法作用域`lexical scope`，由上下文确定（即，外层），而不是调用时确定

---

```javascript {.line-numbers}
const obj = {
  name: 'Alice',
  sayHello: function() {
    setTimeout(() => {
      console.log(`Hello, ${this.name}!`); // `this` is obj
    }, 1000);
  }
};

obj.sayHello(); // Hello, Alice!
```

---

```javascript {.line-numbers}
const obj = {
  name: 'Alice',
  sayHello: function() {
    setTimeout(function() {
      console.log(`Hello, ${this.name}!`); // `this` is top-level object
    }, 1000);
  }
};

obj.sayHello(); // Hello, undefined!
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 参数传递

1. 实参`argument`：函数调用时，用于传递给被调函数的参数
2. 形参`parameter`：函数定义时，用于接收主调函数传递的参数
3. 实参和形参的个数可以不一致
    1. 实参个数大于形参个数：多余的实参被忽略（可通过`arguments[]`访问）
    2. 实参个数小于形参个数：多余的形参为`undefined`

---

```javascript {.line-numbers}
function sum(num1, num2) {
    console.log(num1 + num2);
}
sum(100, 200);
sum(100, 400, 500, 700);
sum(200);
```

---

1. 值传递`pass by value`：原始类型（`Number`、`String`、`Boolean`），修改 **形参值**，不影响 **实参值**
1. 引用传递`pass by reference`：复合类型，修改 **形参的元素的值**，影响 **实参的元素的值**

⚠️ 事实上，`JavaScript`与`Java`一样只有传值传递，其引用传递是通过指针方式模拟成的引用传递，`C++`有引用传递 ⚠️

---

![image-20230906162301816](./.assets/image/image-20230906162301816.png)

---

![image-20230906162346717](./.assets/image/image-20230906162346717.jpg)

---

![image-20230906162415407](./.assets/image/image-20230906162415407.jpg)

---

```javascript {.line-numbers}
let v = 2;

function f1(p) {
    p = 3;
}
f1(v);

console.log(v); // 2
```

```javascript {.line-numbers}
let obj = { p: 2 };

function f2(o) {
  o.p = 3;
}
f2(obj);

console.log(obj.p); // 3
```

---

```javascript {.line-numbers}
/*形参指向新对象，不改变实参指向的对象*/
let arr = [1, 2, 3];

function f3(o) {
  o = [2, 3, 4];
}
f3(arr);

console.log(arr); // [1, 2, 3]
```

---

1. `arguments`对象：可用于传递不定数量的参数，类似于形参形成的数组
2. `...args`：`ES6`引入的`rest parameter`，用于接收不定数量的参数，类似于`arguments`对象

```javascript {.line-numbers}
let f = function(a, b) {
    arguments[0] = 3;
    arguments[1] = 2;
    console.log(arguments[2])
    return a + b;
}

f(1, 1, 10) // 5
```

---

```javascript {.line-numbers}
function sum(...theArgs) {
  let total = 0;
  for (const arg of theArgs) {
    total += arg;
  }
  return total;
}

console.log(sum(1, 2, 3));
// Expected output: 6

console.log(sum(1, 2, 3, 4));
// Expected output: 10
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 作用域

1. 作用域`scope`：标识符的有效范围，即，标识符的可见域
    1. 全局作用域`global scope`：可在全局范围访问的标识符
    2. 函数作用域`function scope`：在函数内定义的标识符，只能在函数内访问
    3. 块级作用域`block scope`（`es6`新增）：在代码块内定义的标识符，只能在代码块内访问

>[阮一峰. 函数作用域.](https://wangdoc.com/javascript/types/function#%E5%87%BD%E6%95%B0%E4%BD%9C%E7%94%A8%E5%9F%9F)

---

1. 模块作用域（Module Scope）：在ES6中引入的模块系统中，每个模块都有自己的作用域。模块中的变量、函数等不会污染全局作用域。
2. 动态作用域（Dynamic Scope）： JavaScript本身没有动态作用域，但在某些情况下（例如使用with语句或eval函数）可以模拟动态作用域。
3. 词法作用域（Lexical Scope）：JavaScript采用词法作用域（也称为静态作用域），即作用域在代码编写时就已经确定，而不是在代码执行时确定。词法作用域通过嵌套函数和闭包来实现。
4. 私有作用域（Private Scope）：使用立即调用函数表达式（IIFE）或ES6中的类和模块，可以创建私有作用域，防止变量和函数泄漏到全局作用域。

---

```javascript {.line-numbers}
// 模块作用域
// module1.js
export const a = 1;

// module2.js
import { a } from './module1.js';
console.log(a); // 1

// 词法作用域
function outer() {
    const outerVar = 'I am outside!';
    function inner() {
        console.log(outerVar); // 'I am outside!'
    }
    inner();
}
outer();

// 私有作用域
(function() {
    const privateVar = 'This is private';
    console.log(privateVar); // 'This is private'
})();
console.log(typeof privateVar); // 'undefined'
```

---

1. `js`引擎在运行代码时分为两步：预解析和代码执行
2. 提升hoisting
    1. 变量预解析/变量提升：函数内，使用`var`声明的变量会被提升到函数作用域的顶部，变量被提升后，会给变量设置默认值为`undefined`（⚠️ `var`的变量提升被视为是`js`最初的设计缺陷，`es6`的`let`和`const`不进行变量提升 ⚠️）
    2. 函数预解析/函数提升：函数的声明被提升到 **当前作用域** 的最上面（但不会调用该函数）

>[浅谈JavaScript变量提升.](https://juejin.cn/post/7007224479218663455)

<!-- ---

```javascript {.line-numbers}
console.log(a);
var a = 1;
console.log(a);
```

---

```javascript {.line-numbers}
console.log(a);
var a = 1;
console.log(a);
function a(){
    return false;
}
```

---

```javascript {.line-numbers}
function f(){
    console.log(a);
    var a = 1;
    console.log(a);
}

f();
``` -->

---

```javascript {.line-numbers}
function f(){
    console.log(a); // [Function: a]
    function a(){
        return false;
    }
    var a = 1;
    console.log(a); // 1
}

f();
```

---

```javascript {.line-numbers}
function f(){
    console.log(a); // [Function: a]
    var a = 1;
    console.log(a); // 1
    function a(){
        return false;
    }
}

f();
```

---

```javascript {.line-numbers}
function f(){
  for(var i= 0;i<5;++i){
  }
  console.log(i);   // 5
}

f();
```

---

#### 作用域链与闭包

1. 作用域链：内层可访问外层定义的变量的机制
1. 闭包`closure`：能读取其他函数定义的标识符的函数，或者理解成"定义在一个函数内部的函数"

---

```javascript {.line-numbers}
function createIncrementor(start) {
  return function () {  // closure, remember the value of start
    return start++;
  };
}

var inc = createIncrementor(5);

console.log(inc()); // 5
console.log(inc()); // 6
console.log(inc()); // 7
```

---

```javascript {.line-numbers}
function Person(name) {
    var _age;
    function setAge(n) {
        _age = n;
    }
    function getAge() {
        return _age;
    }

    return {
        name: name,
        getAge: getAge,
        setAge: setAge
    };
}

var p1 = Person('张三');
p1.setAge(25);
console.log(p1.getAge()); // 25
```

<!-- ---

##### 立即执行函数表达式（Immediately Invoked Function Expression, IIFE）

>[阮一峰. 立即调用的函数表达式（IIFE）.](https://wangdoc.com/javascript/types/function#%E7%AB%8B%E5%8D%B3%E8%B0%83%E7%94%A8%E7%9A%84%E5%87%BD%E6%95%B0%E8%A1%A8%E8%BE%BE%E5%BC%8Fiife) -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `object`

>1. [廖雪峰. JavaScript教程 - 面向对象编程.](https://www.liaoxuefeng.com/wiki/1022910821149312/1023022126220448)

---

### JavaScript的对象

1. $\text{object} = \text{attribute} + \text{method}$，attribute也称为property
1. 内置对象`built-in object`：`Object`、`Array`、`Function`、`Date`、`RegExp`、`Error`等
1. 宿主对象`host object`：由`JavaScript`运行时环境提供的对象，如`DOM`和`BOM`等
1. 自定义对象`custom object`：由`Object`构造方法或者自定义构造方法创建的对象

---

1. `object`是`JavaScript`的核心概念，也是最重要的数据类型
1. `object`是一个`key-value`的集合，是一种无序的复合数据集合
    1. `key`均是`string`（`es6`引入`symbol`也可以作为`key`）
    1. 定义`object`时，`key`既可以加引号，也可以不加引号，`key`如果即不是数值，也不符合`identifier`要求，则必须加引号
1. `object`的属性之间使用`,`分隔，最后一个属性后面可以加`,`也可不加
1. `object`的属性可动态创建，不必在定义时就创建

---

```javascript {.line-numbers}
var obj = {
    1: 'a',
    3.2: 'b',
    1e2: true,
    1e-2: true,
    .234: true,
    0xFF: true,
    //1p: 'Hello World'    // 报错，应为 '1p': 'Hello World'
};

obj
// Object {
//   1: "a",
//   3.2: "b",
//   100: true,
//   0.01: true,
//   0.234: true,
//   255: true
// }

obj['100'] // true
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 创建对象

1. `object literal`对象字面量：`{}`内包含对象的属性和方法
2. `Object.constructor()`对象构造方法：`new Object()`，实际上是`{}`的语法糖
3. `constructor()`构造方法：构造方法首字母通常大写，构造方法不需要`return`，使用`new`关键字调用构造方法

---

#### `object Literal`

```javascript {.line-numbers}
var star = {
    name : '张三',
    age : 28,
    gender : '男',
    sayHi : function(){
        alert('大家好');
    }
};
console.log(star.name)
console.log(star['name'])    // 关联数组
star.sayHi();
```

---

```javascript {.line-numbers}
var obj = {
    foo: 'Hello',
    bar: 'World'
};
obj.zoo = '!';
console.log(obj);
```

---

##### `new`

1. 在内存中分配空间，将`this`指向新分配的内存空间
1. 执行构造方法，返回`this`对象（`constructor`不需要`return`）

---

#### `Object.constructor()`

```javascript {.line-numbers}
var andy = new Object();
andy.name = '张三';
andy.age = 28;
andy.gender = '男';
andy.sayHi = function(){
    alert('大家好');
}
```

---

#### `constructor`

```javascript {.line-numbers}
function Person(name, age, gender) {
    this.name = name;
    this.age = age;
    this.gender = gender;
    this.sayHi = function() {
        console.log('我的名字叫：' + this.name + '，年龄：' + this.age + '，性别：' + this.gender);
    }
}

Person.prototype.toString = function (){
    return '(' + this.name + ', ' + this.age + ', ' + this.gender + ')';
}

var p1 = new Person('abc', 100, '男');
var p2 = new Person('def', 21, '男');
console.log(p1.name);                          // abc
console.log(p2.name);                          // def
console.log(p1.toString());                    // (abc, 100, 男)
console.log(p1.sayHi === p2.sayHi);            // false
console.log(p1.toString === p2.toString);      // true
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 对象的属性

---

#### 属性的读取

1. `object.property`：`property`为合法的标识符
2. `object['property']`：`property`为合法的或不合法的标识符
    1. `property`为数值时可以不加引号（数值不能作为变量名，且会自动转成`string`）
    2. 除数值外，`property`必须在引号内（否则会被当作变量处理）

```javascript {.line-numbers}
var obj = {
    p: 'Hello World'
};

console.log(obj.p);        // "Hello World"
console.log(obj['p']);     // "Hello World"
```

---

#### 属性的赋值

1. `object.property = value`
1. `object['property'] = value`

```javascript {.line-numbers}
var obj = {};

obj.foo = 'Hello';
obj['bar'] = 'World';
```

---

#### 属性的查看与遍历

1. `in`运算符：`key in object`，返回`true`或`false`
1. `Object.hasOwnProperty()`：返回一个布尔值，判断某个属性是否定义在对象自身（`true`）
1. `Object.keys()`：返回一个数组，包含对象自身的所有 **_可遍历_** 的属性的`key`
1. `for...in`：遍历对象自身的和继承的 **_可枚举_** 的属性

---

```javascript {.line-numbers}
var obj = {
    key1: 1,
    key2: 2
};

console.log(Object.keys(obj));// ['key1', 'key2']
console.log('key1' in obj);   // true
console.log(obj.hasOwnProperty('toString'));// false
```

---

```javascript {.line-numbers}
var obj = {a: 1, b: 2, c: 3};

for (let i in obj) {
  console.log('键名：', i);
  console.log('键值：', obj[i]);
}
```

---

#### 属性的删除

1. `delete`用于删除对象的属性，删除成功后返回`true`
1. 删除一个不存在的属性，`delete`不报错，而且返回`true`
1. 属性存在，且不得删除时，`delete`才返回`false`
1. `delete`不能删除`object`继承而来的属性

```javascript {.line-numbers}
var obj = { p: 1 };
Object.keys(obj) // ["p"]

console.log(delete obj.p);    // true
console.log(obj.p)            // undefined
console.log(Object.keys(obj)) // []
console.log(delete obj.p);    // true
```

---

```javascript {.line-numbers}
Object.defineProperty(obj, prop, descriptor);
```

```javascript {.line-numbers}
var obj = Object.defineProperty({}, 'p', {
  value: 123,
  configurable: false
});

console.log(obj.p);         // 123
console.log(delete obj.p);  // false
```

---

```javascript {.line-numbers}
var obj = {};

console.log(delete obj.toString)  // true
console.log(obj.toString)         // function toString()
```

---

#### `with`语句

```javascript {.line-numbers}
var obj = {
    p1: 1,
    p2: 2,
};
with (obj) {
    p1 = 4;
    p2 = 5;
}
// 等同于
obj.p1 = 4;
obj.p2 = 5;
```

---

1. `with`块内有变量的赋值操作时，如果当前对象不存在该属性，则，定义一个当前作用域的变量（`with`不改变作用域）

```javascript {.line-numbers}
var obj = {};

with (obj) {
    p1 = 4;
    p2 = 5;
}

console.log(obj.p1);  // undefined
console.log(p1);      // 4
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `prototype-based` and `class-based`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `prototype-based`基于原型

<!-- ---

```javascript {.line-numbers}
var Student = {
    name: 'Robot',
    height: 1.2,
    run: function () {
        console.log(this.name + ' is running...');
    }
};

var xiaoming = {
    name: '小明'
};

xiaoming.__proto__ = Student;
```

---

![height:400](./.assets/image/l.png) -->

---

#### 原型与原型链

1. `JavaScript`不区分`class`和`instance`，而是通过`prototype`对象（`prototype`也是一个对象）来实现面向对象编程
1. `JavaScript`的继承关系是将一个对象的原型指向另一个对象，即`prototype chain`
1. `obj.xxx`访问一个对象的属性时，先在当前对象上查找，如果未找到，就到其原型对象上找，如果仍未找到，则一直上溯到`Object.prototype`对象（`Object.prototype`是`null`
），最后，如果还没有找到，刚返回undefined

---

1. 原型对象定义了所有实例对象共享的属性和方法，实例对象可以视作从原型对象衍生出来的子对象。
1. 按照约定，构造方法首字母应当大写，而普通函数首字母应当小写
<!-- 1. JavaScript对每个创建的对象都会设置一个原型，指向它的原型对象。
1. 如果原型链很长，那么访问一个对象的属性就会因为花更多的时间查找而变得更慢

---

1. prototype对象有一个constructor属性，默认指向prototype对象所在的构造函数 -->

---

```javascript {.line-numbers}
function Student(name) {
    this.name = name;
    this.hello = function () {
        console.log('Hello, ' + this.name + '!');
    }
}

let xiaoming = new Student('小明');
xiaoming.hello();                                             // Hello, 小明!
let xiaohong = new Student('小红');
xiaohong.hello();                                             // Hello, 小红!

console.log(xiaoming.constructor === Student.prototype.constructor);  // true
console.log(Student.prototype.constructor === Student);               // true
console.log(Object.getPrototypeOf(xiaoming) === Student.prototype);   // true
console.log(xiaoming instanceof Student);                             // true

console.log(xiaoming.hello === xiaohong.hello);                       //false
```

---

![protos](./.assets/image/l-20231009161827315.png)

---

```javascript {.line-numbers}
function Student(name) {
    this.name = name;
}

Student.prototype.hello = function () {
        console.log('Hello, ' + this.name + '!');
}

var xiaoming = new Student('小明');
xiaoming.hello();                                             // Hello, 小明!
var xiaohong = new Student('小红');
xiaohong.hello();                                             // Hello, 小红!

console.log(xiaoming.constructor === Student.prototype.constructor);  // true
console.log(Student.prototype.constructor === Student);               // true
console.log(Object.getPrototypeOf(xiaoming) === Student.prototype);   // true
console.log(xiaoming instanceof Student);                             // true

console.log(xiaoming.hello === xiaohong.hello);                       //true
```

---

![protos2](./.assets/image/l-20231009162036883.png)

---

#### 基于原型的继承

1. [阮一峰. JavaScript教程 - 对象的继承.](https://wangdoc.com/javascript/oop/prototype)
1. [廖雪峰. JavaScript教程 - 原型继承.](https://www.liaoxuefeng.com/wiki/1022910821149312/1023021997355072)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `class-based`基于类

1. `JavaScript`的`class-based`类似于其他面向对象编程语言
1. `es6`引入了`class`这个概念，作为`object`的模板
1. 虽然只是一个语法糖，但可以以更加清晰、更加面向对象的方式编程

>1. [阮一峰. Class的基本语法.](https://wangdoc.com/es6/class)
>1. [阮一峰. Class的继承.](https://wangdoc.com/es6/class-extends.html)

---

1. 用`class{}`包裹原构造函数+原型对象方法
2. 原构造函数名升级为整个class名字，所有构造函数统一命名为`constructor`
3. 原型对象中的方法，不再加`prototype`前缀，也不用`=function`，直接简写为`方法名()`
4. 直接定义在class{}内的方法，保存在原型对象中

---

#### `es2022`之前

```javascript {.line-numbers}
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    toString() {
        return '(' + this.x + ', ' + this.y + ')';
    }
}

console.log(typeof Point);                          // "function"
console.log(Point === Point.prototype.constructor)  // true

let point = new Point(2, 3);
```

---

#### `es2022`

```javascript {.line-numbers}
class Point {
    x = 0;
    y = 0;
    constructor(x, y) {
        this.x = x || 0;
        this.y = y || 0;
    }

    toString() {
        return '(' + this.x + ', ' + this.y + ')';
    }
}

console.log(typeof Point);                          // "function"
console.log(Point === Point.prototype.constructor)  // true

let point = new Point(2, 3);
```

---

#### 静态方法和静态属性

```javascript {.line-numbers}
class Foo {
    static classMethod() {
        return 'hello';
    }
        static prop = 1;
}

console.log(Foo.classMethod()); // 'hello'
console.log(Foo.prop);          // 1

var foo = new Foo();
foo.classMethod(); // TypeError: foo.classMethod is not a function
```

---

```javascript {.line-numbers}
// 静态方法和静态属性与以下写法等价
class MyClass {}
MyClass.say = function() {}
```

```javascript {.line-numbers}
class MyClass {
    static say() {
        alert("yes");
    }
}
// 静态方法和静态属性可被动态修改
MyClass.say = function() {
    alert("no");
}
```

---

#### 私有方法和私有属性（`es2022`）

1. `#`：私有属性和私有方法的标识符

---

```javascript {.line-numbers}
class IncreasingCounter {
  #count = 0;

  get value() {
    console.log('getting the value...');
    return this.#count;
  }
  set value(value){
    console.log('setting the value...');
    this.#count = value;
  }
  increment(){
    this.#increment();
  }
  #increment() {
    this.#count++;
  }
}

let counter = new IncreasingCounter();
console.log(counter.value);
counter.value = 100;
console.log(counter.value);
counter.increment();
console.log(counter.value);
//console.log(counter.#count);        // 报错
//counter.#increment();               // 报错
```

---

#### `getter`和`setter`

1. 使用`get`和`set`关键字，对某个属性设置存值函数和取值函数，拦截该属性的存取行为

---

```javascript {.line-numbers}
class MyClass {
    constructor() {
        this.value = 100;
    }
    static svalue = 200;
    get prop() {
        return 'getter: ' + this.value;
    }
    set prop(value) {
        this.value = value;
        console.log('setter: '+ this.value);
    }
    static get sprop(){
        // `this` point to `MyClass`
        // the same as `return 'static getter: ' + MyClass.svalue;`
        return 'static getter: ' + this.svalue;
    }
    static set sprop(value){
        this.svalue = value;
        console.log('static setter: '+ this.svalue);
    }
}

let inst = new MyClass();

console.log(inst.prop);   // getter: 100
inst.prop = 123;          // setter: 123
console.log(inst.prop);   // getter: 123

console.log(MyClass.sprop); // static getter: 200
MyClass.sprop = 300;        // static setter: 300
console.log(MyClass.sprop); // static getter: 300
```

---

#### 继承

```javascript {.line-numbers}
class Point { /* ... */ }

class ColorPoint extends Point {
  constructor(x, y, color) {
    super(x, y); // 调用父类的constructor(x, y)
    this.color = color;
  }

  toString() {
    return this.color + ' ' + super.toString(); // 调用父类的toString()
  }
}
```

---

1. `ES6`规定，子类必须在`constructor`中首先调用父类的`constructor`，否则报错

```javascript {.line-numbers}
class Point { /* ... */ }

class ColorPoint extends Point {
  constructor() {
  }
}

let cp = new ColorPoint(); // ReferenceError
```

---

1. 父类所有的属性和方法，都会被子类继承，除了私有属性和私有方法
1. 父类的静态属性和静态方法，也会被子类继承

---

```javascript {.line-numbers}
class Foo {
  #p = 1;
  #m() {
    console.log('hello');
  }
}

class Bar extends Foo {
  constructor() {
    super();
    console.log(this.#p);  // 报错
    this.#m();             // 报错
  }
}
```

---

```javascript {.line-numbers}
class A {
  static hello() {
    console.log('hello world');
  }
}

class B extends A {
}

B.hello()  // hello world
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
