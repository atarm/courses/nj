---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_JavaScript_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# JavaScript

>1. [阮一峰. JavaScript教程.](https://wangdoc.com/javascript)
>1. [廖雪峰. JavaScript教程.](https://www.liaoxuefeng.com/wiki/1022910821149312)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [简介Introduction](#简介introduction)
2. [基本语法Basic Syntax](#基本语法basic-syntax)
3. [标准库Standard Library](#标准库standard-library)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 简介Introduction

---

### JavaScript是什么类型的编程语言

1. `JavaScript`是一种脚本语言/解释型语言（`Script Language`/`Interpreted Language`），由解释器/解释引擎/宿主环境/运行时环境进行逐行解释执行
2. `JavaScript`是一种弱类型语言（`Weak Typing Language`）/动态类型语言（`Dynamic Typing Language`）：运行时确定变量存储的数据的类型

---

1. `JavaScript`是一种多范式语言（`Multi-paradigm`）：面向过程编程（`POP, Process Oriented Programming`）、基于对象编程（`Object Based Programming`）/面向对象编程（`OOP, Object Oriented Programming`）、函数式编程（`FP, Functional Programming`）
2. `JavaScript`最初被视为玩具语言，主要用于客户端网页的简单交互和动画效果等有限的功能，随着`AJAX`出现逐步被用于开发复杂的网页应用，`Node.js`更是将`JavaScript`推向了服务器端

<!--
1. `JavaScript`是一种嵌入型语言（`Embedded`），语言本身不提供`IO`，需要依赖运行时环境
1. `JavaScript`是一种对象模型语言（`Object Model`），运行时环境通过对象模型，描述自己提供的功能和操作接口
-->

---

![width:800](./.assets/diagram/javascript_vs_node.jpg)

---

### 浏览器运行时环境WEB-API

1. `DOM(Document Object Model)`文档对象模型
2. `BOM(Browser Object Model)`浏览器对象模型
3. `event binding`事件绑定
4. `ajax(Asynchronous JavaScript and XML)`
5. ...

$\text{JavaScript} = \text{ECMAScript} + \text{web-api}$

---

1. 渲染引擎：用来解析渲染`HTML`与`CSS`，比如`Chrome`的`Blink`（`WebKit`的分支）
1. 解释引擎：用来解析执行`JavaScript`代码，比如`Chrome`的`V8`

---

### 服务器运行时环境NODE-API

1. 文件系统操作
1. 网络通信
1. 数据库系统操作
1. 其他

$\text{node} = \text{ECMAScript} + \text{node-api}$

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### JavaScript的历史

---

#### Part One: JavaScript VS Java

1. 1995：`Netscape（网景）`凭借`Netscape Navigator`成为`Web`时代开启时最著名的第一代互联网公司
2. 1995：`Netscape`希望能在静态`HTML`页面上添加一些动态效果，`Netscape`与`Sun`公司合作，计划在`Netscape Navigator`中嵌入`Java Applet`以增强该功能，但`Java`过于庞大
3. 1995-05：`Netscape`公司的`Brendan Eich`以`Scheme`（`Lisp`的两种主要方言之一，函数式语言）为蓝本，使用10天时间设计并实现`JavaScript`的第一个版本，命名为`Mocha`
4. 1995-09：改名为`LiveScript`

---

1. 1995-12：`Netscape`与`Sun`公司达成协议，允许将`LiveScript`改名为`JavaScript`，因此，`Netscape`公司可以借助`Java`的声势，将`LiveScript`推向市场，`Sun`公司也能将自己的影响力扩展到浏览器（最终失败）
1. 1995-12-04：`Netscape`与`Sun`联合发布`JavaScript`，对外宣称`JavaScript`是`Java`的补充，属于轻量级的`Java`，专门用于操作网页
1. 1996-03：`Navigator 2.0`正式内置`JavaScript`

>[阮一峰. JavaScript语言的历史.](https://wangdoc.com/javascript/basic/history)

---

1. `JavaScript`和`Java`是两种不同的语言
2. 名字类似，语法 **_有些_** 类似，命名规范 **_有些_** 类似
3. 基本上，`JavaScript`这个名字的原意是"一个很像`Java`的脚本语言"

---

#### Part Two: JavaScript VS ECMAScript

1. 1996-08：`Microsoft`模仿`JavaScript`开发相近的语言`JScript`并内置于`IE 3.0`
2. 1996-11
    1. `Netscape`将`JavaScript`提交给`ECMA`（`European Computer Manufacturers Association`，欧洲计算机制造商协会）并联合几个公司（`microsoft`, `google`等），希望`JavaScript`能够成为国际标准
    2. `ECMA`的第39号技术专家委员会（Technical Committee 39，简称`TC39`）负责制订`ECMAScript`（简称`ES`）标准

---

1. 1997-07：`ECMA`发布`ES 1.0`标准，基本上参考`JavaScript`的实现,`Java`是`Sun`的商标、`JavaScript`是`Netscape`的商标
    1. 一方面，根据授权协议，只有`Netscape`可以合法地使用`JavaScript`这个名字（`Sun公司`于1995年申请`JavaScript`的商标，于2000年获批）
    2. 另一方面，`ECMA`希望体现这门语言的制定者是`ECMA`，而不是`Netscape`，因此将`JavaScript`改名为`ECMAScript`

>[JavaScript 之父联手近万名开发者集体讨伐 Oracle：给 JavaScript 一条活路吧！. 2024-09-19.](https://mp.weixin.qq.com/s/vZEPAsAyEMh_h1rH2IumBg)

---

2. **`ES`只标准化语言标准，与运行时环境相关的标准由其他标准规定**，例如，`DOM`的标准由`W3C`组织制定
3. `JS`是`ES`的一种实现（`Dialect`），其他著名的实现有`Microsoft`的`JScript`、`Adobe`的`ActionScript`

在大多数情况下，也将`JS`指代`ES`

---

1. 1998-06：`ES 2.0`发布
1. 1999-12：`ES 3.0`发布，成为通行标准，得到广泛支持
1. 2001：`IE 5`支持`XMLHttpRequest`（`AJAX`）
1. 2007-10：`ES 4.0`草案发布，原预计2008-10发布正式版，但由于过于激进和多方势力角逐而未能通过
    1. 涉及现有功能改善的一小部分发布为`ES 3.1`，不久，`ES 3.1`改名为`ES 5`
    2. 激进的部分则进入冷冻期，成为`ES 6`的主要内容
2. 2009-10：`ES 5.0`发布，`ES 5`与`ES 3`基本保持兼容

---

1. 2015-06：`ES 6(es6)`发布，目标是使得可以用来编写复杂的大型应用程序，成为企业级开发语言
    1. `es6`新增的重要特性包括`class`, `module`, `arrow function`, `async/await`, `Promise`, `symbol`, `generator`, `let`, `const`, `spread operator`等
    2. `es6`的原意指`es5`的下一个版本
    3. `es6`更名为`ES 2015`，计划每年发布一个版本，版本号以年份命名，版本升级成为了一个不断滚动的过程
    4. `es6`既是一个历史名词，也是一个泛指，在不太严格的语境下，`es6`泛指`ES 2015`及其后续版本

>1. [阮一峰. ECMAScript6简介.](https://wangdoc.com/es6/intro)
>1. [MDN. JavaScript技术概览.](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/JavaScript_technologies_overview)

---

#### Part Three: JavaScript VS TypeScript

1. `TypeScript(TS)`是由`Microsoft`开发的一种`JS`的超集编程语言，主要提供了类型系统
2. 2012年`Microsoft`宣布推出`TS`语言，其设计者是`Anders Hejlsberg`（`Turbo Pascal`、`Delphi`、`C#`）
3. `TS`的最初动机是减少`.Net`程序员的转移和学习成本（`TS`既能让`JS`程序员快速上手，也能让`.Net`程序员感到熟悉）

>[阮一峰. TypeScript语言简介.](https://wangdoc.com/typescript/intro)

---

1. `ES`的运行时环境（`Browser`或`Node`）不认识`TS`
2. `TS`官方没有提供运行时环境，只提供编译器`tsc`（一个`npm`包），需要将`TS`进行编译后运行

```bash {.line-numbers}
$ npm install -g typescript
$ tsc --help
```

```bash {.line-numbers}
$ npm install -g ts-node
$ ts-node --help
```

---

1. `TypeScript`的类型检查是编译时类型检查，不支持运行时类型检查

```typescript {.line-numbers}
function add(a: number, b: number): number {
    return a + b;
}

// 编译报错
// 如果忽略编译错误，运行时不报错
const result = add(10, "20");
console.log(result);
// ts-node --transpile-only type_check.ts
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 混乱复杂而异常成功的JavaScript

---

#### 混乱复杂

1. 历史包袱：JavaScript在1995年由`Brendan Eich`在短短10天内设计出来，初期设计和实现时间非常短，导致一些设计上的妥协和不一致
2. 向后兼容：为了保持与早期版本的兼容性，JavaScript保留了一些不一致和奇怪的行为。这些行为在现代开发中可能显得不合理，但为了不破坏已有的代码，必须保留
3. 动态类型：JavaScript是动态类型语言，这意味着变量可以在运行时改变类型。这种灵活性虽然强大，但也带来了很多潜在的错误和不一致

---

1. 语法宽松：JavaScript的语法相对宽松，允许开发者在编写代码时有更多的自由，但也因此容易引入错误。例如，自动分号插入（ASI）机制有时会导致意外的行为。
2. 多范式支持：JavaScript支持多种编程范式，包括面向对象编程、函数式编程和命令式编程。这种多样性虽然强大，但也增加了语言的复杂性。

---

```javascript {.line-numbers}
// 动态类型
let x = 42;    // x 是一个数字
x = "hello";   // 现在 x 是一个字符串

// 自动分号插入
function foo() {
  return
  {
    bar: "baz"
  };
}

console.log(foo()); // undefined，因为 return 后自动插入了分号
```

---

```javascript {.line-numbers}
// 多范式支持
// 面向对象编程
class Animal {
  constructor(name) {
    this.name = name;
  }
  speak() {
    console.log(`${this.name} makes a noise.`);
  }
}
```

---

```javascript {.line-numbers}
// 高阶函数：接受函数作为参数或返回一个函数
const map = (arr, fn) => {
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    result.push(fn(arr[i]));
  }
  return result;
};

// 使用高阶函数
const numbers = [1, 2, 3, 4, 5];
const doubled = map(numbers, x => x * 2);
console.log(doubled); // 输出: [2, 4, 6, 8, 10]

// 使用内置的高阶函数
const filtered = numbers.filter(x => x > 2);
console.log(filtered); // 输出: [3, 4, 5]

const reduced = numbers.reduce((acc, x) => acc + x, 0);
console.log(reduced); // 输出: 15
```

---

#### 异常成功

1. Web平台的唯一性和无处不在
2. 简单的入门门槛
3. 广泛的应用场景：浏览器端（JavaScript）、服务器端（Node.js）、移动应用开发（React Native）、桌面应用开发（Electron）
4. 活跃的社区与生态系统
5. 浏览器厂商的支持和优化
6. 不断发展的标准（ECMAScript）

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 基本语法Basic Syntax

---

### 前端脚本书写位置

1. 行内脚本`inline script`
1. 内部脚本`internal script`
1. 外部脚本`extern script`

---

#### 行内脚本Inline Script

```html {.line-numbers}
<input type="button" value="click me..." onclick="alert('Hello World')" />
```

1. 可以将单行或少量`js`代码写在`HTML`标签的事件属性中（以`on`开头的属性，如：`onclick`）
1. 可读性差， 编写`js`大量代码时，不方便阅读
1. 引号易错，引号多层嵌套匹配时，容易混乱

---

#### 内嵌脚本Internal Script

```html {.line-numbers}
<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        alert('Hello World');
    </script>
</head>
</html>
```

---

#### 外部脚本External Script

```html {.line-numbers}
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="my.js"></script>
</head>
</html>
```

1. 方便文件级别的复用
1. 引用外部`js`文件的`<script>`标签内不能包含代码

---

### 注释

1. 单行注释：`//`
1. 多行注释：`/* */`

---

### 输入输出语句

1. `prompt(info)`：浏览器弹出输入框，用户输入信息
1. `alert(msg)`： 浏览器弹出警示框
1. `console.log(msg)`：浏览器控制台打印输出信息

---

### 表达式Expression，语句Statement和语句块Block

1. 表达式`expression`：一个可求值的代码片段，通常由操作数（`operands`）和运算符（`operators`）组成，**_用于产生一个值_**
1. 语句`statement`：一个执行操作的指令
1. 语句块`block`：由一个或多个语句组成的代码块，通常被包含在一对花括号（`{}`）中，语句块用于将多个语句组合在一起，形成一个单独的执行单元

**表达式的目的是为了得到返回值，语句的目的是为执行某种操作**

---

```javascript {.line-numbers}
// statement without expression
let x;
function myFunction() {
}
break;
continue;
return;
```

---

1. 表达式可以包含在语句中，例如，`x = 5 + 3;`中的`5 + 3`是一个表达式
2. 语句是程序的基本构建块，描述了执行的动作，大多数语句中包含表达式，表达式可以用于计算或操作数据，然后根据结果采取不同的行动
3. 语句块是一组语句的集合，用于组织和控制代码的结构

---

<!-- 1. `JavaScript`的执行单位为行（line），即，一行一行地执行，一般情况下，一行是一个语句 -->
1. `ES`规范里没有规定所有语句末尾必须加分号，但加分号能避免不必要的错误

>JavaScript 并不要求在每个语句后面一定要加分号，因为在大部分情况下JavaScript引擎是可以识别出语句的结束位置并且自动插入分号，这个过程被称为 ASI(Automatic Semicolon Insertion,自动分号插入) 。但是，并不是所有的语句都能省略分号，因为ASI解析规则会导致某些情况代码异常。
>>[KooFE. JavaScript 要不要加分号呢？.](https://juejin.cn/post/6932863412783284231)

---

```javascript {.line-numbers}
var name = 3
(function () {})()

// 由于没有分号，上面的会被解析为下面的语句，导致出现报错
var name = 3(function () {})()
```

---

### 标识符Identifier

1. 标识符`identifier`：用于标识程序某个对象的字符序列（名称），比如变量、函数等
1. `JavaScript`标识符规则：
    1. `unicode`字母（英文或非英文字母）或数字或下划线（`"_"`）或美元符号（`"$"`），不能以数字开头
    1. 大小写敏感
1. 字符串是数据，标识符是代码的一部分

---

```javascript {.line-numbers}
var $name = 'Tom';
var _age = 25;
var $ = 'dollar';
var _ = 'underscore';

var 名字 = 'Tom';
var 年龄 = 25;

console.log(名字); // 输出: Tom
console.log(年龄); // 输出: 25
```

---

### 变量Variable与数据类型Type

---

#### 变量Variable

1. 变量`variable`：用于储存数据的空间，通过变量名（变量标识符）访问数据
1. 初始化`initialization`：定义变量时，同时为变量赋值，称为变量初始化
1. 赋值`assignment`：为变量重新赋值，称为变量赋值
<!-- 1. `javascript`可以直接赋值（而不需要定义） -->

```javascript {.line-numbers}
var age;
age = 10; //assignment

var name = 'Tom'; //initialization

var i = 10, j = 11, z = 12;
```

---

#### 声明Declaration VS 定义Definition

1. 声明`declaration`：告诉计算机有某个变量，但没有为该变量分配空间
1. 定义`definition`：告诉计算机有某个变量，同时为该变量分配空间

---

#### 数据类型Type

1. $\text{information} = \text{bits} + \text{context}$
1. 数据类型是变量的上下文，数据类型告诉计算机如何解释和处理数据
1. `js`是一种弱类型语言（动态类型语言/动态语言），变量的数据类型可以随时改变，即，变量没有数据类型，变量值有数据类型，变量的数据类型由运行时环境自动确定

---

<!--
TODO:

1. `String`是不是值类型？
2. `String`有没有`len`属性？
3. 值类型对应的包装类型是什么？
-->

1. 原始数据类型`primitive type`/值类型`value type`：`Number`、`String`、`Boolean`
2. 复合数据类型`complex type`/引用类型`reference type`：`Object`
    1. `Object`
    2. `Function`
    3. `Array`: `Object`的特殊形式，键值为数字字符串的`Object`
3. 特殊类型（特殊只读变量）：`undefined`
4. 特殊值：`null`

---

##### 数值Number

1. `Number`：整型`integer`、浮点型`float`
1. `Number.MAX_VALUE`, `Number.MIN_VALUE`
1. `Infinity`, `-Infinity`
1. `NaN`(Not a Number): `isNaN()`

>[MDN. Number.](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Number)

---

1. 不区分整型和浮点型，都被存储为双精度64位浮点数

```javascript {.line-numbers}
let integer = 42;       // integer
let float = 3.14;       // float

console.log(typeof integer); // "number"
console.log(typeof float);   // "number"
```

---

###### `ES6`风格进制表示法

1. 二进制`0b`或`0B`
2. 八进制`0o`或`0O`
3. 十六进制`0x`或`0X`

```javascript {.line-numbers}
let binary = 0b1010; // 10 in decimal
console.log(binary); // 输出: 10

let octal = 0o666; // 438 in decimal
console.log(octal); // 输出: 438

let hex = 0x1A; // 26 in decimal
console.log(hex); // 输出: 26
```

---

##### 字符串String

1. 单引号或双引号包围的文本
1. 两种引号可嵌套：单引号嵌套双引号，或双引号嵌套单引号
1. 转义字符：`\n`、`\\`、`\"`、`\'`、`\t`、`\b`

>[MDN. String.](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String)

---

##### Template Strings模板字符串

1. 反引号包围的字符串
    1. multi-line strings多行字符串
    2. string interpolation字符串插值

>[MDN. Template literals (Template strings).](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)

---

```javascript {.line-numbers}
console.log(`string text line 1
string text line 2`);
// "string text line 1
// string text line 2"

const name = "World";
const greeting = `Hello, ${name}!`;
console.log(greeting); // Hello, World!
```

---

##### null and undefined and Boolean

1. `null`：空值，为变量分配了空间，该空间里没有值（或者是垃圾值）
2. `undefined`：未定义，该变量不存在（未分配空间）
3. `boolean`：`true`或`false`

>1. [null, undefined 和布尔值.](https://wangdoc.com/javascript/types/null-undefined-boolean)
>1. [MDN. Boolean.](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Boolean)

---

1. 当预期某个位置应该是布尔值时，会将该位置上现有的值自动转为布尔值
1. 为`false`的值
    1. `undefined`
    1. `null`
    1. `false`
    1. `0`
    1. `NaN`（Not a Number）
    1. `""`或`''`（空字符串）

---

```javascript {.line-numbers}
if (!undefined) { // undefined is false
  console.log('undefined is false');
}

if (!null) { // null is false
  console.log('null is false');
}

console.log(undefined == null);  // true
console.log(undefined === null); // false
console.log(undefined == undefined);  // true
console.log(undefined === undefined); // true
```

---

##### typeof运算符

```javascript {.line-numbers}
console.log(typeof 123);            // "number"
console.log(typeof '123');          // "string"
console.log(typeof false);          // "boolean"

function f() {}
console.log(typeof f);              // "function"

console.log(typeof undefined);      // "undefined"
console.log(typeof undefined_var);  // "undefined"
```

---

##### 类型转换

1. 自动转换/隐式转换implicit type conversion
1. 手动转换/显式转换explicit type conversion
    1. `Number()`：转换为数值
        1. `parseInt()`：忽略小数部分
        1. `parseFloat()`：保留小数部分
    1. `String()`：转换为字符串
        1. `toString()`
    1. `Boolean()`：转换为布尔值

>[阮一峰. 数据类型的转换.](https://wangdoc.com/javascript/features/conversion)

---

```javascript {.line-numbers}
// 1. 弹出输入框，输入出生年份，并存储在变量中
var year = prompt('请输入您的出生年份：');      // 用户输入
// 2. 用今年减去刚才输入的年份
var result = new Date().getFullYear() - year;// 内部处理
// 3. 弹出提示框
alert('您的年龄是:' + result + '岁');          // 输出结果
```

---

```javascript {.line-numbers}
// 1. 先弹出第一个输入框，提示用户输入第一个值
var num1 = prompt('请输入第一个值：');
// 2. 再弹出第二个框，提示用户输入第二个值
var num2 = prompt('请输入第二个值：');
// 3. 将输入的值转换为数字型后，把这两个值相加，并将结果赋给新的变量
var result = parseFloat(num1) + parseFloat(num2);
// 4. 弹出结果
alert('结果是:' + result);
```

---

### 运算符Operator

1. 赋值运算符：`=`
1. 算术运算符：`x+y`、`x-y`、`x*y`、`x/y`、`x**y`、`x%y`、`++x`/`--x`、`x++`/`x--`、`+x`/`-x`
1. 比较运算符：`x==y`、`x===y`、`x!=y`、`x!==y`、`x>y`、`x>=y`、`x<y`、`x<=y`
1. 布尔运算符：`x&&y`、`x||y`、`!x`、`x?y:z`
1. 按位运算符：`x&y`、`x|y`、`~x`、`x^y`、`x<<y`、`x>>y`、`x>>>y`
1. 其他运算符：`typeof`、`void`、`,`

---

1. `==`：比较两个值是否相等，比较前 **_可能会转换_** 数据类型
1. `===`：比较两个值是否严格相等，即是否为同一个值，比较前 **_不转换_** 数据类型

```javascript {.line-numbers}
console.log(null == 0)            // false
console.log(Boolean(null) == 0)   // true
```

❗ 尽可能使用`===`和`!==`，而不是`==`和`!=` ❗

---

1. `++x`先自增再返回值，`x++`先返回值再自增
2. `x&&y`和`x||y`执行短路运算

---

1. `void`运算符的作用主要有：
    1. 防止默认行为，如：`<a href="javascript:void(0)">`
    2. 返回`undefined`，通过`void xx`（如`void 0`）得到`undefined`是一个安全的、兼容的方法

>1. [What's the difference between void, eval, and the Function constructor in JavaScript?](https://stackoverflow.com/questions/10343553/whats-the-difference-between-void-eval-and-the-function-constructor-in-javasc)
>1. [JavaScript void 运算符.](https://juejin.cn/post/6961222811184529445)

---

```javascript {.line-numbers}
var undefined = 42;     // 重新定义undefined
console.log(undefined); // 42
console.log(void 0);    // undefined
```

---

### 内置对象Builtin Object

---

#### 包装对象Wrapper Object

1. `js`提供了三个特殊的对象，用于将原始类型的值包装为对象：`Number`、`String`、`Boolean`
1. 包装对象的设计目的
    1. 使得`Object`这种类型可以覆盖所有的值，整门语言有一个通用的数据模型
    1. 使得原始类型的值也有办法调用自己的方法

---

```javascript {.line-numbers}
var v1 = new Number(123);
var v2 = new String('abc');
var v3 = new Boolean(true);

console.log(typeof v1)    // "object"
console.log(typeof v2)    // "object"
console.log(typeof v3)    // "object"

console.log(v1 == 123)   // true
console.log(v2 == 'abc') // true
console.log(v3 == true)  // true

console.log(v1 === 123)   // false
console.log(v2 === 'abc') // false
console.log(v3 === true)  // false
```

>[阮一峰. JavaScript教程-包装对象.](https://wangdoc.com/javascript/stdlib/wrapper)

---

##### 数组Array与Array对象

1. 数组`Array`：一组数据的有序集合，也称为向量`Vector`
1. `js`的数组可存储任意类型的数据

>1. [阮一峰. JavaScript教程-数组.](https://wangdoc.com/javascript/types/array)
>1. [阮一峰. JavaScript教程-Array对象.](https://wangdoc.com/javascript/stdlib/array)
>1. [JavaScript的数组有什么特殊之处？](https://juejin.cn/post/6844903888215080973)

---

###### `Array`的创建

1. 构造方法：`var arr = new Array();`
1. 字面量：`var arr = [];`
1. `Array.of()`(`es6`新增)
1. `Array.from()`(`es6`新增)

⚠️ `JavaScript`的`Array`不一定是连续的内存空间 ⚠️

---

###### `Array`的操作

1. `index`/`key`：从`0`开始
1. `js`语法规定，对象的`key`一律为`String`，因此，`Array`的`key`本质上是`String`，之所以可以用数值读取，是因为非字符串的`key`会被转为字符串

```javascript {.line-numbers}
let arr = ['a', 'b', 'c'];

console.log(arr['0'] === arr[0]); // true
```

---

```javascript {.line-numbers}
let arr = ['red','green', 'blue'];
console.log(typeof arr);    // "object"
for(let i = 0; i < arr.length; ++i){
    console.log(arr[i]);
}
```

---

### 控制结构Control Structure

![height:500](./.assets/image/image-20230906115242791.png)

---

1. 顺序结构`sequential structure`
1. 分支结构`branch structure`：`if...else if...else...`，`switch...case...default...`，`...?...:...`
1. 循环结构`loop structure`：`while...`，`for...`，`do...while...`
1. 异常结构`exception structure`：`try...catch...finally...`

---

#### 分支结构Branch Structure

```javascript {.line-numbers}
if (<condition>)
    [statement;]
[
else if (<condition>)
    [statement;]
]
[
else
    [statement;]
]
```

---

```javascript {.line-numbers}
var usrAge = prompt('请输入您的年龄：');
if(usrAge >= 18){
    alert('您是成年人！');
}
```

---

```javascript {.line-numbers}
if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
    alert("这个年份是闰年");
} else { // 剩下的是平年
    alert("这个年份是平年");
}
```

```javascript {.line-numbers}
// 使用括号提高代码的可读性
if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
    alert("这个年份是闰年");
} else { // 剩下的是平年
    alert("这个年份是平年");
}
```

---

```javascript {.line-numbers}
var score = prompt('请您输入分数:');
if (score >= 90) {
    alert('优秀');
} else if (score >= 80) {
    alert('良好');
} else if (score >= 70) {
    alert('中等');
} else if (score >= 60) {
    alert('及格');
} else {
    alert('不及格');
}
```

---

```javascript {.line-numbers}
switch(<expression>){
    case <value1>:
        [statement;]
        [break;]
    [
    case <value2>:
        [statement;]
        [break;]
    ]
    [
    default:
        [statement;]
        [break;]
    ]
}
```

---

```javascript {.line-numbers}
let fruit = prompt('请您输入查询的水果:');
switch (fruit) {
    case '苹果':
        alert('苹果的价格是 3.5/斤');
        break;
    case '榴莲':
        alert('榴莲的价格是 35/斤');
        break;
    default:
        alert('没有此水果');
}
```

---

```javascript {.line-numbers}
<condition> ? <condition is true> : <condition is false>
```

1. `if ... else ...`是语句，没有返回值
2. `... ?... : ...`是表达式，有返回值

---

```javascript {.line-numbers}
let time = prompt('请您输入一个 0 ~ 59 之间的一个数字');

let result = time < 10 ? '0' + time : time;
alert(result);
```

---

#### 循环结构Loop Structure

1. `break`：跳出本层循环
1. `continue`：跳过本次循环剩余语句，进入下一次循环

---

```javascript {.line-numbers}
while(<condition>){
    [statement];
}
```

```javascript {.line-numbers}
for([initialization];[condition];[final-expression]){
    [statement];
}
```

```javascript {.line-numbers}
do{
    [statement];
}while(<condition>)
```

---

1. `while`与`for`等价
1. `while`与`do ... with`不等价

```javascript {.line-numbers}
[initialization];
while(<condition>){
    [statement];
    [final-expression];
}
```

---

```javascript {.line-numbers}
for (let i = 1; i <= 100; ++i) {
    if (i == 1) {
        console.log('今年1岁了');
    } else if (i == 100) {
        console.log('今年100岁了');
    } else {
        console.log('今年' + i + '岁了');
    }
}
```

---

```javascript {.line-numbers}
var sum = 0;
for(let i = 1;i <= 100; ++i){
    sumNum += i;
}
console.log('1-100之间整数的和 = ' + sum);
```

---

```javascript {.line-numbers}
var star = '';
for (var j = 1; j <= 3; ++j) {
    for (var i = 1; i <= 3; ++i) {
        star += '☆'
    }
    // 每次满 5个星星 就 加一次换行
    star += '\n'
}
console.log(star);
```

---

#### 异常结构Exception Structure

```javascript {.line-numbers}
throw <Error>;  // throw an error
```

```javascript {.line-numbers}
try{
    [statement;]
}catch(<e>){
    [statement;]
}[finally{
    [statement;]
}]
```

---

1. `Error`
    1. `SyntaxError`
    1. `ReferenceError`
    1. `RangeError`
    1. `TypeError`
    1. `URIError`
    1. `EvalError`
    1. `AggregateError`
    1. `InternalError`

---

```javascript {.line-numbers}
class CustomError extends Error {
    constructor(message) {
        super(message);
        this.name = "CustomError";
    }
}

// 使用自定义异常
try {
    throw new CustomError("This is a custom error message");
} catch (e) {
    console.log(e.name);    // CustomError
    console.log(e.message); // This is a custom error message
}
```



---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `this`关键字

---

1. 动态绑定：this的值在函数调用时确定，而不是在函数定义时确定。它取决于函数的调用方式，而不是函数的定义位置
2. 多种调用方式：
    1. 全局调用：在全局作用域中，this指向全局对象（在浏览器中是window）
    2. 方法调用：作为对象的方法调用时，this指向该对象
    3. 构造函数调用：使用new关键字调用构造函数时，this指向新创建的实例对象
    4. 显式绑定：使用call、apply或bind方法显式绑定this
3. 箭头函数：箭头函数不绑定自己的this，而是继承自外层作用域的this，这与普通函数不同

---

```javascript {.line-numbers}
// 全局调用
function globalFunc() {
  console.log(this); // 在浏览器中，输出: window
}
globalFunc();

// 方法调用
const obj = {
  name: 'Alice',
  greet: function() {
    console.log(this.name); // 输出: Alice
  }
};
obj.greet();

// 构造函数调用
function Person(name) {
  this.name = name;
}
const person = new Person('Bob');
console.log(person.name); // 输出: Bob

// 显式绑定
function showName() {
  console.log(this.name);
}
const user = { name: 'Charlie' };
showName.call(user); // 输出: Charlie

// 箭头函数
const arrowFunc = () => {
  console.log(this); // 输出: 继承自外层作用域的 this
};
arrowFunc.call(user); // 输出: 继承自外层作用域的 this
```

---

1. 全局调用：this指向全局对象
2. 方法调用：this指向调用该方法的对象
3. 构造函数调用：this指向新创建的实例对象
4. 显式绑定：使用call、apply或bind方法可以显式绑定this
5. 箭头函数：箭头函数不绑定自己的this，而是继承自外层作用域的this

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 标准库Standard Library

>[阮一峰. 标准库.](https://wangdoc.com/javascript/stdlib/)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
