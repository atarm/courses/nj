---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Cascading Style Sheets (CSS)_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Cascading Style Sheets (CSS)

>1. [MDN. CSS-设计Web.](https://developer.mozilla.org/zh-CN/docs/Learn/CSS)
>1. [CSS教程.](https://www.w3school.com.cn/css/index.asp)
>1. [pengfeiw/css-tutorial.](https://github.com/pengfeiw/css-tutorial/tree/master)
>1. [wiki. CSS.](https://en.wikipedia.org/wiki/CSS)
>1. [QuickRef.ME. CSS 3 cheatsheet.](https://quickref.me/css3)
>1. [DevDocs. CSS.](https://devdocs.io/css/)
>1. [css知多少.](https://www.cnblogs.com/wangfupeng1988/p/4325007.html)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [CSS介绍](#css介绍)
2. [CSS的基本原理和基本语法](#css的基本原理和基本语法)
3. [CSS的基本使用](#css的基本使用)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## CSS介绍

---

### CSS是什么

1. `CSS(Cascading Style Sheet, 层叠样式表)`是一种用于描述如何显示`HTML`的样式表语言（`Style Sheet Language`）

>[CSS简介-CSS演示.](https://www.w3school.com.cn/css/css_jianjie.asp)

---

### 为什么使用CSS

1. `CSS`实现了内容逻辑与表现逻辑的分离
    1. `CSS`实现了样式的切换
    1. `CSS`实现了样式的复用：extern-stylesheet

---

### The Brief History of CSS

1. 1994-10-10: first proposed by `Håkon Wium Lie`
1. 1996-12-17: `CSS1` was published as a `W3C Recommendation`
1. 1998-05-12: `CSS2` was published as a `W3C Recommendation`
1. 1999-12-17: `CSS2.1` was published as a `W3C Recommendation`, fixes errors in `CSS 2`, removes poorly supported or not fully interoperable features and adds already implemented browser extensions to the specification

---

1. 1999-06: the earliest `CSS3` drafts were published

<!--TODO:
添加后继版本的说明
-->

>Unlike CSS 2, which is a large single specification defining various features, CSS 3 is divided into several separate documents called **_"modules"_**. Each module adds new capabilities or extends features defined in CSS 2, preserving backward compatibility.
>
>>[CSS history.](https://en.wikipedia.org/wiki/CSS#History)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## CSS的基本原理和基本语法

---

### CSS的运行原理

![img](./.assets/image/rendering.jpg)

>[CSS 如何运行.](https://developer.mozilla.org/zh-CN/docs/Learn/CSS/First_steps/How_CSS_works)

---

### Cascading层叠

1. 当为某个`html`元素指定多个样式时，`CSS`根据优先级"层叠"成新的"虚拟"样式
1. 优先级依次为
    1. 行内样式
    1. 外部和内部样式表，按照它们在`HTML`中出现的顺序，后读取的样式表会覆盖先读取的样式表
    1. 浏览器默认样式

>[MDN. 层叠与继承.](https://developer.mozilla.org/zh-CN/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance)

---

```css {.line-numbers}
style-rule ::=
    selectors-list {
        properties-list
    }

selectors-list ::=
    selector[:pseudo-class] [::pseudo-element]
    [, selectors-list]

properties-list ::=
    [property : value] [; properties-list]

comment ::= /* [comment] */
```

---

### 注释Comment

1. `/* */`：单行或多行注释

```css {.line-numbers}
/* 这是一条单行注释 */
/*
这是一条
多行注释
*/
p {
  color: red;
}
```

---

![height:100%](./.assets/image/css-syntax.jpg)

1. `CSS`由`选择器(Selector)`和`声明块(Declaration Block)`组成
1. `选择器(Selector)`指定了`声明块(Declaration Block)`作用的`HTML`元素

---

### 选择器Selector

1. `basic selectors`基本选择器
1. `pseudo selectors`伪选择器
1. `combinator selectors`关系选择器
1. `grouping selectors`组选择器

---

#### 基本选择器

1. `universal selector`全局选择器：`*`
1. `type selector`类型选择器/`tag selector`标签选择器：`tagname`，根据`tag`选取元素
1. `class selector`类选择器：`.classname`，根据`class`选取元素
1. `id selector`id选择器：`#id`，根据`id`选取元素
1. `attribute selector`属性选择器：`[attr=value]`，根据属性或属性值选取元素

---

```css {.line-numbers}
h1 {
}

.box {
}

#unique {
}
```

---

```css {.line-numbers}
/*  */
a[title] {
}

a[href="https://example.com"] {
}
```

---

#### 伪选择器

1. `pseudo-class selector`伪类选择器：`:`，根据特定状态选取元素
1. `pseudo-element selector`伪元素选择器：`::`，选取元素的某个部分而不是整个元素

---

```css {.line-numbers}
/* pseudo-class selector */
a:hover {
}

/* pseudo-element selector */
p::first-line {
}
```

---

#### 关系选择器

1. 关系选择器：根据元素之间的特定关系来选取元素
    1. `descendant combinator`子孙选择器
        1. `descendant combinator`后代选择器（以空格` `分隔）
        1. `child combinator`子代选择器（以大于号`>`分隔）
    1. `sibling combinator`兄弟选择器
        1. `general sibling combinator`后续兄弟选择器（以波浪号`~`分隔）
        1. `adjacent sibling combinator`后续邻接兄弟选择器（以加号`+`分隔）

---

```css {.line-numbers}
div p { background-color:yellow;
}

div > p {
  background-color:yellow;
}
```

---

```css {.line-numbers}
div ~ p {
  background-color:yellow;
}
```

---

#### 组选择器

1. `selector list`/`union selector`并集选择器：选取所有匹配任一选择器的元素

```css {.line-numbers}
/* union selector */
h1, h2, h3, h4, h5, h6 {
    font-family: helvetica;
}
```

---

### 排版Typesetting

>[MDN. CSS布局.](https://developer.mozilla.org/zh-CN/docs/Learn/CSS/CSS_layout)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## CSS的基本使用

---

1. `inline style`行内样式/内联样式：作用于该元素
1. `internal style sheet`内部样式：作用于该文件
1. `external style sheet`文件外部样式：作用于多个文件

---

### Inline Style

```html {.line-numbers}
<body>
    <h1 style="color:blue;text-align:center;">This is a heading</h1>
    <p style="color:red;">This is a paragraph.</p>
</body>
```

---

### Internal Style Sheet

```html {.line-numbers}
<head>
    <style>
        body {
          background-color: linen;
        }

        h1 {
          color: maroon;
          margin-left: 40px;
        }
    </style>
</head>
```

---

### External Style Sheet

```html {.line-numbers}
<head>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
</head>
```

1. 外部样式文件以`.css`扩展名保存
1. 外部样式文件不应包含`html`标签

---

```css
body {
    background-color: lightblue;
}

h1 {
      color: navy;
      margin-left: 20px;
}
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
