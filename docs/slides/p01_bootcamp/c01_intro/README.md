---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Introduction_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [目标Goals](#目标goals)
2. [简介Introduction](#简介introduction)
3. [第一个Node程序The First Node Program](#第一个node程序the-first-node-program)
4. [储备知识Prerequisite](#储备知识prerequisite)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## 目标Goals

---

- **知识目标**：
    1. 理解`Node`的基本概念
    1. 了解`Node`的储备知识
- **能力目标**：
    1. 能搭建`Node`开发环境和运行环境
    1. 能编写`Node`简单程序
- **素养目标**：
    1. 良好的动手实践意识
    1. 良好的结构化思维能力

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 简介Introduction

---

### Node是什么

1. `Node(Node.js)`是运行在服务端的`JavaScript`运行时环境
1. `Node`是一个基于`Chrome V8`引擎的`JavaScript`运行时环境
1. `Node`是一个事件驱动、异步非阻塞IO的`JavaScript`运行时环境
1. `Node`提供`LTS(Long-Time Supported)`版本和`Current`版本
1. `Node`由`Joyent`公司发起（`Joyent`于2016-06-15被`Samsung`收购）
1. `Node`是一个开源项目，由`OpenJS Foundation`（`Linux Foundation`下的子基金）管理

<!-- ---

1. `C/S(Client Server)`
1. `B/S(Browser Server)` -->

---

![image-20230831145937173](./.assets/image/image-20230831145937173.png)

<!--
---

![height:600](./.assets/image/core-principles.jpg)
-->

---

![height:600](./.assets/image/image-20230831150118541.png)

---

1. `Node Standard Library`：`node`标准库，对外提供`JavaScript`接口
2. `Node bindings`：`JavaScript`与`C/C++`的绑定
3. `chrome v8`：解释并执行`JavaScript`代码
4. `libuv`：事件循环、线程池、`file IO`等，负责`IO`任务的分发与执行
5. `DNS(C-ares)`：异步的`DNS`解析
6. `Low-Level Components`：`HTTP`、`SSL`、`zlib`等底层组件

---

```javascript {.line-numbers}
console.log('Hello World!');
```

1. 在`browser`运行`console.log`调用了`BOM`，执行的是`window.console.log('Hello World!')`
2. 在`node`运行，首先在`os`中创建一个新的`process`，然后向`stdout`打印了指定的字符串，执行的是`process.stdout.write('Hello World!\n')`

---

### The Father of Node

![height:500](./.assets/image/image-20230824155101449.png)

---

1. `Ryan Dahl`于2009-05推出的最初版本
2. `Ryan Dahl`专注于实现高性能`web server`的优化
3. `Ryan Dahl`对于`asynchronous io`、`event driven`是基本原则，但是使用`C/C++`编写过于复杂
4. `Ryan Dahl`发现很多高级语言虽然同时提供了`synchronous io`和`asynchronous io`，但开发人员一旦使用了`synchronous io`，就再也懒得写`asynchronous io`
5. `JavaScript`是单线程执行，不能进行`synchronous io`，因此，`JavaScript`的这一"缺陷"导致了它只能使用`asynchronous io`

---

### The History of Node

1. 2009-05：`Ryan Dahl`推出最初版本
1. 2009-11：`Joyent`公司赞助`Ryan Dahl`，`Ryan Dahl`加入`Joyent`全职负责开发`node`
1. 2010-01：`Node`在`GitHub`上开源
1. 2014-12：`io.js` fork
1. 2015：`Ryan Dahl`退出`Node`项目，`Isaac Z. Schlueter`接任`Node`项目负责人

---

1. 2015-02：`Node.js Foundation`成立
1. 2015-06：`Node.js`和`io.js`在`Node.js Foundation`下共同开发
1. 2015-09：`Node.js 0.12`和`io.js 3.3`合并成`Node.js 4.0`
1. 2019：`Node.js Foundation`与`JS Foundation`合并为`OpenJS Foundation`

---

### Node的适用场景

1. server side
    1. `web application`
    1. `server application`
1. client side
    1. `cli application`: `command line tools`
    1. `gui application`
        1. `desktop application`: `electron`(`vscode`)
        1. `mobile application`: `cordova`

适合高并发、IO密集型的场景，不适合CPU密集型的场景

---

### Node版本号规则

1. `Node`的版本号遵循`SemVer`规则
1. Given a version number `MAJOR.MINOR.PATCH`, increment the:
    1. `MAJOR` version when you make incompatible API changes
    1. `MINOR` version when you add functionality in a backward compatible manner
    1. `PATCH` version when you make backward compatible bug fixes

>[Semantic Versioning 2.0.0](https://semver.org/)

---

### LTS VS Current

1. `lts(long term support)`：维护期版本，长期支持版本
1. `current`：开发期版本，每6个月发布一个新的大版本
    1. 偶数版本在`current`阶段后进入`lts`阶段
    2. 奇数版本在`current`阶段后进入`end-of-life`阶段

---

### 运行node程序

1. `REPL(Read Eval Print Loop)`交互模式
    1. enter: `node`
    2. exit: `Ctrl+C` * 2 or `Ctrl+D` or `.exit`
2. `script`脚本模式/`interpret`解释模式
    1. enter: `node` or `node inspect`
    2. exit: `Ctrl+C` or "end of run"

---

```bash {.line-numbers}
$ node

Welcome to Node.js v18.17.1.
Type ".help" for more information.
> .help
.break    Sometimes you get stuck, this gets you out
.clear    Alias for .break
.editor   Enter editor mode
.exit     Exit the REPL
.help     Print this help message
.load     Load JS from a file into the REPL session
.save     Save all evaluated commands in this REPL session to a file

Press Ctrl+C to abort current expression, Ctrl+D to exit the REPL
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 第一个Node程序The First Node Program

---

### 安装Node

1. 下载`Node`：<https://nodejs.org/>
1. 安装`Node`
1. 安装`IDE`：`WebStorm` or `VSCode` or any other `text editor`
1. 编写第一个`Node`程序
1. 运行第一个`Node`程序

---

```javascript {.line-numbers}
// 01_hello.js
let http = require('http');

let server = http.createServer(function(req, res){
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write("<h1>Hello World</h1>");
    res.end();
});

server.listen(3000,function (){
    console.log('server running at http://127.0.0.1:3000/');
})
```

---

1. start: open `shell`/`cmd` and run `node 01_hello.js`
1. access: open `browser` and go to `http://127.0.0.1:3000/`
1. stop: `Ctrl+C`

```bash {.line-numbers}
$ node --help
Usage: node [options] [ script.js ] [arguments]
       node inspect [options] [ script.js | host:port ] [arguments]
```

---

```bash {.line-numbers}
$ npm --help
npm <command>

Usage:
npm install        install all the dependencies in your project
npm install <foo>  add the <foo> dependency to your project
npm test           run this project tests
npm run <foo>      run the script named <foo>
npm <command> -h   quick help on <command>
npm -l             display usage info for all commands
npm help <term>    search for help on <term>
npm help npm       more involved overview

All commands:
    access, adduser, audit, bugs, cache, ci, completion, config, dedupe, deprecate,
    diff, dist-tag, docs, doctor, edit, exec, explain, explore, find-dupes, fund, get,
    help, help-search, hook, init, install, install-ci-test, install-test, link, ll,
    login, logout, ls, org, outdated, owner, pack, ping, pkg, prefix, profile, prune,
    publish, query, rebuild, repo, restart, root, run-script, search, set, shrinkwrap,
    star, stars, start, stop, team, test, token, uninstall, unpublish, unstar, update,
    version, view, whoami
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 储备知识Prerequisite

---

### 程序开发Programming

1. `WebAPP` = `front-end` + `server-end` + `database`
1. `front-end` = `HTML` + `CSS` + `JavaScript`
1. `server-end` --> `Node`
1. `database` --> `MySQL`

---

### 工程开发Engineering

1. `git`
2. `markdown`
3. etc...

---

### `http`

1. `stateless`：HTTP请求之间相互独立
2. `request-response`：单向请求，客户端发起请求，服务端响应请求

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
