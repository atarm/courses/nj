let http= require('http');

let server = http.createServer(function(req, res){
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write("<h1>Hello World</h1>");
    res.end();
});

server.listen(3000,function (){
    console.log('server running at http://127.0.0.1:3000/');
})
