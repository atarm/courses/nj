---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Callback and Event_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Callback and Event

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [异步编程Asynchronous Programming](#异步编程asynchronous-programming)
2. [回调函数Callback Function](#回调函数callback-function)
3. [事件与事件触发器Events and EventEmitter](#事件与事件触发器events-and-eventemitter)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 异步编程Asynchronous Programming

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 同步与异步Synchronous and Asynchronous

---

1. `node`的主线程是单线程模式
1. `node`异步编程模型：实现单线程并发处理多任务
1. `sync(synchronous, 同步)`：顺序相关
1. `async(asynchronous, 异步)`：顺序无关

---

```javascript {.line-numbers}
// synchronous
console.log('起床');
console.log('洗漱');
function eat() {
    console.log('吃早餐');
    console.log('早餐吃完了');
}
eat();
console.log('去上学');
```

---

```javascript {.line-numbers}
起床
洗漱
吃早餐
早餐吃完了
去上学
```

---

```javascript {.line-numbers}
// asynchronous
console.log('起床');
console.log('洗漱');
function eat() {
    console.log('开始吃早餐');
    setTimeout(function () {
        console.log('早餐吃完了');
    }, 0)
}
eat();
console.log('去上学');
```

---

```javascript {.line-numbers}
起床
洗漱
开始吃早餐
去上学
早餐吃完了
```

---

```javascript {.line-numbers}
console.log(1);
setTimeout(function () {
    console.log(2);
    console.log('hello');
},100)
console.log(3);
```

---

```javascript {.line-numbers}
1
3
2
hello
```

---

#### 返回值如何获取

---

```javascript {.line-numbers}
function add(x, y) {
    console.log(1);
    setTimeout(function () {
        console.log(2);
        return x + y;
    }, 1000)
    console.log(3);
}

console.log(add(10, 20));
```

---

```javascript {.line-numbers}
1
3
undefined
2
```

---

```javascript {.line-numbers}
function add(x, y) {
    let ret = null;
    console.log(1);
    setTimeout(function () {
        console.log(2);
        ret = x + y;
    }, 1000)
    console.log(3);
    return ret;
}

console.log(add(10, 20));
```

---

```javascript {.line-numbers}
1
3
null
2
```

---

#### 异常如何处理？

```javascript {.line-numbers}
// synchronous
function parseJsonStrToObj(str) {
    return JSON.parse(str);
}
try {
    let obj = parseJsonStrToObj("jack");
    console.log(obj);
    console.log(obj.name);
} catch (e) {
    console.log('转换失败了');
    console.log(e);
}
```

---

```javascript {.line-numbers}
// asynchronous
function parseJsonStrToObj(str) {
    setTimeout(function() {
        return JSON.parse(str);
    }, 0);
}
try {
    let obj = parseJsonStrToObj('jack');
    console.log('执行结果是：' + obj);
} catch (e) {
    console.log('转换失败了');
}
```

---

```javascript {.line-numbers}
// try-catch写在异步代码中
function parseJsonStrToObj(str) {
    setTimeout(function() {
        try{
            return JSON.parse(str);
        }catch(e){
            console.log('转换失败了');
            console.log(e.message);
        }
    }, 0)
}
//调用方法输出结果：可以捕获异常，无法获取返回值
let obj = parseJsonStrToObj('{"name":"jack"}');
console.log('执行结果是：' + obj);
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 回调函数Callback Function

---

1. `callback function`是实现异步编程的一种方式
2. `node`所有的`async api`都支持`callback function`，`callback function`一般作为`async api`的最后一个形参出现
3. `callback function`一般约定，第一个形参为`error`（如果没有错误，则传递`null`），第二个形参为`result`，这种将第一个形参作为`error`的约定通常被称为`error-first callback`或`errback`

```javascript {.line-numbers}
function async_func_name（param1,param2,callback）{}
callback(err,result);
```

---

```javascript {.line-numbers}
function add(x, y, callback) {
    setTimeout(function () {
        let ret = x + y;
        callback(ret);
    }, 1000);
}

add(10, 20, function (ret) {
    console.log(ret);
});
```

---

```javascript {.line-numbers}
function parseJsonStrToObj(str,callback) {
    setTimeout(function() {
        try {
            let obj = JSON.parse(str);
            callback(null, obj);
        } catch (e) {
            callback(e, null);
        }
    }, 0);
}

parseJsonStrToObj('{"jack"}',function (err, result) {
    //区分错误信息和正确的数据信息
    if (err) {
        console.log('转换失败了')
        return console.log(err.message)
    }
    console.log('数据转换成功，没有问题可以直接使用了：' + result)
    console.log(result.name)
});
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 事件与事件触发器Events and EventEmitter

>1. [Node.js 事件循环[DB/OL].](https://www.runoob.com/nodejs/nodejs-event-loop.html)
>1. [Node.js EventEmitter[DB/OL].](https://www.runoob.com/nodejs/nodejs-event.html)

---

![img](./.assets/image/event_loop.jpg)

---

![width:1120](./.assets/image/node-event-loop.png)

---

1. `EventEmitter`用于实现事件驱动编程，`EventEmitter`的核心是`事件触发`与`事件监听器`功能的封装
2. `events`是`builtin module`，该模块只提供了一个对象：`events.EventEmitter`
3. 一般情况不直接使用`EventEmitter`，而是在对象中继承`EventEmitter`，例如：`fs`、`net`、 `http`在内的
4. 支持事件响应的`builtin class`都是`EventEmitter`的子类

---

```javascript {.line-numbers}
let EventEmitter = require('events').EventEmitter;
let event = new EventEmitter();
event.on('some_event', function () {
    console.log('some_event 事件触发');
});
setTimeout(function () {
    event.emit('some_event');
}, 1000);
```

---

1. `EventEmitter`的事件由一个`eventName`和若干个参数组成，`eventName`是一个`String`，通常表达一定的语义。每个事件，`EventEmitter`可注册若干个`listener`。
1. 当事件触发时，注册到这个事件的事件监听器按顺序被依次调用，事件实参传递给回调函数形参

---

```javascript {.line-numbers}
let events = require('events');
let emitter = new events.EventEmitter();
emitter.on('someEvent', function(param1, param2) {
    console.log('listener1', param1, param2);
});
emitter.on('someEvent', function(param1, param2) {
    console.log('listener2', param1, param2);
});
emitter.emit('someEvent', 'arg1参数', 'arg2参数');
```

---

### `EventEmitter`的常用方法

---

1. 🌟 `emitter.on(eventName, listener)`：为指定事件注册添加一个监听器到监听器数组的尾部
2. 🌟 `emitter.once(eventName, listener)`：为指定事件注册添加一个单次监听器到监听器数组的尾部，即监听器最多只会触发一次，触发后立刻解除该监听器
3. 🌟 `emitter.addListener(eventName, listener)`：alias for `emitter.on(eventName, listener)`

---

1. 🌟 `emitter.off(eventName, listener)`：alias for `emitter.removeListener()`.
1. 🌟 `emitter.removeListener(eventName, listener)`：移除指定事件的某个监听器，监听器必须是该事件已经注册过的监听器
1. `emitter.removeAllListeners([event])`：移除所有事件的所有监听器，如果指定事件，则移除指定事件的所有监听器

---

1. 🌟 `emitter.emit(event, [arg1], [arg2], [...])`：按监听器的顺序执行执行每个监听器，如果该事件有监听器返回`true`，否则返回`false`
1. `emitter.setMaxListeners(n)`：默认情况下，`EventEmitters`如果添加的监听器超过10个会输出警告信息。`setMaxListeners()`用于改变监听器的默认限制的数量
1. `emitter.listeners(event)`：返回指定事件的监听器数组
1. `EventEmitter.listenerCount(emitter, event)`：返回指定事件的监听器数量

---

```javascript {.line-numbers}
emitter.on(eventName, listener)

eventName <string> | <symbol> The name of the event.
listener <Function> The callback function
Returns: <EventEmitter>

Adds the listener function to the end of the listeners array for the event named eventName. \
    No checks are made to see if the listener has already been added. \
    Multiple calls passing the same combination of eventName and listener \
    will result in the listener being added, and called, multiple times.
```

---

```javascript {.line-numbers}
const EventEmitter = require('node:events');
const myEE = new EventEmitter();
myEE.on('foo', () => console.log('a'));
myEE.prependListener('foo', () => console.log('b'));
myEE.emit('foo');
// Prints:
//   b
//   a
```

---

```javascript {.line-numbers}
emitter.once(eventName, listener)

eventName <string> | <symbol> The name of the event.
listener <Function> The callback function
Returns: <EventEmitter>

Adds a one-time listener function for the event named eventName. \
    The next time eventName is triggered, this listener is removed and then invoked.
```

---

```javascript {.line-numbers}
const EventEmitter = require('node:events');
const myEE = new EventEmitter();
myEE.once('foo', () => console.log('a'));
myEE.prependOnceListener('foo', () => console.log('b'));
myEE.emit('foo');
// Prints:
//   b
//   a
```

---

```javascript {.line-numbers}
emitter.removeListener(eventName, listener)

eventName <string> | <symbol>
listener <Function>
Returns: <EventEmitter>

Removes the specified listener from the listener array for the event named eventName.
```

---

```javascript {.line-numbers}
const EventEmitter = require('node:events');
class MyEmitter extends EventEmitter {}
const myEmitter = new MyEmitter();

const callbackA = () => {
  console.log('A');
  myEmitter.removeListener('event', callbackB);
};

const callbackB = () => {
  console.log('B');
};

myEmitter.on('event', callbackA);
myEmitter.on('event', callbackB);

// callbackA removes listener callbackB but it will still be called.
// Internal listener array at time of emit [callbackA, callbackB]
myEmitter.emit('event');
// Prints:
//   A
//   B

// callbackB is now removed.
// Internal listener array [callbackA]
myEmitter.emit('event');
// Prints:
//   A
```

---

```javascript {.line-numbers}
emitter.emit(eventName[, ...args])

eventName <string> | <symbol>
...args <any>
Returns: <boolean>

Synchronously calls each of the listeners registered for the event named eventName, \
    in the order they were registered, passing the supplied arguments to each.

Returns true if the event had listeners, false otherwise.
```

---

```javascript {.line-numbers}
const EventEmitter = require('node:events');
const myEmitter = new EventEmitter();

// First listener
myEmitter.on('event', function firstListener() {
  console.log('Helloooo! first listener');
});
// Second listener
myEmitter.on('event', function secondListener(arg1, arg2) {
  console.log(`event with parameters ${arg1}, ${arg2} in second listener`);
});
// Third listener
myEmitter.on('event', function thirdListener(...args) {
  const parameters = args.join(', ');
  console.log(`event with parameters ${parameters} in third listener`);
});

console.log(myEmitter.listeners('event'));

myEmitter.emit('event', 1, 2, 3, 4, 5);

// Prints:
// [
//   [Function: firstListener],
//   [Function: secondListener],
//   [Function: thirdListener]
// ]
// Helloooo! first listener
// event with parameters 1, 2 in second listener
// event with parameters 1, 2, 3, 4, 5 in third listener
```

---

### `EventEmitter`的常用事件

1. `newListener`：该事件在添加新监听器时被触发
1. `removeListener`：该事件在移除监听器时被触发

---

```javascript {.line-numbers}
Event: 'newListener'

eventName <string> | <symbol> The name of the event being listened for
listener <Function> The event handler function

The EventEmitter instance will emit its own 'newListener' event \
    before a listener is added to its internal array of listeners.
```

---

```javascript {.line-numbers}
const EventEmitter = require('node:events');

const myEmitter = new EventEmitter();
// Only do this once so we don't loop forever
myEmitter.once('newListener', (event, listener) => {
    if (event === 'event') {
        // Insert a new listener in front
        myEmitter.on('event', () => {
            console.log('B');
        });
    }
});
myEmitter.on('event', () => {
    console.log('A');
});
myEmitter.emit('event');
// Prints:
//   B
//   A
```

---

```javascript {.line-numbers}
Event: 'removeListener'

eventName <string> | <symbol> The event name
listener <Function> The event handler function

The 'removeListener' event is emitted after the listener is removed.
```

---

### `error`事件

1. `EventEmitter`定义了特殊的事件`error`，当遇到异常时通常会触发`error`事件
1. 当`error`被触发时，如果没有相应的`listener`，`node`会把它当作异常，退出程序并输出错误信息
1. 一般需要为可能会触发`error`事件的对象设置`listener`，避免遇到错误后整个程序崩溃

<!--TODO: add-->

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
