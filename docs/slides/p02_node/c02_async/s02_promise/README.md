---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Promise and Async-Await_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Promise and Async-Await

>1. [阮一峰. ES6教程-Promise对象.](https://wangdoc.com/es6/promise)
>2. [廖雪峰. JavaScript教程-Promise.](https://liaoxuefeng.com/books/javascript/browser/promise/)
>3. [阮一峰. ES6教程-async函数.](https://wangdoc.com/es6/async)
>4. [廖雪峰. JavaScript教程-async.](https://liaoxuefeng.com/books/javascript/browser/async/index.html)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction](#introduction)
2. [`Promise`](#promise)
3. [Async-Await](#async-await)
4. [MacroTasks and MicroTasks](#macrotasks-and-microtasks)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction

---

1. `promise`是`js`实现异步编程的一种方式，充当了`asynchronous`和`callback`之间的`proxy`
2. `promise`将`callback nested calls`转化为`callback chain calls`，`async-await`则进一步以同步方式书写异步代码
3. `promise`由社区提出和实现（`Bluebird`等），`ES6`将其写入了语言标准，统一了用法
4. `es2017`引入`async`和`await`，使得异步编程更加方便

---

### Callback Hell

![height:560](./.assets/image/image-20240926164516322.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Promise`

---

1. 语法上，`Promise`是一个对象，从它可以获取异步操作的消息
2. 语义上，`Promise`是一个容器，用来封装一个异步操作并可以获取其成功或失败的结果值
3. `Promise`提供统一的`api`，各种异步操作都可以用同样的方法进行处理
4. `Promise`是`ECMAScript`标准的一部分，不需要`require()`或`import`

---

### 三种状态

1. `pending`：初始状态，既不是`fulfilled`/`resolved`，也不是`rejected`
2. `fulfilled`/`resolved`：意味着操作成功
3. `rejected`：意味着操作失败

---

### `promise`的特点

1. 状态不受影响：只有异步操作，可以决定当前是哪一种状态，任何其他操作都无法改变这个状态
2. 状态持久凝固：`Promise`对象的状态改变，只有两种可能，从`pending`变为`fulfilled`和从`pending`变为`rejected`
3. 结果重复可取：一旦状态凝固了，任何时候都可以得到这个结果

promise the result of an asynchronous operation

---

### `promise`的缺点

1. 无法取消`Promise`，一旦新建它就会立即执行，无法中途取消
2. 如果不设置回调函数，`Promise`内部抛出的错误，不会反应到外部（未来可能会更新）
3. 当处于`pending`状态时，无法得知进展到哪一个阶段（刚刚开始还是即将完成）

---

![width:1120](./.assets/image/image-20240926154919349.png)

---

1. 创建`Promise`对象，启动`executor`定义的异步任务
2. 通过`.then()`给`Promise`对象添加回调函数，在回调函数中处理异步任务的结果

---

```javascript {.line-numbers}
new Promise(executor);
executor(resolve, reject) => {};
```

```javascript {.line-numbers}
Promise.prototype.then(onFulfilled, onRejected) => {};
onFulfilled(value) => {};
onRejected(reason) => {};
```

---

1. `new Promise(executor);`: `pending`
2. `resolve(value);`: `pending` -> `fulfilled`
3. `reject(reason);`: `pending` -> `rejected`
4. `throw new Error('error message');`: `pending` -> `rejected`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Promise`的构造函数

---

```javascript {.line-numbers}
new Promise(executor);
executor(resolve, reject);
```

1. `executor`立即同步调用
2. `executor`中启动异步任务
3. `resolve`和`reject`由运行时提供

---

```javascript {.line-numbers}
const promise = new Promise(function(resolve, reject) {
  // ... some code
  if (/* 成功 */){
    resolve(value);
  } else {
    reject(error);
  }
});
```

---

1. `Promise`构造函数接受一个函数作为参数，该函数的两个形参分别是`resolve`和`reject`
2. `resolve`函数的作用：将`Promise`对象的状态从`pending`变为`resolved`，在异步操作成功时调用，并将异步操作的结果，作为参数传递出去
3. `reject`函数的作用：将`Promise`对象的状态从`pending`变为`rejected`，在异步操作失败时调用，并将异步操作的错误，作为参数传递出去

---

```javascript {.line-numbers}
Promise.resolve(value);
Promise.reject(reason);
```

---

1. `Promise`实例生成以后，可以用`then()`分别指定`resolved`状态和`rejected`状态的回调函数

```javascript {.line-numbers}
promise.then(function(value) {
    // resolved handler
}, function(error) {
    // rejected handler
});
```

---

```javascript {.line-numbers}
function timeout(ms) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, ms, 'done');
    });
}

timeout(100).then((value) => {
    console.log(value);
});
```

---

1. `Promise`新建后会立即执行

```javascript {.line-numbers}
let promise = new Promise(function(resolve, reject) {
  console.log('Promise');
  resolve();
});

promise.then(function() {
  console.log('resolved.');
});

console.log('Hi!');

// Promise
// Hi!
// resolved
```

---

```javascript {.line-numbers}
const p1 = new Promise(function (resolve, reject) {
  // ...
});

const p2 = new Promise(function (resolve, reject) {
  // ...
  resolve(p1);
});
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Promise`的实例方法：`then()`, `catch()` and `finally()`

---

```javascript {.line-numbers}
Promise.prototype.then(onFulfilled);
Promise.prototype.then(onFulfilled, onRejected);

// alias of Promise.prototype.then(null, onRejected)
// alias of Promise.prototype.then(undefined, onRejected)
Promise.prototype.catch(onRejected);

Promise.prototype.finally(onFinally);

promise
.then(result => {···})
.catch(error => {···})
.finally(() => {···});
```

---

```javascript {.line-numbers}
const someAsyncThing = function() {
  return new Promise(function(resolve, reject) {
    // 下面一行会报错，因为x没有声明
    resolve(x + 2);
  });
};

someAsyncThing().then(function() {
  console.log('everything is great');
});

// will print the error, but not stop the program
setTimeout(() => { console.log(123) }, 2000);
// Uncaught (in promise) ReferenceError: x is not defined
// 123
```

---

```javascript {.line-numbers}
const fs = require('fs');

// 直接利用readfile来进行读取
/* fs.readFile(__dirname + '/data.txt',(err,data)=>{
    if(err) throw err;
    console.log(data.toString());
}) */

// 利用promise来实现文件的读取
const p = new Promise((resolve, reject) => {
    fs.readFile(__dirname + '/data.txt', (err, data) => {
        if (err) {
            reject(err);
        }else{
            resolve(data);
        }
    });
});

p.then(value=>{
    console.log(value.toString());
},reason=>{
    console.log(reason);
})
```

---

```javascript {.line-numbers}
const mongoose = require('mongoose');

new Promise((resolve, reject) => {
    mongoose.connect('mongodb://127.0.0.1/project');
    mongoose.connection.on('open', ()=>{
        resolve();  // connect successfully
    });

    mongoose.connection.on('error', () => {
        reject();   // connect failed
    })
}).then(value => {
    // create schema
    const NoteSchema = new mongoose.Schema({
        title: String,
        content: String
    })
    // create model
    const NoteModel = mongoose.model('notes', NoteSchema);

    // find all notes
    NoteModel.find().then(value => {
        console.log(value);
    }, reason => {
        console.log(reason);
    })
}, reason => {
    console.log('connect failed');
})
```

---

```javascript {.line-numbers}
// callback function mode
const fs = require('fs');
fs.readFile('./resource/1.txt',(err,data1)=>{
    if(err) throw err;
    fs.readFile('./resource/2.txt',(err,data2)=>{
        if(err) throw err;
        fs.readFile('./resource/3.txt',(err,data3)=>{
            if(err) throw err;
            console.log(data1 + data2 + data3);
        })
    })
})
```

---

```javascript {.line-numbers}

// promise mode
new Promise((resolve,reject)=>{
    fs.readFile('./resource/1.txt',(err,data)=>{
         //如果失败 则修改promise对象状态为失败
        if(err) reject(err);
        //如果成功 则修改promise对象状态为成功
        resolve(data);
    })
}).then(value=>{
    return new Promise((resolve,reject)=>{
        fs.readFile('./resource/2.txt',(err,data)=>{
             //失败
            if(err) reject(err);
            //成功
            resolve([value,data]);
        })
    })
}).then(value=>{
    return new Promise((resolve,reject)=>{
        fs.readFile('./resource/3.txt',(err,data)=>{
             //失败
            if(err) reject(err);
            value.push(data);
            //成功
            resolve(value);
        })
    })
}).then(value=>{
    console.log(value.join(""));
})
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Promise`的静态方法：`all()`, `allSettled()`， `any()`， `race()`, `rejected()` and `resolved()`

---

<!--TODO: add it-->

```javascript {.line-numbers}

```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Async-Await

>1. [async function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function)
>2. [await](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await)

---

1. `async`是一个加在函数前的修饰符，被`async`定义的函数会返回一个`Promise`对象
2. `promise`对象的结果由`async`函数执行的返回值决定
<!-- 1. `async`函数是`Generator`的语法糖 -->
<!-- 1. `async`函数的返回值是`promise`对象 -->

---

```javascript {.line-numbers}
async function fun0(){
    console.log(1);
    return 1;
}
fun0().then(val=>{
    console.log(val) // 1,1
})

async function fun1(){
    console.log('Promise');
    return new Promise(function(resolve,reject){
        resolve('Promise')
    })
}
fun1().then(val => {
    console.log(val); // Promise Promise
});
```

---

1. `await`的表达式一般为`promise`对象，也可以是其它的值
2. 如果表达式是`promise`对象
    1. 如果成功，`await`返回`promise`成功的值
    2. 如果异常，会抛出异常，需要通过`try...catch`捕获处理
3. 如果表达式是其它值, 直接将此值作为`await`的返回值
4. `await`必须写在`async`函数中，但`async`函数中可以没有`await`

---

```javascript {.line-numbers}
async function fun() {
    let a = await 1;
    let b = await new Promise((resolve, reject) => {
        setTimeout(function () {
            resolve("setTimeout");
        }, 3000);
    });
    let c = await (function () {
        return "function";
    })();
    console.log(a, b, c);
}
fun(); // print after 3 second： 1 "setTimeout" "function"
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## MacroTasks and MicroTasks

---

1. `JS`中用来存储待执行回调函数的队列包含2个不同特点的列队
    1. 宏列队: 用来保存待执行的宏任务(回调), 比如: `Timer`回调，`AJAX`回调，`Event`回调等
    2. 微列队: 用来保存待执行的微任务(回调), 比如: `Promise`回调，`Mutation`回调等
2. 运行时会区别这2个队列
    1. 首先必须先执行所有的初始化同步任务代码
    2. 每次准备取出第一个宏任务执行前, 都要将所有的微任务一个一个取出来执行

---

![height:600](./.assets/image/js_macrotasks_and_microtasks.png)

---

```javascript {.line-numbers}
setTimeout(function() {
  console.log(1);
}, 0);

new Promise(function (resolve, reject) {
  resolve(2);
}).then(console.log);

console.log(3);
// 3
// 2
// 1
```

---

<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
