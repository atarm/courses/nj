setTimeout(() => {
    console.log("setTimeout(): " + 111);
});

new Promise((resolve, reject) => {
    resolve();
    reject();
    console.log("executor: " + 222);
}).then((v) => {
    console.log("then(): " + 333);
});

console.log("main thread: " + 444);
