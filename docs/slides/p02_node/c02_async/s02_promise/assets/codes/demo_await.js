async function fun() {
    let a = await 1;
    let b = await new Promise((resolve, reject) => {
        setTimeout(function () {
            resolve("setTimeout");
        }, 3000);
    });
    let c = await (function () {
        return "function";
    })();
    console.log(a, b, c);
}
fun(); // print after 3 second： 1 "setTimeout" "function"
