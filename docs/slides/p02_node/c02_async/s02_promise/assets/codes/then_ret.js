Promise.resolve(2) // resolve(2) 函数返回一个Promise<number>对象
    .then((x) => {
        console.log(x); // 输出2， 表示x类型为number且值为2，也就是上面resolve参数值
        return 'hello world'; // 回调函数返回字符串类型，then函数返回Promise<string>
    }) // then函数返回Promise<string>类型
    .then((x) => {
        console.log(x); // 输出hello world，也就是上一个then回调函数返回值，表明上一个then的返回值就是下一个then的参数
    }) // then函数回调函数中没有返回值，则为Promise<void>
    .then((x) => {
        // 前面的then的回调函数没有返回值所以这个x是undefined
        console.log(x); // undefined
    }) // Promise<void>
    .then(() => {
        // 前面没有返回值，这里回调函数可以不加返回值
        return Promise.resolve("hello world"); // 返回一个Promise<string>类型
    }) // 这里then的返回值是Promise<string>
    .then((x) => {
        // 虽然上面的then中回调函数返回的是Promise<string>类型但是这里x并不是Promise<string>类型而是string类型
        console.log(x); // hello world
        return Promise.resolve(2); // 返回一个Promise<number>类型对象
    }); // 返回Promise<number>类型
