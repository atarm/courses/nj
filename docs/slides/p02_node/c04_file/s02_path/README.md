---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Path_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Path

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction](#introduction)
2. [`path`的使用](#path的使用)
3. [Dynamic Absolute Path动态绝对路径](#dynamic-absolute-path动态绝对路径)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction

---

1. 提供一些工具函数，用于处理文件与目录的路径（`string`）
2. 提供一种跨操作系统的方式处理文件路径

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `path`的使用

---

### 跨平台

1. `path.delimiter`：环境变量分隔符
2. `path.sep`：路径分隔符
3. `path.normalize(path)`：规范化路径
4. `path.parse(path)`：将路径字符串转换成路径对象

---

### `path.basename()`获取文件名

```javascript {.line-numbers}
path.basename(path[, suffix])

path <string>
suffix <string> An optional suffix to remove

Returns: <string>
```

---

```javascript {.line-numbers}
path.basename('/foo/bar/baz/asdf/quux.html'); // Returns: 'quux.html'

path.basename('/foo/bar/baz/asdf/quux.html', '.html'); // Returns: 'quux'
```

---

### `path.dirname()`获取目录名

```javascript {.line-numbers}
path.dirname(path)

path <string>

Returns: <string>
```

---

```javascript {.line-numbers}
path.dirname('/foo/bar/baz/asdf/quux'); // Returns: '/foo/bar/baz/asdf'
```

---

### `path.extname()`获取扩展名

```javascript {.line-numbers}
path.extname(path)

path <string>

Returns: <string>
```

---

```javascript {.line-numbers}
path.extname('index.html'); // Returns: '.html'

path.extname('index.coffee.md'); // Returns: '.md'

path.extname('index.'); // Returns: '.'

path.extname('index'); // Returns: ''

path.extname('.index'); // Returns: ''

path.extname('.index.md'); // Returns: '.md'
```

---

### `path.isAbsolute()`判断是否是绝对路径

```javascript {.line-numbers}
path.isAbsolute(path)

path <string>

Returns: <boolean>
```

---

```javascript {.line-numbers}
path.isAbsolute('/foo/bar'); // true
path.isAbsolute('/baz/..');  // true
path.isAbsolute('qux/');     // false
path.isAbsolute('.');        // false
```

---

### `path.normalize()`规范化路径

```javascript {.line-numbers}
const path = require('path');

const windowsPath = 'C:\\temp\\foo\\bar\\..\\';
const normalizedWindowsPath = path.normalize(windowsPath);
console.log(normalizedWindowsPath); // 'C:\temp\foo\bar\..\'

const unixPath = '/foo/bar//baz/asdf/quux/..';
const normalizedUnixPath = path.normalize(unixPath);
console.log(normalizedUnixPath); // '/foo/bar/baz/asdf/'
```

---

### `path.parse()`将路径字符串转换成路径对象

```javascript {.line-numbers}
path.parse(path)

path <string>

Returns: <Object>
The path.parse() method returns an object whose properties represent \
    significant elements of the path. Trailing directory separators \
    are ignored, see path.sep.

The returned object will have the following properties:

dir <string>
root <string>
base <string>
name <string>
ext <string>
```

---

```javascript {.line-numbers}
path.parse('/home/user/dir/file.txt');
// Returns:
// { root: '/',
//   dir: '/home/user/dir',
//   base: 'file.txt',
//   ext: '.txt',
//   name: 'file' }
```

---

### `path.format()`将路径对象转换成路径字符串

```javascript {.line-numbers}
path.format(pathObject)

pathObject <Object> Any JavaScript object having the following properties:
dir <string>
root <string>
base <string>
name <string>
ext <string>

Returns: <string>
The path.format() method returns a path string from an object. \
    This is the opposite of path.parse().
```

---

```javascript {.line-numbers}
// If `dir`, `root` and `base` are provided,
// `${dir}${path.sep}${base}`
// will be returned. `root` is ignored.
path.format({
  root: '/ignored',
  dir: '/home/user/dir',
  base: 'file.txt',
});
// Returns: '/home/user/dir/file.txt'

// `root` will be used if `dir` is not specified.
// If only `root` is provided or `dir` is equal to `root` then the
// platform separator will not be included. `ext` will be ignored.
path.format({
  root: '/',
  base: 'file.txt',
  ext: 'ignored',
});
// Returns: '/file.txt'

// `name` + `ext` will be used if `base` is not specified.
path.format({
  root: '/',
  name: 'file',
  ext: '.txt',
});
// Returns: '/file.txt'

// The dot will be added if it is not specified in `ext`.
path.format({
  root: '/',
  name: 'file',
  ext: 'txt',
});
// Returns: '/file.txt'
```

---

### `path.join()`路径拼接

```javascript {.line-numbers}
path.join([...paths])

...paths <string> A sequence of path segments

Returns: <string>
```

---

```javascript {.line-numbers}
path.join('/foo', 'bar', 'baz/asdf', 'quux', '..');
// Returns: '/foo/bar/baz/asdf'

path.join('foo', {}, 'bar');
// Throws 'TypeError: Path must be a string. Received {}'
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## Dynamic Absolute Path动态绝对路径

---

1. 脚本中的相对路径相对的是运行脚本的`shell`的`pwd`，而不是相对于脚本所在目录
2. `__dirname`：当前文件模块所属目录的绝对路径
3. `__filename`：当前文件模块的绝对路径
4. 通过`__dirname`和`__filename`动态构建绝对路径

<!--
1. `working_directory`：当前工作目录，其他相对路径均基于该目录
-->

---

```javascript {.line-numbers}
let fs = require('fs');
let path = require('path');
fs.readFile(path.join(__dirname, 'a.txt'), 'utf-8', function (err, data) {
    if (err) {
        return console.log(err);
    }
    console.log(data.toString());
});
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
