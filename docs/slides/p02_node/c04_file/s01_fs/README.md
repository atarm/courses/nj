---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_File IO_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# File IO

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`fs`模块](#fs模块)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `fs`模块

---

1. `fs`提供类似`POSIX`的文件操作`api`
1. `fs`同时提供了同步方法、异步方法和`Promise`方法（`fs/promises`），同步方法比异步方法/`Promise`方法多一个`Sync`，例如：`fs.readFileSync()`和`fs.readFile()`

---

### 文件读取

```javascript {.line-numbers}
fs.readFile(path[, options], callback)

path <string> | <Buffer> | <URL> | <integer> filename or file descriptor
options <Object> | <string>
    encoding <string> | <null> Default: null
    flag <string> See support of file system flags. Default: 'r'.
    signal <AbortSignal> allows aborting an in-progress readFile
callback <Function>
    err <Error> | <AggregateError>
    data <string> | <Buffer>
```

---

```javascript {.line-numbers}
// synchronous
let fs = require("fs");
let data = fs.readFileSync("read.txt");
console.log(data);
console.log(data.toString());
```

---

```javascript {.line-numbers}
let fs = require("fs");
fs.readFile("read.txt", function (error, data) {
    if (error) {
        console.log("读取文件失败");
        console.log(error);
        return;
    }
    console.log(data);
    console.log(data.toString());
})
```

---

### 文件写入

```javascript {.line-numbers}
fs.writeFile(path, data[, options], callback)

path <string> | <Buffer> | <URL> | <integer> filename or file descriptor
data <string> | <Buffer> | <TypedArray> | <DataView>
options <Object> | <string>
    encoding <string> | <null> Default: 'utf8'
    mode <integer> Default: 0o666
    flag <string> See support of file system flags. Default: 'w'.
    signal <AbortSignal> allows aborting an in-progress writeFile
callback <Function>
    err <Error> | <AggregateError>
```

---

```javascript {.line-numbers}
// asynchronous
let fs = require('fs');
fs.writeFile('write.txt', '你好 node.js', function (err) {
    if (err) {
        console.log('不好意思，文件写入失败了');
    } else {
        console.log('内容已成功写入');
    }
});
```

---

### 文件追加

```javascript {.line-numbers}
fs.appendFile(path, data[, options], callback)

path <string> | <Buffer> | <URL> | <number> filename or file descriptor
data <string> | <Buffer>
options <Object> | <string>
    encoding <string> | <null> Default: 'utf8'
    mode <integer> Default: 0o666
    flag <string> See support of file system flags. Default: 'a'.
callback <Function>
    err <Error>
```

---

```javascript {.line-numbers}
// 向文件追加内容
let fs = require('fs');
let data = 'Node.js';
fs.appendFile('append.txt', data, function(err) {
    if (err) {
        return console.log('文件追加失败了');
    }
    console.log('文件追加成功了');
});
```

---

### 文件复制

```javascript {.line-numbers}
// 文件复制
let fs = require('fs');
//读取a.txt文件数据
fs.readFile('src.txt', function(err, data) {
    if (err) {
        return console.log('读取文件失败了');
    }
    console.log(data);
    //将数据写入b.txt文件
    fs.writeFile('dst.txt', data.toString(), function(err) {
        if (err) {
            return console.log('写入文件失败了');
        }
    })
    console.log('文件复制成功了');
});
```

---

```javascript {.line-numbers}
/*
* 文件复制模块
*/
let fs = require('fs');
/*
* 定义文件复制函数copy()
* src：需要读取的文件
* dst:目标文件
* callback:回调函数
* */
function copy(src, dst, callback) {
    //读取文件
    fs.readFile(src, function(err, data) {
        if (err) {
            return callback(err);
        }
        //写入文件
        fs.writeFile(dst, data.toString(), function(err) {
            if (err) {
                return callback(err);
            }
            callback(null);
        });
    });
}
//导出模块
module.exports = copy;
```

---

### 删除文件

```javascript {.line-numbers}
fs.unlink(path, callback)#

path <string> | <Buffer> | <URL>
callback <Function>
err <Error>
```

---

```javascript {.line-numbers}
let fs = require("fs");

fs.unlink("./files/file_to_rm.txt", function (err) {
    if (err) {
        return console.error(err);
    }
    console.log("文件删除成功！");
});
```

---

### 获取文件信息

```javascript {.line-numbers}
fs.stat(path[, options], callback)#

path <string> | <Buffer> | <URL>
options <Object>
    bigint <boolean> Whether the numeric values in the returned <fs.Stats> object should be bigint. Default: false.
callback <Function>
    err <Error>
    stats <fs.Stats>
```

---

| 方法                        | 说明                                      |
| :-------------------------- | :---------------------------------------- |
| `stats.isFile()`            | 如果是文件返回`true`，否则返回`false`     |
| `stats.isDirectory()`       | 如果是目录返回`true`，否则返回`false`     |
| `stats.isBlockDevice()`     | 如果是块设备返回`true`，否则返回`false`   |
| `stats.isCharacterDevice()` | 如果是字符设备返回`true`，否则返回`false` |

---

| 方法                     | 说明                                      |
| :----------------------- | :---------------------------------------- |
| `stats.isSymbolicLink()` | 如果是软链接返回`true`，否则返回`false`   |
| `stats.isFIFO()`         | 如果是`FIFO`，返回`true`，否则返回`false` |
| `stats.isSocket()`       | 如果是`Socket`返回`true`，否则返回`false` |

---

```javascript {.line-numbers}
let fs = require('fs');
fs.stat('.', function (err, stats) {
    //判断是否是文件
    console.log("是否是文件：" + stats.isFile());
    //输出文件信息
    console.log(stats);
});
```

---

### 创建目录

```javascript {.line-numbers}
fs.mkdir(path[, options], callback)

path <string> | <Buffer> | <URL>
options <Object> | <integer>
    recursive <boolean> Default: false
    mode <string> | <integer> Not supported on Windows. Default: 0o777.
callback <Function>
    err <Error>
    path <string> | <undefined> Present only if a directory is created with recursive set to true.
```

---

```javascript {.line-numbers}
let fs = require('fs');
console.log('在当前目录下创建目录"add/"...');
fs.mkdir('./add',{'recursive':true},function(err){
    if (err) {
        return console.error(err);
    }
    console.log("目录创建成功");
});
```

---

### 读取目录

```javascript {.line-numbers}
fs.readdir(path[, options], callback)#

path <string> | <Buffer> | <URL>
options <string> | <Object>
    encoding <string> Default: 'utf8'
    withFileTypes <boolean> Default: false
    recursive <boolean> Default: false
callback <Function>
    err <Error>
    files <string[]> | <Buffer[]> | <fs.Dirent[]>
```

---

```javascript {.line-numbers}
let fs = require('fs');
console.log('查看当前目录下的"add/"...')
fs.readdir('./add',function(err, files){
    if (err) {
        return console.error(err);
    }

    console.log(files);
    files.forEach( function (file){ //遍历所有文件
        console.log( file ); // 输出文件名
    })
});
```

---

### 删除目录

```javascript {.line-numbers}
fs.rmdir(path[, options], callback)

path <string> | <Buffer> | <URL>
options <Object>
    maxRetries <integer> If an EBUSY, EMFILE, ENFILE, ENOTEMPTY, or EPERM error is encountered, \
        Node.js retries the operation with a linear backoff wait of retryDelay milliseconds longer on each try. \
        This option represents the number of retries. This option is ignored if the recursive option is not true. Default: 0.
    recursive <boolean> If true, perform a recursive directory removal. \
        In recursive mode, operations are retried on failure. Default: false. Deprecated.
    retryDelay <integer> The amount of time in milliseconds to wait between retries. \
        This option is ignored if the recursive option is not true. Default: 100.
callback <Function>
    err <Error>
```

---

```javascript {.line-numbers}
// remove empty directory
let fs=require('fs');
fs.rmdir('add',function (err) {
    if (err){
        return console.log(err);
    }
    console.log('目录删除成功');
});
```

---

```javascript {.line-numbers}
/*
* remove files in directory
*/
let fs = require('fs');

let dir = 'files/dir_to_rm_nonempty/';

fs.readdir(dir, function (err, files) {// 先读取目录中的内容，再删除
    if (err) {
        return console.error(err);
    }
    console.log(files);

    files.forEach(function (file) { // 遍历所有文件
        fs.stat(dir + file, function (err, stats) {
            if (stats.isFile()) {
                fs.unlink(dir + file, function (err) {
                    if (err) {
                        return console.error(err);
                    }
                    console.log(file + '文件删除成功！');
                })
            } else if (stats.isDirectory()) {
                fs.rmdir(dir + file, function (err) {
                    console.log(dir + file)
                    if (err) {
                        return console.log(err);
                    }
                    console.log('目录删除成功');
                });
            }
        });
    });
});
```

---

```javascript {.line-numbers}
/*
* remove nonempty directory
*/
var fs = require('fs');

let dir = 'files/dir_to_rm_nonempty/';

fs.readdir(dir, function (err, files) {
    if (err) {
        return console.error(err);
    }
    //遍历所有文件
    files.forEach(function (file) {
        // 输出文件名
        console.log(file);
        //删除文件
        fs.stat(dir + file, function (err, stats) {
            if (stats.isFile()) {
                fs.unlink(dir + file, function (err) {
                    if (err) {
                        return console.error(err);
                    }
                    console.log(file + '文件删除成功！');
                });
            }
        });
    });
    fs.rmdir(dir, function (err) {
        if (err) {
            return console.error(err.message);
        }
        console.log("目录删除成功!");
    });
});
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
