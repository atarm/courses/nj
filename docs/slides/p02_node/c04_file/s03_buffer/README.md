---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Buffer_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Buffer

>1. [Node.js Buffer[DB/OL].](https://www.runoob.com/nodejs/nodejs-buffer.html)
>1. [node核心模块-Buffer[DB/OL].](https://segmentfault.com/a/1190000017442398)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction](#introduction)
2. [`Buffer`的使用](#buffer的使用)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction

---

1. `es6`之前，`js`只有字符串数据类型，没有二进制数据类型
2. `Buffer`用于创建一个存放二进制数据的缓存区，类似于一个字节数组
3. `Buffer`创建后大小固定，不能调整
4. `Buffer`最大为 **_1GB_**
5. `Buffer`不需要`require()`（全局对象），可直接访问
6. `fs`进行的文件操作默认基于`Buffer`

<!--

```javascript {.line-numbers}
Class: Buffer

The Buffer class is a global type for dealing with binary data directly. \
    It can be constructed in a variety of ways.
```

>[Node.js v18.18.2 documentation. Buffer.](https://nodejs.org/dist/latest-v18.x/docs/api/buffer.html)

-->

---

![width:1120](./.assets/images/image-20231018103716939.png)

---

### Buffer的核心

1. 分配一块内存空间
2. 按字节为单位操作内存

```javascript {.line-numbers}
Buffer objects are used to represent a fixed-length sequence of bytes. \
    Many Node.js APIs support Buffers.

The Buffer class is a subclass of JavaScript Uint8Array class and \
    extends it with methods that cover additional use cases. \
    Node.js APIs accept plain Uint8Arrays wherever Buffers are supported as well.

While the Buffer class is available within the global scope, \
    it is still recommended to explicitly reference it \
    via an import or require statement.
```

---

### 字符集

<!--TODO:有哪些常用的字符集-->

1. `ascii`：7位编码
1. `utf8`：多字节编码的`Unicode`
1. `utf16le`：2或4字节，小字节序编码的`Unicode`
1. `ucs2`：`utf16le`的别名
1. `base64`：`Base64`编码
1. `latin1`：1字节编码
1. `binary`：`latin1`的别名
1. `hex`：将每个字节编码为两个十六进制字符

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Buffer`的使用


---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 创建`Buffer`类

---

1. `Buffer.alloc(size[, fill[, encoding]])`： 返回一个指定大小的`Buffer`实例，如果没有设置`fill`，则默认填满`0`
1. `Buffer.allocUnsafe(size)`： 返回一个指定大小的`Buffer`实例，但是它不会被初始化，所以它可能包含原有的数据
1. `Buffer.allocUnsafeSlow(size)`：返回一个指定大小的`Buffer`实例，与`Buffer.allocUnsafe`的区别在于不使用预分配`buffer pool`，而是从外部获取一块新的内存空间，可以避免`buffer pool`创建太多的`Buffer`实例

---

1. `Buffer.from(array)`：返回一个被`array`的值初始化的新的`Buffer`实例
1. `Buffer.from(buffer)`：复制传入的`Buffer`实例的数据，并返回一个新的`Buffer`实例
1. `Buffer.from(string[, encoding])`： 返回一个被`string`值初始化的新的`Buffer`实例
1. `Buffer.from(arrayBuffer[, byteOffset[, length]])`：返回一个新建的与给定的 `ArrayBuffer` **_共享同一内存_** 的`Buffer`

---

```javascript {.line-numbers}
Static method: Buffer.alloc(size[, fill[, encoding]])

size <integer> The desired length of the new Buffer.
fill <string> | <Buffer> | <Uint8Array> | <integer> \
    A value to pre-fill the new Buffer with. Default: 0.
encoding <string> If fill is a string, this is its encoding. Default: 'utf8'.

Allocates a new Buffer of size bytes. If fill is undefined, \
    the Buffer will be zero-filled.
```

---

```javascript {.line-numbers}
const buf = Buffer.alloc(5);

console.log(buf);
// Prints: <Buffer 00 00 00 00 00>
```

---

```javascript {.line-numbers}
Static method: Buffer.allocUnsafe(size)

size <integer> The desired length of the new Buffer.

Allocates a new Buffer of size bytes. If size is larger than \
    buffer.constants.MAX_LENGTH or smaller than 0, ERR_OUT_OF_RANGE is thrown.
The underlying memory for Buffer instances created in this way is not initialized. \
    The contents of the newly created Buffer are unknown and may contain sensitive data. \
    Use Buffer.alloc() instead to initialize Buffer instances with zeroes.
```

---

```javascript {.line-numbers}
const buf = Buffer.allocUnsafe(10);

console.log(buf);
// Prints (contents may vary): <Buffer a0 8b 28 3f 01 00 00 00 50 32>

buf.fill(0);

console.log(buf);
// Prints: <Buffer 00 00 00 00 00 00 00 00 00 00>
```

---

```javascript {.line-numbers}
Static method: Buffer.allocUnsafeSlow(size)

size <integer> The desired length of the new Buffer.

Allocates a new Buffer of size bytes. If size is larger than \
    buffer.constants.MAX_LENGTH or smaller than 0, ERR_OUT_OF_RANGE is thrown. \
    A zero-length Buffer is created if size is 0.
```

<!-- ---

```javascript {.line-numbers}
// Need to keep around a few small chunks of memory.
const store = [];

socket.on('readable', () => {
  let data;
  while (null !== (data = readable.read())) {
    // Allocate for retained data.
    const sb = Buffer.allocUnsafeSlow(10);

    // Copy the data into the new allocation.
    data.copy(sb, 0, 0, 10);

    store.push(sb);
  }
});
``` -->

---

```javascript {.line-numbers}
Static method: Buffer.from(array)
```

>array <integer[]>
>
>Allocates a new Buffer using an array of bytes in the range 0 – 255. Array entries outside that range will be truncated to fit into it.

---

```javascript {.line-numbers}
// Creates a new Buffer containing the UTF-8 bytes of the string 'buffer'.
const buf = Buffer.from([0x62, 0x75, 0x66, 0x66, 0x65, 0x72]);
```

---

```javascript {.line-numbers}
Static method: Buffer.from(arrayBuffer[, byteOffset[, length]])

arrayBuffer <ArrayBuffer> | <SharedArrayBuffer> An ArrayBuffer, SharedArrayBuffer, \
    for example the .buffer property of a TypedArray.
byteOffset <integer> Index of first byte to expose. Default: 0.
length <integer> Number of bytes to expose. Default: arrayBuffer.byteLength - byteOffset.

This creates a view of the ArrayBuffer without copying the underlying memory. \
    For example, when passed a reference to the .buffer property of a TypedArray instance, \
    the newly created Buffer will share the same allocated memory as \
    the TypedArray's underlying ArrayBuffer.
```

---

```javascript {.line-numbers}
const arr = new Uint16Array(2);

arr[0] = 5000;
arr[1] = 4000;

// Shares memory with `arr`.
const buf = Buffer.from(arr.buffer);

console.log(buf);
// Prints: <Buffer 88 13 a0 0f>

// Changing the original Uint16Array changes the Buffer also.
arr[1] = 6000;

console.log(buf);
// Prints: <Buffer 88 13 70 17>
```

---

```javascript {.line-numbers}
Static method: Buffer.from(buffer)

buffer <Buffer> | <Uint8Array> An existing Buffer or Uint8Array from \
    which to copy data.

Copies the passed buffer data onto a new Buffer instance.
```

---

```javascript {.line-numbers}
const buf1 = Buffer.from('buffer');
const buf2 = Buffer.from(buf1);

buf1[0] = 0x61;

console.log(buf1.toString());
// Prints: auffer
console.log(buf2.toString());
// Prints: buffer
```

---

```javascript {.line-numbers}
Static method: Buffer.from(object[, offsetOrEncoding[, length]])

object <Object> An object supporting Symbol.toPrimitive or valueOf().
offsetOrEncoding <integer> | <string> A byte-offset or encoding.
length <integer> A length.

For objects whose valueOf() function returns a value not strictly equal to object,\
    returns Buffer.from(object.valueOf(), offsetOrEncoding, length).
```

---

```javascript {.line-numbers}
const buf = Buffer.from(new String('this is a test'));
// Prints: <Buffer 74 68 69 73 20 69 73 20 61 20 74 65 73 74>
```

---

```javascript {.line-numbers}
Static method: Buffer.from(string[, encoding])

string <string> A string to encode.
encoding <string> The encoding of string. Default: 'utf8'.

Creates a new Buffer containing string. The encoding parameter identifies \
    the character encoding to be used when converting string into bytes.
```

---

```javascript {.line-numbers}
const buf1 = Buffer.from('this is a tést');
const buf2 = Buffer.from('7468697320697320612074c3a97374', 'hex');

console.log(buf1.toString());
// Prints: this is a tést
console.log(buf2.toString());
// Prints: this is a tést
console.log(buf1.toString('latin1'));
// Prints: this is a tÃ&copy;st
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Buffer`与`String`的转换

---

1. `buf.toString([encoding[, start[, end]]])`：将`buffer`转换为`string`

```javascript {.line-numbers}
buf.toString([encoding[, start[, end]]])

encoding <string> The character encoding to use. Default: 'utf8'.
start <integer> The byte offset to start decoding at. Default: 0.
end <integer> The byte offset to stop decoding at (not inclusive). \
    Default: buf.length.

Returns: <string>

Decodes buf to a string according to the specified character encoding in encoding.\
    start and end may be passed to decode only a subset of buf.
```

---

```javascript {.line-numbers}
const buf1 = Buffer.allocUnsafe(26);

for (let i = 0; i < 26; i++) {
  // 97 is the decimal ASCII value for 'a'.
  buf1[i] = i + 97;
}

console.log(buf1.toString('utf8'));
// Prints: abcdefghijklmnopqrstuvwxyz
console.log(buf1.toString('utf8', 0, 5));
// Prints: abcde

const buf2 = Buffer.from('tést');

console.log(buf2.toString('hex'));
// Prints: 74c3a97374
console.log(buf2.toString('utf8', 0, 3));
// Prints: té
console.log(buf2.toString(undefined, 0, 3));
// Prints: té
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Buffer`与`JSON`的转换

---

1. `buf.toJSON()`：转换为`JSON`对象

```javascript {.line-numbers}
buf.toJSON()

Returns: <Object>
Returns a JSON representation of buf. JSON.stringify() implicitly \
    calls this function when stringifying a Buffer instance.

Buffer.from() accepts objects in the format returned from this method. \
    In particular, Buffer.from(buf.toJSON()) works like Buffer.from(buf).
```

---

```javascript {.line-numbers}
const buf = Buffer.from([0x1, 0x2, 0x3, 0x4, 0x5]);
const json = JSON.stringify(buf);

console.log(json);
// Prints: {"type":"Buffer","data":[1,2,3,4,5]}

const copy = JSON.parse(json, (key, value) => {
    return value && value.type === 'Buffer' ?
        Buffer.from(value) : value;
});

console.log(copy);
// Prints: <Buffer 01 02 03 04 05>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Buffer`的读写操作

---

1. `buf[index]`：读取或写入`buffer`中指定位置的字节
1. `buf.write(string[, offset[, length]][, encoding])`：将`string`代表的字节数组写入`buffer`
1. `buf.writeInt8(value[, offset])`：将单个字节写入`buffer`

---

```javascript {.line-numbers}
buf.write(string[, offset[, length]][, encoding])

string <string> String to write to buf.
offset <integer> Number of bytes to skip before starting to write string. \
    Default: 0.
length <integer> Maximum number of bytes to write (written bytes will not exceed buf.length - offset). \
    Default: buf.length - offset.
encoding <string> The character encoding of string. Default: 'utf8'.

Returns: <integer> Number of bytes written.

Writes string to buf at offset according to the character encoding in encoding. \
    The length parameter is the number of bytes to write. \
    If buf did not contain enough space to fit the entire string, \
    only part of string will be written. However, \
    partially encoded characters will not be written.
```

---

```javascript {.line-numbers}
const buf = Buffer.alloc(256);

const len = buf.write('\u00bd + \u00bc = \u00be', 0);

console.log(`${len} bytes: ${buf.toString('utf8', 0, len)}`);
// Prints: 12 bytes: ½ + ¼ = ¾

const buffer = Buffer.alloc(10);

const length = buffer.write('abcd', 8);

console.log(`${length} bytes: ${buffer.toString('utf8', 8, 10)}`);
// Prints: 2 bytes : ab
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Buffer`的其他操作

---

1. `buf.length`：`buffer`长度
2. `buf.copy(targetBuffer[, targetStart[, sourceStart[, sourceEnd]]])`：复制`buffer`
3. `Buffer.concat(list[, totalLength])`：拼接多个`buffer`
4. `buf.compare(otherBuffer)`：比较`buffer`
5. `Buffer.compare(buf1, buf2)`：比较`buffer`

---

```javascript {.line-numbers}
buf.length

<integer>
Returns the number of bytes in buf.
```

---

```javascript {.line-numbers}
// Create a `Buffer` and write a shorter string to it using UTF-8.

const buf = Buffer.alloc(1234);

console.log(buf.length);
// Prints: 1234

buf.write('some string', 0, 'utf8');

console.log(buf.length);
// Prints: 1234
```

---

```javascript {.line-numbers}
buf.compare(target[, targetStart[, targetEnd[, sourceStart[, sourceEnd]]]])

target <Buffer> | <Uint8Array> A Buffer or Uint8Array with which to compare buf.
targetStart <integer> The offset within target at which to begin comparison. Default: 0.
targetEnd <integer> The offset within target at which to end comparison (not inclusive).\
    Default: target.length.
sourceStart <integer> The offset within buf at which to begin comparison.\
    Default: 0.
sourceEnd <integer> The offset within buf at which to end comparison (not inclusive).\
    Default: buf.length.

Returns: <integer>

Compares buf with target and returns a number indicating whether buf comes before,\
    after, or is the same as target in sort order. \
    Comparison is based on the actual sequence of bytes in each Buffer.

0 is returned if target is the same as buf
1 is returned if target should come before buf when sorted.
-1 is returned if target should come after buf when sorted.
```

---

```javascript {.line-numbers}
const buf1 = Buffer.from('ABC');
const buf2 = Buffer.from('BCD');
const buf3 = Buffer.from('ABCD');

console.log(buf1.compare(buf1));
// Prints: 0
console.log(buf1.compare(buf2));
// Prints: -1
console.log(buf1.compare(buf3));
// Prints: -1
console.log(buf2.compare(buf1));
// Prints: 1
console.log(buf2.compare(buf3));
// Prints: 1
console.log([buf1, buf2, buf3].sort(Buffer.compare));
// Prints: [ <Buffer 41 42 43>, <Buffer 41 42 43 44>, <Buffer 42 43 44> ]
// (This result is equal to: [buf1, buf3, buf2].)
```

---

```javascript {.line-numbers}
Static method: Buffer.compare(buf1, buf2)

buf1 <Buffer> | <Uint8Array>
buf2 <Buffer> | <Uint8Array>

Returns: <integer> Either -1, 0, or 1, depending on the result of the comparison. \
    See buf.compare() for details.

Compares buf1 to buf2, typically for the purpose of sorting arrays of Buffer instances. \
    This is equivalent to calling buf1.compare(buf2).
```

---

```javascript {.line-numbers}
const buf1 = Buffer.from('1234');
const buf2 = Buffer.from('0123');
const arr = [buf1, buf2];

console.log(arr.sort(Buffer.compare));
// Prints: [ <Buffer 30 31 32 33>, <Buffer 31 32 33 34> ]
// (This result is equal to: [buf2, buf1].)
```

---

```javascript {.line-numbers}
buf.copy(target[, targetStart[, sourceStart[, sourceEnd]]])

target <Buffer> | <Uint8Array> A Buffer or Uint8Array to copy into.
targetStart <integer> The offset within target at which to begin writing. \
    Default: 0.
sourceStart <integer> The offset within buf from which to begin copying. \
    Default: 0.
sourceEnd <integer> The offset within buf at which to stop copying (not inclusive). \
    Default: buf.length.

Returns: <integer> The number of bytes copied.

Copies data from a region of buf to a region in target, \
    even if the target memory region overlaps with buf.
```

---

```javascript {.line-numbers}
// Create two `Buffer` instances.
const buf1 = Buffer.allocUnsafe(26);
const buf2 = Buffer.allocUnsafe(26).fill('!');

for (let i = 0; i < 26; i++) {
  // 97 is the decimal ASCII value for 'a'.
  buf1[i] = i + 97;
}

// Copy `buf1` bytes 16 through 19 into `buf2` starting at byte 8 of `buf2`.
buf1.copy(buf2, 8, 16, 20);
// This is equivalent to:
// buf2.set(buf1.subarray(16, 20), 8);

console.log(buf2.toString('ascii', 0, 25));
// Prints: !!!!!!!!qrst!!!!!!!!!!!!!
```

---

```javascript {.line-numbers}
Static method: Buffer.concat(list[, totalLength])

list <Buffer[]> | <Uint8Array[]> List of Buffer or Uint8Array instances to concatenate.
totalLength <integer> Total length of the Buffer instances in list when concatenated.

Returns: <Buffer>
Returns a new Buffer which is the result of concatenating all the Buffer instances \
    in the list together.

If the list has no items, or if the totalLength is 0, \
    then a new zero-length Buffer is returned.

If totalLength is not provided, it is calculated from the Buffer instances \
    in list by adding their lengths.

If totalLength is provided, it is coerced to an unsigned integer. \
    If the combined length of the Buffers in list exceeds totalLength, \
    the result is truncated to totalLength.
```

---

```javascript {.line-numbers}
// Create a single `Buffer` from a list of three `Buffer` instances.

const buf1 = Buffer.alloc(10);
const buf2 = Buffer.alloc(14);
const buf3 = Buffer.alloc(18);
const totalLength = buf1.length + buf2.length + buf3.length;

console.log(totalLength);
// Prints: 42

const bufA = Buffer.concat([buf1, buf2, buf3], totalLength);

console.log(bufA);
// Prints: <Buffer 00 00 00 00 ...>
console.log(bufA.length);
// Prints: 42
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
