---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Stream_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Stream

>1. [Node.js Stream(流).](https://www.runoob.com/nodejs/nodejs-stream.html)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction](#introduction)
2. [`fs.ReadStream` and `fs.WriteStream`](#fsreadstream-and-fswritestream)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction

---

1. `Stream`是一个抽象接口，常用实现`Stream`的有：`http.request`、`fs.createReadStream`、`zlib.createGzip`等
1. 超过`1GB`的文件无法使用`Buffer`直接完成读写，`Stream`可分多次读写，内存占用量较少

---

![height:600](./.assets/images/image-20231018103806185.png)

---

### `Buffer` VS `Stream`

1. 相同点：都可以处理二进制数据
2. 不同点： `Buffer`一次性将所有数据读入内存，再处理；`Stream`分多次分块读入内存
3. 联系：
    1. `Stream`可以使用`Buffer`处理数据，每个数据块通常是一个`Buffer`实例
    2. 可以将`Stream`转换为`Buffer`，也可以将`Buffer`转换为`Stream`

---

```javascript {.line-numbers}
const fs = require("fs");
const file_path = "resources/example.txt";

// 使用 Buffer 读取文件
fs.readFile(file_path, (err, data) => {
    if (err) throw err;
    console.log("Buffer data:", data);
    console.log("Buffer to string:", data.toString());
});

// 使用 Stream 读取文件
const readableStream = fs.createReadStream(file_path);
readableStream.on("data", (chunk) => {
    console.log("Stream chunk:", chunk);
    console.log("Chunk to string:", chunk.toString());
});
readableStream.on("end", () => {
    console.log("Stream ended");
});
```

---

### Types of `Stream`

1. `readable`：可读
2. `writable`：可写
3. `duplex`：可读可写
4. `transform`：可读可写，在读写过程中可以修改数据的`duplex`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `fs.ReadStream` and `fs.WriteStream`

---

1. `fs.createReadStream(path[, options])`：创建可读流，返回`fs.ReadStream`
2. `fs.createWriteStream(path[, options])`：创建可写流，返回`fs.WriteStream`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `fs.ReadStream`

---

```javascript {.line-numbers}
Class: fs.ReadStream

Extends: <stream.Readable>
```

---

```javascript {.line-numbers}
fs.createReadStream(path[, options])

path <string> | <Buffer> | <URL>
options <string> | <Object>
    flags <string> See support of file system flags. Default: 'r'.
    encoding <string> Default: null
    fd <integer> | <FileHandle> Default: null
    mode <integer> Default: 0o666
    autoClose <boolean> Default: true
    emitClose <boolean> Default: true
    start <integer>
    end <integer> Default: Infinity
    highWaterMark <integer> Default: 64 * 1024
    fs <Object> | <null> Default: null
    signal <AbortSignal> | <null> Default: null

Returns: <fs.ReadStream>
```

---

#### `fs.ReadStream` Events

<!--TODO: add description-->

1. `stream.Readable`
    1. `readable`
    2. `data`
    3. `pause`
    4. `resume`
    5. `end`
    6. `close`
    7. `error`

---

1. `fs.ReadStream`
    1. `open`
    2. `ready`
    3. `close`

---

```javascript {.line-numbers}
let fs = require('fs');

let total = '';
let rs = fs.createReadStream('./data/src/read_stream.txt');
rs.setEncoding('utf8');

rs.on('data', function (chunk) {
    total += chunk;
});

rs.on('end', function (chunk) {
    console.log(total);
});

rs.on('error', function (error) {
    console.log(error);
});
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `fs.WriteStream`

---

```javascript {.line-numbers}
Class: fs.WriteStream

Extends <stream.Writable>
```

---

```javascript {.line-numbers}
fs.createWriteStream(path[, options])

path <string> | <Buffer> | <URL>
options <string> | <Object>
    flags <string> See support of file system flags. Default: 'w'.
    encoding <string> Default: 'utf8'
    fd <integer> | <FileHandle> Default: null
    mode <integer> Default: 0o666
    autoClose <boolean> Default: true
    emitClose <boolean> Default: true
    start <integer>
    fs <Object> | <null> Default: null
    signal <AbortSignal> | <null> Default: null
    highWaterMark <number> Default: 16384
    flush <boolean> If true, the underlying file descriptor is flushed prior to closing it. \
        Default: false.

Returns: <fs.WriteStream>
```

---

#### `fs.WriteStream` Events

<!--TODO: add description-->

1. `stream.Writable`
    1. `drain`
    1. `finish`
    1. `pipe`
    1. `unpipe`
    1. `close`
    1. `error`

---

1. `fs.WriteStream`
    1. `open`
    1. `ready`
    1. `close`

---

```javascript {.line-numbers}
let fs = require('fs');

let ws = fs.createWriteStream('./data/out/write_stream.txt');
console.log(ws);

ws.once('open', function () {
    console.log('写入流打开');
    ws.write('好好学习，天天向上');
    ws.end();
});
ws.once('close', function () {
    console.log('写入流关闭');
});
ws.once('finish', function () {
    console.log('写入流完成');
});
```

---

```javascript {.line-numbers}
let fs = require('fs');

let rs = fs.createReadStream('./data/src/a.jpg');
let ws = fs.createWriteStream('./data/out/b.jpg');

rs.once('open', function () {
    console.log('读出通道已经打开');
}).once('close', function () {
    console.log('读出通道已经关闭');
    ws.end(); // close the write stream when read stream is closed
});

ws.once('open', function () {
    console.log('写入通道已经打开');
}).once('close', function () {
    console.log('写入通道已经关闭');
});

rs.on('data', function (chunk) {
    ws.write(chunk);
});
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `pipe()`

---

1. `pipe()`将读取流的数据写入到写入流中
1. `pipe()`方法可链式调用

---

```javascript {.line-numbers}
readable.pipe(destination[, options])

destination <stream.Writable> The destination for writing data
options <Object> Pipe options
end <boolean> End the writer when the reader ends. Default: true.

Returns: <stream.Writable> The destination, allowing for a chain of pipes \
    if it is a Duplex or a Transform stream

The readable.pipe() method attaches a Writable stream to the readable, \
    causing it to switch automatically into flowing mode and \
    push all of its data to the attached Writable. \
    The flow of data will be automatically managed so that \
    the destination Writable stream is not overwhelmed \
    by a faster Readable stream.
```

---

```javascript {.line-numbers}
let fs = require('fs');

let rs = fs.createReadStream('./data/src/a.jpg');
let ws = fs.createWriteStream('./data/out/c.jpg');

rs.pipe(ws).on('finish', function () {
    setTimeout(function () {
        console.log('复制成功');
    }, 1000);
}).on('error', function () {
    setTimeout(function () {
        console.log('复制失败');
    }, 1000);
});

// would be executed before the finish event is emitted
console.log('程序执行完毕');
```

---

```javascript {.line-numbers}
let fs = require('fs');
let zlib = require('zlib');

let r = fs.createReadStream('./data/src/a.jpg');
let z = zlib.createGzip();
let w =fs.createWriteStream('./data/out/a.jpg.gz');

r.pipe(z).pipe(w);
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
