---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Local Environment_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Local Environment

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction](#introduction)
2. [`os` Module](#os-module)
3. [`process` Object](#process-object)
4. [`child_process` Module](#child_process-module)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction

1. 本地环境是指在本地计算机上运行的`node`环境
2. `node`提供了一些模块和对象，用于访问和操作本地环境
    1. `os`模块
    2. `process`对象
    3. `child_process`模块
    4. `fs`模块
    5. `path`模块

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `os` Module

>[node.js api. os.](https://nodejs.org/docs/latest-v20.x/api/os.html)

---

1. `os`模块提供了访问`os`相关信息的途径，例如，当前环境资源的使用情况和能力等信息
2. `os`是帮助创建具有跨平台功能的`app`的有用工具之一

---

```javascript {.line-numbers}
const os = require('os');

console.log('Using end of line' + os.EOL + 'to insert a new line');
console.log(os.endianness());
console.log(os.tmpdir());
console.log(os.homedir());

console.log(os.freemem());
console.log(os.totalmem());

console.log(os.loadavg());  // only available on POSIX, Windows returns [0, 0, 0]
console.log(os.cpus());
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `process` Object

>[node.js api. process.](https://nodejs.org/docs/latest-v20.x/api/process.html)

---

1. `process`对象是`node`环境中的基础组件
2. `process`对象提供了对`node`环境和其运行环境的信息的访问

---

```bash {.line-numbers}
node -p "process.versions"
node -p "process.env"
node -p "process.release"
```

---

```javascript {.line-numbers}
process.stdin
process.stdout
process.stderr
```

---

```javascript {.line-numbers}
process.stdin.setEncoding('utf8');

process.stdin.on('readable', () => {
  const chunk = process.stdin.read();
  conlog.log("chunk type: " + typeof(chunk));
  if (chunk !== null) {
    process.stdout.write(`data: ${chunk}`);

    if(chunk.trim() === 'exit') {
      process.exit(0);
    }
  }
});
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `child_process` Module

---

1. `child_process`模块提供了创建子进程的功能，同时监听其输入输出
2. 创建子进程的方式有4种：`exec`, `execFile`, `spawn`和`fork`
3. 子进程运行的命令可以是`node`脚本或者其他系统命令

---

<!--TODO: add codes-->

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
