---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Modular and NPM_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Modular and NPM

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [模块化思想](#模块化思想)
2. [Node模块](#node模块)
3. [全局对象与顶层对象Global Objects and Top-Level Objects](#全局对象与顶层对象global-objects-and-top-level-objects)
4. [模块Module](#模块module)
5. [package](#package)
6. [npm与yarn](#npm与yarn)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 模块化思想

---

### 未引入模块机制存在的问题

1. 命名冲突多
1. 复用困难大
1. 依赖困难大

---

### 引入模块机制的优势

1. 减少命名冲突
2. 方便模块复用
3. 方便依赖管理
4. 提升可维护性

---

### 模块规范的价值

1. 模块语法规范
1. 模块依赖解析

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Node模块

---

1. 将"高内聚"的代码封装成模块
1. `node`中，一个`js`文件即一个模块
1. `node`的模块类型
    1. 内置模块（`builtin module`）/核心模块：`node`自带的模块
    1. 文件模块（`file module`）
        1. 第三方模块（`third-party modules`）：`npm`下载的模块
        1. 自定义模块（`custom module`）/本地模块（`local module`）：自定义编写的模块或手动下载的模块

---

### 内置模块

<!-- ```javascript {.line-numbers}
const builtin = require('node:module').builtinModules;
console.log(builtin.length);
console.log(builtin);

``` -->

```javascript {.line-numbers}
// npm install builtin-modules
const builtinModules = require('builtin-modules');
console.log(builtinModules.length);
console.log(builtinModules);
```

```javascript {.line-numbers}
const built_modules_static = require('builtin-modules/static');
console.log(built_modules_static.length);
console.log(built_modules_static);
```

>1. [yarnpkg. builtin-modules`[DB/OL].`](https://yarnpkg.com/package?name=builtin-modules)
>1. [`w3schools. Node.js Built-in Modules[DB/OL].`](https://www.w3schools.com/nodejs/ref_modules.asp)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 全局对象与顶层对象Global Objects and Top-Level Objects

>1. [globals.](https://nodejs.org/docs/latest-v18.x/api/globals.html)
>2. [Standard built-in objects.](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects)
>1. [require() 源码解读.](https://www.ruanyifeng.com/blog/2015/05/require.html)

---

### `Global Objects` VS `Top-Level Objects`

1. `Top-Level Objects`：运行时环境中处于顶层的对象
2. `Global Objects`：运行时环境中始终存在的对象，包括`Top-Level Objects`、`Top-Level Objects`的属性以及其他`Global Objects`
3. `es2020`后`globalThis`作为所有运行环境的顶层对象，用于统一`browser`的`window`和`node`的`global`

---

### `Global Objects` VS `Global Available Objects`

1. `Global Objects`/`Globals`：整个`node`运行环境中始终存在的对象，使用时可加前缀`global`、也可省略
2. `Global Available Objects`：不属于`global`，但不需要进行显式的加载可直接使用的对象，使用时不需要前缀
3. 广义的`Global Available Objects`包括`Global Objects`
4. `Global Objects`和`Global Available Objects`的主要区别在于生命周期不一样
5. `broswe`中，文件顶层自定义的标识符是 **_`globalThis`全局对象的属性（即，全局共享）_**
6. `node`中，文件顶层自定义的标识符是 **_`module private`模块私有_**

---

### Globals全局

---

![height:600](./.assets/image/16effab0ba3ae9a9~tplv-t2oaga2asx-jj-mark:3024:0:0:0:q75.png)

---

1. `ECMAScript Objects`/`standard built-in objects`：`browser`和`node`均有，并且属于`ECMAScript`语言定义的一部分，例如，`Date`、`String`、`Promise`等
2. `host globals`
    1. `browser`和`node`均有，但实现方式不同，例如，`console`、`setTimeout`、`setInterval`等
    2. `browser`专属，例如，`window`、`alert`等
    3. `Node`专属，例如，`process`、`Buffer`等

---

#### Node Globals

1. `globalThis`/`global`
2. `console`：最初由`IE`的`JScript`引擎提供的调试工具，后来被`ECMAScript`采纳，成为`ECMAScript`的标准规范，`node`中的`console`对象用于向`stdout`和`stderr`输出字符流
3. `process`：当前`node`进程状态的对象，提供了一些`os`操作
4. `Buffer`：用于操作二进制数据流类，类似于`Array`，但操作单元为`byte`，`node`中的`Buffer`对象是`ECMAScript`中`Uint8Array`的子类
5. ...

---

##### `console`

---

##### `process`

>1. [阮一峰. process对象.](https://javascript.ruanyifeng.com/nodejs/process.html#toc10)

---

### Node Global Functions

1. `setTimeout(cb, ms)`：在指定的毫秒数后执行指定函数，对应`timer`阶段执行
1. `clearTimeout(t)`
1. `setInterval(cb, ms)`：每隔指定的毫秒数执行指定函数，对应`timer`阶段执行
1. `clearInterval(t)`
1. `setImmediate(cb, ms)`：`node`专属，在下一次`io event`迭代、任何定时器事件之前执行回调函数，对应`check`阶段执行
1. `clearImmediate()`：`node`专属

---

```javascript {.line-numbers}
console.log('outer01');

setTimeout(() => {
    setTimeout(() => {
        console.log('setTimeout01...');
    }, 0);
    setImmediate(() => {
        console.log('setImmediate01...');
    });
}, 0);

console.log('outer02...');

setTimeout(() => {
    console.log('setTimeout02...');
}, 0);

setImmediate(() => {
    console.log('setImmediate02...');
});
```

>[setTimeout和setImmediate到底谁先执行.](https://juejin.cn/post/6844904100195205133)

---

### Global Available全局可用

1. `module`
1. `exports`：`let exports = module.exports`
1. `__filename`：当前脚本的文件名（绝对路径）
1. `__dirname`：当前脚本所在的目录（绝对路径）
1. `require()`

---

```javascript {.line-numbers}
let module = {};
let exports = module.exports = {};
// module wrapper function
(function(exports, require, module, __filename, __dirname) {
    // module code actually lives in here
})(exports, require, module, __filename, __dirname);
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 模块Module

>1. [阮一峰. ES6教程-Module的语法.](https://wangdoc.com/es6/module)
>2. [阮一峰. ES6教程-Module的加载实现.](https://wangdoc.com/es6/module-loader.html)

---

### 模块规范

1. `node`遵循`CommonJS`规范或`ESM(ECMAScript Module)`规范
2. 其他常见的模块化规范
    1. `AMD(Asynchronous module definition)`：主要用于`browser`环境，例如，`RequireJS`
    2. `CMD(Common Module Definition)`：主要用于`browser`环境，例如，`SeaJS`
    3. `UMD(Universal Module Definition)`：兼容`CommonJS`和`AMD`的模块化方案

>[JS 模块化规范：CommonJS, AMD, CMD, UMD, ESM.](https://qwqaq.com/2018/07/js-modules/)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### CommonJS规范

---

1. `require`：加载模块，获取一个模块的引用
2. `module.exports`/`exports`：导出模块/开放模块，将模块的成员导出给其他模块使用

```javascript {.line-numbers}
console.log(module);
console.log(exports);
console.log(module.exports === exports); // true
```

---

1. `module`：`node`在执行一个模块时，会给该模块内生成一个`module`对象和一个`exports`对象，当模块执行完毕后，返回`module.exports`对象（`exports`对象不返回），以便后续被其他模块`require`引用
1. 加载某个模块，即加载该模块的`module.exports`

>[一文搞懂exports和module.exports的关系和区别.](https://juejin.cn/post/6844904079626338318)

---

#### `exports`

```javascript {.line-numbers}
// let exports = module.exports;
exports = {} // would break the reference to module.exports

module.exports = "hello" // would make module.exports point to a string
```

---

```javascript {.line-numbers}
//向外开放变量name
exports.name = 'Tom';
//向外开放变量age
module.exports.age='10';
//向外开放函数
module.exports.sayHello= function () {
    console.log('hello');
}
```

---

#### `require`

1. 执行被加载模块的代码
1. 得到被加载模块的`module.exports`对象

```javascript {.line-numbers}
// module_name point to module.exports in ./module_name.js
let module_name = require('./module_name.js');
```

---

1. 模块名称：通常是核心模块或第三方文件模块，例如 os、express 等
1. 模块路径
    1. 模块相对路径：以`.`或`..`开关的路径，例如`./utils`
    1. 模块绝对路径：以`/`开头的路径，例如`/home/xxx/MyProject/utils`
    1. 当模块扩展名为`.js`或`.json`或`.node`时，扩展名可加也可不加
    1. 当有同名但扩展名不同的模块时，`require`会优先加载`.js`文件，其次加载`.json`文件，最后加载`.node`文件
    1. 当路径是文件夹时，按以下顺序加载
        1. `require`会寻找该文件夹下的`package.json`文件中的`main`属性，加载该属性指定的文件
        1. 如果没有`package.json`文件或没有`main`属性，则加载该文件夹下的`index.js`或`index.json`
        1. 如果没有`index.js`文件，则会报错

---

1. 内置模块：模块名称
1. 第三方模块：模块名称或模块路径
1. 自定义模块：模块路径

---

```javascript {.line-numbers}
// local module
const hello = require('./modules/hello.js');

// builtin module
const fs = require('fs');

// third-party module
const express = require('express');
```

---

```javascript {.line-numbers}
// load module
let myModule = require('./info.js');
console.log(myModule);

// access module variable
console.log('name:' + myModule.name);
console.log('type:' + myModule.type);
console.log('age:' + myModule.age);

// call module function
myModule.sayHello();
```

---

##### 包加载流程

1. 搜索路径`module.paths`
2. 如果`require`的参数不带路径，默认将其做为内置模块
3. 如果不是内置模块，则将其做为第三方模块
    1. 在当前目录的`node_modules`目录下寻找
    2. 如果不在当前目录的`node_modules`，则到父目录的`node_modules`目录下寻找，直到根目录
    3. 如果找到了该模块对应的目录名，就会读取该模块的`package.json`文件，获取该文件中`main`属性的值，根据`main`属性指定的路径值进行加载

---

#### module对象

1. id：模块的唯一标识符，如果是被运行的主程序（例如 main.js）则为 .，如果是被导入的模块（例如 myModule.js）则等同于此文件名（即下面的 filename 字段）
2. path 和 filename：模块所在路径和文件名，没啥好说的
3. exports：模块所导出的内容，实际上之前的 exports 对象是指向 module.exports 的引用。例如对于 myModule.js，刚才我们导出了 add 函数，因此出现在了这个 exports 字段里面；而 main.js 没有导出任何内容，因此 exports 字段为空

---

1. parent 和 children：用于记录模块之间的导入关系，例如 main.js 中 require 了 myModule.js，那么 main 就是 myModule 的 parent，myModule 就是 main 的 children
2. loaded：模块是否被加载，从上图中可以看出只有 children 中列出的模块才会被加载
3. paths：这个就是 Node 搜索文件模块的路径列表，Node 会从第一个路径到最后一个路径依次搜索指定的 Node 模块，找到了则导入，找不到就会报错

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### ECMAScript Module

---

#### `ESM` VS `CommonJS`

1. `CommonJS`模块输出的是值的拷贝，`ESM`模块输出的是值的引用
2. `CommonJS`模块是运行时加载，`ESM`模块是静态解析时加载
3. `CommonJS`模块的`require()`是同步加载模块，`ESM`的`import`命令是异步加载
4. `CommonJS`模块无论加载多少次，只会在第一次加载时运行一次，以后再加载，就返回第一次运行的结果，除非手动清除系统缓存
5. `CommonJS`模块的顶层`this`指向当前模块，`ESM`模块中，顶层的`this`指向`undefined`

---

#### `ESM`不存在的模块顶层变量

1. `module`
2. `arguments`
3. `require`
4. `exports`
5. `__filename`
6. `__dirname`

---

#### 值的拷贝 VS 值的引用

1. `CommonJS`模块输出的是值的拷贝，即，一旦输出一个值，模块内部的变化就影响不到这个值
2. JS 引擎对脚本静态分析的时候，遇到模块加载命令import，就会生成一个只读引用。等到脚本真正执行时，再根据这个只读引用，到被加载的那个模块里面去取值。换句话说，ES6 的import有点像 Unix 系统的"符号连接"，原始值变了，import加载的值也会跟着变

---

#### 运行时加载 VS 静态解析时加载

1. `CommonJS`加载的是一个对象（即module.exports属性），该对象只有在脚本运行完才会生成
2. `ESM`模块不是对象，它的对外接口只是一种静态定义，在代码静态解析阶段就会生成。
3. `ESM` `export`通过接口，输出的是同一个值。不同的脚本加载这个接口，得到的都是同样的实例。

---

#### `Node`对`ESM`的支持

1. 从`v13.2`开始，`node`默认打开`ESM`模块支持，无需添加`--experimental-modules`参数
2. `node`要求`ESM`采用`.mjs`后缀文件名或在`package.json`中指定`type`字段为`module`(此时，`CommonJS`需要采用`.cjs`后缀文件名)
3. `.mjs`文件总是以`ESM`模块加载，`.cjs`文件总是以`CommonJS`模块加载，`.js`文件的加载取决于`package.json`中`type`字段的设置

---

### 循环依赖

---

#### `CommonJS`处理循环依赖

---

```javascript {.line-numbers}
// main.cjs
var a = require('./a.cjs');
var b = require('./b.cjs');
console.log('在 main.js 之中, a.done=%j, b.done=%j', a.done, b.done);
```

---

```javascript {.line-numbers}
// a.cjs
exports.done = false;
var b = require('./b.cjs');
console.log('在 a.cjs 之中，b.done = %j', b.done);
exports.done = true;
console.log('a.cjs 执行完毕');
```

```javascript {.line-numbers}
// b.cjs
exports.done = false;
var a = require('./a.cjs');
console.log('在 b.cjs 之中，a.done = %j', a.done);
exports.done = true;
console.log('b.cjs 执行完毕');
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## package

---

1. `package`是`module`的进一步抽象，便于分发、安装和管理
2. `package`包含了`package.json`文件和`bin`、`lib`等子目录，**_必然包含`package.json`文件_**，其他文件或目录非强制要求
3. `package.json`是`node`的包规范，`npm`和`yarn`都遵守这个规范

![height:250](./.assets/image/4271d4b56f5786c584b28506fcf5d51f1da6fe.jpg)

---

### Package Structure

![width:1120](./.assets/image/image-20231011164003308.png)

---

### `package.json`

1. 创建`package.json`的方式
    1. 手动新建`package.json`文件
    1. 运行`npm init`或`npm init -y`
1. 唯一标识符：`name` + `version`（`package.json`内必须包含）
    1. `name`须是`url friendly`，不能包含中文和大写字母，规则详见[npm/validate-npm-package-name](https://github.com/npm/validate-npm-package-name)
    1. `version`须是`x.y.z`形式

---

![width:1120](./.assets/image/image-20231011164044266.png)

---

#### 依赖版本号描述

1. 精确版本：例如，`1.0.0`，只会安装版本为`1.0.0`的依赖
1. 锁定主版本和次版本：例如，`~1.0.0`、`1.0.x`、`1.0`，可能会安装`1.0.8`的依赖，但不会安装`1.1.0`的依赖
1. 仅锁定主版本：例如，`^1.0.0`、`1`、`1.x`，可能会安装`1.1.0`的依赖，但不会安装`2.0.0`的依赖
1. 最新版本（不锁定版本）：`*`或`x`，安装最新版本

---

### `package-lock.json`

1. 锁定全部直接依赖和间接依赖的精确版本号：用于确保在项目中所有人都能有完全一致的依赖

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## npm与yarn

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `npm(node package manager)`

1. 本地工具
    1. `npm`命令行工具（安装`node`时附带安装）
2. 网络服务
    1. `registry`：集中式依赖仓库，存放开发者分享的`package`
    2. `npmjs.com`：`registry`对外的服务网站，用于搜索`package`、管理网站账号等

---

![height:500](./.assets/image/image-20231011164257096.png)

---

![width:1120](./.assets/image/image-20231011164528277.png)

---

![width:800](./.assets/image/image-20231011164609735.png)

---

### `npm install`

1. 不加参数：安装`package.json`中的全部依赖
2. `--save <package>`/`--save-prod`/`-S <package>`/`-P <package>`（`npm 5.0+`是默认选项）：安装`package`并将其添加到`dependencies`中
3. `--sava-dev <package>`/`-D <package>`：安装`package`并将其添加到`devDependencies`中
4. `-g`：全局安装`package`

---

### `npm scripts`

1. 预定义脚本：例如`test`、`start`、`install`、`publish`等，直接通过`npm <script>`运行，例如`npm test`
2. 自定义脚本：除预定义脚本外的脚本，需要通过`npm run <script>` 运行，例如`npm run my_check`

>[npmjs.com. scripts](https://docs.npmjs.com/cli/v10/using-npm/scripts): pre-defined scripts

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### yarn

---

1. `yarn`是`Facebook`（现`Meta`）开发的`JavaScript`包管理工具，用于替代`npm`
2. 旨在解决`npm`在速度、一致性、安全性等方面的问题

```bash {.line-numbers}
npm install -g yarn

# macOS
brew install yarn

# windows
choco install yarn
```

---

1. 速度
    - `yarn`：通过并行和缓存机制提高安装速度
    - `npm`：自`npm v5`以来，速度有了显著提升
2. 一致性
    - `yarn`：使用`yarn.lock`文件确保每次安装的依赖包版本一致
    - `npm`：使用`package-lock.json`文件确保每次安装的依赖包版本一致
3. 安全性
    - `yarn`：通过校验包的完整性来提高安全性
    - `npm`：自`npm v6`以来，引入`npm audit`命令检查依赖包的安全性

---

|功能|`yarn`|`npm`|
|--|--|--|
|初始化项目|`yarn init`|`npm init`|
|添加依赖包|`yarn add <pkg>`|`npm install <pkg>`|
|移除依赖包|`yarn remove <pkg>`|`npm uninstall <pkg>`|
|安装依赖包|`yarn install`|`npm install`|
|升级依赖包|`yarn upgrade`|`npm update`|
|运行脚本|`yarn run <script>`|`npm run <script>`|

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
