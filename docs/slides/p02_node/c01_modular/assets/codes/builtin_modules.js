  // 
const builtin = require('node:module').builtinModules;
console.log(builtin.length);
console.log(builtin);

  // npm install builtin-modules
  // returns an array of builtin modules fetched from the running Node.js version.
const builtinModules = require("builtin-modules");
console.log(builtinModules.length);
console.log(builtinModules);

  // npm install builtin-modules
  // returns a static array of builtin modules generated from the latest Node.js version.
const built_modules_static = require("builtin-modules/static");
console.log(built_modules_static.length);
  // console.log(built_modules_static);

  // npm install is-builtin-module
  // returns true if a string matches the name of a Node.js builtin module.

const isBuiltinModule = require("is-builtin-module");

console.log(isBuiltinModule("fs"));                //=> true
console.log(isBuiltinModule("fs/promises"));       //=> true
console.log(isBuiltinModule("node:fs/promises"));  //=> true
console.log(isBuiltinModule("unicorn"));           //=> false
