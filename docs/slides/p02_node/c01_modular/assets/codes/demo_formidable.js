let http = require('http');
let fs = require('fs');
let path = require('path');
let formidable = require('formidable');

let server = http.createServer();
server.on('request', function (req, res) {
    console.log('ddd');
    let url = req.url;
    console.log(req.url)
    if (url === '/upload') {
        let form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            if (err) {
                return console.log(err);
            } else {
                let dataFields = JSON.stringify(fields);
                let rs = fs.createReadStream(files.img.path);
                let ws = fs.createWriteStream('./image/' + files.img.name);
                rs.pipe(ws);
                rs.on("end", function () {
                    res.setHeader('content-type', 'text/html;charset=utf-8');
                    res.write("上传成功");
                    res.end();
                });
            }

        });
    } else if (url === "/uploadTest") {
        fs.readFile(path.join(__dirname, '/uploadTest.html'), 'utf-8', function (err, data) {
            if (err) {
                throw err;
            }
            res.end(data);

        });

    } else {
        res.end('404 not found');
    }

});
server.listen(3000, function () {
    console.log("server is listening");
});
