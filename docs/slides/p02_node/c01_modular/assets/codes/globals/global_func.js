console.log('outer01');

setTimeout(() => {
    setTimeout(() => {
        console.log('setTimeout01...');
    }, 0);
    setImmediate(() => {
        console.log('setImmediate01...');
    });
}, 0);

console.log('outer02...');

setTimeout(() => {
    console.log('setTimeout02...');
}, 0);

setImmediate(() => {
    console.log('setImmediate02...');
});