// main.cjs
var a = require('./a.cjs');
var b = require('./b.cjs'); // will not execute again, just return the exports object cached
console.log('在 main.js 之中, a.done=%j, b.done=%j', a.done, b.done);