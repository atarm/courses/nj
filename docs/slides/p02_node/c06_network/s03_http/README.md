---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_HTTP Programming_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# HTTP Programming

>1. [node docs. module http.](https://nodejs.org/docs/latest-v20.x/api/http.html)

<!--TODO: add it

 ---

### HTTP Protocol

---

### DNS Protocol
-->

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`http` Module](#http-module)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `http` Module

---

1. `http module`专注于`HTTP`流处理和消息解析
2. `http module`建立在`net module`之上，提供了更高级别的抽象
3. `http module`为`Express`等上层框架或应用提供支持

---

1. `module variables`
2. `module functions`
3. class
    1. `http.Server`：`http server`
    2. `http.IncomingMessage`：`request`
    3. `http.ServerResponse`：`response`
    4. `http.Agent`
    5. `http.ClientRequest`
    6. `http.OutgoingMessage`: `http.ServerResponse`和`http.ClientRequest`的父类

---

1. `s03_http/server.js`
2. `s03_http/client.js`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `module variables`

---

1. `http.METHODS`
2. `http.STATUS_CODES`
3. `http.maxHeaderSize`
<!--TODO:

4. `http.globalAgent` -->

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `module functions`

---

1. `http.createServer([options][, requestListener])`
2. `client`
    1. `http.get(options[, callback])`
    2. `http.get(url[, options][, callback])`
    3. `http.request(options[, callback])`
    4. `http.request(url[, options][, callback])`
<!-- 1. `http.setMaxIdleHTTPParsers(max)`
1. `http.validateHeaderName(name[, label])`
2. `http.validateHeaderValue(name, value)` -->

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `http.Server` Class

---

```javascript {.line-numbers}
Class: http.Server

Extends: <net.Server>
```

---

#### `http.Server` Properties

1. `server.maxHeadersCount`: 每个请求/响应允许的最大头部数量
2. `server.timeout`: 请求超时时间（毫秒）
3. `server.keepAliveTimeout`: 保持活动连接的超时时间（毫秒）
4. `server.headersTimeout`: 头部超时时间（毫秒）

---

#### `http.Server` Events

1. `'request'`: 当接收到请求时触发，回调函数参数为`(req, res)`
2. `'connect'`: 当接收到`HTTP CONNECT`方法请求时触发，回调函数参数为`(req, socket, head)`
3. `'connection'`: 当一个新`TCP`连接建立时触发，回调函数参数为`(socket)`
4. `'close'`: 当服务器关闭时触发

---

```javascript {.line-numbers}
const http = require('http');

const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello, World!\n');
});

server.on('connection', (socket) => {
  console.log('new tcp connection established');
  socket.setTimeout(60000); // 设置连接超时
});

server.on('connect', (req, socket, head) => {
  console.log('new http connect established');
  // 处理隧道连接
  socket.write('HTTP/1.1 200 Connection Established\r\n' +
               'Proxy-agent: Node.js-Proxy\r\n' +
               '\r\n');
  socket.pipe(socket); // 简单地回显服务器
});

server.listen(3000, () => {
  console.log('Server running at http://localhost:3000/');
});
```

---

#### `http.Server` Methods

1. `server.listen(port[, hostname] [, backlog] [, callback])`: 绑定并监听指定的端口
2. `server.close([callback])`: 停止服务器接收新连接，并关闭所有现有连接
3. `server.setTimeout([msecs] [, callback])`: 设置请求超时时间，并可选地设置超时回调函数。
4. `server.getConnections(callback)`: 异步获取当前服务器的活动连接数，回调函数参数为`(err, count)`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `http.IncomingMessage` Class

---

1. `server`和`client`都会创建`http.IncomingMessage`对象
1. 一般由`http.Server`的`request`事件触发，作为第一个参数传递，通常简称为`request`或`req`

```javascript {.line-numbers}
Class: http.IncomingMessage
    Extends: <stream.Readable>
```

---

#### `http.IncomingMessage` Properties

1. `message.headers`
1. `message.httpVersion`
1. `message.method`
1. `message.socket`: The `net.Socket` object associated with the connection.
<!--
1. `message.statusCode`
1. `message.url` -->

---

#### `http.IncomingMessage` Methods

1. `message.destroy([error])`: Calls `destroy()` on the `socket` that received the `IncomingMessage`. If `error` is provided, an `error` `event` is emitted on the `socket` and `error` is passed as an argument to any `listeners` on the `event`.
1. `message.setTimeout(timeout[, callback])`

---

#### `http.IncomingMessage` Events

1. `close`

<!--TODO: add events inherited from super-->

---

### `http.ServerResponse` Class

1. 一个可写流，用于发送响应报文给客户端，决定了用户最终能看到的结果
1. `http.Server`的`request`事件触发的第二个参数，一般简称`response`或`res`

```javascript {.line-numbers}
Class: http.ServerResponse
    Extends: <http.OutgoingMessage>
```

```javascript {.line-numbers}
Class: http.OutgoingMessage
    Extends: <Stream>
```

---

#### `http.ServerResponse` Properties

1. `response.headerSent`
1. `response.setDate`：如果设为true，则自动生成`Date`标头的值，并作为响应的一部分发送
1. `response.statusCode`：
1. `response.statusMessage`：

---

#### `http.ServerResponse` Methods

1. `response.writeHead(statusCode,[headers])`：发送响应头部
1. `response.write(data[,encoding])`：发送响应体
1. `response.writeContinue()`:
1. `response.end([data],[encoding])`：结束响应

---

1. `response.getHeader(name)`
1. `response.setHeader(name,value)`
1. `response.removeHeader(name)`
1. `response.setTimeout(timeout[, callback])`

---

#### `http.ServerResponse` Events

1. `http.OutgoingMessage`
    1. `prefinish`
    1. `finish`
1. `http.ServerResponse`
    1. `finish`
    1. `close`

---

```javascript {.line-numbers}
//导入http模块
const http = require("http");
//调用createServer()创建http.Server服务器对象
const server = http.createServer();
//绑定request事件，一旦客户端请求则触发该事件，并接收request和response对象
server.on("request", function (req, res) {
    console.log(req);
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.write("hello");
    res.end();
});
//调用listen()函数启动服务器监听
server.listen(3000, function () {
    console.log("server listening on port 3000......");
});
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
