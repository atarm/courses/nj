---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Socket Programming_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Socket Programming

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`socket`](#socket)
2. [`net` Module](#net-module)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `socket`

>1. [node docs. module net.](https://nodejs.org/docs/latest-v20.x/api/net.html)

---

1. `socket`套接字，全称`network socket`，因`TCP/IP`协议的主流地位（还有`IPX/SPX`等其他协议模型），也称为`internet socket`或`ip socket`
1. `socket`是对`tcp`和`udp`的封装，是应用层与传输层之间的接口
1. `socket`是`os`提供的一种`ipc`/`rpc`机制，应用程序可以通过它收发网络数据，而不必关心底层网络细节，
1. `socket`最早来自于`bsd 4.2`，当前常见的`socket`大多源自于`berkeley sockets`
1. `socket`是对`tcp/ip`的封装，`socket`本身不是协议

---

### `socket`五元组

1. 本地`socket address`：`local ip address` + `local port number`
1. 远程`socket address`：`remote ip address` + `remote port number`
1. 通信`ip protocol`

>1. [wiki. 网络套接字.](https://zh.wikipedia.org/zh-cn/%E7%B6%B2%E8%B7%AF%E6%8F%92%E5%BA%A7)

---

### `ip socket` and `unix socket`

1. `UNIX socket` is an **inter-process communication mechanism** that allows bidirectional data exchange between **_processes running on the same machine_**
1. `IP sockets` (especially `TCP/IP sockets`) are a mechanism allowing communication between **_processes over the network_**

---

1. In some cases, you can use `TCP/IP sockets` to talk with processes running on the same computer (by **_using the loopback interface `127.0.0.1`_**)
1. `UNIX domain sockets` know that they're executing on the same system, **so they can avoid some checks and operations (like routing)**, which makes them **_faster and lighter_** than `IP sockets`. So if you plan to communicate with processes on the same host, this is a better option than `IP sockets`.

>[Unix domain socket 和 TCP/IP socket 的区别.](https://jaminzhang.github.io/network/the-difference-between-unix-domain-socket-and-tcp-ip-socket/)

**主机内本质是可靠通信，主机间本质是不可靠通信**

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `net` Module

---

1. `net`模块提供了基于`tcp`的网络通信，包含：
    1. 封装`os`提供的`tcp socket`
    2. 创建`tcp socket`的`server`和`client`
2. `net`模块的主要内容有：
    1. `module functions`
    2. `net.Server`：`tcp server`的抽象
    3. `net.Socket`：`tcp socket`/`tcp connection`的抽象

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `net module functions`

---

#### Create `tcp server`

1. `net.createServer([options][,connectionListener])`：创建一个`tcp server`

---

#### Create `net.Socket`

1. `net.createConnection(options[,connectionListener])`：创建一个根据`options`的`net.Socket`，返回`net.Socket`
2. `net.createConnection(port[, host][, connectListener])`：创建一个`port`和`host`的`ip socket`，返回`net.Socket`
3. `net.createConnection(path[, connectListener])`：创建连接到`path`的`unix socket`，返回`net.Socket`

---

#### Connect to `tcp server`

1. `net.connect(options[, connectionListener])`：Alias to `net.createConnection(options[, connectListener])`.
1. `net.connect(port[, host][, connectListener])`：Alias to `net.createConnection(port[, host][, connectListener])`.
1. `net.connect(path[, connectListener])`：Alias to `net.createConnection(path[, connectListener])`.

---

#### Etcetera

1. `net.isIP(input)`：检测是否为`ip address`，`ipv4 address`返回4，`ipv6 address`返回6，其他情况返回 0
1. `net.isIPv4(input)`：如果为`ipv4 address`，返回`true`，否则返回`false`
1. `net.isIPv6(input)`：如果为`ipv6 address`，返回`true`，否则返回`false`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `net.Server` Class

1. `net.Server`主要用于监听客户端的请求、返回响应等服务
2. `net.createServer([options][,connectionListener])`返回`net.Server`对象

---

#### `net.Server` Events

1. `listening -> ()=>{}`：当服务器调用`server.listen`绑定后，触发该事件
1. `connection -> (socket)=>{}`：当新连接创建后，触发该事件
1. `drop -> (connect)=>{}`：当超过`server.maxConnections`，新请求连接时，触发该事件
1. `close -> ()=>{}`：服务器关闭（所有连接断开）时，触发该事件
1. `error -> (err)=>{}`：发生错误时，触发该事件

---

#### `net.Server` Methods

1. `server.listen(options[, callback])`
1. `server.listen(port[, host][, backlog][, callback])`：监听`host:port`
1. `server.listen(path[, callback])`：
<!-- 1. `server.listen(handle[, callback])` -->

---

1. `server.getConnections(callback)`：异步获取`server`当前活跃连接的数量，回调函数有2个形参（`err`和`count`）
1. `server.close([callback])`：`server`停止接收新的连接，保持现有连接。当所有连接结束的时，`server`关闭并触发`close`事件
1. `server.address()`：返回`server`绑定的`ip protocol`、`ip address`和`port number`

<!-- ---

1. `server.unref()`：Calling `unref()` on a server will allow the program to exit if this is the only active server in the event system. If the server is already unrefed calling `unref()` again will have no effect.
1. `server.ref()`：Opposite of `unref()`, calling `ref()` on a previously unrefed server will not let the program exit if it's the only server left (the default behavior). If the server is refed calling `ref()` again will have no effect. -->

---

```javascript {.line-numbers}
//1. 加载net模块
let net=require("net");
//2. 创建Net.Server对象
let server=net.createServer();
//3. 绑定connection事件，监听客户的连接
server.on("connection",function (socket) {
    console.log("有客户端连接成功了。。。")
});
//4. 调用listen()启动监听
server.listen(3000,"127.0.0.1",function () {
    console.log("服务器在端口3000启动了。。。")
});
```

---

```bash {.line-numbers}
node server.js

telnet 127.0.0.1 3000
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `net.Socket` Class

---

1. `net.Socket`封装了`os`提供的`tcp socket`
1. `net.Socket`实现一个双向流（可读可写）接口

---

#### `net.Socket` Events

1. `lookup -> (err, address, family, host)=>{}`：在解析域名后、连接前，触发该事件
1. `connect -> ()=>{}`：成功建立`socket`连接时，触发该事件
1. `ready -> ()=>{}`：`socket`可用时，触发该事件
1. `end -> ()=>{}`：当`socket`另一端发送`FIN`包时，触发该事件
1. `close -> ()=>{}`：当`socket`：完全关闭时，触发该事件
1. `error -> (err)=>{}`：当错误发生时，触发该事件

---

1. `timeout -> ()=>{}`：当`socket`空闲超时时，触发该事件，该事件仅仅表明`socket`已经空闲，必须手动关闭连接
1. `data -> (data) => {}`：当接收到数据时，触发该事件
1. `drain -> () => {}`：当写缓存为空时，触发该事件

---

#### `net.Socket` Properties

1. `socket.remoteFamily`/`socket.localFamily`：远程/本地`ip protocol`字符串，例如：`IPv4`或`IPv6`
1. `socket.remoteAddress`/`socket.localAddress`：远程/本地的`ip address`字符串，例如：`74.125.127.100`或`2001:4860:a005::68`
1. `socket.remotePort`/`socket.localPort`：远程/本地`port number`，例如：`80`或`21`

---

1. `socket.bufferSize`：缓冲区的字节数
1. `socket.bytesRead`：接收的字节数
1. `socket.bytesWritten`：发送的字节数

---

#### `net.Socket` Methods

1. `socket.connect(options[, connectListener])`
1. `socket.connect(port[, host] [, connectListener])`
1. `socket.connect(path[, connectListener])`
1. `socket.end([data][, encoding])`：发送一个`FIN`包，请求断开`socket`连接

---

1. `socket.pause()`：暂停读取数据，不再触发`data`事件
1. `socket.write(data[, encoding][, callback])`

---

1. `socket.setEncoding([encoding])`
1. `socket.setTimeout(timeout[, callback])`
1. `socket.address()`

---

```javascript {.line-numbers}
//1.加载net模块
let net = require("net");

//2.创建Net.Server对象
let server = net.createServer();
let count = 0;   //保存在线人数

//3.绑定connection事件，监听客户的连接
server.on("connection", function (socket) {
    //获取客户端端口识别不同客户端
    console.log("客户端" + socket.remotePort + "连接成功了。")
    //当前在线人数+1
    count++;
    console.log("当前在线人数：" + count);
    //利用当前客户端的socket向客户端发送消息
    socket.write("当前在线人数：" + count);

    socket.on("error", function () {
        console.log("客户端" + socket.remotePort + "退出了");
    });
});

//4.调用listen()启动监听
server.listen(3000, "127.0.0.1", function () {
    console.log("服务器在端口3000启动了。")
});
```

---

```javascript {.line-numbers}
//1.加载net模块
let net = require("net");
//2.调用createConnection()创建socket对象，并与服务器建立连接
let client=net.createConnection({
    host:"127.0.0.1",
    port:3000
});
//3.绑定connect事件，连接建立成功触发
client.on("connect",function () {
    console.log("与服务器连接成功。")
});
//4.绑定data事件，接收服务器发送过来的数据
client.on("data",function (data) {
   console.log(data.toString());
});
```

---

```bash {.line-numbers}
node server.js

node client.js
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Simple Chatroom

---

#### Features

---

1. `03_chatroom_simple/server.js`
1. `03_chatroom_simple/client.js`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Advanced Chatroom

---

#### 功能

1. 用户注册登录
1. 全体消息
1. 私聊消息

---

#### 消息类型

1. 使用`json`格式化封装消息
1. `mstype`：消息类型
1. `code`：消息状态码
1. `from`：消息发送者
1. `to`：消息接收者
1. `message`：消息内容

---

#### 服务器维护客户端

1. `username`(`key`) <==> `socket`(`value`)

---

#### 消息输入格式

1. `[to :] <message>`
1. 使用`regexp`解析消息

---

#### 功能实现

1. `config.js`：服务端`ip address`和`port number`
1. server
    1. `server/app.js`：服务端
    2. `server/signup.js`：用户注册登录，用户名不能重复
    3. `server/broadcast.js`：全体消息
    4. `server/p2p.js`：私聊消息
1. client
    1. `client/app.js`：客户端

---

```bash {.line-numbers}
# run server
node server/app.js

# run client
node server/app.js
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
