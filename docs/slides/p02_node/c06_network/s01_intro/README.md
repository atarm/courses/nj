---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Introduction of Node.js Network Programming_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction of Node.js Network Programming

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [What is Network Programming](#what-is-network-programming)
2. [Modules of Node.js Network Programming](#modules-of-nodejs-network-programming)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## What is Network Programming

---

1. 网络通信：两个或多个通信实体（主机设备）之间的信息传递的过程
1. 计算机网络：将地理位置不同的多台计算机和相关设备连接起来，通过通信设备和线路实现信息传递，以及资源共享的系统
1. 网络模型：`ISO OSI`七层模型、`TCP/IP`四层模型
1. 网络编程：编程实现基于网络通信的应用程序，应用程序的网络编程基于传输层协议或应用层协议开发

>1. [OSI各个分层分别负责哪些功能？有哪些主要协议？涉及到哪些设备？](https://zhuanlan.zhihu.com/p/135878729)

---

1. `protocol`协议：人为约定、由参与方共同遵守的规范
1. `ip address`ip地址：网络中用于寻址主机的地址
1. `port number`端口号：网络中用于寻址进程的地址，
    1. `tcp`和`udp`端口号相互独立
    1. `0 - 65535`
    1. `0 - 1023`为系统保留端口号，`1024 - 65535`为用户端口号

---

1. `tcp(Transmission Control Protocol)`传输控制协议：一种面向连接的、可靠的传输层通信协议，由`IETF`的`RFC 793`定义
1. `udp(User Datagram Protocol)`用户数据报协议：一种无连接的、不可靠的传输层通信协议，由`IETF`的`RFC 768`定义

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Modules of Node.js Network Programming

---

1. `transport layer`传输层：
    1. `net`：`tcp`的封装
    1. `gdram`：`udp`的封装
1. `application layer`应用层：
    1. `http`：`HTTP/1.1`协议
    1. `https`：基于`HTTP/1.1`的`HTTPS`协议
    1. `http2`：`HTTP/2`协议、基于`HTTP/2`的`HTTPS`协议
    1. `...`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
