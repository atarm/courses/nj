---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Node and Redis_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Node and Redis

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction](#introduction)
2. [`redis` Package Programming](#redis-package-programming)
3. [`ioredis` Package Programming](#ioredis-package-programming)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction

---

### `redis` Package and `ioredis` Package

1. `Redis`是一个开源的`NoSQL`数据库
2. `Redis`主要用于缓存、消息队列、会话管理等
3. `node`可通过`redis`、`ioredis`等访问和操作`Redis`数据库

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `redis` Package Programming

---

```javascript {.line-numbers}
// 引入redis库
const redis = require('redis');

// 创建Redis客户端
const client = redis.createClient({
    host: 'localhost', // Redis服务器地址
    port: 6379        // Redis服务器端口
});

// 监听连接事件
client.on('connect', () => {
    console.log('Connected to Redis');
});

// 监听错误事件
client.on('error', (err) => {
    console.error('Redis error:', err);
});

// 设置一个键值对
client.set('key', 'value', (err, reply) => {
    if (err) throw err;
    console.log('SET:', reply);
});

// 获取一个键的值
client.get('key', (err, reply) => {
    if (err) throw err;
    console.log('GET:', reply);

    // 关闭Redis客户端
    client.quit((err) => {
        if (err) throw err;
        console.log('Redis client closed');
    });
});
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `ioredis` Package Programming

---

```javascript {.line-numbers}
const Redis = require('ioredis');
const redis = new Redis({
    host: 'localhost',
    port: 6379
});

redis.on('connect', () => {
    console.log('Connected to Redis');
});

redis.set('foo', 'bar');
redis.get('foo', (err, result) => {
    console.log(result); // 输出 'bar'
});
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
