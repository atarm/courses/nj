---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Node and MySQL_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Node and MySQL

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction](#introduction)
2. [`MySQL2` Programming](#mysql2-programming)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction

---

### `mysql` Package and `mysql2` Package

---

1. [mysql](https://www.npmjs.com/package/mysql)：早期的`MySQL`模块（最后更新：2020-01-24），基于`callback`实现异步
1. [mysql2](https://www.npmjs.com/package/mysql2)：后续的`MySQL`模块，提供`callback`和`promise`两种方式实现异步

---

>`MySQL2` project is a continuation of `MySQL-Native`. Protocol parser code was rewritten from scratch and api changed to match popular `mysqljs/mysql`. `MySQL2` team is working together with `mysqljs/mysql` team to factor out shared code and move it under `mysqljs` organisation.
>
>MySQL2 is mostly API compatible with mysqljs and supports majority of features.
>>[History and Why MySQL2.](https://github.com/sidorares/node-mysql2/blob/master/README.md#history-and-why-mysql2)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `mysql2` package Programming

>1. [mysqljs/mysql. README.md.](https://github.com/mysqljs/mysql/blob/master/Readme.md)
>2. [sidorares/node-mysql2.](https://github.com/sidorares/node-mysql2/blob/master/README.md)
    1. [node-mysql2 documentation.](https://sidorares.github.io/node-mysql2/docs/documentation)

---

1. 连接到`MySQL`数据库
2. 操作数据库
3. 关闭数据库连接

---

```javascript {.line-numbers}
// npm install mysql2

// 导入 mysql2 模块
const mysql = require('mysql2');

// 创建连接
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'password',
  database: 'test'
});

// 连接到数据库
connection.connect(err => {
  if (err) {
    console.error('连接失败: ' + err.stack);
    return;
  }
  console.log('已连接，连接ID: ' + connection.threadId);
});

// 执行查询
connection.query('SELECT * FROM users', (err, results, fields) => {
  if (err) {
    console.error('查询失败: ' + err.stack);
    return;
  }
  console.log('查询结果: ', results);
});

// 关闭连接
connection.end();
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
