---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Node and MongoDB_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Node and MongoDB

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction](#introduction)
2. [`mongodb` Package Programming](#mongodb-package-programming)
3. [`mongoose` Package Programming](#mongoose-package-programming)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction

---

### `mongodb` Package and `mongoose` Package

1. `mongodb`驱动是`MongoDB`官方提供的驱动程序，用于在`node`与`MongoDB`进行交互
2. `mongoose`是一个基于`mongodb`驱动的`ODM`库，提供了更高级别的抽象和功能，`mongoose`依赖`mongodb`

```bash {.line-numbers}
npm install mongodb
npm install mongoose
```

---

1. `mongodb`的特点：
    1. 直接操作：提供对`MongoDB`的直接访问和操作
    2. 灵活：允许开发者使用`MongoDB`的原生查询和命令
    3. 性能：由于是低级驱动，性能较高，但需要更多的手动配置和管理
2. `mongoose`的特点：
    1. 抽象：提供了更高级别的抽象`ODM(Object Document Mapping)`，使开发者可以更容易地操作`MongoDB`
    2. 模型：提供了模型的概念，使开发者可以更容易地定义和操作数据模型
    3. 插件：提供了插件的概念，使开发者可以更容易地扩展和定制功能

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `mongodb` Package Programming

---

```javascript {.line-numbers}
const { MongoClient } = require('mongodb');

// MongoDB 连接 URI
const uri = 'mongodb://localhost:27017';

// 创建一个新的 MongoClient
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

async function run() {
  try {
    // 连接到 MongoDB 服务器
    await client.connect();

    console.log('Connected successfully to server');

    // 选择数据库
    const database = client.db('myDatabase');
    // 选择集合
    const collection = database.collection('myCollection');

    // 插入文档
    const insertResult = await collection.insertOne({ name: 'Alice', age: 25 });
    console.log('Inserted document:', insertResult.insertedId);

    // 查询文档
    const findResult = await collection.findOne({ name: 'Alice' });
    console.log('Found document:', findResult);

  } finally {
    // 关闭连接
    await client.close();
  }
}

run().catch(console.dir);
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `mongoose` Package Programming

---

```javascript {.line-numbers}
const mongoose = require('mongoose');

// 连接到 MongoDB
mongoose.connect('mongodb://localhost:27017/myDatabase', { useNewUrlParser: true, useUnifiedTopology: true });

// 定义一个模式
const userSchema = new mongoose.Schema({
  name: String,
  age: Number
});

// 创建一个模型
const User = mongoose.model('User', userSchema);

async function run() {
  try {
    // 插入文档
    const user = new User({ name: 'Alice', age: 25 });
    await user.save();
    console.log('User saved:', user);

    // 查询文档
    const foundUser = await User.findOne({ name: 'Alice' });
    console.log('User found:', foundUser);

  } finally {
    // 关闭连接
    await mongoose.connection.close();
  }
}

run().catch(console.dir);
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
