---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Static Resource_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Static Resource and `underscore`

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Static Resource](#static-resource)
2. [`underscore`](#underscore)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Static Resource

---

1. `server`读取静态资源文件，然后把这些资源内容作为响应返回给客户端

---

```bash {.line-numbers}
p03_web/c01_routing/s02_static/static_resource_http/app_abstract.js

p03_web/c01_routing/s02_static/static_resource_http/app_static.js

p03_web/c01_routing/s02_static/static_resource_http/app_dynamic.js
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `underscore`

---

```bash {.line-numbers}
npm install underscore --save
```

>1. [underscorejs.org](https://underscorejs.org/)
>1. [npmjs. package/underscore.](https://www.npmjs.com/package/underscore)
>1. [github. jashkenas/underscore](https://github.com/jashkenas/underscore)

---

>Underscore is a JavaScript library that provides **_a whole mess of useful functional programming helpers_** **_without extending any built-in objects_**.
>
>Underscore provides **_over 100 functions that support both your favorite workaday functional helpers: map, filter, invoke_** - as well as **_more specialized goodies_**: function binding, javascript templating, creating quick indexes, deep equality testing, and so on.
>
>>[underscorejs.org](https://underscorejs.org/)

---

```bash {.line-numbers}
const _ = require('underscore');
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `_.map()`静态资源请求映射

---

```javascript {.line-numbers}
_.map(list, iteratee, [context])

Produces a new array of values by mapping each value in list through a transformation function (iteratee). \
    The iteratee is passed three arguments: the value, then the index (or key) of the iteration, \
    and finally a reference to the entire list.

_.map([1, 2, 3], function(num){ return num * 3; });
=> [3, 6, 9]
_.map({one: 1, two: 2, three: 3}, function(num, key){ return num * 3; });
=> [3, 6, 9]
_.map([[1, 2], [3, 4]], _.first);
=> [1, 3]
```

---

```bash {.line-numbers}
p03_web/c01_routing/s02_static/static_resource_underscore/
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `_.template()`页面模板

---

1. 将`templateString`编译为可以用于页面渲染的函数
2. 可通过`json`数据生成复杂的`html`

---

>Compiles JavaScript templates into functions that can be **_evaluated for rendering_**. Useful for rendering complicated bits of HTML from JSON data sources. Template functions can both interpolate values

---

1. execute code
   1. using `<%= … %>`, as well as execute arbitrary `JavaScript code`
2. interpolate value
   1. with `<% … %>`. If you wish to interpolate a value, and have it be `HTML-escaped`
   2. use `<%- … %>`. When you evaluate a template function, pass in a data object that has properties corresponding to the template's free variables.

---

>The settings argument should be a hash containing any `_.templateSettings` that should be overridden.
>
>>[underscorejs.org. template.](https://underscorejs.org/#template)

---

```javascript {.line-numbers}
_.template(templateString, [settings]);

var compiled = _.template("hello: <%= name %>");
compiled({name: 'moe'});
=> "hello: moe"

var template = _.template("<b><%- value %></b>");
template({value: '<script>'});
=> "<b>&lt;script&gt;</b>"

var compiled = _.template("<% print('Hello ' + epithet); %>");
compiled({epithet: "stooge"});
=> "Hello stooge"
```

---

```html {.line-numbers}
<!-- index.html -->
<h1><%= title %></h1>
<ul>
    <% arr.forEach(function(item){ %>
    <li> <%= item.name %></li>
    <% })%>
</ul>
```

---

```javascript {.line-numbers}
let compiled = _.template(data);
let htmlStr = compiled({
    title: 'hello world', arr: [{name: 'Jack'}, {name: 'rose'}, {name: 'levi'}]
});
res.end(htmlStr);
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
