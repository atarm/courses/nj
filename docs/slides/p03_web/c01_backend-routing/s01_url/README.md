---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_URL Parsing_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# URL Parsing

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [URL的结构](#url的结构)
2. [`url` Module](#url-module)
3. [`querystring` Module](#querystring-module)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## URL的结构

---

![width:1120](./.assets/image/url-structure-composition.png)

<!-- ```bash {.line-numbers}
┌────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                              href                                              │
├──────────┬──┬─────────────────────┬────────────────────────┬───────────────────────────┬───────┤
│ protocol │  │        auth         │          host          │           path            │ hash  │
│          │  │                     ├─────────────────┬──────┼──────────┬────────────────┤       │
│          │  │                     │    hostname     │ port │ pathname │     search     │       │
│          │  │                     │                 │      │          ├─┬──────────────┤       │
│          │  │                     │                 │      │          │ │    query     │       │
"  https:   //    user   :   pass   @ sub.example.com : 8080   /p/a/t/h  ?  query=string   #hash "
│          │  │          │          │    hostname     │ port │          │                │       │
│          │  │          │          ├─────────────────┴──────┤          │                │       │
│ protocol │  │ username │ password │          host          │          │                │       │
├──────────┴──┼──────────┴──────────┼────────────────────────┤          │                │       │
│   origin    │                     │         origin         │ pathname │     search     │ hash  │
├─────────────┴─────────────────────┴────────────────────────┴──────────┴────────────────┴───────┤
│                                              href                                              │
└────────────────────────────────────────────────────────────────────────────────────────────────┘
(All spaces in the "" line should be ignored. They are purely for formatting.)
``` -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `url` Module

>1. [node docs. module url.](https://nodejs.org/docs/latest-v20.x/api/url.html)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Current-api and Legacy-api

---

1. `url`模块提供了两套`api`：`current-api`/`WHATWG-api`与`legacy-api`

>WHATWG（Web Hypertext Application Technology Working GroupWeb 超文本应用程序技术工作组）是一个负责维护与开发 Web 标准的社区，他们的工作成果包括 DOM、Fetch API，和 HTML。一些来自 Apple、Mozilla，和 Opera 的员工在 2004 年建立了 WHATWG。
>
>>[MDN. WHATWG.](https://developer.mozilla.org/zh-CN/docs/Glossary/WHATWG)

---

1. `current api`
    1. `url.URL` class
    2. `url.URLSearchParams` class
2. `legacy api`
    1. `url.Url` class
    2. `url` module functions: `url.parse()`, `url.format()`, `url.resolve()`

>[node docs(v18.x lts). module url.](https://nodejs.org/docs/latest-v18.x/api/url.html)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Current-api: `url.URL` and `url.URLSearchParams`

---

#### `url.URL`

1. 用于解析和格式化URL

```javascript {.line-numbers}
Class: URL

History
v10.0.0 The class is now available on the global object.
v7.0.0, v6.13.0 Added in: v7.0.0, v6.13.0

Browser-compatible URL class, implemented by following the WHATWG URL Standard. \
    Examples of parsed URLs may be found in the Standard itself.
    The URL class is also available on the global object.
```

```javascript {.line-numbers}
console.log(URL === require('node:url').URL); // Prints 'true'.
```

>[node docs(v18.x lts). class URL.](https://nodejs.org/docs/latest-v18.x/api/url.html#class-url)

---

##### `url.URL` Constructor

```javascript {.line-numbers}
new URL(input[, base])

input <string> The absolute or relative input URL to parse. If input is relative, \
    then base is required. If input is absolute, the base is ignored. \
    If input is not a string, it is converted to a string first.
base <string> The base URL to resolve against if the input is not absolute. \
    If base is not a string, it is converted to a string first.

Creates a new URL object by parsing the input relative to the base. \
    If base is passed as a string, it will be parsed equivalent to new URL(base).
```

---

##### `url.URL` Properties

1. `url.href`
1. `url.origin`
1. `url.protocol`
1. `url.username`
1. `url.password`
1. `url.host`
1. `url.hostname`
1. `url.port`

---

1. `url.pathname`
1. `url.search`
1. `url.searchParams`
1. `url.hash`

---

##### `url.URL` Methods

1. `url.toJSON()`
1. `url.toString()`

---

#### `URLSearchParams` Class

1. 用于解析和格式化`URL`的查询字符串

```javascript {.line-numbers}
Class: URLSearchParams

History
v10.0.0 The class is now available on the global object.
v7.5.0, v6.13.0 Added in: v7.5.0, v6.13.0

The URLSearchParams API provides read and write access to the query of a URL. \
    The URLSearchParams class can also be used standalone \
    with one of the four following constructors. \
    The URLSearchParams class is also available on the global object.
```

>[node docs(v18.x lts). class URLSearchParams.](https://nodejs.org/docs/latest-v18.x/api/url.html#class-urlsearchparams)

---

##### `URLSearchParams` Properties

1. `urlSearchParams.size`

---

##### `URLSearchParams` Methods

1. `urlSearchParams.get(name)`
1. `urlSearchParams.getAll(name)`
1. `urlSearchParams.has(name[, value])`
1. `urlSearchParams.entries()`
1. `urlSearchParams.keys()`
1. `urlSearchParams.values()`
1. `urlSearchParams.toString()`
1. `urlSearchParams.size`

---

1. `urlSearchParams.set(name, value)`
1. `urlSearchParams.append(name, value)`
1. `urlSearchParams.delete(name[, value])`
1. `urlSearchParams.sort()`

---

1. `urlSearchParams.forEach(fn[, thisArg])`
1. `urlSearchParams[Symbol.iterator]()`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### legacy-api: `url.Url` and `url module functions`

---

1. `url.parse()`返回`Url`对象
2. `url.Url`包含了`URL`的各个部分，例如`href`, `protocol`, `host`, `pathname`, `search`等

```javascript {.line-numbers}
url.parse(urlString[, parseQueryString[, slashesDenoteHost]])
```

![width:1120](./.assets/image/url-module_and_url-class.png)

<!-- ---

### `function Url()` class

```javascript {.line-numbers}
function Url() {
    this.protocol = null;
    this.slashes = null;
    this.auth = null;
    this.host = null;
    this.port = null;
    this.hostname = null;
    this.hash = null;
    this.search = null;
    this.query = null;
    this.pathname = null;
    this.path = null;
    this.href = null;
}
``` -->

---

```javascript {.line-numbers}
const url = require('url');

let parsed_url = url.parse('https://example.com:8080/path/name?query=string#hash');

console.log(parsed_url);
console.log(parsed_url.query);

parsed_usrl = url.parse('https://example.com:8080/path/name?query=string#hash', true);

console.log(parsed_url);
console.log(parsed_url.query);
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Similarities and Differences between `current-api` and `legacy-api`

1. 相同点
    1. 功能基本一致：解析和格式化`URL`
2. 不同点
    1. `current-api`是用于替代`legacy-api`的新的`api`
    2. `current-api`基于`WHATWG URL Standard`实现，`legacy-api`基于`node`自己的实现
    3. `current-api`的`URL`对象是`immutable`的（修改属性时生成一个新的`URL`对象，而不是直接修改原`URL`对象），`legacy-api`的`Url`对象是`mutable`的
    4. `current-api`的`URL`对象不支持相对`URL`，`legacy-api`的`Url`对象支持相对`URL`

---

#### `immutable` vs `mutable`

```javascript {.line-numbers}
const myURL = new URL('https://example.com/path?name=John&age=30');
console.log(myURL.href); // 输出: https://example.com/path?name=John&age=30

// 尝试修改属性
myURL.pathname = '/newpath';
console.log(myURL.href); // 输出: https://example.com/newpath?name=John&age=30

// 尝试修改searchParams
myURL.searchParams.set('name', 'Jane');
console.log(myURL.href); // 输出: https://example.com/newpath?name=Jane&age=30
```

---

```javascript {.line-numbers}
const url = require('url');

const parsedUrl = url.parse('https://example.com/path?name=John&age=30', true);
console.log(parsedUrl.href); // 输出: undefined

// 修改属性
parsedUrl.pathname = '/newpath';
parsedUrl.query.name = 'Jane';
parsedUrl.search = null; // 需要重新生成search部分

const formattedUrl = url.format(parsedUrl);
console.log(formattedUrl); // 输出: https://example.com/newpath?name=Jane&age=30
```

---

#### Relative URL

>1. [Relative URLs in WHATWG URL API #12682](https://github.com/nodejs/node/issues/12682)

---

1. `current-api`的`new URL()`构造函数不支持相对`URL`
2. `legacy-api`的`url.parse()`函数支持相对`URL`
3. 怎样使用`current-api`构造相对`URL`？

```javascript {.line-numbers}
const url = require('url');

console.log(url.parse('hash.js'));                // will work
console.log(new URL('hash.js', 'http://complete-url/'));  // will work
// console.log(new URL('hash.js'));                       // TypeError: Invalid URL: #hash
```

---

```javascript {.line-numbers}
const http = require('http');

const server = http.createServer((req, res) => {
    // 判断请求使用的协议
    const protocol = req.connection.encrypted ? 'https://' : 'http://';
    // 基准 URL，可以是服务器的主机名和端口
    const baseUrl = `${protocol}${req.headers.host}`;
    // 将相对链接转换为绝对链接
    const absoluteUrl = new URL(req.url, baseUrl);
    // 输出绝对链接和协议
    console.log(`Protocol: ${protocol}`);
    console.log(`Absolute URL: ${absoluteUrl.href}`);

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end(`Your Request URL is: ${absoluteUrl.href}`);
});

server.listen(8080, () => {
    console.log('Server running at http://127.0.0.1:8080/');
});
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## `querystring` Module

---

1. 解析和格式化`URL`查询字符串

>The `node:querystring` module provides utilities for **_parsing and formatting URL query strings_**.
>
>`querystring` is more performant than `<URLSearchParams>` but **_is not a standardized API_**. Use `<URLSearchParams>` when **_performance is not critical or when compatibility with browser_** code is desirable.

>[node docs(v18.x lts). module querystring.](https://nodejs.org/docs/latest-v18.x/api/querystring.html)

---

1. `querystring.parse(str[, sep[, eq[, options]]])`
1. `querystring.stringify(obj[, sep[, eq[, options]]])`
2. `querystring.escape(str)`: performs URL percent-encoding on the given str in a manner that is optimized for the specific requirements of URL query strings
3. `querystring.unescape(str)`: performs decoding of URL percent-encoded characters on the given str.

---

```javascript {.line-numbers}
const http = require('http');
const url = require('url');
const querystring = require('querystring');

let server = http.createServer();

server.on('request', function (req, res) {
    console.log(req.url);
    let realpath = url.parse(req.url, true);
    console.log(realpath);
    console.log(realpath.query.username);
});

server.listen(3000, function () {
    console.log('server is listening at port 3000');
});

/**
 * http://127.0.0.1:3000/login?username=jack&password=123456
 */
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Back-End Routing Examples

---

```javascript {.line-numbers}
const http = require('http');
const url = require('url');

const server = http.createServer((req, res) => {
    console.log(`req.url is ${req.url}`);

    const req_url = url.parse(req.url);

    switch (req_url.pathname) {
        case '/':
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end('home page');
            break;
        case '/login':
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end('login page');
            break;
        case '/register':
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end('register page');
            break;
        default:
            res.writeHead(404, { 'Content-Type': 'text/plain' });
            res.end('404 not found');
            break;
    }
});

server.listen(3000, () => {
    console.log('server is listening at port 3000');
});
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
