---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Form Processing and formidable_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Form Processing and `formidable`

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [表单处理](#表单处理)
2. [`formidable`](#formidable)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 表单处理

---

1. `GET`：请求数据嵌入在路径中，`URL`是完整的请求，包括了`?`后面的部分，可以解析后面的内容作为`GET`请求的参数
2. `POST`：
    1. `node`为了非阻塞，会将`post`请求数据分成多个数据块，`post`请求的内容在请求体中
    2. `http.ServerRequest()`没有提供解析请求体的方法或获取请求体属性，因此，需要自己来实现获取请求体

---

```bash {.line-numbers}
p03_web/c01_routing/s03_form/get.js

p03_web/c01_routing/s03_form/post.js
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `formidable`

---

```bash {.line-numbers}
npm install formidable --save
```

>1. [github. node-formidable/formidable.](https://github.com/node-formidable/formidable)
>1. [npmjs. package/formidable.](https://www.npmjs.com/package/formidable)

---

>a service focused on uploading and encoding images and videos. It has been battle-tested against hundreds of GBs of file uploads from a large variety of clients and is considered production-ready and is used in production for years.
>
>>[github. node-formidable/formidable.](https://github.com/node-formidable/formidable)

---

```javascript {.line-numbers}
const formidable = require('formidable');

let form = new formidable.IncomingForm();
form.parse(req, function (err, fields, files) {
});
```

---

```bash {.line-numbers}
p03_web/c01_routing/s03_form/formidable
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
