---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Middleware_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Middleware

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction of `middleware`](#introduction-of-middleware)
2. [应用级中间件与路由级中间件](#应用级中间件与路由级中间件)
3. [`built-in middleware`内置中间件](#built-in-middleware内置中间件)
4. [`third-part middleware`第三方中间件](#third-part-middleware第三方中间件)
5. [`custom middleware`自定义中间件](#custom-middleware自定义中间件)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction of `middleware`

---

1. `middleware`指将具体的业务逻辑和底层逻辑解耦的组件，即，`middleware`是能适用于多个业务场景、可复用性的代码
2. `express`中的`middleware`可以理解为与`endpoint`的业务逻辑无关的通用处理函数

>1. [中间件是什么？如何解释比较通俗易懂？](https://www.zhihu.com/question/19730582)

---

![height:600](./.assets/image/middleware.png)

---

```javascript {.line-numbers}
function someMiddleware(req, res, next) {
    // handle logic
    next();
}
```

1. 按`middleware chain`中的顺序依次执行，`middleware`在`chain`中的顺序由`mount`的顺序决定
1. `middleware chain`中所有`middleware`共享一份`req`和`res`
1. 既可传递给下一个`middleware`，也可直接返回响应
1. 既不调用`next()`，也不直接返回响应时，将会导致请求挂起
1. 调用`next("err msg")`包含实参时（`next("route")`除外），则抛出异常，异常的描述为`"err msg"`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 应用级中间件与路由级中间件

---

1. `application-level middleware`应用级中间件：对用户发起任何请求会执行的中间件，通过`app.use(someMiddleware)`进行注册，也称为"全局中间件"
2. `router-level middleware`路由级中间件：对某个或某些路由`endpoint`执行的中间件，也称为"局部中间件"
    1. 通过`app.use(PATH, someMiddleware)`进行注册
    2. 通过`app.<METHOD>(PATH, someMiddleware)`进行注册
    3. 通过`router.use(PATH, someMiddleware)`进行注册

---

```javascript {.line-numbers}
app.use(someMiddleware);
```

```javascript {.line-numbers}
app.use('/middleware', someMiddleware);

app.get('/middleware', someMiddleware, (req, res) => {
  res.send('Hello World');
});

router.use('/bar', function (req, res, next) {
  // ... maybe some additional /bar logging ...
  next();
})
```

---

```javascript {.line-numbers}
const app = express();

function loggingMiddleware(req, res, next) {
    const time = new Date();
    // only for demo code
    console.log(`[${time.toLocaleString()}] ${req.method} ${req.url}`);
    next();
}

app.use(loggingMiddleware);

app.get('/', (req, res) => {
    res.send('Hello World');
});
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `built-in middleware`内置中间件

---

1. `express.json([options])`： It parses incoming requests with JSON payloads and is based on `body-parser`.
2. `express.raw([options])`: It parses incoming request payloads into a Buffer and is based on `body-parser`.
3. `express.text([options])`: It parses incoming request payloads into a string and is based on `body-parser`.
4. `express.urlencoded([options])`: It parses incoming requests with urlencoded payloads and is based on `body-parser`.
5. `express.static(root, [options])`: It serves static files and is based on `serve-static`.

>[express().](https://expressjs.com/en/4x/api.html#express)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `third-part middleware`第三方中间件

---

1. 不是`built-in`的`middleware`
2. 有些由`expressjs team`维护，有些由`community`维护

>1. [Express middleware](https://expressjs.com/en/resources/middleware.html)

---

### 重要的由`expressjs team`维护的`third-part middleware`

```javascript {.line-numbers}
body-parser       // Parse HTTP request body
cookie-parser     // Parse cookie header and populate req.cookies
cookie-session    // Establish cookie-based sessions
method-override   // Override HTTP methods using header
multer            // Handle multi-part form data
```

---

```javascript {.line-numbers}
// $ npm install body-parser
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(function (req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.write('you posted:\n');
    res.end(JSON.stringify(req.body, null, 2));
})
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `custom middleware`自定义中间件

---

1. `function myMiddleware(req, res, next)`定义中间件函数
   1. 监听`req`的`data`事件
   2. 监听`req`的`end`事件
   3. 在`end`事件中决定调用`next()`抑或直接返回响应
2. `app.use(myMiddle)`使用中间件函数

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
