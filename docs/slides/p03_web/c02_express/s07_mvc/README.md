---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Model-View-Controller (MVC)_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Model-View-Controller (MVC)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction of MVC](#introduction-of-mvc)
2. [MVC in Express](#mvc-in-express)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction of MVC

---

1. `Model`: 数据模型，或者说是`model engine`（产生数据的模型）
1. `View`: 视图模型，或者说是`view engine`（渲染数据的模型）
1. `Controller`: 控制器，控制数据模型和视图模型的交互

---

### `Model`再分层

1. `handler`: 将`req`处理成`service`能接收的实参
2. `service`: 处理业务逻辑
3. `persistence`: 外理数据访问

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## MVC in Express

---

1. `Model` ==> `callback function` of `route`
1. `View` ==> `template engine`
1. `Controller` ==> `router`/`routing mechanism`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
