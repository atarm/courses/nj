---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "__"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction of Express

>[wiki. Express.js.](https://en.wikipedia.org/wiki/Express.js)
>[expressjs.](https://expressjs.com/)
>[express api reference.](https://expressjs.com/en/4x/api.html)
>[express examples.](https://github.com/expressjs/express/tree/master/examples)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [内置模块的不足](#内置模块的不足)
2. [Express的扩展](#express的扩展)
3. [Express是什么](#express是什么)
4. [Express的核心](#express的核心)
5. [Koa](#koa)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 内置模块的不足

---

1. 需要编写较多的代码：例如`http status code`和`http headers`的设置
2. 没有专门的路由机制：需要自己编写`if-else`语句来判断请求的路径和方法
3. 缺乏设计结构的支撑：容易将所有代码写在一个文件，容易一人一种结构风格，不利于维护和扩展

---

```javascript {.line-numbers}
const http = require('http');

const hostname = 'localhost';
const port = 3000;

const server = http.createServer((req, res) => {
    if (req.url === '/') {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html');
        res.end('Hello World\n');
    }
    else if (req.url === '/login') {
        let fullPath = path.join(__dirname, 'views/login.html');
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html');
        fs.readFile(fullPath, function (err, data) {
            err === null ? res.end(data) : console.log(err.message);
        });
    }
});

server.listen(port, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Express的扩展

1. 功能扩展
    1. 强大的扩展对象：更强大的`req`和`res`对象
    2. 灵活的路由机制：灵活方便的路由的定义与解析，能实现方便地进行代码拆分
    3. 易用的分治机制：**_"anything is middleware"_** 和 **_"sub-app"_** 的设计理念使得`express`的模块化容易、扩展性强大
2. 规则约束与设计模式

---

```javascript {.line-numbers}
const express = require('express');
const app = express();

const hostname = 'localhost';
const port = 3000;

app.engine('art', require('express-art-template'));
app.set('views', path.join(__dirname, 'views'));

app.get('/', (req, res) => {
    res.send('Hello World');
});

app.get("/login", (req, res) => {
    res.render("login.art");
});

app.listen(port, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Express是什么

---

1. 建立在`built-in http module`之上，兼容`built-in http module`的所有功能
2. 一个`super lightweight`的`web framework`
3. 核心是`middleware`和`routing`

>Express is a **_`routing`_** and **_`middleware`_** web framework that has minimal functionality of its own: An Express application is essentially a series of middleware function calls.
>
>>[Using middleware.](https://expressjs.com/en/guide/using-middleware.html)

---

### Brief History of Express

1. 2010-05-22: the first release, `version 0.12`
2. 2014-06-23: rights to manage the project were acquired by `StrongLoop`
3. 2015-09-09: `StrongLoop` was acquired by `IBM`
4. 2016-01-20: `IBM` announced that it would place `Express.js` under the stewardship of the `Node.js Foundation` incubator
5. 2014-04-09: `version 4.0.0` was released
6. 2024-09-10: `version 4.20.0` was released
7. 2024-09-10: `version 5.0.0` was released
8. 2024-09-11: `version 4.21.0` was released
9. 2024-10-08: `version 5.0.1` was released

---

>[Release Change Log(4.x).](https://expressjs.com/en/changelog/4x.html)
>[express history.](https://github.com/expressjs/express/blob/master/History.md)
>[Introducing Express v5: A New Era for the Node.js Framework.](https://expressjs.com/2024/10/15/v5-release.html)
>[Moving to Express 5.](https://expressjs.com/en/guide/migrating-5.html)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Express的核心

---

1. 核心理念
    1. `middleware`：一个`middleware`是一个`callback`
    2. `routing`: 从`req`到`res`是在`middleware chain`的`routing`过程
2. 核心组件
    1. `Application`: 一个`app`
    2. `Router`: 一个`sub-app`
    3. `Request`: 对请求的封装
    4. `Response`: 对响应的封装

❗ `express`本身不提供数据访问功能 ❗

---

### `app`对象的方法

1. `app.listen([port[, host[, backlog]]][, callback])`: Binds and listens for connections on the specified `host` and `port`. This method is identical to `Node's` `http.Server.listen()`.
2. `app.path()`: Returns the canonical path of the app, a string.

---

#### `settings` Relative

1. `app.get(name)`
    1. `app.enabled(name)`: equal to `app.get(name) === true ? true : false`
    1. `app.disabled(name)`: equal to `app.get(name) === false ? true : false`
1. `app.set(name, value)`
    1. `app.enable(name)`: equal to `app.set(name, true)`
    1. `app.disable(name)`: equal to `app.set(name, false)`

---

#### `middleware` Relative

1. `app.use([path,] callback [, callback...])`: Mounts the specified middleware function or functions at the specified path: the middleware function is executed when the base of the requested path matches path.

---

#### `routing` Relative

1. `app.param([name], callback)`: Add callback triggers to `route parameters`, where `name` is the name of the parameter or an `array` of them, and `callback` is the callback function.
1. `app.route(path)`

---

1. `app.METHOD(path, callback [, callback ...])`
    1. `app.get(path, callback [, callback ...])`
    1. `app.post(path, callback [, callback ...])`
    1. `app.put(path, callback [, callback ...])`
    1. `app.delete(path, callback [, callback ...])`
    1. `app.all(path, callback [, callback ...])`

---

#### `view` Relative

```javascript {.line-numbers}
app.engine(ext, callback)

/* It is like res.render(), except it cannot \
        send the rendered view to the client on its own.
Internally res.render() uses app.render() to render views.*/
app.render(view, [locals], callback)`
```

---

### `express-generator`

```bash {.line-numbers}
npm install -g express-generator

express <app_name>

cd <app_name>
npm install
npm start   # see more in `package.json`
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Koa

>[Koa.](https://koajs.com/)
>[koajs/examples.](https://github.com/koajs/examples)
>[Koa vs Express.](https://github.com/koajs/koa/blob/master/docs/koa-vs-express.md)

---

1. `Koa` is a new web framework designed by the team behind `Express`, which aims to be a smaller, more expressive, and more robust foundation for web applications and APIs
2. `Koa` requires `node v12` or higher for `ES2015` and `async` function support

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
