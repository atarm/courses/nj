---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Request and Response_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Request and Response

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction of `Request` and `Response` in `Express`](#introduction-of-request-and-response-in-express)
2. [Request](#request)
3. [Response](#response)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction of `Request` and `Response` in `Express`

---

1. 对`http`模块中的`req`和`res`对象的进一步封装
2. 兼容`http`模块中的`req`和`res`对象，即，既可以使用`http`模块中的所有方法，也可以使用`express`中的增强方法实现相同的功能
3. `Request`增强了对请求行、请求头、请求体的解析
4. `Response`增强了对响应行、响应头、响应体的设置

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Request

>1. [express api docs - Request.](https://expressjs.com/en/4x/api.html#req)

---

### 请求行处理

1. `req.protocol`：请求协议
2. `req.path`：请求路径（不包含查询参数）
3. `req.query`：请求`URL`中的查询参数对象
4. `req.params`：请求`URL`中的路由参数对象

```javascript {.line-numbers}
app.get('/',(req,res)=>{
    console.log(req.protocol);
    console.log(req.path);
    console.log(req.query);
})
```

---

#### 路由参数

1. 基本路由参数：路由参数通过冒号`:`定义，例如，`/user/:id`中的`:id`是一个路由参数
2. 可选路由参数：路由参数是可选的，通过在参数后加上问号`?`实现，例如，`/user/:id?`中的`:id`是一个可选参数
3. 多个路由参数：可以在一个路由中定义多个参数，例如，`/user/:id/book/:bookId`中的`:id`和`:bookId`是两个路由参数
4. 正则表达式：可以使用正则表达式来匹配路由参数，例如，`/user/:id(\d+)`只匹配数字形式的`id`

---

```javascript {.line-numbers}
const express = require('express');
const app = express();

// 基本路由参数
app.get('/user/:id', (req, res) => {
    console.log(req.params.id); // 输出路由参数 id
    res.send(`User ID: ${req.params.id}`);
});

// 可选路由参数
app.get('/user/:id?', (req, res) => {
    console.log(req.params.id); // 输出路由参数 id 或 undefined
    res.send(`User ID: ${req.params.id}`);
});

// 多个路由参数
app.get('/user/:id/book/:bookId', (req, res) => {
    console.log(req.params.id); // 输出路由参数 id
    console.log(req.params.bookId); // 输出路由参数 bookId
    res.send(`User ID: ${req.params.id}, Book ID: ${req.params.bookId}`);
});

// 正则表达式路由参数
app.get('/user/:id(\\d+)', (req, res) => {
    console.log(req.params.id); // 只匹配数字形式的 id
    res.send(`User ID: ${req.params.id}`);
});

app.listen(3000, () => {
    console.log('Server is running on port 3000');
});
```

---

### 请求头处理

1. `req.cookies`：请求`cookies`对象
1. `req.get(field)`：获取请求头中的字段值

---

### 请求体处理

1. `req.body`：请求体的数据，可能是表单或`JSON`数据或其他

---

### 其他

1. `req.baseUrl`：The URL path on which a router instance was mounted

```javascript {.line-numbers}
let greet = express.Router();

greet.get('/jp', function (req, res) {
    console.log(req.baseUrl);   // /greet
    res.send('Konnichiwa!');
})

app.use('/greet', greet);       // load the router on '/greet'
```

<!-- ---

req.app：当callback为外部文件时，用req.app访问express的实例
req.baseUrl：获取路由当前安装的URL路径
req.body / req.cookies：获得「请求主体」/ Cookies
req.fresh / req.stale：判断请求是否还「新鲜」
req.hostname / req.ip：获取主机名和IP地址
req.originalUrl：获取原始请求URL
req.params：获取路由的parameters
req.path：获取请求路径
req.protocol：获取协议类型
req.route：获取当前匹配的路由
req.subdomains：获取子域名
req.accepts()：检查可接受的请求的文档类型
req.acceptsCharsets / req.acceptsEncodings / req.acceptsLanguages：返回指定字符集的第一个可接受字符编码
req.get()：获取指定的HTTP请求头
req.is()：判断请求头Content-Type的MIME类型 -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Response

>1. [express api docs - Response.](https://expressjs.com/en/4x/api.html#res)

---

### 响应行处理

```javascript {.line-numbers}
res.status(code)
res.sendStatus(statusCode)
res.redirect([status,] path)
```

---

### 响应头处理

```javascript {.line-numbers}
res.set(field [, value])
res.cookie(name, value [, options])
res.type()
```

<!-- res.app：同req.app一样
res.append()：追加指定HTTP头
res.set()在res.append()后将重置之前设置的头
res.cookie(name，value [，option])：设置Cookie
opition: domain / expires / httpOnly / maxAge / path / secure / signed
res.clearCookie()：清除Cookie
res.download()：传送指定路径的文件
res.get()：返回指定的HTTP头
res.json()：传送JSON响应
res.jsonp()：传送JSONP响应
res.location()：只设置响应的Location HTTP头，不设置状态码或者close response
res.redirect()：设置响应的Location HTTP头，并且设置状态码302
res.render(view,[locals],callback)：渲染一个view，同时向callback传递渲染后的字符串，如果在渲染过程中有错误发生next(err)将会被自动调用。callback将会被传入一个可能发生的错误以及渲染后的页面，这样就不会自动输出了。
res.send()：传送HTTP响应
res.sendFile(path [，options] [，fn])：传送指定路径的文件 -会自动根据文件extension设定Content-Type
res.set()：设置HTTP头，传入object可以一次设置多个头
res.status()：设置HTTP状态码
res.type()：设置Content-Type的MIME类型 -->

---

### 响应体处理

```javascript {.line-numbers}
res.send([body])
res.json([body])
res.jsonp([body])
res.download(path [, filename] [, fn])
res.sentFile(path [, options] [, fn])
res.render(view [, locals] [, callback])
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
