---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Template Engine_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Template Engine

>[Using template engines with Express.](https://expressjs.com/en/guide/using-template-engines.html)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction of `template engine` in `Express`](#introduction-of-template-engine-in-express)
2. [Pug](#pug)
3. [Art-Template](#art-template)
4. [EJS](#ejs)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction of `template engine` in `Express`

---

1. `template engine`是一种`server-side`渲染`page`的技术
2. `template engine`实现了`server-side`的`view`和`data`的分离
3. `express`支持`Pug`、`Handlebars`、`EJS(Embedded JavaScript Templates)`、`art-template`等模板引擎

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Pug

---

1. `Pug`曾用名`Jade`，由于`Jade`是一个已被注册的商标，因此更名为`Pug`

```bash {.line-numbers}
npm install pug
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Art-Template

---

```bash {.line-numbers}
npm install art-template
npm install express-art-template
```

---

```javascript {.line-numbers}
var express = require('express');
var app = express();

// view engine setup
app.engine('art', require('express-art-template'));
app.set('view', {
    debug: process.env.NODE_ENV !== 'production'
});
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'art');

// routes
app.get('/', function (req, res) {
    res.render('index.art', {
        user: {
            name: 'aui',
            tags: ['art', 'template', 'nodejs']
        }
    });
});
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## EJS

---

```bash {.line-numbers}
npm install ejs
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
