---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Cookie and Session_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Cookie and Session

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction of Cookie and Session](#introduction-of-cookie-and-session)
2. [Cookie](#cookie)
3. [Session](#session)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction of Cookie and Session

---

1. `cookie`：储存在`browser`中的一小段文本信息，像小甜饼一样
    1. 储存在`browser`中：`server`生成、`client`保存，`client`请求时，发送给`server`
    2. 一小段文本信息：一般限制在4kb以内
2. `session`
    1. 储存在`server`中：`server`生成、`server`保存全部信息、`client`以`cookie`形式保存`session_id`
    2. `client`请求时，发送`session_id`给`server`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Cookie

---

```bash {.line-numbers}
npm install cookie-parser
```

---

```javascript {.line-numbers}
const express = require("express");
const cookieParser = require("cookie-parser");
const app = express();

app.use(cookieParser());

app.get("/", (req, res) => {
    // 服务器端通过req来获取cookie数据
    if (req.cookies.username) {
        console.log(req.cookies);
        res.send(`再次欢迎您，${req.cookies.username}`);
    } else {
        res.cookie("username", "admin", { maxAge: 86400 * 1000 });
        res.send("欢迎你~");
    }
});
app.listen(8080);
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Session

---

```bash {.line-numbers}
npm install cookie-session
```

---

```javascript {.line-numbers}
const express = require("express");
const session = require("cookie-session");
const app = express();

app.use(
    session({
        name: "sid",
        // 对session_id加密的key
        secret: "afsfwefwlfjewlfewfef",
        maxAge: 20 * 60 * 1000, // 20分钟
    })
);
app.get("/", (req, res) => {
    if (!req.session.view) {
        req.session.view = 1;
    } else {
        req.session.view++;
    }
    res.send(`欢迎您第 ${req.session.view} 次访问！`);
});
app.listen(8081);
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
