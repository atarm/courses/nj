---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Routing in Express_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Routing in Express

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction Of Routing in Express](#introduction-of-routing-in-express)
2. [静态资源路由](#静态资源路由)
3. [404路由与错误处理路由](#404路由与错误处理路由)
4. [子路由Sub-Router](#子路由sub-router)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction Of Routing in Express

---

1. `endpoint = http.url.pathname + http.method`
1. `app.METHOD(PATH, HANDLER)`本质上是`app.use(path, callback)`的`specialization特化`
1. `express`本身只支持`get/post/put/delete`4种请求动词，其他请求方法可使用第三方中间件或自定义中间件进行支持，如[`method-override`](https://expressjs.com/en/resources/middleware/method-override.html)

>`Routing` refers to how an application's `endpoints (URIs)` respond to client requests.
>
>>[Express. Routing.](https://expressjs.com/en/guide/routing.html)

---

### `app.param([name], callback)`

1. `app.param()`是`express`的`middleware`，用于对`URL`中的路由参数进行预处理
2. `name`: 要预处理的路由参数名称
3. `callback`: 预处理函数，形参为`req, res, next, id`，其中`id`接收路由参数的值

---

```javascript {.line-numbers}
const express = require('express');
const app = express();

// 定义一个预处理逻辑
app.param('userId', (req, res, next, userId) => {
    // 这里可以对 userId 进行验证或其他处理
    console.log(`Received userId: ${userId}`);
    // 将处理后的值存储在 req 对象上
    req.user = { id: userId };
    next();
});

// 使用预处理后的路由参数
app.get('/user/:userId', (req, res) => {
    res.send(`User ID: ${req.user.id}`);
});

app.listen(3000, () => {
    console.log('Server is running on port 3000');
});
```

---

### `app.route(path)`

1. Returns an instance of a single route, which you can then use to handle `HTTP verbs` with optional middleware
2. Use `app.route()` to avoid duplicate route names (and thus typo errors)
3. 创建一个可链接的路由处理器实例，用于处理特定路径上的多个`method`，可以避免重复定义相同路径的路由，从而减少代码中的重复和潜在的拼写错误

---

```javascript {.line-numbers}
const express = require('express');
const app = express();

app.route('/events').all((req, res, next) => {
    // 适用于所有 HTTP 动词的中间件
    console.log('Accessing the events route');
    next();
  }).get((req, res) => {
    // 处理 GET 请求
    res.json({ message: 'GET request to /events' });
  }).post((req, res) => {
    // 处理 POST 请求
    res.json({ message: 'POST request to /events' });
  }).put((req, res) => {
    // 处理 PUT 请求
    res.json({ message: 'PUT request to /events' });
  }).delete((req, res) => {
    // 处理 DELETE 请求
    res.json({ message: 'DELETE request to /events' });
  });

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
```

>[app.route(path).](https://expressjs.com/en/4x/api.html#app.route)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 静态资源路由

---

<!--
q: if the static resource mount to `/`, how the express handle the static resource request and dynamic request? If there path conflict, what would happen?
a: the express will first try to match the static resource, if not match, then try to match the dynamic request.
-->

```javascript {.line-numbers}
// mount to "/"
app.use(express.static(path.join(__dirname, "public")));

// mount to "/static/"
app.use("/static", express.static(path.join(__dirname, "public")));
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 404路由与错误处理路由

---

![width:1120](./.assets/image/middleware_with_404-error.png)

---

1. `error handle routing`是一种特殊的`middleware`，其`callback`函数的参数为`err, req, res, next`
2. `404 routing`一般`mount`在`error handle routing`之前（因为，`404 routing`也可能抛出异常）

---

```javascript {.line-numbers}
app.use('*', (req, res) => {
    res.status(404).render('404', {url: req.originalUrl});
});

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).render('500');
});

app.listen(port, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 子路由Sub-Router

---

1. `express.Router`可以理解为`sub-app`，支持加载`middleware`和`routing`
2. `express.Router`对`routing`进行模块化，以便更好地管理`routing`
3. `express.Router`挂载到`express`实例上，形成`sub-router`

---

```javascript {.line-numbers}
let adminRouter = express.Router();

adminRouter.use(someMiddleware);

adminRouter.get('/hello', helloHandler);
adminRouter.post('/world', worldHandler);

module.exports = adminRouter;
```

```javascript {.line-numbers}
let app = express();

const adminRouter = require('./routers/admin.js');

app.use('/admin', adminRouter);
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
