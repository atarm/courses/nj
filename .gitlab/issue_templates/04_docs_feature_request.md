<!--
---
name: :rainbow: docs feature request
about: suggest a docs feature
---
-->

<!--
🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅

oh hi there! 😄

to expedite issue processing please search open and closed issues before submitting a new one.

existing issues often contain information about workarounds, resolution, or progress updates.

🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅
-->

<!--
borrow from:

https://raw.githubusercontent.com/angular/angular/master/.github/ISSUE_TEMPLATE/2-feature-request.md

https://raw.githubusercontent.com/stevemao/github-issue-templates/master/emoji-guide/Feature_request.md
-->

# :rainbow: Docs Feature Request

## :incoming_envelope: Short Description

<!-- :memo: a clear and concise description of the feature you want... -->

## :hammer_and_wrench: Possible Solution

<!-- :memo: -->

## :thinking: Alternatives considered

<!-- :memo: have you considered any alternative solutions or workarounds? -->
