# Node.js技术

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [关于About](#关于about)
    1. [关于本课程About Course](#关于本课程about-course)
    2. [关于本仓库About Repository](#关于本仓库about-repository)
        1. [许可协议Licenses](#许可协议licenses)
        2. [目录组织Structure](#目录组织structure)
        3. [帮助完善Contribution](#帮助完善contribution)
2. [参考资料References](#参考资料references)
3. [工具Tools](#工具tools)

<!-- /code_chunk_output -->

## 关于About

### 关于本课程About Course

1. 基于`node 18.x lts`

### 关于本仓库About Repository

#### 许可协议Licenses

![CC-BY-SA-4.0](./.assets/LICENSES/cc-by-sa-4.0/image/CC_BY-SA_88x31.png)

采用`CC-BY-SA-4.0`协议，署名：aRoming。

<!--

### 编写环境和使用方法

请详见<https://gitlab.com/arm_commons/commons/-/blob/master/ENVS/MD_RAW/README.md>
-->

#### 目录组织Structure

```bash {.line-numbers}
.
├── .editorconfig       ==> EditorConfig
├── .git                ==> git repository
├── .gitignore          ==> gitignore
├── README.md           ==> 本文件
├── codes               ==> 代码
└── docs                ==> 文档
    ├── abouts          ==> 相关介绍，如：大纲等
    ├── addons          ==> 附加内容，如：专题论述等
    ├── exercises       ==> 习题及其解析
    ├── experiments     ==> 实践指引及FAQ
    ├── glossaries      ==> 术语表
    ├── lectures        ==> 讲义
    └── slides          ==> 幻灯片/课件/PPT（在`VSCode`上使用`MARP`编辑）
```

#### 帮助完善Contribution

期待您一起完善，您可以通过以下方式帮助完善：

1. **`merge request`**：通过`GitLab`的`merge request`到`master`分支
1. **`issue`**： 通过`GitLab`的`issue`发起一个新的`issue`（标签设置成`optimize`）

## 参考资料References

1. [廖雪峰. JavaScript教程[DB/OL]](https://www.liaoxuefeng.com/wiki/1022910821149312)
2. 阮一峰. HTML教程[DB/OL]. <https://wangdoc.com/html/>
3. 阮一峰. JavaScript教程[DB/OL]. <https://wangdoc.com/javascript/>
4. 阮一峰. ES6教程[DB/OL]. <https://wangdoc.com/es6/>
5. [阮一峰. Express框架[DB/OL].](https://javascript.ruanyifeng.com/nodejs/express.html)
6. Mozilla Developer Network. 学习Web开发[DB/OL]. <https://developer.mozilla.org/zh-CN/docs/Learn>
7. [Mozilla Developer Network. Express Web 框架（Node.js/JavaScript）[DB/OL].](https://developer.mozilla.org/zh-CN/docs/Learn/Server-side/Express_Nodejs)
8. 狼叔. 如何正确的学习Node.js[DB/OL]. <https://i5ting.github.io/How-to-learn-node-correctly/>
    1. i5ting/How-to-learn-node-correctly[DB/OL]. <https://github.com/i5ting/How-to-learn-node-correctly/tree/master>
9. Node.js v18.x documentation[DB/OL]. <https://nodejs.org/dist/latest-v18.x/docs/api/index.html>
    1. Node.js v18.x 文档[DB/OL]. <https://nodejs.cn/dist/latest-v18.x/docs/api/index.html>
10. [GeeksForGeeks. NodeJS Tutorial | Learn NodeJS[DB/OL].](https://www.geeksforgeeks.org/nodejs/?ref=shm)
11. [TutorialsPoint. Node.js Tutorial[DB/OL].](https://www.tutorialspoint.com/nodejs/index.htm)
12. [TutorialsTeacher. Learn Node.js[DB/OL].](https://www.tutorialsteacher.com/nodejs)
13. [图雀社区. 一杯茶的时间，上手 Node.js[DB/OL].](https://juejin.cn/post/6844904020813807629)
14. [图雀社区. 一杯茶的时间，上手 Express 框架开发[DB/OL]. 2019-12-18.](https://juejin.cn/post/6844904023380721678)
15. [Express实战[DB/OL].](https://bignerdcoding.gitbooks.io/express/content/)
16. [Node入门[DB/OL].](https://www.nodebeginner.org/index-zh-cn.html)
17. [The Express + Node.js Handbook - Learn the Express JavaScript Framework for Beginners [2022 Edition](DB/OL).](https://www.freecodecamp.org/news/the-express-handbook/)

## 工具Tools

1. [Javascript Playground.](https://playcode.io/)
